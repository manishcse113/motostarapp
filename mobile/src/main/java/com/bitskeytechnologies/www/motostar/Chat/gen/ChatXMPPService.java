package com.bitskeytechnologies.www.motostar.Chat.gen;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

//import com.bitskeytechnologies.www.motostar.Chat.R;
//import com.bitskeytechnologies.www.motostar.Chat.chat.ChatBubbleActivity;
import com.bitskeytechnologies.www.motostar.Chat.chat.MesgCmdInfo;
import com.bitskeytechnologies.www.motostar.Chat.chat.MesgCmdProcessor;
//import com.bitskeytechnologies.www.motostar.Chat.chatRoom.MultiChat;
//import com.bitskeytechnologies.www.motostar.Chat.chatRoom.RoomMaps;
import com.bitskeytechnologies.www.motostar.Chat.db.ChatRecord;
import com.bitskeytechnologies.www.motostar.Chat.users.UserMaps;
import com.bitskeytechnologies.www.motostar.Chat.users.UserObject;
import com.bitskeytechnologies.www.motostar.CustomExceptionHandler;
import com.bitskeytechnologies.www.motostar.Init.Utility;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.R;
import com.bitskeytechnologies.www.motostar.RC.RcMessages;
import com.bitskeytechnologies.www.motostar.TimeClock;
import com.bitskeytechnologies.www.motostar.db.DataSource;
import com.bitskeytechnologies.www.motostar.Chat.db.MyRecord;
//import com.bitskeytechnologies.www.motostar.Chat.db.RoomRecord;
//import com.bitskeytechnologies.www.motostar.Chat.db.SharedPreference;
import com.bitskeytechnologies.www.motostar.Chat.db.UserRecord;
import com.bitskeytechnologies.www.motostar.Chat.login.ChatConnection;
import com.bitskeytechnologies.www.motostar.ConnectivityReceiver;
//import com.bitskeytechnologies.www.motostar.Chat.login.LoginActivity;
//import com.bitskeys.track.map.LocationListenerImpl;
//import com.bitskeys.track.map.MapInfoProcessInBackground;
//import com.bitskeys.track.map.ServiceCallback;
//import com.bitskeytechnologies.www.motostar.Chat.profile.ProfileInfo;
//import com.bitskeytechnologies.www.motostar.Chat.users.ChatUsersListActivity;
//import com.bitskeys.track.users.UserMaps;
import com.bitskeytechnologies.www.motostar.Chat.users.UserPresenceChanged;
import com.bitskeytechnologies.www.motostar.Chat.users.UserPresenceMap;
import com.bitskeytechnologies.www.motostar.db.SharedPreference;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.entity.StringEntity;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterGroup;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

//import com.bitskeys.track.R;

//import org.jivesoftware.smack.xevent.MessageEventManager;

//import com.bitskeys.track.chat2.MainActivity;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ChatXMPPService extends IntentService //implements ServiceCallback
{
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_LOGIN = "com.bitskeytechnologies.www.motostar.action.LOGIN";

    public static final String ACTION_BACKGROUND = "com.bitskeytechnologies.www.motostar.action.BACKGROUND";

    private static final String ACTION_REGISTER = "com.bitskeytechnologies.www.motostar.action.REGISTER";

    public static final String ACTION_USERLIST = "com.bitskeys.track.action.USERLIST";
    public static final String ACTION_USERLIST_USERS_ADDED = "com.bitskeys.track.action.USERLIST_USERS_ADDED";
    public static final String ACTION_USERLIST_USERS_UPDATED = "com.bitskeys.track.action.USERLIST_USERS_UPDATED";
    public static final String ACTION_USERLIST_USERS_PRESENCE = "com.bitskeys.track.action.USERLIST_USERS_PRESENCE";
    public static final String ACTION_USERLIST_PRESENCE_OFF = "com.bitskeys.track.action.USERLIST_PRESENCE_OFF";
    public static final String ACTION_USERLIST_PRESENCE_ON = "com.bitskeys.track.action.USERLIST_PRESENCE_ON";
    public static final String ACTION_USERLIST_PRESENCE_AWAY = "com.bitskeys.track.action.USERLIST_PRESENCE_AWAY";

    public static final String ACTION_CHATROOM_ADDED = "com.bitskeys.track.action.CHATROOM_ADDED";
    public static final String ACTION_CHATROOM_REMOVED = "com.bitskeys.track.action.CHATROOM_REMOVED";

    public static final String ACTION_CHATROOMLIST_ADDED = "com.bitskeys.track.action.ROOMLIST_ADDED";


    public static final String ACTION_USERS_MAP_UPDATES = "com.bitskeys.track.action.USERS_MAP_UPDATES";
    public static final String ACTION_USERS_MAP_UPDATES_FAILED = "com.bitskeys.track.action.USERS_MAP_UPDATESFAILED";
    public static final String ACTION_USER_MAP_UPDATES = "com.bitskeys.track.action.USER_MAP_UPDATES";
    public static final String ACTION_USER_MAP_UPDATES_FAILED = "com.bitskeys.track.action.USER_MAP_UPDATES_FAILED";

    public static final String ACTION_SENDMESSAGE = "com.bitskeys.track.action.SENDMESSAGE";
    public static final String ACTION_RECEIVEDMESSAGE = "com.bitskeys.track.action.RECEIVEDMESSAGE";
    public static final String ACTION_RECEIVEDIMAGE = "com.bitskeys.track.action.RECEIVEDIMAGE";

    public static final String ACTION_TRACKING_PREFERENCE = "com.bitskeys.track.action.TRACKING_PREFERENCE";

    //public static final String HOSTIP = "54.148.100.240";
    public static final String HOSTIP = "13.58.106.255";
    public static final String GROUPNAME = "GroupTest1";

    public static final String CHAT_MESSAGETYPE = "com.bitskeys.track.action.MESSAGETYPE";
    public static final String CHAT_TIMESTAMP = "com.bitskeys.track.action.TIMESTAMP";
    public static final String AWAY = "away";
    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";

    //public static String DOMAIN = "ec2-54-148-100-240.us-west-2.compute.amazonaws.com";
    public static String DOMAIN = "ec2-13-58-106-255.us-east-2.compute.amazonaws.com";

    //TBD: Following to be corrected later- this is done, as we are testing sometimes with spark and sometimes with our application
    //public static String DOMAINJID = "ec2-54-148-100-240.us-west-2.compute.amazonaws.com/Spark 2.7.0";//
    public static String DOMAINJID = DOMAIN + "/Spark 2.7.0";//
    public static String DOMAINJID_BARE = DOMAIN;
    public static String SERVICE_NAME = "ip-172-31-19-174.us-east-2.compute.internal";

    // TODO: Rename parameters
    public static final String USER_NAME = "com.bitskeys.track.extra.PARAM1";
    public static final String PASSWORD = "com.bitskeys.track.extra.PARAM2";
    public static final String LOGGEDIN = "com.bitskeys.track.extra.PARAM3";

    public static final String STR_LOGGEDIN = "LOGGEDIN";

    private static final String MESSAGETOSEND = "com.bitskeys.track.extra.PARAM4";
    private static final String TOUSER = "com.bitskeys.track.extra.PARAM5";

    public static final String MESSAGERECEIVED = "com.bitskeys.track.extra.PARAM6";

    public static final String FILESIZEEXPECTED = "com.bitskeys.track.extra.PARAM7";
    public static final String FILESIZEDISK = "com.bitskeys.track.extra.PARAM8";
    public static final String TIMESTAMP = "com.bitskeys.track.extra.PARAM9";
    public static final String FILESENT = "com.bitskeys.track.extra.PARAM10";
    public static final String MESSAGESENT = "com.bitskeys.track.extra.PARAM11";
    public static final String RECEIPTID = "com.bitskeys.track.extra.PARAM12";
    public static final String RECEIPTSTATUS = "com.bitskeys.track.extra.PARAM13";
    public static final String MESSAGEDIRECTION = "com.bitskeys.track.extra.PARAM14";

    /// messagener code: for send receive from service to activity
    /**
     * Message type: register the activity's messenger for receiving responses
     * from Service. We assume only one activity can be registered at one time.
     */
    public static final int INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST = 1;
    public static final int INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST = 2;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_QUERY = 3;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST = 4;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_ADDED = 5;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_UPDATED = 6;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_PRESENCE_CHANGED = 7;

    public static final int INTER_TASK_MESSAGE_TYPE_REGISTER_LOGIN = 8;
    public static final int INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN = 9;
    public static final int INTER_TASK_MESSAGE_TYPE_LOGIN_QUERY = 10;

    public static final int INTER_TASK_MESSAGE_TYPE_REGISTER_CHAT = 11;
    public static final int INTER_TASK_MESSAGE_TYPE_UNREGISTER_CHAT = 12;
    public static final int INTER_TASK_MESSAGE_TYPE_CHAT_QUERY = 13;

    public static final int INTER_TASK_MESSAGE_TYPE_MESSAGE = 14;

    public static final int INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYREPORT = 15;
    public static final int INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYID = 16;

    public static final int INTER_TASK_MESSAGE_TYPE_USERS_MAP_LOCATIONS = 17;
    public static final int INTER_TASK_MESSAGE_TYPE_USER_MAP_LOCATIONS = 18;
    public static final int INTER_TASK_MESSAGE_TYPE_USERS_MAP_LOCATIONS_FAILED = 19;
    public static final int INTER_TASK_MESSAGE_TYPE_USER_MAP_LOCATIONS_FAILED = 20;

    public static final int INTER_TASK_MESSAGE_TYPE_START_TRACKING = 21;
    public static final int INTER_TASK_MESSAGE_TYPE_STOP_TRACKING = 22;

    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOM_ADDED = 23;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOM_REMOVED = 24;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOMLIST_ADDED = 25;

    public static final int INTER_TASK_MESSAGE_TYPE_REGISTER_CHATROOMCALLBACK = 26;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_TRACKING_PREFERENCE = 27;


    //public static final int MESSAGE_TYPE_MESSAGE_RECEIVEDTEXT = 12;
    //public static final int MESSAGE_TYPE_MESSAGE_RECEIVEDIMAGE = 13;

    public static String CHAT_MESSAGE_TYPE_CMD = "Cmd";
    public static String CHAT_MESSAGE_TYPE_TEXT = "Text";
    public static String CHAT_MESSAGE_TYPE_IMAGE = "Image";
    public static String CHAT_MESSAGE_TYPE_STREAM = "Stream";
    public static int MESSAGEDIRECTIONRECEIVED = 0;
    public static int MESSAGEDIRECTIONSENT = 1;

    public static String MESSAGE_ACK_AWAITED = "ACKAWAITED";
    public static String MESSAGE_ACKED = "ACKED";
    public static String MESSAGE_RESEND_REQUIRED = "RESEND";
    public static String MESSAGE_READYTO_SEND = "READYTOSEND";
    public static String MESSAGE_FILE_UPLOADED = "UPLOADED";
    public static String MESSAGE_FILE_TODOWNLOAD = "TODOWNLOAD";

    public static String MESSAGE_FAILED_PERMANENT = "PERMANENT_FAILED";


    public static final String ACTION_CHAT_SEND = "com.bitskeys.track.action.CHATSEND";
    public static final String ACTION_CHAT_RECEIVE = "com.bitskeys.track.action.CHATRECEIVE";
    public static final String ACTION_CHAT_START = "com.bitskeys.track.action.CHATSTART";
    public static final String ACTION_CHAT_START_EXTERNAL_FILE_SHARE = "com.bitskeys.track.action.CHATSTARTEXTERNALFILESHARE";
    public static final String ACTION_CHAT_START_INTERNAL_FILE_SHARE = "com.bitskeys.track.action.CHATSTARTINTERNALFILESHARE";
    public static final String ACTION_CHAT_START_EXTERNAL_TEXT_SHARE = "com.bitskeys.track.action.CHATSTARTEXTERNALTEXTSHARE";
    public static final String ACTION_CHAT_START_INTERNAL_TEXT_SHARE = "com.bitskeys.track.action.CHATSTARTINTERNALTEXTSHARE";
    public static final String REMOTE_USER_NAME = "com.bitskeys.track.extra.REMOTE_USER_NAME";
    public static final String MY_NAME = "com.bitskeys.track.extra.MY_NAME";
    public static final String IS_ROOM = "IS_ROOM";
    public static final String EXTERNAL_FILE = "com.bitskeys.track.extra.IS_EXT_FILE";
    public static final String EXTERNAL_TEXT = "com.bitskeys.track.extra.IS_EXT_TEXT";
    public static final String MIME_TYPE = "com.bitskeys.track.extra.MIME_TYPE";



    public final static long DOUBLETOLONG = 10000;

    public static boolean isViewIntentSafe = false;
    private static final String FILENAME = "GBC ChatXMPPService#";
    public static boolean monitoringThreadRunning = false;

    XMPPTCPConnection mConnection;
    Roster mRoster;
    public static UserMaps mUsersListMap;
    public static String mUserName;
    public static String mDisplayName;
    String mPassword;

    ChatManagerListener myChatListener = null;
    static boolean SEND_ACTION_USERLIST;
    static boolean mFirstTimeLaunch;

    private int debugVar = 0;
    public static int count = 0;
    private NotificationManager mNM;

    public static Context mContext = null;

    public static boolean isAnnouncementOn = true;

    //public static LocationListenerImpl locationInstance = null;
    public static SensorManager mSensorManager = null;
    public static LocationManager mLocationManager = null;

    public static LocationManager getLocationManager()
    {
        return mLocationManager;
    }

    public static SensorManager getSensorManager()
    {
        return mSensorManager;
    }
    public static void setAnnouncement(Context context, boolean flag)
    {

        isAnnouncementOn = flag;
        SharedPreference.getInstance().saveSharedPreferenceSpeaker(context, flag);
    }

    public static void announce(String body)
    {
        if (isAnnouncementOn)
        {
            String toSpeak = "MotoStar " + body;

            /*
            if (t1 != null && isAnnouncementOn) {
                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            }
            */
        }
    }

    public static boolean getSharedPrefIsAnnouncementOn(Context context)
    {
        SharedPreference.getInstance().retreiveSharedPreference(context);
        isAnnouncementOn = SharedPreference.getInstance().getSpeakerOn();
        return isAnnouncementOn;
    }


    /*
    public static void setTrackingPreference(Context context, boolean flag)
    {
        //mCanGetLocation = flag;
        SharedPreference.getInstance().saveSharedPreferenceTracking(context, flag);
    }

    public static boolean getTrackingPreference(Context context)
    {
        SharedPreference.getInstance().retreiveSharedPreference(context);
        return SharedPreference.getInstance().getTrackingOn();
    }

    public static void setLocationDialogPreference(Context context, boolean flag)
    {
        SharedPreference.getInstance().saveSharedPreferenceLocationDialog(context, flag);
    }

    public static boolean getLocationDialogPreference(Context context)
    {
        SharedPreference.getInstance().retreiveSharedPreference(context);
        return SharedPreference.getInstance().getLocationDialogPref();
    }
    */

    public static boolean isAnnouncementOn(Context context)
    {
        return isAnnouncementOn;
    }

    public ChatXMPPService() {

        super("ChatXMPPService");
        SEND_ACTION_USERLIST = true;
        mFirstTimeLaunch = true;
    }

    public static void setDisplayName(String displayName) {
        mDisplayName = displayName;
    }


    //BootReceiver bootReceiver = new BootReceiver();

    /*
    @Override
    public void onStart(Intent intent, int startId)
    {
        bootReceiver.SetAlarm(this);
    }
    */
    @Override
    public int onStartCommand(Intent intent, int flag, int startId) {
        int ret = super.onStartCommand(intent, flag, startId);
        Log.d(FILENAME, "onStartCommand");
        debugVar++;

        //bootReceiver.SetAlarm(this);
        return START_STICKY;
    }

    //MapInfoProcessInBackground.MapProcessCallback mapProcessCallback;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(FILENAME, "onCreate service ChatXMPPService + " + debugVar);
        //mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        // Display a notification about us starting.
        //showNotification();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        PackageManager packageManager = getPackageManager();
        List activities = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        isViewIntentSafe = activities.size() > 0;


        Log.d(FILENAME, "Trying to start service in foreground");
        if(BootReceiver.toRunInForeground == false)
        {
            runAsForeground();
        }
        //
        //stopForeground(true);
        /*
        //super.onCreate();
        Notification notification = new Notification(R.drawable.app_icon, getText(R.string.app_name),System.currentTimeMillis());
        Intent notificationIntent = new Intent(this, ChatXMPPService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.setLatestEventInfo(this, "serviceNotification", "Notification", pendingIntent);
        startForeground(Notification.FLAG_ONGOING_EVENT, notification);
        */


        //Manish: To be implemented later::
        //To get autocrash report
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(getApplicationContext()));
        }
        //Start thread only once to monitor the connection with the Server
        if (monitoringThreadRunning == false) {
            mContext = getApplicationContext();
            SharedPreference.getInstance().retreiveSharedPreference(mContext);
            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

            /*
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            locationInstance = LocationListenerImpl.getInstance();
            locationInstance.RegisterCallback(ChatXMPPService.this);
            locationInstance.InitContext(mContext);
            locationInstance.readAndAddToMylocationString();
            locationInstance.initFromPreferences();
            locationInstance.registerGPSIfNeeded();
            */

            isAnnouncementOn = SharedPreference.getInstance().getSpeakerOn();
            monitoringConnectivityThread();
        }
    }

    //Following function - generates the notification intent - this is needed to make the app run background periodically.
    private void runAsForeground() {
        Log.d(FILENAME, "runAsForeground +");

       Intent notificationIntent = new Intent(this, MainActivity.class);
       notificationIntent.setAction(ChatXMPPService.ACTION_LOGIN);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //notificationIntent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
        //notificationIntent.putExtra(ChatXMPPService.MESSAGERECEIVED, screenText);

       TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
       // Adds the back stack
       stackBuilder.addParentStack(MainActivity.class);

       // Adds the Intent to the top of the stack
       stackBuilder.addNextIntent(notificationIntent);

       PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        //NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        //builder.setContentIntent(resultPendingIntent);

       Notification notification= new NotificationCompat.Builder(this).setContentTitle("MotoStar")
                .setSmallIcon(R.drawable.noti_icon)
                .setTicker(getString(R.string.app_title))
                .setAutoCancel(true)
               .setContentIntent(resultPendingIntent).build();
        startForeground(42, notification);
    }


    @Override
    public void onDestroy() {
        Log.d(FILENAME, "onDestroy service ChatXMPPService +" + debugVar);
        //runAsForeground();
        // Cancel the persistent notification.
        //mNM.cancel(R.string.remote_service_started);
        // Tell the user we stopped.
        //Toast.makeText(this, R.string.remote_service_stopped, Toast.LENGTH_SHORT).show();

        //monitoringThreadRunning = false;
        //Intent intent = new Intent("com.android.techtrainner");
        //intent.putExtra("yourvalue", "torestore");
        //sendBroadcast(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_LOGIN.equals(action)) {
                Log.d(FILENAME, "Received intent ACTION_LOGIN");
                final String param1 = intent.getStringExtra(USER_NAME);
                final String param2 = intent.getStringExtra(PASSWORD);
                final String param3 = intent.getStringExtra(LOGGEDIN);
                BootReceiver.toRunInForeground = true; //It comes from the normal path means to be executed in front;
                bUserListActivityStartInProgress = false; //User wants to launch the app - so launch it.
                handleActionLogin(param1, param2, param3);
                /*
                new Thread(new Runnable() {
                    public void run() {
                        ReceiveChatMessages();
                    }
                }).start();
                */
            }
            if (ACTION_BACKGROUND.equals(action))
            {
                Log.d(FILENAME, "Received intent ACTION_BACKGROUND");
                final String param1 = intent.getStringExtra(USER_NAME);
                final String param2 = intent.getStringExtra(PASSWORD);
                final String param3 = intent.getStringExtra(LOGGEDIN);
                BootReceiver.toRunInForeground = false; //It comes from the normal path means to be executed in front;
                handleActionLogin(null, null, "ANYTHING");
                /*
                new Thread(new Runnable() {
                    public void run() {
                        ReceiveChatMessages();
                    }
                }).start();
                */
            }

            if (ACTION_LOGIN.equals(action)) {
                Log.d(FILENAME, "Received intent ACTION_LOGIN");
                final String param1 = intent.getStringExtra(USER_NAME);
                final String param2 = intent.getStringExtra(PASSWORD);
                final String param3 = intent.getStringExtra(LOGGEDIN);
                handleActionLogin(param1, param2, param3);
                /*
                new Thread(new Runnable() {
                    public void run() {
                        ReceiveChatMessages();
                    }
                }).start();
                */
            }
            else if (ACTION_REGISTER.equals(action)) { // This is TODO TBD
                final String param1 = intent.getStringExtra(USER_NAME);
                final String param2 = intent.getStringExtra(PASSWORD);
                handleActionREGISTER(param1, param2);
            } else if (ACTION_SENDMESSAGE.equals(action)) {
                final String param1 = intent.getStringExtra(MESSAGETOSEND);
                final String param2 = intent.getStringExtra(TOUSER);
                final String param3 = intent.getStringExtra(CHAT_MESSAGETYPE);
                //final Long param4 = intent.getStringExtra(CHAT_TIMESTAMP);
                //if (param3.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT)) {
                    handleActionSendMessage(param1, param2, param3);
                //}
                /*
                else if (param3.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_IMAGE)) {
                    new Thread(new Runnable() {
                        public void run() {
                            FileTransferWrapper.SendFile(param1, param2 + "@" + ChatXMPPService.DOMAIN, false);
                        }
                    }).start();
                }*/
                /*
                new Thread(new Runnable() {
                    public void run() {
                        handleActionSendMessage(param1, param2);
                    }
                }).start();*/
            }
        }
    }

    //MultiChatRoomInviteListner multiChatRoomInviteListner = null;

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionLogin(String USER_NAME, String PASSWORD, String loggedIn) {
        mUserName = USER_NAME;
        mPassword = PASSWORD;
        //invokConnectionXmpp();
        Log.d(FILENAME, "handleActionLogin");

        //Load my Location preferences here:

        //mCanGetLocation =

        if (mUserName == null)
        {
            String userName = null;
            String password = null;
            Context ctxt = getApplicationContext();
            DataSource db = new DataSource(getApplicationContext());
            db.open();
            List<MyRecord> reqs = db.getMyRecords();
            for (MyRecord _req : reqs) {
                userName = _req.getUserName();
                password = _req.getPassword();
                if (mUserName == null) {
                    mUserName = userName;
                    mPassword = password;
                }
                //SendtoUserListActivity(ACTION_TRACKING_PREFERENCE);
                Log.d(FILENAME, "username and password are " + userName + " " + password);
            }
            db.close();
            if (userName == null && mUserName == null)
            {
                Log.e(FILENAME, "ChatXmppService:monitoringConnectivityThread: Error - user name and password are null");
                try {
                    Thread.sleep(1000L);
                }
                catch(Exception e)
                {
                    Log.d(FILENAME, "Exceptoin in sleep");
                    e.printStackTrace();
                }
                //return;
            }
        }


        if (loggedIn.equals(STR_LOGGEDIN)) {
            prepareUsersList();// Only from roster: by default no user is in roster
            //Manish: Fixed crash in startuserlistactivity
            // First start user activity and tehn register for roster-- this is done to avoid concurrent updates in userlistmap while in startuserlistActivity
            registerRosterListners();
            ReceiveChatMessages();
            //FileSharing.monitoringReliableDeliveryThread(getApplicationContext());
            //FileTransferWrapper.ReceiveFile(this.getBaseContext());
            //prepareAndSendChatRoomListFromDb();

            /*
            //Register for multi user chat rooms:
            if (multiChatRoomInviteListner == null)
            {
                multiChatRoomInviteListner = new MultiChatRoomInviteListner();
            }
            MultiChat.setUpRoomInvitationListener(mConnection, multiChatRoomInviteListner);
              */
            //mLocation = registerForLocationUpdates(); //This will happen periodically callback function thereafter.
            //StartUserListActivity(ACTION_USERLIST); In this case- launch it from roster only
        }
        else //offline mode
        {

            prepareAndSendUsersListFromDb();
            //prepareAndSendChatRoomListFromDb();

            /*
            //Following has been sent to userList Activity from prepareUsersListFromDb
            if (bUserListActivityStartInProgress !=true) {
                StartUserListActivity(ACTION_USERLIST);
            }
            else if (mUsersListMap != null) {
                SendtoUserListActivity(ACTION_USERLIST);
            }
            */
            //mLocation = StartMyLocationUpdates(); //This will happen periodically callback function thereafter.
            // As soon as network connection is up, it shall try to login from a thread,
            // on succes it shall do the remainder of task such as
            //registerRosterListners();
            //ReceiveChatMessages();
            //FileTransferWrapper.ReceiveFile(this.getBaseContext());
        }


        //Start thread only once to monitor the connection with the Server
        if (monitoringThreadRunning == false) {
            monitoringThreadRunning = true;
            mContext = getApplicationContext();
            SharedPreference.getInstance().retreiveSharedPreference(mContext);
            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

            /*
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            locationInstance = LocationListenerImpl.getInstance();
            locationInstance.RegisterCallback(ChatXMPPService.this);
            locationInstance.InitContext(mContext);
            locationInstance.readAndAddToMylocationString();
            locationInstance.initFromPreferences();
            locationInstance.registerGPSIfNeeded();
            */
            isAnnouncementOn = SharedPreference.getInstance().getSpeakerOn();

            monitoringConnectivityThread();
        }
    }



    private void monitoringConnectivityThread() {
        new Thread() {
            @Override
            public void run() {
                boolean isNetworkConnected = false;
                boolean isServerConnected = false;
                boolean isLoginSuccessful = false;
                String userName = null;
                String password = null;


                int firstTimeLoginAttempts = 0;
                //Retrieve username and password;
                while(userName == null) {
                    DataSource db = new DataSource(mContext);
                    db.open();
                    List<MyRecord> reqs = db.getMyRecords();
                    for (MyRecord _req : reqs) {
                        userName = _req.getUserName();
                        password = _req.getPassword();
                        setDisplayName(_req.getDisplayName());
                        if (mUserName == null) {
                            mUserName = userName;
                            mPassword = password;
                        }
                        //SendtoUserListActivity(ACTION_TRACKING_PREFERENCE);
                        Log.d(FILENAME, "username and password are " + userName + " " + password);
                    }
                    db.close();
                    if (userName == null && mUserName == null) {
                        firstTimeLoginAttempts++;
                        Log.e(FILENAME, "ChatXmppService:monitoringConnectivityThread: Error - user name and password are null");
                        try {
                            Thread.sleep(1000L);
                        }
                        catch(Exception e)
                        {
                            Log.d(FILENAME, "Exceptoin in sleep");
                            e.printStackTrace();
                        }
                        if (firstTimeLoginAttempts >= 5)
                        {
                            monitoringThreadRunning = false;
                            Log.d(FILENAME, "User hasn't logged in once - so no point in monitoring the user");
                            return;
                        }
                        //return;
                    }
                    else
                    {
                        break;
                    }
                }
                //First time registration for location Updates
                //mLocation =
                //locationInstance.registerForLocationUpdates(); //This will happen periodically callback function thereafter.

                firstTimeLoginAttempts = 0;
                long networkMonitoringTime = 30000L; //For inhouse testing - reducing the time by 10%
                long connectionMonitoringTime = 30000L;
                long serverConnectionRetryTime = 30000L;
                long sleepTime = 0;
                //long locationCaptureTreshHoldTime = 120000L;
                while (true) //infinite monitoring thread:
                {
                    //sleepTime= locationInstance.obtainLocationifNeeded(sleepTime);
                    try
                    {
                        //Get the network state
                        isNetworkConnected = ConnectivityReceiver.isConnected(getBaseContext());
                        if (isNetworkConnected == false)
                        {
                            sleepTime+=networkMonitoringTime;
                            Thread.sleep(networkMonitoringTime);
                            continue;
                        }
                        //locationInstance.registerGPSIfNeeded();
                        //Get the connection state
                        mConnection = ChatConnection.getInstance().getConnection();
                        if (mConnection != null) {
                            isServerConnected = mConnection.isConnected();
                            isLoginSuccessful = ChatConnection.getInstance().isLoginSuccessful();
                        }
                        // if connectionState is down and network state is up - attempt login
                        if (isNetworkConnected == true && (isServerConnected == false || isLoginSuccessful == false))
                        //if (isNetworkConnected == true)
                        {
                            isServerConnected = ChatConnection.getInstance().connect(mContext, userName, password);
                            if (isServerConnected)
                            {
                                // If login is successful - register for various listners - update users list:
                                prepareUsersList();// Only from roster: by default no user is in roster
                                registerRosterListners();
                                ReceiveChatMessages();
                                //FileTransferWrapper.ReceiveFile(getApplicationContext());
                                //FileSharing.monitoringReliableDeliveryThread(getApplicationContext());

                                /*
                                //Load old rooms if not already loaded
                                prepareAndSendChatRoomListFromDb();

                                //Register for multi user chat rooms:
                                if (multiChatRoomInviteListner == null)
                                {
                                    multiChatRoomInviteListner = new MultiChatRoomInviteListner();
                                }
                                MultiChat.setUpRoomInvitationListener(mConnection, multiChatRoomInviteListner);
                                */
                                try
                                {
                                    FileSharing.loadAndDeliverableMessagesFromDB(mContext, ChatXMPPService.mUserName);
                                } catch (Exception e)
                                {
                                    Log.e(FILENAME, "Exception" + e.getMessage());
                                }
                            }
                            sleepTime+=serverConnectionRetryTime;
                            Thread.sleep(serverConnectionRetryTime);
                        }
                        else //netowrk is connected, server is also connected
                        {
                            //Can check every minute
                            //For debugging the roster issue
                            /*
                            if (GROUPNAME.equals("GroupTest2")) {
                                mRoster = Roster.getInstanceFor(mConnection);
                                if (mRoster.isLoaded() == false) {
                                    mRoster.reloadAndWait();
                                    registerRosterListners();
                                }
                            }
                            */
                            sleepTime+=connectionMonitoringTime;
                            Thread.sleep(connectionMonitoringTime);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e(FILENAME, e.getMessage());
                    }
                }
            }
        }.start();
    }

    public void prepareUsersList() {
        Log.d(FILENAME, " prepareUsersList");
        mConnection = ChatConnection.getInstance().getConnection();
        if (mConnection == null) {
            return;
            //if connection is null - then next statement crashes:
        }
        mRoster = Roster.getInstanceFor(mConnection);
        mRoster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
        Collection<RosterEntry> entries = mRoster.getEntries();
        if (mUsersListMap == null) {
            mUsersListMap = new UserMaps();
        }
        int i = 0;
        for (RosterEntry entry : entries) {
            String displayName = entry.getName();//This is displayName
            String userName = entry.getUser();//this is User Name
            String temp = userName.substring(0, userName.indexOf("@"));
            if (temp != null) {
                userName = temp;
            }

            //String name = entry.substring(0, entry.indexOf("@"));
            Presence presence = null;
            if (userName != null) {

                //presence = mRoster.getPresence(displayName);
                presence = mRoster.getPresence(userName);

                String fullName = presence.getFrom();
                if (presence.isAvailable()) {
                    String jid = fullName.substring(fullName.indexOf("@"));
                    mUsersListMap.mUserJidMap.put(userName, jid);
                    //mUsersListMap.mUserJidMap.put(displayName, jid);
                }
            }
            /* to pass user and presence mapping*/
            UserRecord.storeUniqueNameRecordNew(this, userName, displayName, null, null, null);//except name - everything is stored as null, presence is temporary and not stored.
            if ((userName != null) && (presence.isAvailable())) {
                if (presence.isAway()) {
                    Log.d(FILENAME, " prepareUsersList name" + userName + AWAY);
                    mPresenceMap.put(userName, AWAY);
                } else {
                    Log.d(FILENAME, " prepareUsersList name" + userName + ONLINE);
                    mPresenceMap.put(userName, ONLINE);
                }
            } else {
                Log.d(FILENAME, " prepareUsersList name" + userName + OFFLINE);
                mPresenceMap.put(userName, OFFLINE);
            }
            Log.d(FILENAME, userName);
        }
    }

    /*

    public static RoomMaps mRoomListMap = null;
    private void prepareAndSendChatRoomListFromDb() {
        Log.d(FILENAME, "prepareAndSendChatRoomListFromDb");
        boolean onlyJoinRequired = false;
        if (mRoomListMap == null) {
            mRoomListMap = new RoomMaps();
        }

        mConnection = ChatConnection.getInstance().getConnection();
        Set<String> joinedRooms = null;
        if(mConnection != null)
        {
            joinedRooms = MultiChat.getJoinedRooms(mConnection);

        }

        DataSource db = new DataSource(this);
        try {
            db.open();
            List<RoomRecord> reqs = db.getRoomRecords();
            int i = 0;
            for (RoomRecord _req : reqs) {
                String roomName = _req.getRoomName();
                String roomJid = _req.getRoomJid();
                String room = roomName + roomJid;

                String inviterName = _req.getInviterName();
                String inviterJid = _req.getInviterJid();
                String inviter = inviterName + inviterJid;

                mRoomListMap.addRoomNameToMap(roomName, roomJid);

                MultiUserChat multiUserChat = MultiChat.getMultiUserChatInstanceForRoom(roomName, mUserName, mConnection);
                if (multiUserChat != null) {
                    registerChatInAction(multiUserChat, inviter);
                    //MultiChatInActionCallback newRoomListner = new MultiChatInActionCallback(multiUserChat, inviter, null, null);
                    //MultiChat.setUpGroupChatInActionListeners(multiUserChat, newRoomListner);
                }
                i++;
            }
            Log.d(FILENAME, "Added rooms from DB ###" + i);
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "error in prepareAndSendChatRoomListFromDb");
        }
        db.close();
        SendtoUserListActivity(ACTION_CHATROOMLIST_ADDED);
    }
    */

    private void prepareAndSendUsersListFromDb()
    {
        Log.d(FILENAME, "prepareAndSendUsersListFromDb");
        if (mUsersListMap == null) {
            mUsersListMap = new UserMaps();
        }
        if (mPresenceMap == null) {
            mPresenceMap = new UserPresenceMap();
        }
        if (mPresenceMapTemp == null) {
            mPresenceMapTemp = new UserPresenceMap();
        }
        if (mUserName==null)
        {
            return;
        }
        UserRecord.clearFromDB(mContext, mUserName);
        DataSource db = new DataSource(this);

        db.open();
        List<UserRecord> reqs = db.getUserRecords();
        //To enhance user experience:
        //  First get only 20 records and send to user list activity
        //Then update all users and final list will be sent again from the function calling this function
        int i = 0;
        if (mPresenceMapTemp != null) {
            mPresenceMapTemp.clear(); //clean temporary map:
        }
        for (UserRecord _req : reqs) {
            mPresenceMap.put(_req.getUserName(), OFFLINE);
            mUsersListMap.addUserNameToMap(_req.getUserName(), _req.getDisplayName());
            mUsersListMap.addDisplayNameToMap(_req.getDisplayName(), _req.getUserName());
            //mUsersListMap.mUserNameToDisplayNameMap.put(_req.getUserName(), _req.getDisplayName());
            //mUsersListMap.mDisplayNameToUserNameMap.put(_req.getDisplayName(),_req.getUserName());

            mPresenceMapTemp.put(_req.getUserName(), OFFLINE);
            /*
            if (i%10 == 0) {
                SEND_ACTION_USERLIST = true;
                if (bUserListActivityStartInProgress != true) {
                    StartUserListActivity(ACTION_USERLIST);
                }
                else if ((mPresenceMapTemp != null) && (mPresenceMapTemp.isEmpty()==false))
                {
                    //SendtoUserListActivity(ACTION_USERLIST);
                    SendtoUserListActivity(ACTION_USERLIST_USERS_ADDED);
                    mPresenceMapTemp.clear(); //clean temporary map:
                }
            }
            */
            i++;
        }
        Log.d(FILENAME, "Added users from DB ###" + i);
        db.close();
        informUsers();

        SEND_ACTION_USERLIST = true;

        mPresenceMapTemp.clear(); //clean temporary map:
    }

    public void informUsers()
    {
        if (bUserListActivityStartInProgress != true) {
            //StartUserListActivity(ACTION_USERLIST);
        } else if ((mPresenceMapTemp != null) && (mPresenceMapTemp.isEmpty() == false)) {
            //SendtoUserListActivity(ACTION_USERLIST);
            SendtoUserListActivity(ACTION_USERLIST_USERS_ADDED);
        }
    }

    public static boolean bUserListActivityStartInProgress = false;

    public static boolean isUserListActivityActive( )
    {
        return bUserListActivityStartInProgress;
    }

    public void StartUserListActivity(String action) {
        Log.d(FILENAME, "StartUserListActivity");

        //If it is to be executed in background:: // do nothing
        if(BootReceiver.toRunInForeground == false) // If this is to run in background - no need to activate userlist activity:
        {
            Log.d(FILENAME, "StartUserListActivity - not started - service running in background");
            return;
        }
        Log.d(FILENAME, "StartUserListActivity - not started - service running in foreground");

        //else - do the normal processing::


        bUserListActivityStartInProgress = true;

        /*
        Intent intent = new Intent(getBaseContext(), ChatUsersListActivity.class);
        intent.setAction(action);//New message to be defined
        //if (mFirstTimeLaunch) {
        //    mFirstTimeLaunch = false;
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //}
        Bundle bundle = new Bundle();
        bundle.putSerializable("userPresenceMap", mPresenceMap);
        intent.putExtras(bundle);
        loguserlist();
        startActivity(intent);
        */
    }


    TextToSpeech t1;

    void ReceiveChatMessages() {
        Log.d(FILENAME, " ReceiveChatMessages");
        ChatManager chatManager = null;
        chatManager = ChatManager.getInstanceFor(mConnection);
        final MyChatMessageListener myChatMessageListener = new MyChatMessageListener();

        t1=new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                   // t1.setLanguage(Locale.UK);
                }
            }
        });

        if (t1!=null)
        {
            t1.setLanguage(Locale.ENGLISH);
        }
        if (myChatListener!=null)
        {
            chatManager.removeChatListener(myChatListener);
            myChatListener = null;
        }


        myChatListener = new ChatManagerListener() {
            @Override
            public void chatCreated(Chat chat, boolean createdLocally) {
                chat.addMessageListener(myChatMessageListener);
            }
        };
        chatManager.addChatListener(myChatListener);
        //FileTransferWrapper.FileReceive();
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionREGISTER(String USER_NAME, String PASSWORD) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public static String getUserName(String displayName) {
        String str = ChatXMPPService.mUsersListMap.getUserName(displayName);
        if (str == null)
        {
            str = displayName;
        }
        return str;
    }

    public static String getDisplayName(String userName) {
        String str = null;
        if (userName == null) {
            return null;
        }

        if (userName.equals(mUserName)) {
            if (mDisplayName!=null)
            {
                str = mDisplayName;
            }
            else
            {
                str= mUserName;
            }

        }

        else if (ChatXMPPService.mUsersListMap != null)
        {
            str = ChatXMPPService.mUsersListMap.getDisplayName(userName);
            //Following can happen - in case of Group
            if (str == null)
            {
                str = userName;
            }
        }
        return str;
    }

    /*
    public static Integer getAvatarId(String userName) {
        return R.drawable.avatar;
        //return str;
    }
    */


    //public static UserMaps mUsersListMapTemp;
    //public static UserMaps mUsersListMapUpdatedTemp;
    public static UserPresenceMap mPresenceMap;
    public static UserPresenceMap mPresenceMapTemp;
    public static UserPresenceMap mAddedMapTemp;

    public static UserPresenceChanged mUserPresenceChanged;

    public void registerRosterListners() {
        Log.d(FILENAME, "registerRosterListners");
        mConnection = ChatConnection.getInstance().getConnection();


        if (mUsersListMap == null) {
            mUsersListMap = new UserMaps();
        }

        if (mPresenceMapTemp == null) {
            mPresenceMapTemp = new UserPresenceMap();
        }

        if (mAddedMapTemp == null) {
            mAddedMapTemp = new UserPresenceMap();
        }

        if (mPresenceMap == null) {
            mPresenceMap = new UserPresenceMap();
        }

        if (mConnection.isConnected()) {

            mRoster = Roster.getInstanceFor(mConnection);
            mRoster.setRosterLoadedAtLogin(true);
            mRoster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
            mRoster.addRosterListener(new RosterListener()

            {
                public void entriesAdded(Collection<String> addresses) {
                    Log.d(FILENAME, " entriesAdded");
                    int i = 0;
                    boolean isValidGroup = false;
                    String name;
                    String rosterName = null;//This is display Name
                    String rosterUser = null;// This is userName
                    String rosterJid = null;//Raw JID
                    String fullName = null;
                    String jid = null;
                    mAddedMapTemp.clear(); //clean temporary map:

                    for (String entry : addresses) {
                        name = entry.substring(0, entry.indexOf("@"));
                        rosterName = null;//This is display Name
                        rosterUser = null;// This is userName
                        rosterJid = null;//Raw JID
                        fullName = null;
                        mRoster = Roster.getInstanceFor(mConnection);
                        try {
                            Collection<RosterGroup> grpList = mRoster.getGroups();
                            RosterEntry rosterEntry = mRoster.getEntry(entry);
                            if (rosterEntry != null) {
                                rosterName = rosterEntry.getName();
                                rosterUser = rosterEntry.getUser();
                                rosterJid = rosterEntry.toString();
                            }

                            for (RosterGroup myGroupName : grpList) {
                                if (myGroupName.getName().equalsIgnoreCase(ChatXMPPService.GROUPNAME)) {
                                    isValidGroup = true;
                                }
                            }
                        } catch (Exception ex) {
                            String msg = ex.getMessage();
                            Log.d("ERROR", msg);
                        }
                        /* If group is not valid, do not show other group users */
                        if (!isValidGroup)
                            return;
                        Presence presence = mRoster.getPresence(name);
                        fullName = presence.getFrom();
                        if (rosterName != null) {
                            mUsersListMap.addUserNameToMap(name, rosterName);
                            mUsersListMap.addDisplayNameToMap(rosterName, name);

                            //mUsersListMapTemp.addUserNameToMap(name, rosterName);
                            //mUsersListMapTemp.addDisplayNameToMap(rosterName, name);
//                            mUsersListMap.mUserNameToDisplayNameMap.put(name,rosterName);
//                            mUsersListMap.mDisplayNameToUserNameMap.put(rosterName,name);
                        } else {
                            /* In case display name is null, which is not likely, it will show username */
                            mUsersListMap.addUserNameToMap(name, name);
                            mUsersListMap.addDisplayNameToMap(name, name);

                            //mUsersListMapTemp.addUserNameToMap(name,name);
                            //mUsersListMapTemp.addDisplayNameToMap(name, name);
//                            mUsersListMap.mUserNameToDisplayNameMap.put(name,name);
//                            mUsersListMap.mDisplayNameToUserNameMap.put(name,name);
                        }
                        if (presence.isAvailable()) {
                            //Seems it never comes here.....
                            jid = fullName.substring(fullName.indexOf("@"));
                            mUsersListMap.mUserJidMap.put(name, jid);
                            //mUsersListMapTemp.mUserJidMap.put(name, jid);
                            if (presence.isAway()) {
                                mAddedMapTemp.put(name, AWAY);
                                mPresenceMap.put(name, AWAY);
                            } else {
                                mAddedMapTemp.put(name, ONLINE);
                                mPresenceMap.put(name, ONLINE);
                            }
                        } else {
                            mAddedMapTemp.put(name, OFFLINE);
                            mPresenceMap.put(name, OFFLINE);
                        }
                        i++;
                        /*
                        //Add 20 entries at a time::- it will make user presence good.
                        if (i%10 == 0) {
                            SEND_ACTION_USERLIST = true;
                            if (bUserListActivityStartInProgress != true) {
                                StartUserListActivity(ACTION_USERLIST);
                            }
                            else if ((mPresenceMapTemp != null) && (mPresenceMapTemp.isEmpty()==false))
                            {
                                //SendtoUserListActivity(ACTION_USERLIST);
                                SendtoUserListActivity(ACTION_USERLIST_USERS_ADDED);
                                mPresenceMapTemp.clear(); //clean temporary map:
                            }
                        }*/
                    }
                    Log.d(FILENAME, "Added users ##### " + i);
                    SEND_ACTION_USERLIST = true;
                    informUsers();

                    /*
                    if (bUserListActivityStartInProgress != true) {
                        //StartUserListActivity(ACTION_USERLIST);
                    } else if ((mAddedMapTemp != null) && (mAddedMapTemp.isEmpty() == false)) {
                        //SendtoUserListActivity(ACTION_USERLIST);
                        SendtoUserListActivity(ACTION_USERLIST_USERS_ADDED);
                        // mUsersListMapTemp.mUserPresenceMap.clear(); //clean temporary map:
                    }
                    */

                    //
                    //new Thread(new Runnable() {
                    //  public void run() {
                            /*
                            try {
                                UserRecord.clearFromDB(getBaseContext()); //as we are getting fresh list from server- so delete the old list from db::
                            }
                            catch(Exception e)
                            {
                                Log.e(FILENAME, "Exception in clearFromDB");
                                e.printStackTrace();
                            }
                            */
                    for (Map.Entry<String, String> entry : mAddedMapTemp.entrySet()) {
                        try {
                            UserRecord.storeUniqueNameRecordNew(getBaseContext(), entry.getKey(), getDisplayName(entry.getKey()), null, null, null);//except name - everything is stored as null, presence is temporary and not stored.
                        } catch (Exception e) {
                            Log.e(FILENAME, "Exception in storeNameRecord");
                            e.printStackTrace();
                        }
                    }
                    //}
                    // }).start();
                    /*
                    int j = 0;
                    for (Map.Entry<String, String> entry : mAddedMapTemp.entrySet())
                    {
                        try {
                            UserRecord.storeNameRecord(getBaseContext(), entry.getKey(), entry.getKey(), null, null, null);//except name - everything is stored as null, presence is temporary and not stored.
                        }
                        catch(Exception e)
                        {
                            Log.e(FILENAME, "Exception in storeNameRecord");
                            e.printStackTrace();
                        }

                    }
                    */
                }

                public void entriesDeleted(Collection<String> addresses) {
                    Log.d(FILENAME, " entriesDeleted");
                    for (String entry : addresses) {
                        String name = entry.substring(0, entry.indexOf("@"));
                        Log.d(FILENAME, name);
                        mPresenceMap.remove(name);

                        //String jid = entry.substring(entry.indexOf("/"));
                        mUsersListMap.mUserJidMap.remove(name);
                        Log.d(FILENAME, "user " + name + " removed ");
                    }
                    //Deleted users are not required to be updated in the user's list on priority
                    /*
                    SEND_ACTION_USERLIST = true;
                    if (bUserListActivityStartInProgress !=true) {
                        StartUserListActivity(ACTION_USERLIST);
                    }
                    else if (mUsersListMap != null) {
                        SendtoUserListActivity(ACTION_USERLIST);
                    }*/
                }

                public void entriesUpdated(Collection<String> addresses) {
                    Log.d(FILENAME, " entriesUpdated");
                    int i = 0;
                    mPresenceMapTemp.clear(); //clean temporary map:
                    String name = null;
                    String presenceStr = OFFLINE;
                    mRoster = Roster.getInstanceFor(mConnection);
                    Presence presence;
                    String fullName = null;
                    String jid = null;

                    for (String entry : addresses) {
                        name = entry.substring(0, entry.indexOf("@"));
                        presenceStr = OFFLINE;
                        presence = mRoster.getPresence(name);
                        fullName = presence.getFrom();
                        jid = null;
                        if (presence.isAvailable()) {
                            jid = fullName.substring(fullName.indexOf("@"));
                            mUsersListMap.mUserJidMap.put(name, jid);

                            if (presence.isAway()) {
                                //Log.d(FILENAME, " entriesUpdated name" + name + AWAY);
                                mPresenceMapTemp.put(name, AWAY);
                                mPresenceMap.put(name, AWAY);

                            } else {
                                //Log.d(FILENAME, " entriesUpdated name" + name + ONLINE);
                                mPresenceMapTemp.put(name, ONLINE);
                                mPresenceMap.put(name, ONLINE);
                            }
                        } else {
                            //Log.d(FILENAME, " entriesUpdated name" + name + OFFLINE);
                            mPresenceMapTemp.put(name, OFFLINE);
                            mPresenceMap.put(name, OFFLINE);
                        }
                        i++;
                        //Log.d(FILENAME, name);
                    }
                    Log.d(FILENAME, "Users updated ###" + i);
                    if (bUserListActivityStartInProgress != true) {
                        //StartUserListActivity(ACTION_USERLIST);
                    } else if ((mPresenceMapTemp != null) && (mPresenceMapTemp.isEmpty() == false)) {
                        //SendtoUserListActivity(ACTION_USERLIST);
                        SendtoUserListActivity(ACTION_USERLIST_USERS_UPDATED);
                    }
                }

                public void presenceChanged(Presence presence) {
                    Log.d(FILENAME, " presenceChanged");
                    String displayName;
                    String fullName = presence.getFrom();
                    String name = fullName.substring(0, fullName.indexOf("@"));
                    UserRecord.storeUniqueNameRecordNew(mContext, name, getDisplayName(name), null, null, null);//except name - everything is stored as null, presence is temporary and not stored.
                    //mPresenceMapTemp.clear(); //clean temporary map:
                    if (mUserPresenceChanged == null) {
                        mUserPresenceChanged = new UserPresenceChanged();
                    }
                    if (presence.isAvailable()) {
                        String jid = fullName.substring(fullName.indexOf("@"));
                        mUsersListMap.mUserJidMap.put(name, jid);

                        if (presence.isAway()) {
                            Log.d(FILENAME, " presenceChanged name" + name + AWAY);
                            //mPresenceMapTemp.put(name, AWAY);
                            mUserPresenceChanged.setInfo(name, AWAY);
                            mPresenceMap.put(name, AWAY);
                        } else {
                            Log.d(FILENAME, " presenceChanged name" + name + ONLINE);
                            mUserPresenceChanged.setInfo(name, ONLINE);
                            mPresenceMap.put(name, ONLINE);
                        }
                    } else {
                        Log.d(FILENAME, " presenceChanged name" + name + OFFLINE);
                        mUserPresenceChanged.setInfo(name, OFFLINE);
                        mPresenceMap.put(name, OFFLINE);
                    }
                    Log.d(FILENAME, name);
                    if (bUserListActivityStartInProgress != true) {
                        //StartUserListActivity(ACTION_USERLIST);
                    } else if (mUserPresenceChanged != null) {
                        //SendtoUserListActivity(ACTION_USERLIST);
                        SendtoUserListActivity(ACTION_USERLIST_USERS_PRESENCE);
                    }
                }
            });
        }
    }


    //CreateUserObjectList
    public static List createUserObjectList(Map<String, String> map,Context context)
    {
        List userObjList= new ArrayList();
        //Map<String, String> map = listobj.mUserPresenceMap;
        int i = 0;
        String userName;
        String presence;
        Integer presenceId = null;
        synchronized (map)
        {
            try {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    //userNameListTemp[i] = entry.getKey();
                    userName = entry.getKey();
                    presence = entry.getValue();

                    /*
                    if (presence.equals(ChatXMPPService.OFFLINE)) {
                        presenceId = R.drawable.ic_3offline_icon;
                    } else if (presence.equals(ChatXMPPService.ONLINE)) {
                        presenceId = R.drawable.ic_1online_icon;
                    } else {
                        presenceId = R.drawable.ic_2away;
                    }
                    */
                    UserObject userObj = new UserObject(userName, ChatXMPPService.getDisplayName(userName), null, presenceId, "", false, false);
                    //Bitmap bitmap = Utility.getSavedBitmapForUser(userName, context);
                    //if (bitmap != null)
                    //    userObj.setMyPhoto(bitmap);
                    userObjList.add(userObj);
                    i++;
                }
            } catch (Exception e) {
                Log.d(FILENAME, "exception in createuserlist" + e.getMessage());
                e.printStackTrace();
            }
        }
        /*
        if (users!=null) {
            updateLocationsList(users,context);
        }
        */
        Log.d(FILENAME, "New/Update Users count## " + i);
        return userObjList;
    }

    public static void sendToAllinGroup(String encodedMessage)
    {
        if (mUsersListMap==null) return;
        List userObjList = createUserObjectList(mUsersListMap.mUserJidMap, mContext);
        int usersCount = userObjList.size();
        final long timeStamp = TimeClock.getEpocTime();
        for (int i=0;i<usersCount; i++)
        {
            String receiptId = ChatXMPPService.SendMessage(
                    mContext,
                    encodedMessage,
                    ((UserObject)(userObjList.get(i))).userName,
                    CHAT_MESSAGE_TYPE_TEXT,
                    timeStamp,
                    false,
                    false
                    ); //false means first time being sent
        }
    }

    public boolean isItRoom(String toUser)
    {
        return false;
    }

    public static String FILEKEY = "!#!#^&#!@";

    void handleActionSendMessage(String textToSend, String toUser, String messageType) {
        ChatManager chatManager = null;
        try {
            if (mConnection.isConnected())
            {
                chatManager = ChatManager.getInstanceFor(mConnection);
                boolean isItRoom = isItRoom(toUser);
                if (isItRoom == false)
                {
                    Chat chat = chatManager.createChat(toUser + "@" + DOMAIN);
                    try {
                        Long timeStamp = TimeClock.getEpocTime();
                        String receiptStatus = MESSAGE_ACK_AWAITED;
                        if (messageType.equals(CHAT_MESSAGE_TYPE_IMAGE))
                        {
                            //Here inform to peer about the sent message - do not store this temp message in db - as we have already stored image path in db
                            textToSend = FILEKEY + textToSend;
                            String mimeType = messageType;

                            chat = chatManager.createChat(toUser + "@" + SERVICE_NAME);
                            Message message = new Message();
                            message.setBody(textToSend);
                            String deliveryReceiptId = DeliveryReceiptRequest.addTo(message);
                            chat.sendMessage(message);
                            Log.d(FILENAME, "sendMessage: deliveryReceiptId for this message is: " + deliveryReceiptId);
                        }
                        else
                        {
                            chat = chatManager.createChat(toUser + "@" + SERVICE_NAME);
                            Message message = new Message();
                            message.setBody(textToSend);
                            String deliveryReceiptId = DeliveryReceiptRequest.addTo(message);
                            message.addExtension(new DeliveryReceipt(deliveryReceiptId));
                            chat.sendMessage(message);
                            Log.d(FILENAME, "sendMessage: deliveryReceiptId for this message is: " + deliveryReceiptId);
                            ChatRecord.storeMessage(this, ChatXMPPService.mUserName, toUser, MESSAGEDIRECTIONSENT, null, CHAT_MESSAGE_TYPE_TEXT, textToSend, null, timeStamp, deliveryReceiptId, receiptStatus, 0, 0);
                            //update receiptId as well in db.
                        }

                    } catch (SmackException.NotConnectedException e) {
                        String string = e.getMessage();
                        Log.e(FILENAME, "Exception" + string);
                        e.printStackTrace();

                    }
                }
                /*
                else
                {
                    MultiUserChat multiUserChat = MultiChat.getMultiUserChatInstanceForRoom(toUser, mUserName, mConnection);
                    multiUserChat.sendMessage(new Message(textToSend));
                    multiUserChat.sendMessage(new Message("TestMessage"));
                    //Chat chat3 = chatManager.createChat(toUser + "@" + "conference." + DOMAIN);
                }
                */
            }
        } catch (Exception e) {
            String string = e.getMessage();
            Log.e(FILENAME, "Exception" + string);
            e.printStackTrace();
        }
    }

    class MyChatMessageListener implements ChatMessageListener {
        @Override
        public void processMessage(Chat chat, Message message) {
            Log.d(FILENAME, "MyChatMessageListener: processMessage:" + message.getBody());
            String from = message.getFrom();
            String body = message.getBody();
            if (body == null) {
                return;
            }

            /* Check if it is a command message */
            MesgCmdInfo cmdInfo = MesgCmdProcessor.processCmdInMessage(from,body);
            if(cmdInfo != null)
            {
                /*
                if(cmdInfo.equals("MCA") && cmdInfo.getArgs() != null && cmdInfo.getArgs().length>0)
                {
                    MultiUserChat muc = MultiChat.FormMultiUserChat(mConnection,cmdInfo.getArgs()[0]);
                    registerChatInAction(muc,from);
                }
                return;
                */
            }

            int indexofAtTheRate = from.indexOf("@");
            final String remoteUserName;
            if (indexofAtTheRate > 0)
            {
                remoteUserName = from.substring(0, indexofAtTheRate);
            } else
            {
                remoteUserName = from;
            }

            //Check if body is an indication for file?
            if(body.contains(FILEKEY))
            {
                final String fileName = body.substring(FILEKEY.length());
                //This is image type- process - as in case of image being received:
                new Thread(new Runnable() {
                    public void run() {
                        Log.d(FILENAME, "file receive success");
                        String fileNameWithPath = FileSharing.downloadFile(fileName);
                        if (fileNameWithPath == null)
                        {
                            return;
                        }
                        File tempFile = new File(fileNameWithPath);
                        String url = tempFile.getAbsolutePath();
                        FileNameMap fileNameMap = URLConnection.getFileNameMap();
                        String mimeType = fileNameMap.getContentTypeFor("file://" + url);
                        long fileSize = tempFile.length();
                        long fileSizetoReceive = 0;
                        tempFile.getTotalSpace();
                        long fileSizeRecieved = fileSize;//.getTotalSpace();//transfer.getAmountWritten();
                        //Log.d(FILENAME, "FileSizetoReceive = " +fileSizetoReceive );
                        //Log.d(FILENAME, "FileSizeReceived = " +fileSizeRecieved );

                        if (ChatXMPPService.mResponseMessengerChat == null) {
                            Log.d(FILENAME, "fileTransferRequest: Cannot send message to activity - no activity registered to this service. So storing and showing notification intent");
                            ChatXMPPService.count++;
                            //String timeStamp = TimeClock.getDateTime();
                            //final String receiptId = timeStamp;
                            final long timeStamp = TimeClock.getEpocTime();
                            final String receiptId = TimeClock.getEpocTimeLongToString(timeStamp);

                            //Store message here.
                            //ChatRecord.storeMessage(getApplicationContext(), ChatXMPPService.mUserName, remoteUserName, ChatBubbleActivity.MESSAGEDIRECTIONRECEIVED, null, mimeType, remoteUserName + ":" + file.getAbsolutePath(), file.getAbsolutePath(), timeStamp, receiptId, ChatBubbleActivity.MESSAGE_ACKED, fileSizetoReceive, fileSizeRecieved);
                            ChatRecord.storeMessage(mContext, ChatXMPPService.mUserName, remoteUserName, MESSAGEDIRECTIONRECEIVED, null, mimeType, remoteUserName + ":" + fileNameWithPath, fileNameWithPath, timeStamp, receiptId, MESSAGE_ACKED, fileSizetoReceive, fileSizeRecieved);

                            //Notification action is just to inform user of messages:
                            //ChatXMPPService.prepareAndSendNotification(getApplicationContext(), remoteUserName, file.getAbsolutePath(), true);
                            ChatXMPPService.prepareAndSendNotification(mContext, remoteUserName, fileName, true);
                        }
                        else
                        {
                            Log.d(FILENAME, "Sending message to CHAT activity: " + remoteUserName + " file: " + fileName);
                            ChatXMPPService.count = 0;
                            Bundle data = new Bundle();
                            data.putSerializable(REMOTE_USER_NAME, remoteUserName);
                            data.putSerializable(ChatXMPPService.CHAT_MESSAGETYPE, mimeType);
                            data.putSerializable(ChatXMPPService.MESSAGERECEIVED, fileNameWithPath);
                            data.putSerializable(ChatXMPPService.FILESIZEEXPECTED, fileSizetoReceive);
                            data.putSerializable(ChatXMPPService.FILESIZEDISK, fileSizeRecieved);
                            android.os.Message msg = android.os.Message.obtain(null, ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGE);
                            msg.setData(data);
                            try {
                                ChatXMPPService.mResponseMessengerChat.send(msg);
                            } catch (RemoteException e) {
                                Log.d(FILENAME, "Exception in sending message to Chat Activity : " + e.getMessage());
                                e.printStackTrace();
                            }
                        }

                    }
                }).start();
                return;
            }
            else {
                /*String screenText = remoteUserName;
                screenText += ": ";
                screenText += body;
                screenText += "\n";
                */
                String screenText = body;
                //String toSpeak = "BitChat received from " + getDisplayName(remoteUserName) + "." + body;
                String toSpeak = "MotoStar " + body;
                /*
                if (t1 != null && MainActivity.isAnnouncementOn) {
                    t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                }
                */
                RcMessages.handleReceivedMessage(body, remoteUserName);

                //informChatActivity(remoteUserName, screenText, body);
                SendtoChatActivity(remoteUserName, screenText);
            }
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    public static Messenger mResponseMessengerUserList = null;
    public static Messenger mResponseMessengerChat = null;
    public static Messenger mResponseMessengerLogin = null;

    @Override
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        debugVar += 2;
        Log.d(FILENAME, "onBind Binding messenger..." + debugVar);
        return mMessenger.getBinder();
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                //following case not used currently
                case INTER_TASK_MESSAGE_TYPE_USERLIST_QUERY:
                    Bundle b = msg.getData();
                    if (SEND_ACTION_USERLIST == true) {
                        if (b != null) {
                            SendtoUserListActivity(ACTION_USERLIST);
                        } else {
                            //sendToActivity("Who's there? Speak!");
                        }
                        SEND_ACTION_USERLIST = false;
                    }
                    break;
                /*
                case INTER_TASK_MESSAGE_TYPE_START_TRACKING:
                    Log.d(FILENAME, "INTER_TASK_MESSAGE_TYPE_START_TRACKING Messenger.");
                    //Context contxt = getApplicationContext();
                    setTrackingPreference(mContext, true);
                    setLocationDialogPreference(mContext, true);
                    locationInstance.AllowMyLocationUpdates(mContext);
                    break;
                case INTER_TASK_MESSAGE_TYPE_STOP_TRACKING:
                    Log.d(FILENAME, "INTER_TASK_MESSAGE_TYPE_STOP_TRACKING Messenger.");
                    //Context contxt = getApplicationContext();
                    setTrackingPreference(mContext, false);
                    setLocationDialogPreference(mContext, false);
                    locationInstance.StopMyLocationUpdates(mContext);
                    break;
                    */
                case INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST:
                    Log.d(FILENAME, "Registered USERLIST Activity's Messenger.");
                    mResponseMessengerUserList = msg.replyTo;
                    SendtoUserListActivity(ACTION_USERLIST);
                    //prepareAndSendChatRoomListFromDb();
                    SendtoUserListActivity(ACTION_TRACKING_PREFERENCE);
                    break;
                case INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST:
                    Log.d(FILENAME, "UnRegistered USERLIST Activity's Messenger.");
                    //bUserListActivityStartInProgress = false;
                    mResponseMessengerUserList = null;
                    break;
                case INTER_TASK_MESSAGE_TYPE_REGISTER_LOGIN:
                    Log.d(FILENAME, "Registered LOGIN Activity's Messenger.");
                    mResponseMessengerLogin = msg.replyTo;
                    break;
                case INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN:
                    Log.d(FILENAME, "UnRegistered LOGIN Activity's Messenger.");
                    mResponseMessengerLogin = null;
                    break;
                case INTER_TASK_MESSAGE_TYPE_LOGIN_QUERY:
                    b = msg.getData();
                    /*
                    if (SEND_ACTION_USERLIST == true) {
                        if (b != null) {
                            SendtoUserListActivity(ACTION_USERLIST);
                        } else {
                            //sendToActivity("Who's there? Speak!");
                        }
                        SEND_ACTION_USERLIST = false;
                    }*/
                    break;
                case INTER_TASK_MESSAGE_TYPE_REGISTER_CHAT:
                    Log.d(FILENAME, "Registered CHAT Activity's Messenger.");
                    mResponseMessengerChat = msg.replyTo;
                    break;
                case INTER_TASK_MESSAGE_TYPE_UNREGISTER_CHAT:
                    Log.d(FILENAME, "UnRegistered CHAT Activity's Messenger.");
                    mResponseMessengerChat = null;
                    break;
                case INTER_TASK_MESSAGE_TYPE_CHAT_QUERY:
                    b = msg.getData();
                    /*
                    if (SEND_ACTION_USERLIST == true) {
                        if (b != null) {
                            SendtoUserListActivity(ACTION_USERLIST);
                        } else {
                            //sendToActivity("Who's there? Speak!");
                        }
                        SEND_ACTION_USERLIST = false;
                    }*/
                    break;

                /*
                case INTER_TASK_MESSAGE_TYPE_REGISTER_CHATROOMCALLBACK:
                    b = msg.getData();
                    String roomName = b.getString("roomName");
                    String inviter = mUserName +DOMAINJID_BARE;
                    MultiUserChat multiUserChat = MultiChat.getMultiUserChatInstanceForRoom(roomName, inviter , mConnection);
                    if (multiUserChat != null)
                    {
                        registerChatInAction(multiUserChat, inviter);
                        //ChatXMPPService.MultiChatInActionCallback newRoomListner = new MultiChatInActionCallback(multiUserChat, inviter, null, null);
                        //MultiChat.setUpGroupChatInActionListeners(multiUserChat, newRoomListner);
                    }
                    break;
                 */
                default:
                    super.handleMessage(msg);
            }
        }
    }

    public static void SendtoUserListActivity(String action) {
        if (mResponseMessengerUserList == null) {
            Log.d(FILENAME, "SendtoUserListActivity: Cannot send message to activity - no activity registered to this service.");
        }

        /*
        else if (action.equals(ACTION_USERS_MAP_UPDATES)) {
            Log.d(FILENAME, "Sending message to USERLIST activity --> Map fragment: " + action);
            Bundle data = new Bundle();
            data.putSerializable("usersMapLocations", locationInstance.usersMapLocations);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERS_MAP_LOCATIONS);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity  usersMapLocations Error" + e.getMessage());
                e.printStackTrace();
            }
        }
        else if (action.equals(ACTION_USERS_MAP_UPDATES_FAILED)) {
            Log.d(FILENAME, "Sending message to USERLIST activity --> Map fragment: " + action);
            Bundle data = new Bundle();
            //data.putSerializable("usersMapLocations", usersMapLocations);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERS_MAP_LOCATIONS_FAILED);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity  usersMapLocations Error" + e.getMessage());
                e.printStackTrace();
            }
        }
        else if (action.equals(ACTION_USER_MAP_UPDATES)) {
            Log.d(FILENAME, "Sending message to user track of today to userlistactivity --> Map fragment: " + action);
            Bundle data = new Bundle();
            data.putSerializable("userMapLocations", locationInstance.userMapLocations);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USER_MAP_LOCATIONS);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity  usersMapLocations Error" + e.getMessage());
                e.printStackTrace();
            }
        }
        else if (action.equals(ACTION_USER_MAP_UPDATES_FAILED)) {
            Log.d(FILENAME, "Sending message to user track of today to userlistactivity --> Map fragment: " + action);
            Bundle data = new Bundle();
            //data.putSerializable("userMapLocations", userMapLocations);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USER_MAP_LOCATIONS_FAILED);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity  usersMapLocations Error" + e.getMessage());
                e.printStackTrace();
            }
        }
        */

        else if (action.equals(ACTION_USERLIST)) {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();
            data.putSerializable("userPresenceMap", mPresenceMap);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
            //loguserlist();
        }
        else if (action.equals(ACTION_USERLIST_USERS_ADDED)) {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();
            data.putSerializable("userPresenceMap", mAddedMapTemp);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_ADDED);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
            //loguserlist();
        }
        else if (action.equals(ACTION_USERLIST_USERS_UPDATED)) {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();
            data.putSerializable("userPresenceMap", mPresenceMapTemp);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_UPDATED);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
            //loguserlist();

        }
        else if (action.equals(ACTION_USERLIST_USERS_PRESENCE)) {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();

            data.putSerializable("userPresenceChanged", mUserPresenceChanged);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_PRESENCE_CHANGED);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
            //loguserlist();
        }

        /*
        else if (action.equals(ACTION_CHATROOM_ADDED))
        {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();

            data.putSerializable("roomName", mRoomName);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOM_ADDED);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
            //loguserlist();

        }
        else if (action.equals(ACTION_CHATROOM_REMOVED))
        {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();

            data.putSerializable("roomName", mRoomName);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOM_REMOVED);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
            //loguserlist();

        }
        else if (action.equals(ACTION_CHATROOMLIST_ADDED))
        {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();

            data.putSerializable("roomList", mRoomListMap);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOMLIST_ADDED);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
        }
        else if (action.equals(ACTION_TRACKING_PREFERENCE))
        {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();


            data.putSerializable("trackingPreference", getTrackingPreference(mContext));
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST_TRACKING_PREFERENCE);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
        }
        */
    }

    /*
    static public ProfileInfo LoadProfileInfoFromServer(String JID) {
        ProfileInfo pInfo = null;
        XMPPTCPConnection mConnection = ChatConnection.getInstance().getConnection();

        if (mConnection == null || !mConnection.isConnected())
            return null;

        VCardManager vCardManager = VCardManager.getInstanceFor(mConnection);
        try {
            VCard vcard = vCardManager.loadVCard(JID);
            if (vcard != null) {
                String firstName = vcard.getFirstName();
                String lastName = vcard.getLastName();
                String emailIdWork = vcard.getEmailWork();
                String emailIdHome = vcard.getEmailHome();
                String nickName = vcard.getNickName();
                byte[] avatarByte = vcard.getAvatar();
                pInfo = new ProfileInfo(firstName, lastName, nickName, emailIdWork);
                pInfo.setAvatarImage(avatarByte);
            }

        } catch (Exception ex) {
            //TODO
        }

        return pInfo;
    }
    */

    void SendtoChatActivity(String remoteUserName, String screenText) {
        if (mResponseMessengerChat == null) {
            Log.d(FILENAME, "SendtoChatActivity: Cannot send message to activity - no activity registered to this service. So showing notification intent");
            count++;

            //String timeStamp = TimeClock.getDateTime();
            //String receiptId = timeStamp;
            final long timeStamp = TimeClock.getEpocTime();
            final String receiptId = TimeClock.getEpocTimeLongToString(timeStamp);

            //Store message here.
            ChatRecord.storeMessage(this, ChatXMPPService.mUserName, remoteUserName, MESSAGEDIRECTIONRECEIVED, null, CHAT_MESSAGE_TYPE_TEXT, screenText, null, timeStamp, receiptId, MESSAGE_ACKED, 0, 0);
            //Notification action is just to inform user of messages:
            prepareAndSendNotification(this, remoteUserName, screenText, false);
        } else {
            Log.d(FILENAME, "Sending message to CHAT activity: " + remoteUserName + " text: " + screenText);
            count = 0;
            Bundle data = new Bundle();
            data.putSerializable(REMOTE_USER_NAME, remoteUserName);
            data.putSerializable(ChatXMPPService.CHAT_MESSAGETYPE, CHAT_MESSAGE_TYPE_TEXT);
            data.putSerializable(ChatXMPPService.MESSAGERECEIVED, screenText);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_MESSAGE);
            msg.setData(data);
            try {
                mResponseMessengerChat.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoChatActivity" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public static void prepareAndSendNotification(Context context, String remoteUserName, String screenText, boolean isFile) {
        //Context context = getApplicationContext();
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.setAction(ChatXMPPService.ACTION_RECEIVEDMESSAGE);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra(REMOTE_USER_NAME, remoteUserName);
        resultIntent.putExtra(ChatXMPPService.MESSAGERECEIVED, screenText);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        // Gets a PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (isFile) {
            screenText = screenText.substring(screenText.lastIndexOf("/") + 1);
        }
        String fromDisplayName = ChatXMPPService.mUsersListMap.getDisplayName(remoteUserName);
        if (fromDisplayName == null || fromDisplayName.length() == 0) {
            fromDisplayName = remoteUserName;
        }
        /*
        mNotificationManager.notify(10, builder.setContentTitle("BitChat" + " " + fromDisplayName)
                .setContentText(screenText)
                .setSmallIcon(R.drawable.noti_icon)
                .setLights(Color.RED, 3000, 3000)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true)
                .setTicker("BitsChat")
                .setNumber(count)
                .build());
                */

        //.setVibrate(new long[]{1000, 1000})
    }



    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionLogin(Context context, String param1, String param2, String loggedIn) {
        Intent intent = new Intent(context, ChatXMPPService.class);
        intent.setAction(ACTION_LOGIN);
        intent.putExtra(USER_NAME, param1);
        intent.putExtra(PASSWORD, param2);
        intent.putExtra(LOGGEDIN, loggedIn);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionREGISTER(Context context, String param1, String param2) {
        Intent intent = new Intent(context, ChatXMPPService.class);
        intent.setAction(ACTION_REGISTER);
        intent.putExtra(USER_NAME, param1);
        intent.putExtra(PASSWORD, param2);
        context.startService(intent);
    }

    // TODO: Customize helper method
    public static void startActionSendMessage(Context context, String param1, String param2, String param3)
    {

        Intent intent = new Intent(context, ChatXMPPService.class);
        intent.setAction(ACTION_SENDMESSAGE);
        intent.putExtra(MESSAGETOSEND, param1);
        intent.putExtra(TOUSER, param2);
        intent.putExtra(CHAT_MESSAGETYPE, param3);
        context.startService(intent);
    }

    public static String SendMessageToRemoteUser(Context context, String textToSend, String toUser, String messageType, Long timeStamp, boolean isResending)
    {
        String deliveryReceiptId = null;
        XMPPTCPConnection mConnection = ChatConnection.getInstance().getConnection();
        if(mConnection==null) return null;
        String receiptStatus = MESSAGE_ACK_AWAITED;
        if (mConnection.isConnected())
        {
            ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
            try {
                Chat chat = chatManager.createChat(toUser + "@" + SERVICE_NAME);
                Message message = new Message();
                if (messageType.equals(CHAT_MESSAGE_TYPE_IMAGE))
                {
                    textToSend = FILEKEY + textToSend;
                }
                message.setBody(textToSend);
                deliveryReceiptId = DeliveryReceiptRequest.addTo(message);
                message.addExtension(new DeliveryReceipt(deliveryReceiptId));

                Log.d(FILENAME, "sendMessage: deliveryReceiptId for this message is: " + deliveryReceiptId);
                /*
                if (isResending == false && messageType.equals(CHAT_MESSAGE_TYPE_TEXT)) //store message - only if it wasn't stored earlier.
                {
                     ChatRecord.storeMessage(context, ChatXMPPService.mUserName, toUser, MESSAGEDIRECTIONSENT, null, CHAT_MESSAGE_TYPE_TEXT, textToSend, null, timeStamp, deliveryReceiptId, receiptStatus, 0, 0);
                }
                else
                {
                    //update chat record for receiptID
                    DataSource db = new DataSource(context);
                    db.open();
                    //Here update chat record for inserting receiptId based on timestamp:
                    db.updateChatRecord(ChatXMPPService.mUserName, toUser, deliveryReceiptId, receiptStatus, timeStamp);
                    db.close();
                }
                */
                chat.sendMessage(message);

            } catch (SmackException.NotConnectedException e) {
                String string = e.getMessage();
                Log.e(FILENAME, "Exception" + string);
                e.printStackTrace();
            }
        }
        /*
        else
        {
            receiptStatus = MESSAGE_RESEND_REQUIRED;
            if (isResending == false && messageType.equals(CHAT_MESSAGE_TYPE_TEXT)) //store message - only if it wasn't stored earlier.
            {
                ChatRecord.storeMessage(context, ChatXMPPService.mUserName, toUser, MESSAGEDIRECTIONSENT, null, CHAT_MESSAGE_TYPE_TEXT, textToSend, null, timeStamp, deliveryReceiptId, receiptStatus, 0, 0);
            }
        }
        */
        return deliveryReceiptId;
    }

    /*

    public static String SendMessageToGroup(Context context, String textToSend, String room, String messageType, Long timeStamp, boolean isResending)
    {
        String deliveryReceiptId = null;
        XMPPTCPConnection mConnection = ChatConnection.getInstance().getConnection();
        String receiptStatus = MESSAGE_ACK_AWAITED;
        if (mConnection.isConnected())
        {
            MultiUserChat muc = MultiChat.FormMultiUserChat(mConnection,room);
            if(muc != null)
            {
                try
                {
                    Message message = new Message();
                    message.setBody(textToSend);
                    deliveryReceiptId = DeliveryReceiptRequest.addTo(message);
                    message.addExtension(new DeliveryReceipt(deliveryReceiptId));
                    if (isResending == false && (messageType.equals(CHAT_MESSAGE_TYPE_TEXT)||messageType.equals(CHAT_MESSAGE_TYPE_CMD))) //store message - only if it wasn't stored earlier.
                    {
                        //TODO: Do handling as commented code for group users as well.
                        //ChatRecord.storeMessage(context, ChatXMPPService.mUserName, room, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT, textToSend, null, timeStamp, deliveryReceiptId, receiptStatus, 0, 0);
                    }
                    else
                    {
                        //TODO: Do handling as done in case of a remote user for group user.
//                        //update chat record for receiptID
//                        DataSource db = new DataSource(context);
//                        db.open();
//                        //Here update chat record for inserting receiptId based on timestamp:
//                        db.updateChatRecord(ChatXMPPService.mUserName, room, deliveryReceiptId, receiptStatus, timeStamp);
//                        db.close();
                    }

                    muc.sendMessage(message);
                }
                catch (Exception ex)
                {
                    String string = ex.getMessage();
                    Log.e(FILENAME, "Exception" + string);
                    ex.printStackTrace();
                }

            }

        }
        else
        {
            receiptStatus = MESSAGE_RESEND_REQUIRED;
            if (isResending == false && (messageType.equals(CHAT_MESSAGE_TYPE_TEXT)||messageType.equals(CHAT_MESSAGE_TYPE_CMD))) //store message - only if it wasn't stored earlier.
            {
                //TODO : Do the handling same as done for remote user
                //ChatRecord.storeMessage(context, ChatXMPPService.mUserName, room, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT, textToSend, null, timeStamp, deliveryReceiptId, receiptStatus, 0, 0);
            }
        }
        return deliveryReceiptId;
    }
    */

    public static String SendMessage(Context context, String textToSend, String toUserOrRoom, String messageType, Long timeStamp, boolean isResending,boolean isRoom)
    {
        String deliveryReceiptId = null;

        if(isRoom)
        {
            //deliveryReceiptId = SendMessageToGroup(context,textToSend,toUserOrRoom, messageType,timeStamp,isResending);
        }
        else
        {
            deliveryReceiptId = SendMessageToRemoteUser(context,textToSend,toUserOrRoom, messageType,timeStamp,isResending);
        }
        return deliveryReceiptId;
    }

    void loguserlist() {
        if (mUsersListMap == null) {
            return;
        }
        /* Observed a rare crash in next line in next due to concurrent modfication execution. commented:
        for (Map.Entry<String, String> searchentry : mUsersListMap.mUserPresenceMap.entrySet()) {
            Log.d(FILENAME, " loguserlist user " + searchentry.getKey() + "presence " + searchentry.getValue());
        }
        */
    }



    //callback functions of MapInfoProcessInBackground
    public static void onUsersMapSuccess() {
        SendtoUserListActivity(ACTION_USERS_MAP_UPDATES);
    }

    public void onFailure(int statusCode) {

    }


/// Group Chat Code starts from here::::::::::::::::::::::::::::::
    /*
    static String mRoomName = null;
    public class MultiChatRoomInviteListner implements MultiChat.RoomInvitationCallback
    {

        MultiChatRoomInviteListner()
        {

        }

        public void OnRoomInvitationRecieve(MultiUserChat room,String inviter,String reason,String password,Message message)
        {
            Log.d(FILENAME, "OnRoomInvitationRecieve");
            registerChatInAction(room, inviter);

            //MultiChatInActionCallback newRoomListner = new MultiChatInActionCallback(room, inviter, reason, message);
            //MultiChat.setUpGroupChatInActionListeners(room, newRoomListner);
            //Add following in db - for future references:
            // inviterName: inviter - split
            // inviterJid - inviter - split
            // RoomName - room.room - split
            // RoomJid - room.room - split
            // 10 users in room
            String roomFullName = room.getRoom();
            String roomName = roomFullName.substring(0, roomFullName.indexOf("@"));
            String roomJid = roomFullName.substring(roomFullName.indexOf("@"));
            String inviterName = inviter.substring(0, inviter.indexOf("@"));
            String inviterJid = inviter.substring(inviter.indexOf("@"));

            //MultiUserChat multiUserChat = MultiChat.getMultiUserChatInstanceForRoom(roomName, inviter, mConnection);
            //registerChatInAction(multiUserChat, inviter);

            RoomRecord.storeUniqueNameRecordNew(getBaseContext(), roomName, roomJid, inviterName, inviterJid,
                    1, null, null, null, null, null, null, null, null, null, null, ChatXMPPService.GROUPNAME);
            mRoomName = roomName;
            SendtoUserListActivity(ACTION_CHATROOM_ADDED);
        }
        public void OnRemoveRoomInvitationRecieve(MultiUserChat room,String inviter,String reason,String password,Message message)
        {
            Log.d(FILENAME, "OnRemoveRoomInvitationRecieve");
            String roomFullName = room.getRoom();
            String roomName = roomFullName.substring(0, roomFullName.indexOf("@"));
            String roomJid = roomFullName.substring(roomFullName.indexOf("@"));
            String inviterName = inviter.substring(0, inviter.indexOf("@"));
            String inviterJid = inviter.substring(inviter.indexOf("@"));
            RoomRecord.clearFromDB(getBaseContext(), roomName);
            mRoomName = roomName;;
            SendtoUserListActivity(ACTION_CHATROOM_REMOVED);
        }
    }


    public class MultiChatInActionCallback implements MultiChat.GroupChatInActionCallback {

        MultiUserChat mRoom;
        String mInviter;
        String mReason;
        Message mMessage;
        String mRoomName;
        MultiChatInActionCallback(MultiUserChat room,String inviter,String reason,Message message)
        {
            Log.d(FILENAME, "MultiChatInActionCallback");
            mRoom = room;
            mInviter = inviter;
            mReason = reason;
            mMessage = message;
            mRoomName = mRoom.getRoom();
        }
        public void processParticipantPresence(Presence presence)
        {
            Log.d(FILENAME, "processParticipantPresence");
        }
        public void processSubjectChange(String subject,String from)
        {
            Log.d(FILENAME, "processSubjectChange");
        }
        public void processRemoveMessage(Message message)
        {
            Log.d(FILENAME, "processRemoveMessage");
        }
        public void processInvitationDeclined(String invitee, String reason)
        {
            Log.d(FILENAME, "processInvitationDeclined");
        }

        public void processRecievedMessage(Message message)
        {
            Log.d(FILENAME, "processReceivedMessage");
            String from = message.getFrom();
            String body = message.getBody();
            //String toSpeak = "BitChat received from "+ getDisplayName(remoteUserName) + "." + body;

            int indexofAtTheRate = from.indexOf("@");
            String remoteUserName;
            if (indexofAtTheRate > 0) {
                remoteUserName = from.substring(0, indexofAtTheRate);
            } else {
                remoteUserName = from;
            }
            if(remoteUserName.equals(mUserName))
            {
                Log.d(FILENAME, "Self Message received - ignore");
                //ignore the message and return back
                return;
            }
            String screenText = body;
            String toSpeak = "BitChat received from "+ getDisplayName(remoteUserName) + "." + body;
            if (t1 != null && isAnnouncementOn)
            {
                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            }
            //informChatActivity(remoteUserName, screenText, body);
            SendtoChatActivity(remoteUserName, body);
        }
    }


    public void registerChatInAction(MultiUserChat multiUserChat, String inviter)
    {
        Log.d(FILENAME, "registerChatInAction");

        if (multiUserChat != null)
        {
            MultiChatInActionCallback newRoomListner = new MultiChatInActionCallback(multiUserChat, inviter, null, null);
            MultiChat.setUpGroupChatInActionListeners(multiUserChat, newRoomListner);
        }
        else
        {
            Log.d(FILENAME, "multiUserChat is null");
        }
    }
    */



    public static void invokeJsonGetFile(Context context, String jsonPath, String jsonString) {
        Log.d(FILENAME, "invokeJsonRequestFile");
        AsyncHttpClient client = new AsyncHttpClient();
        //Applicable in case a list is returned
        client.removeHeader("Content-Type");
        client.addHeader("Content-Type", "application/json;charset=UTF-8");
        client.addHeader("Accept", "application/json");
        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        final Context context2 = context;

        client.get(context, jsonPath, null,
                // client.get(context, jsonPath, null, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, org.apache.http.Header[] headers,
                                          org.json.JSONArray responseArray) {
                        Log.d(FILENAME, "http onSuccess: ");// + responseArray);
                        HashMap<String, String> hmap = new HashMap<String, String>();
                        for (org.apache.http.Header h : headers) {
                            hmap.put(h.getName(), h.getValue());
                            if (h.getName().equals("Content-Length")) {
                                if (h.getValue().equals("0")) {
                                    Log.d(FILENAME, "successfully response");
                                }
                            }
                        }

                        try
                        {
                            for (int i = 0; i < responseArray.length(); i++) {
                                //JSONObject objects = responseArray.getJSONArray(i);
                                JSONObject object = responseArray.getJSONObject(i);
                                String groupName = object.getString("groupName");
                                String userName = object.getString("userName");
                            }
                            //SendtoUserListActivity(ACTION_USERS_MAP_UPDATES);
                        } catch (JSONException e) {
                            Log.d(FILENAME, e.getMessage());
                        }
                    }
                    // When the response returned by REST has Http response code other than '200'
                    @Override
                    public void onFailure(int statusCode, org.apache.http.Header[] headers,
                                          byte[] responseBody, Throwable error) {
                        Log.d(FILENAME, "http onFailure");
                        // When Http response code is '404'
                        if (statusCode == 404) {
                            Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code is '404'
                        else if (statusCode == 409) {
                            Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code other than 404, 500
                        else {
                            Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                        }
                        //SendtoUserListActivity(ACTION_USERS_MAP_UPDATES_FAILED);
                    }
                }
        );
        return;
    }

    public static void invokeJsonPostFile(Context context, String jsonPath, String jsonString) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.removeHeader("Content-Type");
        client.addHeader("Content-Type", "MediaType=multipart/form-data");
        client.addHeader("Accept", "application/json");
        //client.addHeader("MediaType", "multipart/form-data");
        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        client.post(context, jsonPath, se, "application/json", new
                        AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, org.apache.http.Header[] headers,
                                                  byte[] responseBody) {
                                //Log.d(FILENAME, "http onSuccess: " + responseBody);
                                //try {
                                // JSON Object
                                HashMap<String, String> hmap = new HashMap<String, String>();
                                for (org.apache.http.Header h : headers) {
                                    hmap.put(h.getName(), h.getValue());
                                    if (h.getName().equals("Content-Length")) {
                                        if (h.getValue().equals("0")) {
                                            Log.d(FILENAME, "successfully response");
                                        }
                                    }
                                }
                            }

                            // When the response returned by REST has Http response code other than '200'
                            @Override
                            public void onFailure(int statusCode, org.apache.http.Header[] headers,
                                                  byte[] responseBody, Throwable
                                                          error) {
                                // Hide Progress Dialog
                                //prgDialog.hide();
                                Log.d(FILENAME, "http onFailure");
                                // When Http response code is '404'
                                if (statusCode == 404) {
                                    Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                                }
                                // When Http response code is '404'
                                else if (statusCode == 409) {
                                    Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                                }
                                // When Http response code is '500'
                                else if (statusCode == 500) {
                                    Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                                }
                                // When Http response code other than 404, 500
                                else {
                                    Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                                }
                            }
                        }
        );
        return;
    }

    /*

    public void startLocationEnableDialog() {
        Log.d(FILENAME, "Start GPS");
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//Manish: Fixed crash in start activity : observed in tab7
        startActivity(intent);
        Toast.makeText(mContext, "Turn on Locations in settings", Toast.LENGTH_SHORT).show();
        setLocationDialogPreference(mContext, false);
    }
    */
}