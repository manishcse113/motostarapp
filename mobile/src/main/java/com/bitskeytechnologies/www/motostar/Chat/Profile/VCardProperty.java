package com.bitskeytechnologies.www.motostar.Chat.Profile;

/**
 * Created by Manish on 1/22/2016.
 */
public class VCardProperty {
    String property;
    String value;

    public VCardProperty(String property, String value) {
        this.property = property;
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
