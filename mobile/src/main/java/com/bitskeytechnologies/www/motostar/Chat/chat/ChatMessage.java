package com.bitskeytechnologies.www.motostar.Chat.chat;

import com.bitskeytechnologies.www.motostar.TimeClock;
import com.bitskeytechnologies.www.motostar.Chat.gen.ChatXMPPService;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Created by Manish on 28-09-2015.
 */

public class ChatMessage extends Object {
    public int direction;
    public String message;
    //public String messagetype;
    public String filePath;
    public String fileName;
    public String mimeType;
    public Long timeStamp;
    public String timeStampStr;
    public String timeToDisplay;
    public String receiptId;
    public String receiptStatus;
    public long fileSizeExpected;
    public long fileSizeDisk;
    public String strFileSizeDisk;
    private final long KB = 1024;
    private final long MB = 1048576;
    private final long GB = 1073741824;

    //Used only to update existing chatmessages
    public ChatMessage(String receiptId, String receiptStatus)
    {
        this.receiptId = receiptId;

        this.receiptStatus = receiptStatus;
        if (receiptStatus == null)
        {
            this.receiptStatus= ChatXMPPService.MESSAGE_ACKED;
        }
    }

    public ChatMessage(String receiptId, long timeStamp)
    {
        this.receiptId = receiptId;

        this.timeStamp = timeStamp;
    }

    public ChatMessage(int direction, String message, String mimeType, String filePath, Long timeStamp, String receiptId, String receiptStatus, long fileSizeExpected, long fileSizeDisk) {
        super();
        this.direction = direction;
        this.message = message;
        //this.messagetype = messagetype;
        this.filePath = filePath;
        this.mimeType = mimeType;

        if (filePath!=null)
        {
            //get file name;
            fileName = filePath.substring(filePath.lastIndexOf("/")+1);
        }
        this.timeStamp = timeStamp;

        this.timeStampStr = TimeClock.getEpocTimeLongToString(timeStamp);

        this.timeToDisplay = null;
        if (TimeClock.isItToday(timeStamp))
        {
            this.timeToDisplay =  TimeClock.getTimeStrFromEpochTime(timeStamp);
        }
        else
        {
            this.timeToDisplay = timeStampStr;
        }

        this.receiptId = receiptId;

        this.receiptStatus = receiptStatus;
        if (receiptStatus == null)
        {
            this.receiptStatus= ChatXMPPService.MESSAGE_ACKED;
        }
        if (fileSizeExpected == 0)
        {
            fileSizeExpected = fileSizeDisk;
        }
        this.fileSizeExpected = fileSizeExpected;
        this.fileSizeDisk = fileSizeDisk;


        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);


        strFileSizeDisk = "";
        if ( this.fileSizeExpected < KB)
        {
            strFileSizeDisk = String.valueOf(this.fileSizeDisk)  + " B";
        }
        else if ((this.fileSizeExpected >= KB) && (this.fileSizeExpected < MB))//Size in KB
        {
            strFileSizeDisk = String.valueOf(
                    (new Double(df.format(((double) (this.fileSizeExpected)) / KB)))) + " KB";
        }
        else if ((this.fileSizeExpected >= MB) && (this.fileSizeExpected < GB))//Size in MB
        {
            strFileSizeDisk = String.valueOf((new Double(df.format(((double) (this.fileSizeExpected)) / MB)))) + " MB";
        }
        else //size is in GB
        {
            strFileSizeDisk = String.valueOf((new Double(df.format(((double) (this.fileSizeExpected)) / GB)))) + " GB";
        }
    }
}