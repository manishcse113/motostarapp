package com.bitskeytechnologies.www.motostar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.bitskeytechnologies.www.motostar.Chat.db.ChatRecord;
import com.bitskeytechnologies.www.motostar.Chat.db.MyRecord;
import com.bitskeytechnologies.www.motostar.db.MyPhoneRecord;
import com.bitskeytechnologies.www.motostar.Chat.db.UserRecord;

import java.util.HashMap;
import java.util.List;

//Manish: Added for CTalk

public class DataSource {

	// Database fields
	public SQLiteDatabase db;
	private DatabaseHelper helper;

	private static String FILENAME = "MS DataSource";

	public DataSource(Context context) {
		helper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		if (helper!=null)
		{
			db = helper.getWritableDatabase();
		}
	}

	public void close() {
		helper.close();
	}




	public MyPhoneRecord getMyPhoneRecord(long id) {

		//Log.d(FILENAME, "getMyRecord");
		return MyPhoneRecord.loadFromDatabase(db, id);
	}

	public List<MyPhoneRecord> getMyPhoneRecords() {
		//Log.d(FILENAME, "getMyRecords");
		return MyPhoneRecord.loadFromDatabase(db);
	}

	public List<MyPhoneRecord> getMyPhoneRecords(String phone) {
		//Log.d(FILENAME, "getMyRecords");
		return MyPhoneRecord.loadFromDatabase(db, phone);
	}
	public void persistMyPhoneRecord(MyPhoneRecord myPhoneRecord) {

		//Log.d(FILENAME, "persistMyPhoneRecord");
		myPhoneRecord.commit(db);
	}

	public void deleteMyPhoneRecord(MyPhoneRecord myPhoneRecord) {
		//Log.d(FILENAME, "deleteMyRecord");
		myPhoneRecord.delete(db);
	}


	public void updateMyPhoneRecord (String phone, String mac,String os, long sdk, String moto_version, long server_update_required)
	{
		//Log.d(FILENAME, "updateChatRecord");
		MyPhoneRecord.LoadAndUpdateMyPhoneRecord(db, phone, mac, os, sdk, moto_version, server_update_required);
	}

	public void updateMyPhoneRecord (String phone,  long server_update_required)
	{
		//Log.d(FILENAME, "updateChatRecord");
		MyPhoneRecord.LoadAndUpdateMyPhoneRecord(db, phone, server_update_required);
	}


	public DevicesRecords getDeviceRecord(long id) {
		//Log.d(FILENAME, "getUserRecord");
		return DevicesRecords.loadFromDatabase(db, id);
	}
	public List<DevicesRecords> getDeviceRecords() {
		//Log.d(FILENAME, "getUserRecords");
		return DevicesRecords.loadFromDatabase(db);
	}
	public List<DevicesRecords> getDeviceRecords(String deviceName) {
		//Log.d(FILENAME, "getUserRecords");
		return DevicesRecords.loadFromDatabase(db, deviceName);
	}
	public void persistDeviceRecord(DevicesRecords devicesRecord) {
		//Log.d(FILENAME, "persistUserRecords");
		devicesRecord.commit(db);
	}
	public void deleteDeviceRecord(DevicesRecords devicesRecord) {
		//Log.d(FILENAME, "delteUserRecords");
		devicesRecord.delete(db);
	}
	public void updateDeviceRecord (String mac,
									long mor_start_hr, long mor_start_min, long mor_retries, long eve_start_hr, long eve_start_min, long eve_retries,
									long retry_interval, long stopTime	)
	{
		//Log.d(FILENAME, "updateChatRecord");
		DevicesRecords.LoadAndUpdateDeviceRecord(db, mac, mor_start_hr, mor_start_min, mor_retries, eve_start_hr, eve_start_min, eve_retries, retry_interval, stopTime);
	}
	public void updateDeviceRecord (String mac,  long server_update_required)
	{
		//Log.d(FILENAME, "updateChatRecord");
		DevicesRecords.LoadAndUpdateDeviceRecord(db, mac, server_update_required);
	}


	//DevicesLocalNameRecords-methods::::
	public DevicesLocalNameRecords getDeviceLocalNameRecord(long id) {
		//Log.d(FILENAME, "getUserRecord");
		return DevicesLocalNameRecords.loadFromDatabase(db, id);
	}
	public List<DevicesLocalNameRecords> getDeviceLocalNameRecords() {
		//Log.d(FILENAME, "getUserRecords");
		return DevicesLocalNameRecords.loadFromDatabase(db);
	}
	public List<DevicesLocalNameRecords> getDeviceLocalNameRecords(String deviceName) {
		//Log.d(FILENAME, "getUserRecords");
		return DevicesLocalNameRecords.loadFromDatabase(db, deviceName);
	}
	public void persistDeviceLocalNameRecord(DevicesLocalNameRecords devicesRecord) {
		//Log.d(FILENAME, "persistUserRecords");
		devicesRecord.commit(db);
	}
	public void deleteDeviceLocalNameRecord(DevicesLocalNameRecords devicesRecord) {
		//Log.d(FILENAME, "delteUserRecords");
		devicesRecord.delete(db);
	}

	public void updateDeviceLocalNameRecord (String mac,  String localName)
	{
		//Log.d(FILENAME, "updateChatRecord");
		DevicesLocalNameRecords.LoadAndUpdateDeviceRecord(db, mac, localName);
	}


	/*
	public List<ProfileImageRecord> getProfileImageRecords() {
		return ProfileImageRecord.loadFromDatabase(db);
	}

	public List<ProfileImageRecord> getProfileImageRecords(String userName) {
		return ProfileImageRecord.loadFromDatabase(db, userName);
	}

	public void persistProfileImageRecord(ProfileImageRecord profileImageRecord)
	{
		profileImageRecord.commit(db);
	}


	public void deleteProfileImageRecord(ProfileImageRecord profileImageRecord) {
		profileImageRecord.delete(db);
	}

*/

	/*
	public EventRecord getEventRecord(long id) {
		return EventRecord.loadFromDatabase(db, id);
	}
	*/

	public List<EventRecord> getEventRecords() {
		return EventRecord.loadFromDatabase(db);
	}
	public List<EventRecord> getEventRecords(String deviceName) {
		return EventRecord.loadFromDatabase(db, deviceName);
	}

	public List<EventRecord> getEventRecords(long month) {
		return EventRecord.loadFromDatabase(db, month);
	}


	public void persistEventRecord(EventRecord eventRecord) {
		eventRecord.commit(db);
	}
	public void deleteEventRecord(EventRecord eventRecord) {
		eventRecord.delete(db);
	}



	public LastEventRecord getLastEventRecord(long id) {
		return LastEventRecord.loadFromDatabase(db, id);
	}
	public List<LastEventRecord> getLastEventRecords() {
		return LastEventRecord.loadFromDatabase(db);
	}
	public List<LastEventRecord> getLastEventRecords(String deviceName) {
		return LastEventRecord.loadFromDatabase(db, deviceName);
	}
	public void persistLastEventRecord(LastEventRecord lastEventRecord) {
		lastEventRecord.commit(db);
	}
	public void deleteLastEventRecord(LastEventRecord lastEventRecord) {
		lastEventRecord.delete(db);
	}
	public void updateLastEventRecord(LastEventRecord lastEventRecord) {
		lastEventRecord.delete(db);
	}


	public List<AlarmRecord> getAlarmRecords() {
		return AlarmRecord.loadFromDatabase(db);
	}
	public List<AlarmRecord> getAlarmRecords(String deviceName) {
		return AlarmRecord.loadFromDatabase(db, deviceName);
	}

	public List<AlarmRecord> getAlarmRecords(long month) {
		return AlarmRecord.loadFromDatabase(db, month);
	}
	public void persistAlarmRecord(AlarmRecord alarmRecord) {
		alarmRecord.commit(db);
	}
	public void deleteAlarmRecord(AlarmRecord alarmRecord) {
		alarmRecord.delete(db);
	}



	public LastAlarmRecord getLastAlarmRecords(long id) {
		return LastAlarmRecord.loadFromDatabase(db, id);
	}
	public List<LastAlarmRecord> getLastAlarmRecords() {
		return LastAlarmRecord.loadFromDatabase(db);
	}
	public List<LastAlarmRecord> getLastAlarmRecords(String deviceName) {
		return LastAlarmRecord.loadFromDatabase(db, deviceName);
	}
	public void persistLastAlarmRecord(LastAlarmRecord lastAlarmRecord) {
		lastAlarmRecord.commit(db);
	}
	public void deleteLastAlarmRecord(LastAlarmRecord lastAlarmRecord) {
		lastAlarmRecord.delete(db);
	}
	public void updateLastAlarmRecord(LastAlarmRecord lastAlarmRecord) {
		lastAlarmRecord.delete(db);
	}



	public SQLiteDatabase getDb()
	{
		return db;
	}


	/*
	public MyLocationRecord getMyLocationRecord(long id) {
		return MyLocationRecord.loadFromDatabase(db, id);
	}

	public List<MyLocationRecord> getMyLocationRecords(String recordedTime) {
		return MyLocationRecord.loadFromDatabase(db, recordedTime);
	}

	public List<MyLocationRecord> getMyLocationRecords() {
		return MyLocationRecord.loadFromDatabase(db);
	}

	public void persistMyLocationRecord(MyLocationRecord myLocationRecord) {
		myLocationRecord.commit(db);
	}

	public void deleteMyLocationRecord(MyLocationRecord myLocationRecord) {
		myLocationRecord.delete(db);
	}
	*/

	/*

	public RoomRecord getRoomRecord(long id) {
		return RoomRecord.loadFromDatabase(db, id);
	}

	public List<RoomRecord> getRoomRecords() {
		return RoomRecord.loadFromDatabase(db);
	}
	public ChatRoomInfo getChatRoomInfo(String roomName)
	{
		if(db == null)
			db  = helper.getWritableDatabase();

		return RoomRecordNew.loadFromDatabase(db,roomName);
	}

	public HashMap<String,ChatRoomInfo> getChatRoomInfoForAllChatRooms()
	{
		return RoomRecordNew.loadFromDatabase(db);
	}


	public List<RoomRecord> getRoomRecords(String userName) {
		return RoomRecord.loadFromDatabase(db, userName);
	}
	public void persistRoomRecord(RoomRecord roomRecord) {
		roomRecord.commit(db);
	}

	public void deleteRoomRecord(RoomRecord roomRecord) {
		roomRecord.delete(db);
	}
	*/



	public ChatRecord getChatRecord(long id) {

		//Log.d(FILENAME, "getChatRecord");
		return ChatRecord.loadFromDatabase(db, id);
	}

	public List<ChatRecord> getChatRecords() {
		//Log.d(FILENAME, "getChatRecords");

		return ChatRecord.loadFromDatabase(db);
	}

	public List<ChatRecord> getChatRecords(String userName) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName);
	}

	public List<ChatRecord> getChatRecords(String userName, String receiptStatus) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName, receiptStatus);
	}

	public List<ChatRecord> getChatRecords(String userName, String remoteUsername, boolean isGroup) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName, remoteUsername,isGroup);
	}

	public List<ChatRecord> getChatRecords(String userName, String remoteUsername, boolean isGroup, Long startDate) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName, remoteUsername, isGroup, startDate);
	}

	public List<ChatRecord> getChatRecords(String userName, String remoteUsername, boolean isGroup, String receiptId) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabaseReceiptId(db, userName, remoteUsername, isGroup, receiptId);
	}

	public void persistChatRecord(ChatRecord chatRecord) {
		//Log.d(FILENAME, "persistChatRecord");
		chatRecord.commit(db);
	}

	public void deleteChatRecord(ChatRecord chatRecord) {
		//Log.d(FILENAME, "deleteChatRecord");
		chatRecord.delete(db);
	}

	public void updateChatRecord (String userName, String remoteUserName, String receiptId, String receiptStatus)
	{
		//Log.d(FILENAME, "updateChatRecord");
		try
		{
			ChatRecord.LoadAndUpdateChatRecord(db, userName, remoteUserName, false, receiptId, receiptStatus);
		}
		catch (Exception e)
		{
			Log.e(FILENAME,"ChatRecord update failed");
		}
	}

	public void updateChatRecord (String userName, String remoteUserName, String receiptId, String receiptStatus, long timeStamp)
	{
		//Log.d(FILENAME, "updateChatRecord");
		ChatRecord.LoadAndUpdateChatRecord(db, userName, remoteUserName, false, receiptId, receiptStatus, timeStamp);
	}



	public MyRecord getMyRecord(long id) {

		//Log.d(FILENAME, "getMyPhoneRecord");
		return MyRecord.loadFromDatabase(db, id);
	}

	public List<MyRecord> getMyRecords() {
		//Log.d(FILENAME, "getMyPhoneRecords");
		return MyRecord.loadFromDatabase(db);
	}

	public List<MyRecord> getMyRecords(String userName) {
		//Log.d(FILENAME, "getMyPhoneRecords");
		return MyRecord.loadFromDatabase(db, userName);
	}
	public void persistMyRecord(MyRecord myRecord) {

		//Log.d(FILENAME, "persistMyPhoneRecord");
		myRecord.commit(db);
	}

	public void deleteMyRecord(MyRecord myRecord) {
		//Log.d(FILENAME, "deleteMyPhoneRecord");
		myRecord.delete(db);
	}


	public void updateMyRecord (String userName, long trackingPreference)
	{
		//Log.d(FILENAME, "updateChatRecord");
		MyRecord.LoadAndUpdateMyRecord(db, userName, trackingPreference);
	}

	public UserRecord getUserRecord(long id) {
		//Log.d(FILENAME, "getUserRecord");
		return UserRecord.loadFromDatabase(db, id);
	}

	public List<UserRecord> getUserRecords() {
		//Log.d(FILENAME, "getUserRecords");
		return UserRecord.loadFromDatabase(db);
	}

	public List<UserRecord> getUserRecords(String userName) {
		//Log.d(FILENAME, "getUserRecords");
		return UserRecord.loadFromDatabase(db, userName);
	}
	public void persistUserRecord(UserRecord userRecord) {
		//Log.d(FILENAME, "persistUserRecords");
		userRecord.commit(db);
	}

	public void deleteUserRecord(UserRecord userRecord) {
		//Log.d(FILENAME, "delteUserRecords");
		userRecord.delete(db);
	}


}
