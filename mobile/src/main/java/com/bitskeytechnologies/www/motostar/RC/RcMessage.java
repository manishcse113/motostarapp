package com.bitskeytechnologies.www.motostar.RC;

import android.util.Log;

import java.nio.charset.Charset;

/**
 * Created by Manish on 8/2/2017.
 */
public class RcMessage
{
    public static String FILENAME = "MS RcMessage";
    enum RcMessageType
    {
        COMMAND,
        ACK,
        TIMEOUT,
        RESPONSE,
        BROADCAST
    }

    //public static String RcMessageKey = "$2@1#7!A%D*g";
    public static String RcMessageKey = "$";

    String grouName = null;
    String senderUserName = null;
    String receiverUserName = null;
    int equipmentId = -1;
    RcMessageType messageType;
    int messageId=-1;
    int messageLenth =0;
    byte[] message;

    String encodedMessage = null;

    public RcMessage(String encodedMessage)
    {
        decodeMessage(encodedMessage);
    }

    public RcMessage( String grouName, String senderUserName, String receiverUserName, int equipmentId, RcMessageType messageType, int messsageId, int messageLenth, byte[] message)
    {
        this.grouName = grouName;
        this.senderUserName = senderUserName;
        this.receiverUserName = receiverUserName;
        this.equipmentId = equipmentId;
        this.messageType = messageType;
        this.messageId = messsageId;
        this.messageLenth = messageLenth;
        this.message = message;
    }

    public String encodeMessage()
    {
        encodedMessage = new String();
        encodedMessage = RcMessageKey + tokenizer+
                        grouName + tokenizer +
                        equipmentId + tokenizer +
                        messageType + tokenizer +
                        messageId + tokenizer +
                        messageLenth + tokenizer;
        if (messageLenth>0)
        {
            encodedMessage = concat(encodedMessage, message,messageLenth );
            encodedMessage+= tokenizer;
        }
        return encodedMessage;
    }

    private static String concat( final String str, final byte[] bytes, int length) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);

        for (int i = 0; i< length; i++)//byte b : bytes)
        {
            sb.append((char) (bytes[i] & 0xFF));
        }
        return sb.toString();
    }

    public static String tokenizer = ";";
    public boolean decodeMessage(String messageReceived)
    {
        int currentOffset = 0;
        int indexofTokenizer = messageReceived.indexOf(tokenizer);
        String remainingString = messageReceived;
        if (indexofTokenizer > 0)
        {
            if (remainingString.substring(0, indexofTokenizer).equals(RcMessageKey))
            {
                try {
                    remainingString = remainingString.substring(indexofTokenizer + 1);

                    grouName = remainingString.substring(0, remainingString.indexOf(tokenizer));
                    remainingString = remainingString.substring(remainingString.indexOf(tokenizer) + 1);

                    equipmentId = Integer.parseInt(remainingString.substring(0, remainingString.indexOf(tokenizer)));
                    remainingString = remainingString.substring(remainingString.indexOf(tokenizer) + 1);

                    //messageType =  remainingString.substring(0, remainingString.indexOf(tokenizer));
                    remainingString = remainingString.substring(remainingString.indexOf(tokenizer) + 1);

                    messageId = Integer.parseInt(remainingString.substring(0, remainingString.indexOf(tokenizer)));
                    remainingString = remainingString.substring(remainingString.indexOf(tokenizer) + 1);

                    messageLenth = Integer.parseInt(remainingString.substring(0, remainingString.indexOf(tokenizer)));
                    remainingString = remainingString.substring(remainingString.indexOf(tokenizer) + 1);

                    //message = remainingString.getBytes(Charset.forName("UTF-8"));
                    message = remainingString.getBytes();
                    /*
                    for (int i = 0; i < messageLenth; i++) {
                        message[i] = remainingString[i];
                        byte[] b = string.getBytes(Charset.forName("UTF-8"));
                                //Byte.parseByte(remainingString.substring(0, remainingString.indexOf(tokenizer)));
                    }
                    */
                    remainingString = remainingString.substring(remainingString.indexOf(tokenizer) + 1);
                    return true;
                }
                catch(Exception e)
                {
                    Log.e(FILENAME, "Could not parse the received message");
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        else
        {
            return false;
        }
    }
}
