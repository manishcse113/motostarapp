package com.bitskeytechnologies.www.motostar.Chat.users;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Manish on 01-08-2015.
 */
public class UserMaps implements Serializable {

   // public HashMap<String, String> mUserPresenceMap = new HashMap<String, String>();
    public HashMap<String, String> mUserJidMap = new HashMap<String, String>();
    public HashMap<String,String> mUserNameToDisplayNameMap = new HashMap<String,String>();//Jid, Display Name
    public HashMap<String,String> mDisplayNameToUserNameMap = new HashMap<String,String>();//Display Name, Jid

    public void addUserNameToMap(String userName,String displayName)
    {
        if(userName == null)
            return;
        String input = userName.toLowerCase();
        mUserNameToDisplayNameMap.put(input,displayName);
    }

    public void addDisplayNameToMap(String displayName, String userName)
    {
        if(displayName == null)
            return;
        String input = displayName.toLowerCase();
        mDisplayNameToUserNameMap.put(input,userName);
    }

    public String getDisplayName(String userName)
    {
        if(userName == null)
            return null;
        String input = userName.toLowerCase();
        String str =mUserNameToDisplayNameMap.get(input);
        return str;
    }

    public String getUserName(String displayName)
    {
        if(displayName == null)
            return null;
        String input = displayName.toLowerCase();
        String str = mDisplayNameToUserNameMap.get(input);
        return str;
    }
}
