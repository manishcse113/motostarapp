package com.bitskeytechnologies.www.motostar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;
import java.util.Vector;

public class MyPhoneRecord extends DBObject {

	private final static String DB_TABLE = "MyPhoneRecords";

	private final static String COLUMN_PHONE = "phone";
	private final static String COLUMN_MAC = "mac_id";
	private final static String COLUMN_PHONE_NAME = "phone_name";
	private final static String COLUMN_OS = "OS";
	private final static String COLUMN_SDK = "SDK";
	private final static String COLUMN_MOTOSTAR_VERSION = "motostar_sw_version";
	private final static String COLUMN_SERVER_UPDATE_REQUIRED = "server_update";


	private final static String[] ALL_COLUMNS = new String[] {
		COLUMN_ID,
			COLUMN_PHONE,
			COLUMN_MAC,
			COLUMN_PHONE_NAME,
			COLUMN_OS,
			COLUMN_SDK,
			COLUMN_MOTOSTAR_VERSION,
			COLUMN_SERVER_UPDATE_REQUIRED
		/*
		COLUMN_PHONE_NUMBER,
		COLUMN_VALIDATION_CODE_1,
		COLUMN_VALIDATION_CODE_2,
		COLUMN_CREATION_TIMESTAMP,
		COLUMN_AUTH_PASSWORD
		*/
	};

	private final static String CREATE_TABLE =
			"CREATE TABLE " + DB_TABLE + " (" +
			COLUMN_ID + " integer primary key autoincrement, " +
					COLUMN_PHONE  + " text, " +
					COLUMN_MAC + " text, " +
					COLUMN_PHONE_NAME + " text, " +
					COLUMN_OS + " text, " +
					COLUMN_SDK + " integer, " +
					COLUMN_MOTOSTAR_VERSION + " text, " +
					COLUMN_SERVER_UPDATE_REQUIRED + " integer " +

					/*
			COLUMN_PHONE_NUMBER + " text, " +
			COLUMN_VALIDATION_CODE_1 + " text, " +
			COLUMN_VALIDATION_CODE_2 + " text, " +
			COLUMN_CREATION_TIMESTAMP + " integer not null, " +
			COLUMN_AUTH_PASSWORD + " text not null	*/
			");";

	String phone = null;
	String mac = null;
	String phone_name = null;
	String os = null;
	long sdk = 0;
	String moto_version = null;
	long server_update_required = 1;
	//long trackingPreference = 1;
	/*
	String phoneNumber = null;
	String validationCode1 = null;
	String validationCode2 = null;
	long creationTimestamp = 0;
	String authPassword = null;
	*/

	public static void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	public static void onUpgrade(SQLiteDatabase db, int oldVersion,
			int newVersion) {
		if(oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
			onCreate(db);
		}
	}

	public MyPhoneRecord() {
		//setCreationTimestamp(System.currentTimeMillis() / 1000);
	}
	
	public static List<MyPhoneRecord> loadFromDatabase(SQLiteDatabase db) {
		Vector<MyPhoneRecord> v = new Vector<MyPhoneRecord>();
		
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, null,
		null, null, null, null);
		if(cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				v.add(fromCursor(cursor));
				cursor.moveToNext();
			}
			cursor.close();
		}
		return v;
	}
	
	public static MyPhoneRecord loadFromDatabase(SQLiteDatabase db, long id) {
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_ID + " = " + id,
				null, null, null, null);
		MyPhoneRecord req = null;
		if( cursor != null) {
			cursor.moveToFirst();
			if (!cursor.isAfterLast())
				req = fromCursor(cursor);
			cursor.close();
		}
		return req;
	}

	//userName All chat records
	public static List<MyPhoneRecord> loadFromDatabase(SQLiteDatabase db, String phone) {
		Vector<MyPhoneRecord> v = new Vector<MyPhoneRecord>();
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_PHONE +  " =?", new String[]{phone},
				null, null, null, null);
		if(cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				v.add(fromCursor(cursor));
				cursor.moveToNext();
			}
			cursor.close();
		}
		return v;

	}

	private static MyPhoneRecord fromCursor(Cursor cursor) {
		MyPhoneRecord req = new MyPhoneRecord();
		int i = 0;
		req.setId(cursor.getLong(i++));
		req.setPhone(cursor.getString(i++));
		req.setMac(cursor.getString(i++));
		req.setPhoneName(cursor.getString(i++));
		req.setOs(cursor.getString(i++));
		req.setSdk(cursor.getLong(i++));
		req.setMotoVersion(cursor.getString(i++));
		req.setServerUpdateRequired(cursor.getLong(i++));
		//req.setDisplayName(cursor.getString(i++));

		//req.

		/*
		req.setId(cursor.getLong(i++));
		req.setPhoneNumber(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setCreationTimestamp(cursor.getLong(i++));
		req.setAuthPassword(cursor.getString(i++));
		*/
		
		return req;
	}

	public final static String MY_RECORD_ID = "myPhoneRecord";
	
	@Override
	protected String getTableName() {
		return DB_TABLE;
	}
	
	@Override
	protected void putValues(ContentValues values) {
		values.put(COLUMN_PHONE, getPhone());
		values.put(COLUMN_MAC, getMac());
		values.put(COLUMN_PHONE_NAME, getPhone_name());
		values.put(COLUMN_OS, getOs());
		values.put(COLUMN_SDK, getSdk());
		values.put(COLUMN_MOTOSTAR_VERSION, getMotoVersion());
		values.put(COLUMN_SERVER_UPDATE_REQUIRED, getServerUpdateRequired());
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMac()
	{
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getPhone_name()
	{
		return phone_name;
	}

	public void setPhoneName(String phone_name) {
		this.phone_name = phone_name;
	}

	public String getOs()
	{
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public long getSdk()
	{
		return sdk;
	}

	public void setSdk(long sdk) {
		this.sdk = sdk;
	}

	public String getMotoVersion()
	{
		return moto_version;
	}

	public void setMotoVersion(String moto_version) {
		this.moto_version = moto_version;
	}

	public long getServerUpdateRequired()
	{
		return server_update_required;
	}

	public void setServerUpdateRequired(long server_update_required) {
		this.server_update_required = server_update_required;
	}


	/*
	@Override
	public String getTitleForMenu() {
		return getId() + "";
	}

	@Override
	public String getKeyForIntent() {
		return MY_RECORD_ID;
	}
	*/

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MyPhoneRecord))
			return false;
		MyPhoneRecord other = (MyPhoneRecord) obj;

		if (phone == null) {
			if (other.phone!=null)
				return false;
		} else if (!phone.equals(other.phone)) {
			return false;
		}

		if (mac == null) {
			if (other.mac!=null)
				return false;
		} else if (!mac.equals(other.mac)) {
			return false;
		}

		if (phone_name == null) {
			if (other.phone_name!=null)
				return false;
		} else if (!phone_name.equals(other.phone_name)) {
			return false;
		}

		if (os == null) {
			if (other.os!=null)
				return false;
		} else if (!os.equals(other.os)) {
			return false;
		}

		if (moto_version == null) {
			if (other.moto_version!=null)
				return false;
		} else if (!moto_version.equals(other.moto_version)) {
			return false;
		}

		if (sdk!=other.sdk) {
			return false;
		}

		if (server_update_required!=other.server_update_required) {
			return false;
		}

		return true;
	}
	public static void storeRecord(Context context, String phone,String mac, String phone_name, String os, long sdk, String moto_version, long server_update_required) {
		MyPhoneRecord req = new MyPhoneRecord();
		req.setPhone(phone);
		req.setMac(mac);
		req.setPhoneName(phone_name);
		req.setOs(os);
		req.setSdk(sdk);
		req.setMotoVersion(moto_version);
		req.setServerUpdateRequired(server_update_required);
		DataSource db = new DataSource(context);
		db.open();
		db.persistMyPhoneRecord(req);
		db.close();
	}

	public static void LoadAndUpdateMyPhoneRecord(SQLiteDatabase db, String phone, String mac, String os, long sdk, String moto_version, long server_update_required)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_MAC, mac);
		values.put(COLUMN_OS, os);
		values.put(COLUMN_SDK, sdk);
		values.put(COLUMN_MOTOSTAR_VERSION, moto_version);
		values.put(COLUMN_SERVER_UPDATE_REQUIRED, server_update_required);

		db.update(DB_TABLE, values, COLUMN_PHONE + " = ? ", new String[]{phone});
	}

	public static void LoadAndUpdateMyPhoneRecord(SQLiteDatabase db, String phone, long server_update_required)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_SERVER_UPDATE_REQUIRED, server_update_required);
		db.update(DB_TABLE, values, COLUMN_PHONE + " = ? ", new String[]{phone});
	}

	public static void clearFromDB(Context context)
	{
		DataSource db = new DataSource(context);
		db.open();
		List<MyPhoneRecord> reqs = db.getMyPhoneRecords();
		for(MyPhoneRecord _req : reqs) {
			db.deleteMyPhoneRecord(_req);
		}
		db.close();
	}
}
