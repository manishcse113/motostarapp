package com.bitskeytechnologies.www.motostar.fragments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 1/14/2017.
 */

public class PairedDevicesList extends ArrayAdapter {
    int groupid;

    private static List pairedDevicesList = new ArrayList();
    Context context;
    private final String FILENAME = "MS PairedDevicesList#";
    //private UserListCallback callback;
    //private UserListActivityCallback callbackActivity;

    Map<String, String> map;
    Map<String, String> treeMap;

    public PairedDevicesList(Context context, int textViewResourceId)
    {
        super(context, textViewResourceId);
    }

    public PairedDevicesList(Context context, int vg, List userObjList) {
        super(context, vg, userObjList);
        this.context = context;
        groupid = vg;
        this.pairedDevicesList.clear();
        this.pairedDevicesList.addAll(userObjList);
        sortPairedObjList();

        //readThread();
        //monitoringConnectivityThread();
    }

    public void sortPairedObjList() {
        //first sort based on displayname
        Collections.sort(pairedDevicesList, new Comparator<DeviceObject>() {
            @Override
            public int compare(final DeviceObject object1, final DeviceObject object2) {
                return object1.deviceAddress.compareTo(object2.deviceAddress);
            }
        });
    }

    //@Override
    public void add(DeviceObject object) {
        pairedDevicesList.add(object);
        super.add(object);

        // here - inform all - to update themselves with the new information.
    }

    public void update(DeviceObject object) {
        //search message based on unique id: receiptId and replace the contents.
        boolean updated = false;
        for (Object deviceObject : pairedDevicesList) {
            if (((DeviceObject) (deviceObject)).deviceAddress.equals(((DeviceObject) (object)).deviceAddress)) {
                //cop from object to the existing message: Ideally only receiptstatus shall change.
                //((DeviceObject) (userObject)).image = ((DeviceObject) (object)).image;
                //((UserObject)(userObject)).lastSeenInfo = ((UserObject)(object)).lastSeenInfo;
                updated = true;
                break;
            }
        }
        if (!updated) {
            add(object);
        }
        sortPairedObjList();
        //super.notify();
    }

    public void addUsersList(List objectList) {
        updateDevicesList(objectList);
    }

    public void updateDevicesList(List objectList)
    {
        //search message based on unique id: receiptId and replace the contents.
        for (Object userObject : objectList) {
            update((DeviceObject) (userObject));
        }
        //sortPairedObjList();
        //super.notify();
    }

    public void clear() {
        pairedDevicesList.clear();
        super.clear();
    }

    public void delete(DeviceObject object) {
        for (Object deviceObject : pairedDevicesList) {
            if (((DeviceObject) (deviceObject)).deviceAddress.equals(((DeviceObject) (object)).deviceAddress)) {
                pairedDevicesList.remove(deviceObject);
                //userObjList.
                super.remove(object);
                break;
            }
        }
    }

    //@Override
    public void delete(int position) {
        int index = position;
        pairedDevicesList.remove(index);
        super.remove(index);
    }

    public int getCount() {
        return this.pairedDevicesList.size();
    }

    public DeviceObject getItem(int index) {
        DeviceObject deviceObject = (DeviceObject) pairedDevicesList.get(index);
        return deviceObject;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View itemView = inflater.inflate(groupid, parent, false);
        View fragment_list_view = inflater.inflate(R.layout.fragment_connections, null);
        //Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(R.layout.paired_devices_list, (ViewGroup) fragment_list_view, false);
            populateUserListLayout(inflater, row, position);
        } else {
            //You get here in case of scroll
            populateUserListLayout(inflater, row, position);
        }
        //((ViewGroup) fragment_list_view).addView(row);
        return row;
    }

    public View populateUserListLayout(LayoutInflater inflater, final View user_view, final int position) {
        //Log.d(FILENAME, "inflateUserListLayout");
        try {
            TextView textViewDeviceName = (TextView) user_view.findViewById(R.id.devicename);
            String deviceNAme = textViewDeviceName.getText().toString();

            DeviceObject uObj = (DeviceObject) (pairedDevicesList.get(position));
            textViewDeviceName.setText((uObj.deviceName + ":::" + uObj.deviceAddress));

            final TextView deviceLocalnameTextV = (TextView) user_view.findViewById(R.id.deviceLocalname);
            String deviceLocalName = MainActivity.getDeviceLocalNameFromMac(uObj.deviceAddress);
            if (deviceLocalName!=null)
            {
                deviceLocalnameTextV.setText(deviceLocalName);
            }

            ImageView nextview = (ImageView) user_view.findViewById(R.id.nextView);


            nextview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent myIntent = new Intent(getContext(), RemoteUserProfileActivity.class);
                    TextView textDeviceName = (TextView) user_view.findViewById(R.id.devicename);
                    String deviceName = textDeviceName.getText().toString();

                    /*
                    //deviceLocalname.getText()
                    //EditText cc_ev = (EditText)rootView.findViewById(R.id.cc_ev);
                    String deviceLName = null;
                    boolean isValidInput = true;
                    if(deviceLocalnameEditV != null)
                    {
                        deviceLName = deviceLocalnameEditV.getText().toString();
                    }
                    if( (deviceLName.length() <= 0) || (deviceLName.length() >= 10) )
                    {
                        isValidInput = false;
                    }
                    else if ( (deviceLName.charAt(0)>='a' && deviceLName.charAt(0)<='z' ) || (deviceLName.charAt(0)>='A' && deviceLName.charAt(0)<='Z' ) )
                    {
                        isValidInput = true;
                    }
                    if (isValidInput == true) {
                        DeviceObject dvObj = (DeviceObject) (pairedDevicesList.get(position));
                        dvObj.setDeviceName (deviceLName);
                        callback.nextView((DeviceObject) (pairedDevicesList.get(position)));
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Error:Please enter valid name!");
                        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
                        dialog.show();
                    }
                    */
                    DeviceObject dvObj = (DeviceObject) (pairedDevicesList.get(position));
                    callback.nextView((DeviceObject) (pairedDevicesList.get(position)));
                }
            });

            /*
            ImageView renameView = (ImageView) user_view.findViewById(R.id.renameButton);
            renameView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent myIntent = new Intent(getContext(), RemoteUserProfileActivity.class);
                    TextView textDeviceName = (TextView) user_view.findViewById(R.id.devicename);

                    String deviceName = textDeviceName.getText().toString();
                    //save it permanently as mapping: Later

                    //startRemoteProfileActivity(context, userName, userDisplayName);
                    //callbackActivity.onClickForUserAction(userName,userDisplayName, isItARoom);
                }
            });
            */
            textViewDeviceName.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Log.d(FILENAME, "onClickListener");
                    String deviceName = ((TextView) (arg0)).getText().toString();
                    return;
                }
            });
            textViewDeviceName.setSelected(true);
        } catch (Exception e) {
            Log.d(FILENAME, " Exception " + e.getMessage());
        }
        return user_view;
    }

    private FragmentConnections callback;
    //private ScanDevicesListInterface callbackActivity;

    public void setCallback(FragmentConnections callback) {
        Log.d(FILENAME, "setCallback");
        this.callback = callback;
    }

    /*
    public void setScanDevlicesListCallback(FragmentConnections callback) {
        Log.d(FILENAME, "setScanDevlicesListCallback");
        this.callbackActivity = callback;
    }
    */

    public interface PairedDevicesListInterface
    {
        public void unPair(DeviceObject device);
        public void nextView (DeviceObject device);
    }

}