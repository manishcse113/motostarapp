package com.bitskeytechnologies.www.motostar.Chat.RestAPI;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.bitskeytechnologies.www.motostar.Init.AppGeneralSettings;
import com.bitskeytechnologies.www.motostar.Chat.gen.ChatXMPPService;
import com.bitskeytechnologies.www.motostar.Init.Utility;
import com.bitskeytechnologies.www.motostar.Chat.login.LoginBackgroundNoGUI;
//import com.bitskeys.track.profile.ProfileInfo;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Manish on 12/25/2015.
 */
public class OpenfireRestAPI {

    static String userName;
    static String passWord;
    static String displayName;
    static enum ChatRoomRoles{
         ROOM_OWNER,
        ROOM_ADMIN,
        ROOM_MEMBERS,
        ROOM_OUTCASTS
    };

    //static ProfileInfo pInfo = null;
    static LoginBackgroundNoGUI loginBackgroundNoGUI = null;
    static public byte[] tempAvatarByte = null;

    public static String getDisplayName() {
        return displayName;
    }

    public static void setDisplayName(String displayName) {
        OpenfireRestAPI.displayName = displayName;
    }

    public static void instantiateLoginBgNoGUI(Context appCtxt,String userName,String passWord,LoginBackgroundNoGUI.LoginProcessCallback mCallback)
    {
        if(loginBackgroundNoGUI == null) {
            loginBackgroundNoGUI = new LoginBackgroundNoGUI(userName, passWord, appCtxt, mCallback);
            loginBackgroundNoGUI.execute();
        }
    }

    /*
    public static boolean SaveProfileInfoOnServer(ProfileInfo pInfo)
    {
        if(loginBackgroundNoGUI != null) {
            loginBackgroundNoGUI.CreateAndSaveProfileOnServer(pInfo);
            return true;
        }
        return false;
    }

    //static com.bitskeys.track.login. lbGUI = null;


    public static ProfileInfo getProfileInfo()
    {
        return pInfo;
    }

    public static void setProfileInfo(ProfileInfo info)
    {
        pInfo = info;
    }
    */

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        OpenfireRestAPI.userName = userName;
    }

    public static String getPassWord() {
        return passWord;
    }

    public static void setPassWord(String passWord) {
        OpenfireRestAPI.passWord = passWord;
    }

    public interface UpdateUserCallback
    {
        public void onSuccess();
        public void onFailure(int statusCode);
    }

    public interface RegisterUserCallback
    {
        public void onSuccess();
        public void onFailure(int statusCode);
        public String getUserName();
        public String getPassword();
        public String getEmailId();
        public String getUserDisplayName();
        public String getGroupName();
    }

    public interface UserAddToGroupCallback
    {
        public void UAGonSuccess();
        public void UAGonFailure(int statusCode);
        public String getUserName();
        public String getPassword();
        public String getEmailId();
        public String getUserDisplayName();
        public String getGroupName();
    }

    public interface CreateGroupCallback
    {
        public void createGroupOnSuccess(String groupName);
        public void createGroupOnFailure(int statusCode, String goupName);
        //public String getUserName();
        //public String getPassword();
        //public String getEmailId();
        //public String getUserDisplayName();
        //public String getGroupName();
    }


    public interface UserQueryCallBack
    {
        public void UserQuerySuccess();
        public void UserQueryFailure(int statuscode);
    }

    public interface ChatRoomCreateCallback
    {
        public void onCreateRoomSuccess();
        public void onCreateRoomFailure(int statusCode);
    }

    public interface ChatRoomUserAddCallback
    {
        public void onUserAddedSuccess();
        public void onUserAddedFailure(int statusCode);
    }



    static final String FILENAME = "GBC: RegisterActivity#";


    static public void addUserToGroup(Context ctxt, String groupName, final UserAddToGroupCallback mCallback)
    {
        String UserName=mCallback.getUserName();


//                String name = "pulkit";
//        for (int i = 20; i > name.length(); i--) {
//
//            setUserName(name + i);
//
//        }

        JSONObject jsonGrp = new JSONObject();
        String group = ChatXMPPService.GROUPNAME;
        try {
            jsonGrp.put("groupname", groupName);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        addToGroup(jsonGrp.toString(), UserName, groupName, ctxt, mCallback);
    }

    static public boolean getUser(Context ctxt,String userName, final UserQueryCallBack mCallback)
    {
        boolean result=false;
        if(mCallback == null)
            return result;

        JSONObject json = new JSONObject();
        /* Initialize String Entity */
        StringEntity se = null;
        try {
            se = new StringEntity(json.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return result;
        }

        String URL = "http://" + ChatXMPPService.DOMAIN + ":9090/plugins/restapi/v1/users/" + userName;

        AsyncHttpClient client = initializeClient();
        client.get(ctxt,URL,new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody)
            {
                mCallback.UserQuerySuccess();
                Log.w("GET USER", "EXISTS");
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                mCallback.UserQueryFailure(statusCode);
                Log.w("GET USER", "EXISTS");

            }

        });
        //client.put(ctxt,url,se,)
        return  result;
    }

//    //POST /chatrooms/{roomName}/{roles}/{name}
//    static public boolean addUserToChatRoom(Context ctxt,
//                                            String chatRoomName,
//                                            String userName,
//                                            ChatRoomRoles role,
//                                            final ChatRoomUserAddCallback mCallback)
//    {
//        boolean result=false;
//        if(mCallback == null || chatRoomName == null||(chatRoomName.length() ==0) )
//            return result;
//
//
//        JSONObject json = new JSONObject();
////        try
////        {
////            if((chatRoomNaturalName != null) &&(chatRoomNaturalName.length()>0))
////            {
////                json.put("naturalName",chatRoomNaturalName);
////            }
////            if((chatRoomName != null) && (chatRoomName.length()>0))
////            {
////                json.put("roomName",chatRoomName);
////            }
////
////            if((description != null) && (description.length()>0))
////            {
////                json.put("description",description);
////            }
////
////        }
////        catch (JSONException e)
////        {
////            e.printStackTrace();
////            return result;
////        }
//
//        /* Initialize String Entity */
//        StringEntity se = null;
//        try {
//            se = new StringEntity(json.toString());
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            return result;
//        }
//
//        String URL = "http://" + ChatXMPPService.DOMAIN + ":9090/plugins/restapi/v1/users/chatrooms/"+chatRoomName + "/";
//        if(role == ChatRoomRoles.ROOM_ADMIN)
//        {
//            URL = URL + "admins/" + userName;
//        }else if(role == ChatRoomRoles.ROOM_OWNER)
//        {
//            URL = URL + "owners/" + userName;
//        }else if(role == ChatRoomRoles.ROOM_MEMBERS)
//        {
//            URL = URL + "members/" + userName;
//        }else if(role == ChatRoomRoles.ROOM_OUTCASTS)
//        {
//            URL = URL + "outcasts/" + userName;
//        }
//
//        AsyncHttpClient client = initializeClient();
//
//        client.post(ctxt, URL,se, new AsyncHttpResponseHandler() {
//
//            @Override
//            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
//
//                mCallback.onUserAddedSuccess();
//            }
//
//            // When the response returned by REST has Http response code other than '200'
//            @Override
//            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
//                    error) {
//                mCallback.onUserAddedFailure(statusCode);
//
//            }
//
//        });
//        //client.put(ctxt,url,se,)
//        return  result;
//    }

    /*
    static public boolean createChatRoom(Context ctxt,
                                         String chatRoomNaturalName ,
                                         String chatRoomName,
                                         String description,
                                         final ChatRoomCreateCallback mCallback)
    {
        boolean result=false;
        if(mCallback == null || chatRoomName == null||(chatRoomName.length() ==0) )
            return result;


        JSONObject json = new JSONObject();
        try
        {
            if((chatRoomNaturalName != null) &&(chatRoomNaturalName.length()>0))
            {
                json.put("naturalName",chatRoomNaturalName);
            }
            if((chatRoomName != null) && (chatRoomName.length()>0))
            {
                json.put("roomName",chatRoomName);
            }

            if((description != null) && (description.length()>0))
            {
                json.put("description",description);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return result;
        }

        StringEntity se = null;
        try {
            se = new StringEntity(json.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return result;
        }

        String URL = "http://" + ChatXMPPService.DOMAIN + ":9090/plugins/restapi/v1/users/chatrooms";

        AsyncHttpClient client = initializeClient();

        client.post(ctxt, URL, se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {

                mCallback.onCreateRoomSuccess();
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                mCallback.onCreateRoomFailure(statusCode);

            }

        });
        //client.put(ctxt,url,se,)
        return  result;
    }

    static public boolean updateUser(Context ctxt,ProfileInfo pInfo , final UpdateUserCallback mCallback)
    {
        boolean result=false;
        if(pInfo == null)
            return  result;

        String UserName = OpenfireRestAPI.getUserName();
        String Password=OpenfireRestAPI.getPassWord();
        String Email = pInfo.getEmailIdHome();
       // String GroupName = pInfo.getGroupName();
        String NickName = pInfo.getNickName();

        //Log.d(FILENAME, "Registering User=> name#" + UserName + "email#" + Email + "password#" + Password);

        JSONObject json = new JSONObject();
        try
        {
            if((UserName != null) &&(UserName.length()>0))
            {
                json.put("username",UserName);
            }
            if((Password != null) && (Password.length()>0))
            {
                json.put("password",Password);
            }

            if((Email != null) && (Email.length()>0))
            {
                json.put("email",Email);
            }

            if((NickName != null) && (NickName.length()>0))
            {
                json.put("name",NickName);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return result;
        }

        StringEntity se = null;
        try {
            se = new StringEntity(json.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return result;
        }

        String URL = "http://" + ChatXMPPService.DOMAIN + ":9090/plugins/restapi/v1/users/" + UserName;

        AsyncHttpClient client = initializeClient();

        client.put(ctxt, URL, se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {

                mCallback.onSuccess();
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                mCallback.onFailure(statusCode);

            }

        });
        //client.put(ctxt,url,se,)
        return  result;
    }
    */

    static public void registerUser(Context ctxt,final RegisterUserCallback mCallback) {

        String UserName=mCallback.getUserName();
        String Password=mCallback.getPassword();
        //String UserDisplayName = mCallback.getUserDisplayName();
        String Email = mCallback.getEmailId();
        //String GroupName = mCallback.getGroupName();

        Log.d(FILENAME, "Registering User=> name#" + UserName + "email#" + Email + "password#" + Password);
        RequestParams params = new RequestParams();

        //if (Utility.isNotNull(UserName) && Utility.isNotNull(Email) && Utility.isNotNull(Password)) {
        if (Utility.isNotNull(UserName) && Utility.isNotNull(Password)) {
            JSONObject json = new JSONObject();
            try {

                json.put("username",UserName);
                json.put("password", Password);

            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
            //invokeCreateUser(name, name, password, email);
            invokeWS2(json.toString(), ctxt, mCallback);

        }
        else
        {
            Toast.makeText(ctxt, "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }

    }


    static private AsyncHttpClient initializeClient()
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.removeHeader("Content-Type");
        /* Add Secret Key */
        client.addHeader("Authorization", restApiAutherizationKey);
        client.addHeader("Accept", "application/json");
        return client;
    }
    // public void invokeWS(RequestParams params) {

    public static String restApiAutherizationKey = "P2ydi0zr72rTL3lw";
    static public void invokeWS2(String jsonString,final Context ctxt,final RegisterUserCallback mCallback) {
        String st = null;

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        //client.addHeader("Authorization", "664Rk33SFjfs2m7R");
        client.addHeader("Authorization", restApiAutherizationKey);
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Accept", "application/json");
        //client.setBasicAuth("manish", "nswdel10");

        //Log.d(FILENAME, "invokeWS2");

        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
              return;
        }
        //client.post(this.getApplicationContext(), "http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", se, "application/json", new AsyncHttpResponseHandler() {
        client.post(ctxt, "http://" + ChatXMPPService.DOMAIN + ":9090/plugins/restapi/v1/users", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {

                try {
                    // JSON Object
                    HashMap<String, String> hmap = new HashMap<String, String>();
                    for (org.apache.http.Header h : headers) {
                        hmap.put(h.getName(), h.getValue());
                        if (h.getName().equals("Content-Length")) {
                            if (h.getValue().equals("0")) {
                                mCallback.onSuccess();
                                Toast.makeText(ctxt, "You are successfully registered!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    //JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    //if (obj.getBoolean("status")) {
                    if (statusCode==200)
                    {
                        mCallback.onSuccess();
                        Toast.makeText(ctxt, "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        //Toast.makeText(ctxt, obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(ctxt, "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    mCallback.onFailure(statusCode);
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                Log.d(FILENAME, "http onFailure");
                mCallback.onFailure(statusCode);

                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(ctxt, "Failed:404" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '404'
                else if (statusCode == 409) {

                    // Neeraj: This is not an error, it will happen when username already exists.
                    Toast.makeText(ctxt, "User[" + mCallback.getUserName() + "] Already exists", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(ctxt, "Failed:500" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    String toastMsg="Failed:Unexpected Error occcured! ";
                    if(error != null)
                        toastMsg += error.getMessage();
                    else
                        toastMsg += "error message(NULL)";
                    toastMsg += ": [Check Internet availability] ";
                    if(responseBody != null)
                        toastMsg += responseBody.toString();

                    Toast.makeText(ctxt, toastMsg, Toast.LENGTH_LONG).show();
                }
            }
        });



    }

    static public void addToGroup(String jsonString, String userName, String groupName, final Context ctxt,final UserAddToGroupCallback mCallBack) {
        String st = null;
        // Show Progress Dialog

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        //String  contentType = "string/xml;UTF-8";
        client.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", restApiAutherizationKey);
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Accept", "application/json");
        //client.setBasicAuth("manish", "nswdel10");

        String userUrl = "http://" + ChatXMPPService.DOMAIN + ":9090/plugins/restapi/v1/users";
        String grpUrl = userUrl + "/" + userName + "/groups";
        Log.d(FILENAME, "addToGroup");

        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        client.post(ctxt,grpUrl, se, "application/json",new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {

                Log.d(FILENAME, "http onSuccess: " + responseBody);
                if( statusCode >= 200 || statusCode == 0)
                {
                    mCallBack.UAGonSuccess();
                }

                try {
                    // JSON Object
                    HashMap<String,String> hmap=new HashMap<String,String>();
                    for (  org.apache.http.Header h : headers) {
                        hmap.put(h.getName(),h.getValue());
                        if (h.getName().equals("Content-Length"))
                        {
                            if (h.getValue().equals("0"))
                            {
                                Toast.makeText(ctxt, "You are successfully added to group!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    //JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    //if (obj.getBoolean("status"))
                    if (statusCode==200)
                    {
                        // Display successfully registered message using Toast
                        Toast.makeText(ctxt, "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        //Toast.makeText(ctxt, obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                        Toast.makeText(ctxt, "Registration Failure", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    // Neeraj: I checked even code reached this state, when one group had already been added, this is not an error.
                    Toast.makeText(ctxt, "UserAddedToGroup [User is already have one Group]: Success!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                Log.d(FILENAME, "http onFailure");

                if(AppGeneralSettings.isDebugEnabled())
                {
                    String respBody = "";
                    String errorMsg = "";
                    String prefixMsg = "Failed:" + String.valueOf(statusCode);
                    if(responseBody != null)
                        respBody = responseBody.toString();

                    if(error != null)
                        errorMsg = error.getMessage();

                    Toast.makeText(ctxt, prefixMsg + errorMsg + ":" + respBody, Toast.LENGTH_LONG).show();
                }
                mCallBack.UAGonFailure(statusCode);
            }
        });
    }


    public static void createGroup(String groupName, Context ctxt, CreateGroupCallback createGroupCallback)
    {
        JSONObject json = new JSONObject();
        try {

            json.put("name",groupName); //mac-address
            json.put("description", "MotoStar"); //device type

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        //invokeCreateUser(name, name, password, email);
        createGroup(json.toString(), groupName, ctxt, createGroupCallback);

    }

    static public void createGroup(String jsonString, final String groupName, final Context ctxt,final CreateGroupCallback createGroupCallback) {
        String st = null;
        // Show Progress Dialog

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        //String  contentType = "string/xml;UTF-8";
        client.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", restApiAutherizationKey);
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Accept", "application/json");
        //client.setBasicAuth("manish", "nswdel10");

        String grpUrl = "http://" + ChatXMPPService.DOMAIN + ":9090/plugins/restapi/v1/groups";
        //String grpUrl = userUrl + "/" + userName + "/groups";
        Log.d(FILENAME, "createGroup");

        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        client.post(ctxt,grpUrl, se, "application/json",new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {

                Log.d(FILENAME, "http onSuccess: " + responseBody);
                if( statusCode >= 200 || statusCode == 0)
                {
                    if (createGroupCallback!=null) {
                        createGroupCallback.createGroupOnSuccess(groupName);
                    }
                }

                try {
                    // JSON Object
                    HashMap<String,String> hmap=new HashMap<String,String>();
                    for (  org.apache.http.Header h : headers) {
                        hmap.put(h.getName(),h.getValue());
                        if (h.getName().equals("Content-Length"))
                        {
                            if (h.getValue().equals("0"))
                            {
                                Toast.makeText(ctxt, "You are successfully added to group!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    //JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    //if (obj.getBoolean("status"))
                    if (statusCode==200)
                    {
                        // Display successfully registered message using Toast
                        Toast.makeText(ctxt, "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        //Toast.makeText(ctxt, obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                        Toast.makeText(ctxt, "Registration Failure", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    // Neeraj: I checked even code reached this state, when one group had already been added, this is not an error.
                    Toast.makeText(ctxt, "UserAddedToGroup [User is already have one Group]: Success!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                Log.d(FILENAME, "http onFailure");

                if(AppGeneralSettings.isDebugEnabled())
                {
                    String respBody = "";
                    String errorMsg = "";
                    String prefixMsg = "Failed:" + String.valueOf(statusCode);
                    if(responseBody != null)
                        respBody = responseBody.toString();

                    if(error != null)
                        errorMsg = error.getMessage();

                    Toast.makeText(ctxt, prefixMsg + errorMsg + ":" + respBody, Toast.LENGTH_LONG).show();
                }
                if (createGroupCallback!=null) {
                    createGroupCallback.createGroupOnFailure(statusCode, groupName);
                }
                //createGroupCallback.UAGonFailure(statusCode);
            }
        });


    }



    /// Code below this is not used for now///////////////////////

    /**
     * Method that performs RESTful webservice invocations
     *
         */
    public void invokeWS(String jsonString,final Context ctxt) {
        String st = null;

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();

        //String  contentType = "string/xml;UTF-8";
        client.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", "664Rk33SFjfs2m7R");
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Accept", "application/json");

        StringEntity se = null;

        try {
            //se = new StringEntity(params.toString());
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        client.post(ctxt,"http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", se, "application/json",new AsyncHttpResponseHandler() {
            //client.post("http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", params, new AsyncHttpResponseHandler() {

            //client.post(this.getBaseContext(), "http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", httpEntity, params.toString(), "application/json",new AsyncHttpResponseHandler() {
            //client.get("http://192.168.2.2:9999/useraccount/register/doregister",params ,new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {

                Log.d("HTTP", "onSuccess: " + responseBody);

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
                        // Display successfully registered message using Toast
                        Toast.makeText(ctxt, "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        Toast.makeText(ctxt, obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(ctxt, "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                Log.d("HTTP", "onFailure" + statusCode);

                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(ctxt, "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(ctxt, "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(ctxt, "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }



    /**
     * Method which navigates from Register Activity to Login Activity
     */
}
