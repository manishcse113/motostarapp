package com.bitskeytechnologies.www.motostar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

public class DevicesLocalNameRecords extends DBObject
{
	private static String FILENAME = "DevicesLocalNameRecords";
	private final static String DB_TABLE = "DevicesLocalNameRecords";

	private final static String COLUMN_DEVICE_NAME = "device_local_name";
	private final static String COLUMN_MAC = "mac_address";


	private final static String[] ALL_COLUMNS = new String[] {
		COLUMN_ID,
			COLUMN_DEVICE_NAME,
			COLUMN_MAC
		/*
		COLUMN_PHONE_NUMBER,
		COLUMN_VALIDATION_CODE_1,
		COLUMN_VALIDATION_CODE_2,
		COLUMN_CREATION_TIMESTAMP,
		COLUMN_AUTH_PASSWORD
		*/
	};

	private final static String CREATE_TABLE =
			"CREATE TABLE " + DB_TABLE + " (" +
			COLUMN_ID + " integer, " +
					COLUMN_DEVICE_NAME  + " text, " +
					COLUMN_MAC + " text primary key " +


					/*
			COLUMN_PHONE_NUMBER + " text, " +
			COLUMN_VALIDATION_CODE_1 + " text, " +
			COLUMN_VALIDATION_CODE_2 + " text, " +
			COLUMN_CREATION_TIMESTAMP + " integer not null, " +
			COLUMN_AUTH_PASSWORD + " text not null	*/
			");";

	String deviceName = null;
	String mac = null;


	/*
	String phoneNumber = null;
	String validationCode1 = null;
	String validationCode2 = null;
	long creationTimestamp = 0;
	String authPassword = null;
	*/

	public static void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	public static void onUpgrade(SQLiteDatabase db, int oldVersion,
			int newVersion) {
		if(oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
			onCreate(db);
		}
	}

	public DevicesLocalNameRecords() {
		//setCreationTimestamp(System.currentTimeMillis() / 1000);
	}

	public static List<DevicesLocalNameRecords> loadFromDatabase(SQLiteDatabase db) {
		Vector<DevicesLocalNameRecords> v = new Vector<DevicesLocalNameRecords>();

		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, null,
				null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}

	public static DevicesLocalNameRecords loadFromDatabase(SQLiteDatabase db, long id) {
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_ID + " = " + id,
				null, null, null, null);
		cursor.moveToFirst();
		DevicesLocalNameRecords req = null;
		if(!cursor.isAfterLast())
			 req = fromCursor(cursor);
		cursor.close();
		return req;
	}


	public static List<DevicesLocalNameRecords> loadFromDatabase(SQLiteDatabase db, String deviceName) {
		Vector<DevicesLocalNameRecords> v = new Vector<DevicesLocalNameRecords>();
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_DEVICE_NAME +  " =?", new String[]{deviceName},
				null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	private static DevicesLocalNameRecords fromCursor(Cursor cursor) {
		DevicesLocalNameRecords req = new DevicesLocalNameRecords();
		int i = 0;
		req.setId(cursor.getLong(i++));
		req.setDeviceName(cursor.getString(i++));
		req.setMac(cursor.getString(i++));


		return req;
	}

	public final static String DEVICE_RECORD_ID = "deviceLocalNameRecord";

	@Override
	protected String getTableName() {
		return DB_TABLE;
	}

	@Override
	protected void putValues(ContentValues values) {
		values.put(COLUMN_DEVICE_NAME, getDeviceName());
		values.put(COLUMN_MAC, getMac());

		/*
		values.put(COLUMN_PHONE_NUMBER, getPhoneNumber());
		values.put(COLUMN_VALIDATION_CODE_1, getValidationCode1());
		values.put(COLUMN_VALIDATION_CODE_2, getValidationCode2());
		values.put(COLUMN_CREATION_TIMESTAMP, getCreationTimestamp());
		values.put(COLUMN_AUTH_PASSWORD, getAuthPassword());
		*/
	}



	public String getMac()
	{
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getDevoceLocalName() {
		return deviceName;
	}

	public void setDeviceLocalName(String deviceName) {
		this.deviceName = deviceName;
	}

	/*
	public void setServerUpdateRequired(Long serverUpdateRequired)
	{
		this.serverUpdateRequired = serverUpdateRequired;
	}
	*/


	/*
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getValidationCode1() {
		return validationCode1;
	}

	public void setValidationCode1(String validationCode1) {
		this.validationCode1 = validationCode1;
	}

	public String getValidationCode2() {
		return validationCode2;
	}

	public void setValidationCode2(String validationCode2) {
		this.validationCode2 = validationCode2;
	}

	public long getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(long creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getAuthPassword() {
		return authPassword;
	}

	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}
	*/

	/*
	@Override
	public String getTitleForMenu() {
		return getId() + "";
	}

	@Override
	public String getKeyForIntent() {
		return DEVICE_RECORD_ID;
	}
	*/

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		/*
		result = prime * result
				+ ((authPassword == null) ? 0 : authPassword.hashCode());
		result = prime * result
				+ (int) (creationTimestamp ^ (creationTimestamp >>> 32));
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result
				+ ((validationCode1 == null) ? 0 : validationCode1.hashCode());
		result = prime * result
				+ ((validationCode2 == null) ? 0 : validationCode2.hashCode());
		*/
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof DevicesLocalNameRecords))
			return false;
		DevicesLocalNameRecords other = (DevicesLocalNameRecords) obj;

		if (deviceName == null) {
			if (other.deviceName!=null)
				return false;
		} else if (!deviceName.equals(other.deviceName)) {
			return false;
		}

		if (mac == null) {
			if (other.mac!=null)
				return false;
		} else if (!mac.equals(other.mac)) {
			return false;
		}
		return true;
	}


	public static void storeUniqueNameRecordNew(Context context, String deviceName, String mac
			)
	{

		DataSource db = new DataSource(context);
		try {
			db.open();
			ContentValues data=new ContentValues();
			data.put(COLUMN_ID,0); //putting ID as 0 for now:
			data.put(COLUMN_DEVICE_NAME, deviceName);
			data.put(COLUMN_MAC,mac);

			//db.db.update(DB_TABLE, data, COLUMN_USER_NAME + " like " + "'" + userName + "'", null);
			String sqlQuery =
			"insert or replace into "+DB_TABLE +
							" (" +
								COLUMN_ID + "," +
								COLUMN_DEVICE_NAME + "," +
								COLUMN_MAC +

							") values " +
							"(" +
								"( select " + COLUMN_ID + " from " + DB_TABLE + " where " + COLUMN_MAC + " = " + "'" + mac + "'" + ")," +
								"'" + deviceName +  "'" + "," +
								"'" + mac +  "'" +
							");";
			db.db.execSQL(sqlQuery);
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
			db.close();
		}
	}

	public static void storeUniqueNameRecordNew(Context context, String deviceName, String mac, String paired_mode, Long state, Long serverUpdate)
	{

		DataSource db = new DataSource(context);
		try {
			db.open();
			ContentValues data=new ContentValues();
			data.put(COLUMN_ID,0); //putting ID as 0 for now:
			data.put(COLUMN_DEVICE_NAME, deviceName);
			data.put(COLUMN_MAC,mac);



			//db.db.update(DB_TABLE, data, COLUMN_USER_NAME + " like " + "'" + userName + "'", null);
			String sqlQuery =
					"insert or replace into "+DB_TABLE +
							" (" +
							COLUMN_ID + "," +
							COLUMN_DEVICE_NAME + "," +
							COLUMN_MAC +
							") values " +
							"(" +
							"( select " + COLUMN_ID + " from " + DB_TABLE + " where " + COLUMN_MAC + " = " + "'" + mac + "'" + ")," +
							"'" + deviceName +  "'" + "," +
							"'" + mac +  "'" +
							");";
			db.db.execSQL(sqlQuery);
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
			db.close();
		}
	}

	public static void LoadAndUpdateDeviceRecord(SQLiteDatabase db,
						String mac, String deviceLocalName)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_MAC, mac);
		//values.put(COLUMN_SERVER_UPDATE_REQUIRED, server_update_required);

		values.put(COLUMN_DEVICE_NAME,deviceLocalName);

		db.update(DB_TABLE, values, COLUMN_MAC + " = ? ", new String[]{mac});
	}

	/*
	public static void LoadAndUpdateDeviceRecord(SQLiteDatabase db,
												 String mac, long server_update_required)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_MAC, mac);
		values.put(COLUMN_SERVER_UPDATE_REQUIRED, server_update_required);
		db.update(DB_TABLE, values, COLUMN_MAC + " = ? ", new String[]{mac});
	}
	*/



	public static void storeNameRecord(Context context, String deviceName,String mac) {
		DevicesLocalNameRecords req = new DevicesLocalNameRecords();
		req.setDeviceName(deviceName);
		req.setMac(mac);


		DataSource db = new DataSource(context);

		try {
			db.open();
			db.persistDeviceLocalNameRecord(req);
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
		}
	}

	public static void storeUniqueNameRecord(Context context, String deviceName,String mac) {
		DevicesLocalNameRecords req = new DevicesLocalNameRecords();
		req.setDeviceName(deviceName);
		req.setMac(mac);


		DataSource db = new DataSource(context);

		try {
			db.open();

			boolean recordExists = false;
			List<DevicesLocalNameRecords> reqs = db.getDeviceLocalNameRecords(deviceName);
			for (DevicesLocalNameRecords _req : reqs) {
				if (_req.getDeviceName().equalsIgnoreCase(deviceName) == true) {
					recordExists = true;
					break;
				}
			}
			if (recordExists == false) {
				db.persistDeviceLocalNameRecord(req);
			}
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
			db.close();
		}
	}

	public static void clearFromDB(Context context, String deviceName)
	{
		DataSource db = new DataSource(context);
		db.open();
		List<DevicesLocalNameRecords> reqs = db.getDeviceLocalNameRecords(deviceName);
		for(DevicesLocalNameRecords _req : reqs) {
			db.deleteDeviceLocalNameRecord(_req);
		}
		db.close();
	}

	public static void clearFromDB(Context context)
	{
		DataSource db = new DataSource(context);
		db.open();
		List<DevicesLocalNameRecords> reqs = db.getDeviceLocalNameRecords();
		for(DevicesLocalNameRecords _req : reqs) {
			db.deleteDeviceLocalNameRecord(_req);
			//deleteDeviceRecord(db.db);
		}
		db.close();
	}

	public static void deleteDeviceRecord(SQLiteDatabase db) {
		//if(getId() != -1) {
			db.execSQL("DELETE FROM " + DB_TABLE +
					";");
		//}
		//setId(-1);
	}
}
