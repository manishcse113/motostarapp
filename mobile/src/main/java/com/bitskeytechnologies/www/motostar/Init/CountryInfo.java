package com.bitskeytechnologies.www.motostar.Init;

/**
 * Created by Manish on 10-03-2017.
 */
public class CountryInfo {
    String CountryName;
    String CountryCode;
    String isoCodeTwoDigits;
    String isoCodeThreeDigits;

    public CountryInfo(String countryName, String countryCode, String isoCodeTwoDigits, String isoCodeThreeDigits) {
        CountryName = countryName;
        CountryCode = countryCode;
        this.isoCodeTwoDigits = isoCodeTwoDigits;
        this.isoCodeThreeDigits = isoCodeThreeDigits;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getIsoCodeTwoDigits() {
        return isoCodeTwoDigits;
    }

    public void setIsoCodeTwoDigits(String isoCodeTwoDigits) {
        this.isoCodeTwoDigits = isoCodeTwoDigits;
    }

    public String getIsoCodeThreeDigits() {
        return isoCodeThreeDigits;
    }

    public void setIsoCodeThreeDigits(String isoCodeThreeDigits) {
        this.isoCodeThreeDigits = isoCodeThreeDigits;
    }
}
