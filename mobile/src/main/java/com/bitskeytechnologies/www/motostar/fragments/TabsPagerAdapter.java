package com.bitskeytechnologies.www.motostar.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bitskeytechnologies.www.motostar.MainActivity;


//public class TabsPagerAdapter extends FragmentPagerAdapter {

public class TabsPagerAdapter extends FragmentStatePagerAdapter
{
    static String[] tabTitles = new String[] { "Control", "Profile" ,"Events Live", "Events Last", "Alarms All", "Alarms Active" };
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount()
    {
        //return tabTitles.length;
        return 6;//PairedDevicesList.pairedDevicesList.size() +3;
    }

    public boolean isTabsPagerAdapterAdded = false;
    public boolean IsTabsPagerAdapterAdded()
    {
        return isTabsPagerAdapterAdded;
    }

    Fragment fg = null;
    int fgNumber = -1;
    public int getCurrentFragmentNumber()
    {
        return fgNumber;
    }

    public void setTabsPagerAdapterAdded (boolean flag)
    {
        isTabsPagerAdapterAdded = flag;
    }

    @Override
    public Fragment getItem(int position)

    {
        //if (position==0)
            //return MainActivity.mConnectionsFrag;
          //  return MainActivity.mControlsFrag;
     //   if(MainActivity.selectedTank == 0)
            {
                 if (position == 0) {
                     fg = MainActivity.mControlsFrag;
                     fgNumber = 0;
                 }
                 else if (position == 1)
                 {
                     fg = MainActivity.mProfileFrag;
                     fgNumber = 1;
                 }
                 else if (position==2)
                 {
                     fg = MainActivity.mAnalyticsFrag;
                     fgNumber = 2;
                 }
                 else if (position==3)
                 {
                     fg = MainActivity.mLastEventsFrag;
                     fgNumber =3;
                 }
                 else if (position==4)
                 {
                     fg = MainActivity.mAlarmsFrag;
                     fgNumber =4;
                 }
                 else if (position==5)
                 {
                     fg = MainActivity.mLastAlarmsFrag;
                     fgNumber =5;
                 }
                 return fg;
            }
        /*
        else if(MainActivity.selectedTank == 1)
        {
            if (position == 0) {
                return MainActivity.mControlsFrag2;
            }
            else if (position == 1)
            {
                return MainActivity.mProfileFrag2;
            }
            else if (position==2)
            {
                return MainActivity.mAnalyticsFrag2;
            }
            else
            {
                return MainActivity.mLastEventsFrag2;
            }
        }
        else //assumed last tank has been selected
        {
            if (position == 0) {
                return MainActivity.mControlsFrag3;
            }
            else if (position == 1)
            {
                return MainActivity.mProfileFrag3;
            }
            else  if (position==2)
            {
                return MainActivity.mAnalyticsFrag3;
            }
            else
            {
                return MainActivity.mLastEventsFrag3;
            }
        }
        */
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
