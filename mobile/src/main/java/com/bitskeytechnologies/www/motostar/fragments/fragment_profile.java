package com.bitskeytechnologies.www.motostar.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.R;
import com.bitskeytechnologies.www.motostar.db.DataSource;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link fragment_profile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link fragment_profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_profile extends Fragment
    implements AdapterView.OnItemSelectedListener,
        DeviceObject.DeviceUpdateListner
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String FILENAME = "msfragmentProfile";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public fragment_profile() {
        // Required empty public constructor
        instanceId = instanceCount;
        instanceCount++;
        if (instanceCount==1) //assumption is that - we will have max 3 tanks for now.
        {
            instanceCount = 0;
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragment_profile.
     */
    // TODO: Rename and change types and number of parameters
    public static fragment_profile newInstance(String param1, String param2) {
        fragment_profile fragment = new fragment_profile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        refresh();
    }


    TextView tvMorningProfileView;
    TextView tvMorningStartTime;
    TextView tvMorningRetryCount;

    TextView tvEvningProfileView;
    TextView tvEveningStartTime;
    TextView tvEveningRetryCount;

    TextView tvCommonProfileView;
    TextView tvRetryInterval;
    TextView tvStopNoWaterTime;

    Spinner spinMorningStartTime;
    Spinner spinMorningRetryCount;

    Spinner spinEveningStartTime;
    Spinner spinEveningRetryCount;

    Spinner spinRetryInterval;
    Spinner spinNoWaterStopTime;

    ImageButton ibRefreshButton;
    ImageButton ibPushButton;

    DeviceObject dvObj=null;
    private static int instanceCount = 0;
    private int instanceId = 0;

    ArrayAdapter<CharSequence> spinnerMorningStartTimeAdapter;
    ArrayAdapter<CharSequence> spinnerMorningRetryAdapter;

    ArrayAdapter<CharSequence> spinnerEveningStartTimeAdapter;
    ArrayAdapter<CharSequence> spinnerEveningRetryAdapter;

    ArrayAdapter<CharSequence> spinnerRetryIntervalAdapter;
    ArrayAdapter<CharSequence> spinnerNoWaterStopTimeAdapter;

    Context context;


    /*
    byte mor_start_hr = 5;  //morning start hr: 3
    byte mor_start_min = 0; //morning start min: 4
    byte mor_max_retry = 8; //morningmaxretry : 5
    byte eve_start_hr = 19;  //evening start hr: 6
    byte eve_start_min = 0;   //evening start min : 7
    byte eve_max_retry = 8;   //evening max retry : 8
    byte retry_interval = 15;  //retryInterval : 9
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);



        tvMorningProfileView = (TextView) view.findViewById(R.id.MorningProfileView);
        tvMorningStartTime = (TextView) view.findViewById(R.id.MorningStartTimeView);
        tvMorningRetryCount = (TextView) view.findViewById(R.id.MorningRetryCountView);

        tvEvningProfileView = (TextView) view.findViewById(R.id.EveningProfileView);
        tvEveningStartTime = (TextView) view.findViewById(R.id.EveningStartTimeView);
        tvEveningRetryCount = (TextView) view.findViewById(R.id.EveningRetryCountView);

        tvCommonProfileView = (TextView) view.findViewById(R.id.Profile_common_params_view);
        tvRetryInterval = (TextView) view.findViewById(R.id.RetryIntervalView);
        tvStopNoWaterTime = (TextView) view.findViewById(R.id.NoWaterStopTimeView);

        context = getActivity();
        spinMorningStartTime = (Spinner) view.findViewById(R.id.spinSelectMorningStartTime);
             spinMorningStartTime.setOnItemSelectedListener(this);
             spinnerMorningStartTimeAdapter = ArrayAdapter.createFromResource(context,
                R.array.morning_start_time, android.R.layout.simple_spinner_item);
             // Specify the layout to use when the list of choices appears
             spinnerMorningStartTimeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
             // Apply the adapter to the spinner
             spinMorningStartTime.setAdapter(spinnerMorningStartTimeAdapter);

        spinMorningRetryCount = (Spinner) view.findViewById(R.id.spinSelectMorningRetryCount);
                spinMorningRetryCount.setOnItemSelectedListener(this);
                spinnerMorningRetryAdapter = ArrayAdapter.createFromResource(context,
                    R.array.morning_retry_count, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                spinnerMorningRetryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                 spinMorningRetryCount.setAdapter(spinnerMorningRetryAdapter);


        spinEveningStartTime = (Spinner) view.findViewById(R.id.spinSelectEveningStartTime);
                spinEveningStartTime.setOnItemSelectedListener(this);
                spinnerEveningStartTimeAdapter = ArrayAdapter.createFromResource(context,
                R.array.evening_start_time, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                spinnerEveningStartTimeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spinEveningStartTime.setAdapter(spinnerEveningStartTimeAdapter);


        spinEveningRetryCount = (Spinner) view.findViewById(R.id.spinSelectEveningRetryCount);
                spinEveningRetryCount.setOnItemSelectedListener(this);
                spinnerEveningRetryAdapter = ArrayAdapter.createFromResource(context,
                R.array.evening_retry_count, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                spinnerEveningRetryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spinEveningRetryCount.setAdapter(spinnerEveningRetryAdapter);

        spinRetryInterval = (Spinner) view.findViewById(R.id.spinnerEveningRetryInterval);
                spinRetryInterval.setOnItemSelectedListener(this);
                spinnerRetryIntervalAdapter = ArrayAdapter.createFromResource(context,
                R.array.retry_interval, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                spinnerRetryIntervalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spinRetryInterval.setAdapter(spinnerRetryIntervalAdapter);

        spinNoWaterStopTime = (Spinner) view.findViewById(R.id.spinSelectStopNoWater);
                spinNoWaterStopTime.setOnItemSelectedListener(this);
                spinnerNoWaterStopTimeAdapter = ArrayAdapter.createFromResource(context,
                R.array.nowaterStop_time, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                spinnerNoWaterStopTimeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spinNoWaterStopTime.setAdapter(spinnerNoWaterStopTimeAdapter);

        ibRefreshButton =  (ImageButton) view.findViewById(R.id.left_button);
        ibPushButton =  (ImageButton) view.findViewById(R.id.right_button);

        initSpinnerValueArrays();

        ibRefreshButton.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View arg0) {
                                                   if (mListener!=null) {

                                                       mListener.Vibrate();
                                                   }

                                                   if (MainActivity.getPairedDevicesList() != null) {
                                                       if (MainActivity.getPairedDevicesList().size() > 0) {
                                                           //dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                                                           dvObj = MainActivity.getSelectedDevice();
                                                           if (dvObj==null)
                                                           {
                                                               return;
                                                           }
                                                           //dvObj.setCallback(this , getActivity());
                                                           //connected status
                                                           if (dvObj.isPaired) {
                                                               dvObj.writeSynch('0');
                                                           }
                                                       }
                                                   }
                                                   refresh();
                                               }
                                           }
        );


        ibPushButton.setOnClickListener(new View.OnClickListener()
               {
                   @Override
                   public void onClick(View arg0) {
                       if (mListener!=null) {
                           mListener.Vibrate();
                       }
                       if (MainActivity.getPairedDevicesList() != null) {
                           if (MainActivity.getPairedDevicesList().size() > 0) {
                               //dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                               dvObj = MainActivity.getSelectedDevice();
                               if (dvObj==null)
                               {
                                   return;
                               }
                               //dvObj.setCallback(this , getActivity());
                               //connected status
                               if (dvObj!=null && dvObj.isPaired)
                               {
                                   if (dvObj.motorStatus==null)
                                   {
                                       return;
                                   }
                                   boolean result = dvObj.writeSynch(new String(dvObj.motorStatus.encodeProfile
                                           (
                                                   morningStartHr,
                                                   morningStartMin,
                                                   morning_retry_count,
                                                   eveningStartHr,
                                                   eveningStartMin,
                                                   evening_retry_count,
                                                   retry_interval,
                                                   noWaterStopTime
                                   )));

                                   if (result == true)
                                   {
                                       //update db as well:
                                       DataSource ds = new DataSource(context);
                                       ds.open();
                                       ds.updateDeviceRecord(dvObj.deviceAddress,
                                               morningStartHr,
                                               morningStartMin,
                                               morning_retry_count,
                                               eveningStartHr,
                                               eveningStartMin,
                                               evening_retry_count,
                                               retry_interval,
                                               noWaterStopTime
                                               );
                                       ds.close();

                                       dvObj.encodeAndPostProfile();
                                   }
                                   else
                                   {
                                       createNotConnectedDialog();
                                       refresh();
                                   }
                                   //dvObj.writeSynch("5");
                               }
                           }
                       }
                       //refresh();
                   }
               }
        );

        refresh();
        return view;
    }


    void createNotConnectedDialog()
    {
        final CharSequence[] items = {" Email "," Phone "};
        // arraylist to keep the selected items
        final ArrayList seletedItems=new ArrayList();
        int inputSelection = -1;

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Warning : MotoStar is not connected. Please ensure MotoStar is nearby and is switched on. " +
                "If problem still doesn't resolve - consider calling support from top menu");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id)
            {
                //  Your code when user clicked on OK
                //  You can write the code  to save the selected item here
                //Just a temporarily code to test the specific scenario:
                //sendEmail();
                //timeSyncRetries = 0;
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel
                        //timeSyncRetries = 0;
                    }
                });

        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
            refresh();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public boolean isResumed = false;
    @Override
    public void onResume() {
        super.onResume();
        Log.d(FILENAME, "onResume");
        isResumed = true;
        refresh();
        //mListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(FILENAME, "onPause");
        isResumed = false;
        //mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
        void updateConnectionView(DeviceObject dvObj);
        void Vibrate();
    }


    public void refresh()
    {
        if (isResumed==false)
        {
            return;
        }
        if (context==null)
        {
            return;
        }
        //Need to send request to fetch from device
        if (MainActivity.getPairedDevicesList()!=null)
        {
            if ( MainActivity.getPairedDevicesList().size() > 0)
            {
                //dvObj = (DeviceObject)MainActivity.getPairedDevicesList().get(instanceId);
                dvObj = MainActivity.getSelectedDevice();
                if (dvObj==null)
                {
                    return;
                }
                if (mListener!=null) {
                    mListener.updateConnectionView(dvObj);
                }

                //dvObj.setCallback(this, getActivity());
                //connected status
                if (dvObj==null)
                {
                    return;
                }
                dvObj.setCallbackProfile(this,getActivity());
                if (dvObj!=null)
                {
                    if (dvObj.motorStatus!=null)
                    {
                        dvObj.motorStatus.initMotorStatus();
                    }
                }
                if(dvObj.isPaired)
                {

                }
                else
                {
                    Log.d(FILENAME, "Not Connected, need to return");
                    return;
                    //not paired
                }
                //dvObj.deviceName;
                if (dvObj.motorStatus==null)
                {
                    //tvInfo.setText("Waiting Updates from Device");
                    //tvInfo.setTextColor(Color.RED);
                    return;
                }
                if (spinMorningStartTime!=null)
                {
                    //Update levels
                    setMorningTime(dvObj.motorStatus.morningStartHour, dvObj.motorStatus.morningStartMinute);
                    setMorningRetryCount(dvObj.motorStatus.morningMaxRetry);
                    setEveningTime(dvObj.motorStatus.eveningStartHour, dvObj.motorStatus.eveningStartMinute);
                    setEveningRetryCount(dvObj.motorStatus.eveningMaxRetry);
                    setInterval(dvObj.motorStatus.retryInterval);
                    setNoWaterStopTime(dvObj.motorStatus.MAXMINSWITHOUTWATER);
                }
                //dvObj.encodeAndPostProfile();
            }
        }
        else
        {
            if (mListener!=null) {
                mListener.updateConnectionView(null);
            }
        }
        //this.notifyDataSetChanged();
    }


    int spinnerMorningStart_pos = 23;

    String [] morning_start_time;
    String[] morning_start_hrs;//
    String[] morning_start_mins;//
    int morningStartHr = 5;
    int morningStartMin = 45;
    int spinnerMorningRetryCount_pos = 4;
    String[] morning_retry_counts;
    int morning_retry_count = 8;


    int spinnerEveningStart_pos = 28;

    String[] evening_start_time;
    String[] evening_start_hrs;
    String[] evening_start_mins;
    int eveningStartHr = 19;
    int eveningStartMin = 0;

    int spinnerEveningRetryCount_pos = 4;
    String[] evening_retry_counts;

    int evening_retry_count = 8;

    int spinnerRetryInterval_pos = 1;
    String[] retry_intervals;
    String[] retry_values;
    int retry_interval = 15;

    int spinnerNoWaterStopTime_pos = 1;
    //String[] noWaterStopTimerStrings;
    String[] noWaterStopTimes;
    int noWaterStopTime = 2;

    public void initSpinnerValueArrays()
    {
        morning_start_time  =  getResources().getStringArray(R.array.morning_start_time);
        morning_start_hrs = getResources().getStringArray(R.array.morning_start_hr);
        morning_start_mins = getResources().getStringArray(R.array.morning_start_min);

        morning_retry_counts = getResources().getStringArray(R.array.morning_retry_count);

        evening_start_time  =  getResources().getStringArray(R.array.evening_start_time);
        evening_start_hrs = getResources().getStringArray(R.array.evening_start_hr);
        evening_start_mins = getResources().getStringArray(R.array.evening_start_min);

        evening_retry_counts = getResources().getStringArray(R.array.evening_retry_count);

        retry_intervals = getResources().getStringArray(R.array.retry_interval);
        retry_values = getResources().getStringArray(R.array.retry_value);

        noWaterStopTimes = getResources().getStringArray(R.array.nowaterStop_value);
    }

    public void setMorningTime(int hr, int min) {
        int pos = getTimePos(morning_start_hrs, String.valueOf(hr), morning_start_mins, String.valueOf(min));

        //if (pos==0) pos++; //hack to avoid a crash at the time of OOBE:

        if (spinMorningStartTime!=null)
        {
            spinMorningStartTime.setSelection(pos);
        }
    }

    public void setMorningRetryCount(int morningRetryCount)
    {
        int pos = getSpinnerPosition(morning_retry_counts,String.valueOf(morningRetryCount));
        if (morning_retry_counts.length<=pos)
        {
            pos =0 ;
        }
        spinMorningRetryCount.setSelection(pos);
    }

    public void setEveningTime(int hr, int min) {
        int pos = getTimePos(evening_start_hrs, String.valueOf(hr), evening_start_mins, String.valueOf(min));

        pos = pos%48; //This is to avoid a crash at the time of OOBE:
        spinEveningStartTime.setSelection(pos);
    }


    public void setEveningRetryCount(int eveningRetryCount)
    {
        int pos = getSpinnerPosition(evening_retry_counts, String.valueOf(eveningRetryCount));
        if (evening_retry_counts.length<=pos)
        {
            pos =0 ;
        }
        spinEveningRetryCount.setSelection(pos);
    }

    public void setInterval(int interval)
    {
        int pos = getSpinnerPosition(retry_values,String.valueOf(interval));
        if (retry_values.length<=pos)
        {
            pos =0 ;
        }
        spinRetryInterval.setSelection(pos);
    }

    public void setNoWaterStopTime(int noWaterTime)
    {
        int pos = getSpinnerPosition(noWaterStopTimes,String.valueOf(noWaterTime));
        if (noWaterStopTimes.length<=pos)
        {
            pos =0 ;
        }
        spinNoWaterStopTime.setSelection(pos);
    }




    public int getTimePos(String[] hrs, String hr, String[] mins, String min)
    {
        int hrsPos = getSpinnerPosition(hrs, hr);
        int minsPos = getSpinnerPosition(mins, min);

        return hrsPos*4 + minsPos;
        /*

        if (minsPos%4==0)
        {
            //even position - means return hrsPos
            return hrsPos;
        }
        else if (minsPos%4==1)
        {
            hrsPos++; // it is 9:15 type time
            return hrsPos;
        }
        else if (minsPos%4==2)
        {
            hrsPos++; // it is 9:30 type time
            return hrsPos;
        }
        else
        {
            hrsPos++; // it is 9:45 type time
            return hrsPos;
        }
        */

    }

    public int getSpinnerPosition(String [] strArray, String str)
    {
        int i = 0;
        //find the value in this array;
        for (String st : strArray)
        {
            if (st.equals(str))
            {
                break;
            }
            i++;
        }
        return i;
    }

    //Spinner interface
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id)
    {
        // An item was selected. You can retrieve the selected item using
        //if ( view.equals(spinMorningStartTime))
        {
            //if (parent.getItemAtPosition(pos)==0)
        }

        spinnerMorningStart_pos = spinMorningStartTime.getSelectedItemPosition();
        int hrposition = spinnerMorningStart_pos/4;
        int minposition = spinnerMorningStart_pos%4;

        morningStartHr = Integer.valueOf(morning_start_hrs[hrposition]);
        morningStartMin = Integer.valueOf(morning_start_mins[minposition]);

        spinnerMorningRetryCount_pos = spinMorningRetryCount.getSelectedItemPosition();
        morning_retry_count = Integer.valueOf(morning_retry_counts[spinnerMorningRetryCount_pos]);

        spinnerEveningStart_pos = spinEveningStartTime.getSelectedItemPosition();
        hrposition = spinnerEveningStart_pos/4;
        minposition = spinnerEveningStart_pos%4;
        eveningStartHr = Integer.valueOf(evening_start_hrs[hrposition]);
        eveningStartMin = Integer.valueOf(evening_start_mins[minposition]);

        spinnerEveningRetryCount_pos = spinEveningRetryCount.getSelectedItemPosition();
        evening_retry_count = Integer.valueOf(evening_retry_counts[spinnerEveningRetryCount_pos]);

        spinnerRetryInterval_pos = spinRetryInterval.getSelectedItemPosition();
        retry_interval = Integer.valueOf(retry_values[spinnerRetryInterval_pos]);


        spinnerNoWaterStopTime_pos = spinNoWaterStopTime.getSelectedItemPosition();
        noWaterStopTime = Integer.valueOf(noWaterStopTimes[spinnerNoWaterStopTime_pos]);
    }


    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

}
