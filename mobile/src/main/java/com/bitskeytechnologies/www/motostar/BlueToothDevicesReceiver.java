package com.bitskeytechnologies.www.motostar;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.bitskeytechnologies.www.motostar.Device.DeviceObject;

import java.util.ArrayList;
import java.util.List;

public class BlueToothDevicesReceiver extends BroadcastReceiver
{
    final String FILENAME = "MS BTDevicesReceiver";
    //public static List tempScanDevicesObjList= new ArrayList();

    MainActivity ma;
    Context context;
    Context appContext = null;
    public static List tempScanDevicesObjList= new ArrayList();
    public static List tempScanBTDevicesObjList= new ArrayList();

    //public static  ArrayList<BluetoothDevice> arrayListBluetoothDevices = new ArrayList<BluetoothDevice>();

    public BlueToothDevicesReceiver() {
    }

    public BlueToothDevicesReceiver(MainActivity ma, Context appContext) {
        //arrayListBluetoothDevices = new ArrayList<BluetoothDevice>();
        Log.d(FILENAME,"BlueToothDevicesReceiver" );
        this.ma = ma;
        this.appContext = appContext;
        Log.d(FILENAME,"MainActivity has been initialized" );
        for (int i = 0; i < tempScanBTDevicesObjList.size(); i++)
        {
            if (appContext != null)
            {
                BluetoothDevice btDevice = (BluetoothDevice)(tempScanBTDevicesObjList.get(i));
                boolean isPaired = false;
                if (btDevice.getBondState()==BluetoothDevice.BOND_BONDED)
                {
                    isPaired = true;

                }
                DeviceObject deviceObject = new DeviceObject(appContext,btDevice.getName() , btDevice.getAddress(), null, btDevice, isPaired);
                tempScanDevicesObjList.add(deviceObject);
            }

        }

        if (ma!=null)
        {
            //update FragmentConnections by sending some signal
            if (ma.mConnectionsFrag!=null)
            {
                ma.mConnectionsFrag.update();
            }
        }
        else
        {
            Log.d(FILENAME, "MA is null - could not update fragment");
        }
        tempScanBTDevicesObjList.clear();
    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //throw new UnsupportedOperationException("Not yet implemented");

        Log.d(FILENAME, "BroadcastReceiver::onReceive");


        Message msg = Message.obtain();
        String action = intent.getAction();
        BluetoothDevice devicetemp = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        if (devicetemp!=null)
        {
            Log.d(FILENAME, "\n device name " + devicetemp.getName() + " address " + devicetemp.getAddress() + "action : " + action);
        }
        boolean isPaired = false;
        if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action))
        {
            //can be done here:: //handle all the codes:
            Log.d(FILENAME, "ACTION_BOND_STATE_CHANGED");
            //int newDeviceState = intent.getParcelableExtra(BluetoothDevice.EXTRA_BOND_STATE);
            if (devicetemp.getBondState()==BluetoothDevice.BOND_BONDED)
            {
                if (ma!=null)
                {
                    isPaired = true;
                    ma.refreshPairedDevices(devicetemp, isPaired);
                }
                //device is paired now: tell everyone:
            }
            if (devicetemp.getBondState()==BluetoothDevice.BOND_NONE)
            {
                //Bond removed -- do we need to do something here?
                //refreshPairedDevices(devicetemp);
                //device is paired now: tell everyone:
            }


        }
        else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action))
        {
            Log.d(FILENAME, "ACTION_ACL_CONNECTED");
            if (ma!=null)
            {
                isPaired =false;
                ma.refreshPairedDevices(devicetemp, isPaired);
            }
        }
        else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action))
        {
            Log.d(FILENAME, "ACTION_ACL_DISCONNECT_REQUESTED");
        }
        else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action))
        {
            Log.d(FILENAME, "ACTION_ACL_DISCONNECTED");
        }
        else if (BluetoothDevice.ACTION_CLASS_CHANGED.equals(action))
        {
            Log.d(FILENAME, "ACTION_CLASS_CHANGED");
        }
        else if (BluetoothDevice.ACTION_NAME_CHANGED.equals(action))
        {
            Log.d(FILENAME, "ACTION_NAME_CHANGED");
        }
        else if (BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action))
        {
            Log.d(FILENAME, "ACTION_PAIRING_REQUEST");
        }
        else if (BluetoothDevice.ACTION_UUID.equals(action))
        {
            Log.d(FILENAME, "ACTION_UUID");
        }
        else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED))
        {
            Log.d(FILENAME, "ACTION_DISCOVERY_STARTED");
        }
        else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
        {
            Log.d(FILENAME, "ACTION_DISCOVERY_FINISHED");
            if (ma!=null)
            {
                isPaired = true;
                ma.refreshPairedDevices(null, isPaired); //null means - system will do it's work correctly
            }
        }
        else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED))
        {
            Log.d(FILENAME,"BlueToothDevicesReceiver: ACTION_STATE_CHANGED" );
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch(state) {
                case BluetoothAdapter.STATE_OFF:
                    Log.d(FILENAME, "STATE_OFF");
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    Log.d(FILENAME, "STATE_TURNING_OFF");
                    break;
                case BluetoothAdapter.STATE_ON:
                    Log.d(FILENAME, "STATE_ON");
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    Log.d(FILENAME, "STATE_TURNING_ON");
                    break;
            }
        }
        else if(action.equals(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED))
        {
            Log.d(FILENAME,"BlueToothDevicesReceiver: ACTION_SCAN_MODE_CHANGED" );
            int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

            switch(mode){
                case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                    break;
                case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                    break;
                case BluetoothAdapter.SCAN_MODE_NONE:
                    break;
            }
        }
        else if(BluetoothDevice.ACTION_FOUND.equals(action))
        {
            Log.d(FILENAME, "ACTION_FOUND");
            //Toast.makeText(context, "ACTION_FOUND", Toast.LENGTH_SHORT).show();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (device==null)
            {
                Log.e(FILENAME, "This is null device");
                return;

            }
            Log.d(FILENAME, "DEVICE + :: " + device.getName() + "::" +device.getAddress() );

            if (MainActivity.isItMotoStar(device.getName())==false)
            {
                Log.d(FILENAME, "This is not MotoStar - ignore it + :: " + device.getName() + "::" +device.getAddress() );
                return;
            }


            Log.d(FILENAME, "Old scanList Size " + tempScanDevicesObjList.size());
            if (tempScanDevicesObjList.size() < 1) // this checks if the size of bluetooth device is 0,then add the
            {
                //ParcelUuid[] uuids = device.getUuids();
                Log.d(FILENAME,"Adding device to scanList, Device name:" +  device.getName() + " mac:" +device.getAddress() );
                {
                    if (appContext != null)
                    {
                        DeviceObject deviceObject = new DeviceObject(appContext, device.getName(), device.getAddress(), null, device, isPaired);
                        tempScanDevicesObjList.add(deviceObject);
                    }
                    else
                    {
                        //add this into a temporary list - which can be added to temoScanDevicesObjList -as appContext is initialized
                        tempScanBTDevicesObjList.add(device);
                    }
                }
            }
            else
            {
                boolean addDevice = true;    // flag to indicate that particular device is already in the arlist or not
                for (int i = 0; i < tempScanDevicesObjList.size(); i++)
                {
                    DeviceObject iDevice = (DeviceObject)tempScanDevicesObjList.get(i);
                    if (device.getAddress().equals(iDevice.deviceAddress))
                    {
                        Log.d(FILENAME, "DEVICE + :: " + device.getName() + "It is Already present in the list" );
                        addDevice = false;
                    }
                }
                if (addDevice == true)
                {

                    //ParcelUuid[] uuids = device.getUuids();
                    //BluetoothSocket socket;
                    Log.d(FILENAME, "Adding device to scanList, Device name:" + device.getName() + " mac:" + device.getAddress());
                    if (this!=null)
                    {
                        DeviceObject deviceObject = new DeviceObject(appContext, device.getName(), device.getAddress(), null, device, isPaired);
                        tempScanDevicesObjList.add(deviceObject);
                    }
                }
            }
            if (ma!=null)
            {
                if (ma.mConnectionsFrag!=null)
                {
                    //update FragmentConnections by sending some signal
                    ma.mConnectionsFrag.update();
                }
            }
            else
            {
                Log.d(FILENAME, "MA is null - could not update fragment");
            }
        }
    }

}
