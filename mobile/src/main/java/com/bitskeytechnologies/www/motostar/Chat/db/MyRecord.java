package com.bitskeytechnologies.www.motostar.Chat.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bitskeytechnologies.www.motostar.db.DBObject;
import com.bitskeytechnologies.www.motostar.db.DataSource;
import com.bitskeytechnologies.www.motostar.db.DatabaseHelper;

import java.util.List;
import java.util.Vector;

public class MyRecord extends DBObject {

	private final static String DB_TABLE = "MyRecords";

	private final static String COLUMN_USER_NAME = "user_name";
	private final static String COLUMN_GROUP_NAME = "group_name";
	private final static String COLUMN_PASSWORD = "password";
	private final static String COLUMN_EMAILID = "emailId";
	private final static String COLUMN_AVATAR = "avatar";
	private final static String COLUMN_TRACKING_PREFERENCE = "tracking_preference";
	private final static String COLUMN_DISPLAY_NAME = "display_name";


	private final static String[] ALL_COLUMNS = new String[] {
		COLUMN_ID,
			COLUMN_USER_NAME,
			COLUMN_GROUP_NAME,
			COLUMN_PASSWORD,
			COLUMN_EMAILID,
			COLUMN_AVATAR,
			COLUMN_TRACKING_PREFERENCE,
			COLUMN_DISPLAY_NAME
		/*
		COLUMN_PHONE_NUMBER,
		COLUMN_VALIDATION_CODE_1,
		COLUMN_VALIDATION_CODE_2,
		COLUMN_CREATION_TIMESTAMP,
		COLUMN_AUTH_PASSWORD
		*/
	};

	private final static String CREATE_TABLE =
			"CREATE TABLE " + DB_TABLE + " (" +
			COLUMN_ID + " integer primary key autoincrement, " +
			COLUMN_USER_NAME  + " text, " +
			COLUMN_GROUP_NAME + " text, " +
			COLUMN_PASSWORD + " text, " +
			COLUMN_EMAILID + " text, " +
			COLUMN_AVATAR + " text, " +
					COLUMN_TRACKING_PREFERENCE + " integer not null, " +
			COLUMN_DISPLAY_NAME + " text " +
					/*
			COLUMN_PHONE_NUMBER + " text, " +
			COLUMN_VALIDATION_CODE_1 + " text, " +
			COLUMN_VALIDATION_CODE_2 + " text, " +
			COLUMN_CREATION_TIMESTAMP + " integer not null, " +
			COLUMN_AUTH_PASSWORD + " text not null	*/
			");";

	String userName = null;
	String groupName = null;
	String displayName = null;
	String password = null;
	String emailId = null;
	String avatar = null;
	long trackingPreference = 1;
	/*
	String phoneNumber = null;
	String validationCode1 = null;
	String validationCode2 = null;
	long creationTimestamp = 0;
	String authPassword = null;
	*/

	public static void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	public static void onUpgrade(SQLiteDatabase db, int oldVersion,
			int newVersion) {
		if(oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
			onCreate(db);
		}
	}

	public MyRecord() {
		//setCreationTimestamp(System.currentTimeMillis() / 1000);
	}
	
	public static List<MyRecord> loadFromDatabase(SQLiteDatabase db) {
		Vector<MyRecord> v = new Vector<MyRecord>();
		
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, null,
		null, null, null, null);
		if(cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				v.add(fromCursor(cursor));
				cursor.moveToNext();
			}
			cursor.close();
		}
		return v;
	}
	
	public static MyRecord loadFromDatabase(SQLiteDatabase db, long id) {
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_ID + " = " + id,
				null, null, null, null);
		MyRecord req = null;
		if( cursor != null) {
			cursor.moveToFirst();
			if (!cursor.isAfterLast())
				req = fromCursor(cursor);
			cursor.close();
		}
		return req;
	}

	//userName All chat records
	public static List<MyRecord> loadFromDatabase(SQLiteDatabase db, String userName) {
		Vector<MyRecord> v = new Vector<MyRecord>();
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_USER_NAME +  " =?", new String[]{userName},
				null, null, null, null);
		if(cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				v.add(fromCursor(cursor));
				cursor.moveToNext();
			}
			cursor.close();
		}
		return v;

	}

	private static MyRecord fromCursor(Cursor cursor) {
		MyRecord req = new MyRecord();
		int i = 0;
		req.setId(cursor.getLong(i++));
		req.setUserName(cursor.getString(i++));
		req.setGroupName(cursor.getString(i++));
		req.setPassword(cursor.getString(i++));
		req.setEmailId(cursor.getString(i++));
		req.setAvatar(cursor.getString(i++));
		req.setTrackingPreference(cursor.getLong(i++));
		req.setDisplayName(cursor.getString(i++));

		//req.

		/*
		req.setId(cursor.getLong(i++));
		req.setPhoneNumber(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setCreationTimestamp(cursor.getLong(i++));
		req.setAuthPassword(cursor.getString(i++));
		*/
		
		return req;
	}

	public final static String MY_RECORD_ID = "myRecord";
	
	@Override
	protected String getTableName() {
		return DB_TABLE;
	}
	
	@Override
	protected void putValues(ContentValues values) {
		values.put(COLUMN_USER_NAME, getUserName());
		values.put(COLUMN_GROUP_NAME, getGroupName());
		values.put(COLUMN_PASSWORD, getPassword());
		values.put(COLUMN_EMAILID, getEmailId());
		values.put(COLUMN_AVATAR, getAvatar());
		values.put(COLUMN_TRACKING_PREFERENCE, getTrackingPreference());
		values.put(COLUMN_DISPLAY_NAME,getDisplayName());
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGroupName()
	{
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAvatar()
	{
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}


	public long getTrackingPreference()
	{
		return trackingPreference;
	}
	public void setTrackingPreference(long trackingPreference) {
		this.trackingPreference = trackingPreference;
	}



	//@Override
	public String getTitleForMenu() {
		return getId() + "";
	}

	//@Override
	public String getKeyForIntent() {
		return MY_RECORD_ID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MyRecord))
			return false;
		MyRecord other = (MyRecord) obj;

		if (userName == null) {
			if (other.userName!=null)
				return false;
		} else if (!userName.equals(other.userName)) {
			return false;
		}

		if (displayName == null) {
			if (other.displayName!=null)
				return false;
		} else if (!displayName.equals(other.displayName)) {
			return false;
		}

		if (groupName == null) {
			if (other.groupName!=null)
				return false;
		} else if (!groupName.equals(other.groupName)) {
			return false;
		}

		if (password == null) {
			if (other.password!=null)
				return false;
		} else if (!password.equals(other.password)) {
			return false;
		}

		if (emailId == null) {
			if (other.emailId!=null)
				return false;
		} else if (!emailId.equals(other.emailId)) {
			return false;
		}

		if (avatar == null) {
			if (other.avatar!=null)
				return false;
		} else if (!avatar.equals(other.avatar)) {
			return false;
		}

		return true;
	}
	public static void storeRecord(Context context, String userName,String displayName, String groupName, String password, String emailId, String avatar) {
		MyRecord req = new MyRecord();
		req.setUserName(userName);
		req.setGroupName(groupName);
		req.setPassword(password);
		req.setEmailId(emailId);
		req.setAvatar(avatar);
		req.setDisplayName(displayName);
		DataSource db = new DataSource(context);
		db.open();
		db.persistMyRecord(req);
		db.close();
	}

	public static void LoadAndUpdateMyRecord(SQLiteDatabase db, String userName, long myTrackingPreference)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_TRACKING_PREFERENCE, myTrackingPreference);
		db.update(DB_TABLE, values, COLUMN_USER_NAME + " = ? ", new String[]{userName});
	}


	public static void clearFromDB(Context context)
	{
		DataSource db = new DataSource(context);
		db.open();
		List<MyRecord> reqs = db.getMyRecords();
		for(MyRecord _req : reqs) {
			db.deleteMyRecord(_req);
		}
		db.close();
	}

}
