package com.bitskeytechnologies.www.motostar;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import com.astuetz.PagerSlidingTabStrip;
import com.bitskeytechnologies.www.motostar.Chat.gen.ChatXMPPService;
import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.Init.RegistrationInfo;
import com.bitskeytechnologies.www.motostar.RC.RcState;
import com.bitskeytechnologies.www.motostar.fragments.WelcomeFragment;
import com.bitskeytechnologies.www.motostar.db.AlarmRecord;
import com.bitskeytechnologies.www.motostar.db.DataSource;
import com.bitskeytechnologies.www.motostar.db.DatabaseHelper;
import com.bitskeytechnologies.www.motostar.db.DevicesLocalNameRecords;
import com.bitskeytechnologies.www.motostar.db.DevicesRecords;
import com.bitskeytechnologies.www.motostar.db.EventRecord;
import com.bitskeytechnologies.www.motostar.db.LastAlarmRecord;
import com.bitskeytechnologies.www.motostar.db.LastEventRecord;
import com.bitskeytechnologies.www.motostar.db.MyPhoneRecord;
import com.bitskeytechnologies.www.motostar.db.SharedPreference;
import com.bitskeytechnologies.www.motostar.fragments.FragmentConnections;
import com.bitskeytechnologies.www.motostar.fragments.FragmentControl;
import com.bitskeytechnologies.www.motostar.fragments.TabsPagerAdapter;
import com.bitskeytechnologies.www.motostar.fragments.fragment_alarm;
import com.bitskeytechnologies.www.motostar.fragments.fragment_analytics;
import com.bitskeytechnologies.www.motostar.fragments.fragment_lastAlarmsList;
import com.bitskeytechnologies.www.motostar.fragments.fragment_lastEventsList;
import com.bitskeytechnologies.www.motostar.fragments.fragment_profile;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
   implements FragmentConnections.OnConnectionsFragmentDone,
     FragmentControl.OnFragmentInteractionListener,
        fragment_analytics.OnFragmentInteractionListener,
        fragment_lastEventsList.OnFragmentInteractionListener,
        fragment_alarm.OnFragmentInteractionListener,
        fragment_lastAlarmsList.OnFragmentInteractionListener,
        fragment_profile.OnFragmentInteractionListener,
        WelcomeFragment.OnWelcomFragmentDone,
        AdapterView.OnItemSelectedListener
       //public class MainActivity extends Activity
{
    public static String support_Phone = "9810414413";
    public static Context mContext = null;
    private static final String FILENAME = "MS MainActivity#";
    //TextView textview1;
    //static TextView textview2;

    TextView tvConnection;
    Spinner spinDeviceSelection;
    ArrayAdapter<CharSequence> spinnerDeviceSelectionAdapter;
    public static String devicesNames[];
    public static String devicesMacs[];
    TextView ivConnection;


    public static String phone = null;
    public static String phoneName = null;
    public static String mac = null;
    public static String os = null;
    public static int sdk = 0;
    public static String motoVersion = null;
    public static int serverUpdateRequired = 0;

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_DISCOVERABLE_BT = 0;

    public static String PAIRED = "Paired";
    public static String UNPAIRED = "unPaired";
    BluetoothDevice bdDevice;
    BluetoothClass bdClass;
    public static BluetoothAdapter mBluetoothAdapter;

    public static ArrayList<BluetoothDevice> arrayListPairedBluetoothDevices = null;
    private static List pairedDevicesList = new ArrayList();
    //TextView mConnectionMessage ; //ConnectionMessage

    //public static List tempScanDevicesObjList= new ArrayList();


    public static TabsPagerAdapter mTabPageAdapter;
    ViewPager mpager;
    DrawerLayout mDrawerLayout;

    public static int selectedTank = 0;

    ArrayAdapter<CharSequence> spinnerAdapter;
    public static DeviceObject mSelectedDevice;

    public static Context mainActivitythis = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        Log.d(FILENAME, "onCreate");
        mReceiver = new BlueToothDevicesReceiver(this, getApplicationContext());

        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(getApplicationContext()));
        }

        fragmentManager = getSupportFragmentManager();
        mConnectionsFrag = new FragmentConnections();
        //initFragment(R.id.connections, mConnectionsFrag, TAG_CONNECTIONS, true, false );

        mControlsFrag = new FragmentControl();
        mProfileFrag = new fragment_profile();
        mAnalyticsFrag = new fragment_analytics();
        mLastEventsFrag = new fragment_lastEventsList();

        mAlarmsFrag = new fragment_alarm();
        mLastAlarmsFrag = new fragment_lastAlarmsList();

        pWelcomeFrag = new WelcomeFragment();
        mTabPageAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        mainActivity = this;

        //mConnectionMessage =  (TextView) findViewById(R.id.ConnectionMessage);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setTitle("MotoStar");


            mDefaultMenu = true;
            showActionBar();
            activateDefaultMenu();
            //toolbar.getBackground().setAlpha(0);//This shall make toolbar as transparent.
        }
        mContext = this.getApplicationContext();
        mainActivitythis = this;

        //tvConnection = (TextView) findViewById(R.id.connection2);
        spinDeviceSelection = (Spinner) findViewById(R.id.spinnerConnection);
        spinDeviceSelection.setOnItemSelectedListener(this);
        spinnerDeviceSelectionAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        spinnerDeviceSelectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinDeviceSelection.setAdapter(spinnerDeviceSelectionAdapter);
        devicesNames = new String[MAX_TANKS];//{};
        devicesMacs = new String[MAX_TANKS];//{};
        spinDeviceSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedTank = position;
                mac = devicesMacs[selectedTank];
                mSelectedDevice = identifySelectedDevice();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        populateDeviceSelectSpinner();
        ivConnection = (TextView) findViewById(R.id.SyncButton2);
        ivConnection.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DeviceObject dvObj = identifySelectedDevice();
                        //dvObj.setupCommand();
                    }
                }
        );
        arrayListPairedBluetoothDevices = new ArrayList<BluetoothDevice>();
        speechInit();
        cleanupHistoricRecords();

        startNextFragment();
        SharedPreference.getInstance().retreiveSharedPreference(mContext);
        isAnnouncementOn = SharedPreference.getInstance().getSpeakerOn();


        Intent myIntent = new Intent(mContext, ChatXMPPService.class);
        myIntent.setAction(ChatXMPPService.ACTION_BACKGROUND);
        mContext.startService(myIntent);

    }

    public final static int MAX_TANKS = 10;

    public void selectDevice(String mac) {
        int selectedTankIndex = getIndexOfSelectedDevice(mac);
        if (selectedTankIndex >= 0) {
            mac = devicesMacs[selectedTankIndex];
            spinDeviceSelection.setSelection(selectedTankIndex);
        }
        mSelectedDevice = identifySelectedDevice();
        startNextFragment();
    }

    public static DeviceObject getDeviceReference(String mac) {
        int selectedTankIndex = getIndexOfSelectedDevice(mac);
        if (selectedTankIndex >= 0) {
            mac = devicesMacs[selectedTankIndex];
        }
        if (MainActivity.mContext == null) {
            Log.e(FILENAME, "MainActivity.mContext is null");
            return null;
        }
        return identifySelectedDevice();
    }





    public static int  getIndexOfSelectedDevice(String mac)
    {
        int i = 0;
        boolean addressFound = false;
        if (devicesMacs==null) {return -1;}
        for (String address : devicesMacs)
        {
            if (address== null)
            {
                i  = -1;
                break;
            }
            if (address.equals(mac))
            {
                addressFound=true;
                break;
            }
            i++;
        }
        if (addressFound==false)
        {
            i=-1;
        }
        return i;
    }

    /*
    public void populateDeviceSelectSpinner()
    {
        initDevicesNamesFromDb();
        spinnerAdapter.add("value");
        spinnerAdapter.notifyDataSetChanged();
    }
    */

    public void populateDeviceSelectSpinner()
    {
        DataSource ds = new DataSource(mContext);
        ds.open();

        List<DevicesLocalNameRecords> devicelocalDevicesNamesRecords = ds.getDeviceLocalNameRecords();
        ds.close();
        if (devicelocalDevicesNamesRecords == null)
        {
            //do nothing
        }
        else //It means user has atleast entered his phone number.
        {
            //boolean deviceRecordExists = false;
            int i=0;
            spinnerDeviceSelectionAdapter.clear();
            for (DevicesLocalNameRecords _req : devicelocalDevicesNamesRecords)
            {
                devicesNames[i]=_req.getDevoceLocalName();
                devicesMacs[i]=_req.getMac();
                if (spinnerDeviceSelectionAdapter!=null) {
                    spinnerDeviceSelectionAdapter.add(devicesNames[i]);
                }
                i++;
                if (i==MAX_TANKS)
                { //We can not have more than MAX_TANKS while allowing the user to chose.
                    break;
                }
            }
            if (spinnerDeviceSelectionAdapter!=null)
            {
                spinnerDeviceSelectionAdapter.notifyDataSetChanged();
            }
        }
    }

    public static DeviceObject identifySelectedDevice()
    {
        DeviceObject dvObj = null;
        if (MainActivity.getPairedDevicesList() != null)
        {
            for (Object dvObject: MainActivity.getPairedDevicesList())
            {
                //dvObject = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                //dvObj.setCallback(this , getActivity());
                //connected status
                if (mac==null || mac.equals("NA"))
                {
                    mac = ((DeviceObject)MainActivity.pairedDevicesList.get(0)).deviceAddress;
                }

                if (dvObject != null && ((DeviceObject)dvObject).deviceAddress.equals(mac))
                {
                    //Select this device:
                    dvObj = ((DeviceObject)dvObject);
                }
            }
        }
        mSelectedDevice = dvObj;
        if (mSelectedDevice!=null)
        {
            mac = mSelectedDevice.deviceAddress;

            DeviceObject dvObj2 = null;
            Handler mainHandler = new Handler(MainActivity.mContext.getMainLooper());
            Runnable myRunnable = new Runnable()
            {

                @Override
                public void run() {
                    mAnalyticsFrag.reInitEventsList();
                    mLastEventsFrag.reInitEventsList();

                    mAlarmsFrag.reInitAlarmsList();
                    mLastAlarmsFrag.reInitAlarmsList();

                    refreshCurrentFragment();

                } // This is your code
            };
            mainHandler.post(myRunnable);

        }
        return dvObj;
    }

    public static String getDeviceLocalNameFromMac(String macAddress)
    {
        String deviceLocalName = "MotoStar";
        int i =0;
        for ( String mac : devicesMacs)
        {
            if (mac==null)
            {
                break;
            }
            if (mac.equals(macAddress))
            {
                deviceLocalName = devicesNames[i];
                break;
            }
            i++;
        }
        return deviceLocalName;
    }

    public static String getDeviceMacFromLocalName(String deviceLocalName)
    {
        String mac = null;
        int i =0;
        for ( String name : devicesNames)
        {
            if (name.equals(deviceLocalName))
            {
                mac = devicesMacs[i];
                break;
            }
            i++;
        }
        return mac;
    }

    public static void setDeviceMacFromLocalName(String mac, String deviceLocalName)
    {
        //String mac = null;
        int i =0;
        for ( String macaddress : devicesMacs)
        {
            if (macaddress.equals(mac))
            {
                devicesNames[i] = deviceLocalName;
                break;
            }
            i++;
        }

        //return mac;
    }

    public static DeviceObject getSelectedDevice()
    {
        if (mSelectedDevice== null)
        {
            mSelectedDevice  = identifySelectedDevice();
        }
        return mSelectedDevice;
    }

    public static void refreshConnectionStatus() {
        //int fgNumber = mTabPageAdapter.getCurrentFragmentNumber();
        //if (fgNumber ==1)
        ((MainActivity) (mainActivity)).updateConnectionView(mSelectedDevice);
    }

    public static void refreshCurrentFragment()
    {
        //int fgNumber = mTabPageAdapter.getCurrentFragmentNumber();
        //if (fgNumber ==1)
        ((MainActivity)(mainActivity)).updateConnectionView(mSelectedDevice);
        mControlsFrag.refresh();
        mProfileFrag.refresh();
        mAnalyticsFrag.refresh();
        mLastEventsFrag.refresh();
        mAlarmsFrag.refresh();
        mLastAlarmsFrag.refresh();
    }

    public void startNextFragment()
    {
        Log.d(FILENAME, "startNextFragment");
        //here first read - if we need to go to welcome or to createpagingView
        DataSource ds = new DataSource(getApplicationContext());
        ds.open();
        List<MyPhoneRecord> reqs = ds.getMyPhoneRecords();
        ds.close();
        if (reqs == null || reqs.size() == 0) {
            createWelcomeFragment(

            );
        }
        else //It means user has atleast entered his phone number.
        {
            for (MyPhoneRecord _req : reqs) {
                phone = _req.getPhone();
                mac = _req.getMac();
                phoneName = _req.getPhone_name();
                os = _req.getOs();
                sdk = (int) _req.getSdk();
                motoVersion = _req.getMotoVersion();
                serverUpdateRequired = (int) _req.getServerUpdateRequired();
                Log.d(FILENAME, "phone " + phone);
            }

            RegistrationInfo regInfo = new RegistrationInfo(phone, "");
            pWelcomeFrag.regInfo = regInfo;
            //pWelcomeFrag.startAutoLogin();


            ds.open();
            List<DevicesRecords> devices = ds.getDeviceRecords();
            ds.close();
            Log.d(FILENAME, "MotoStar present in db : " + devices.size());

            if (devices == null || devices.size() == 0) {
                Log.d(FILENAME, "Will create Connection Fragment");
                //It means it has never been connected to motostar. Take it to connectivity screen.
                hideFragment(TAG_WELCOME);
                createConnectionsView();
            }
            else //It means - user has connected to motostar previously. So we can move to paging view
            {
                if (mTabPageAdapter.IsTabsPagerAdapterAdded()==false)
                {
                    Log.d(FILENAME, "Will create Paging Fragment");
                    //initBluetoothConnection();
                    enableBluetooth();
                    //it means it was connected to motostar. However, it seems that
                    hideFragment(TAG_WELCOME);
                    hideFragment(TAG_CONNECTIONS);


                    createPagingView();
                    populateDeviceSelectSpinner();
                    onNavigateToPagingView();
                }
                else
                {
                    populateDeviceSelectSpinner();
                    hideFragment(TAG_CONNECTIONS);
                    showPagingView();
                }
                /*
                else
                {
                   // Log.d(FILENAME, "refresing  paired devices");
                    //may be just a refresh is required:
                    //refreshPairedDevices(null, true);
                    hideFragment(TAG_WELCOME);
                    hideFragment(TAG_CONNECTIONS);
                    createPagingView();
                }*/
            }
        }
    }

    public void createWelcomeFragment()
    {
        Log.d(FILENAME, "createWelcomeFragment");
        if (pWelcomeFrag.getIsFragmentAlreadyAdded() == true)
        {
            showFragment(TAG_WELCOME);
            return;
        }
        android.app.FragmentManager managerF = getFragmentManager();
        if (managerF != null) {

            initFragment(R.id.welcome, pWelcomeFrag, TAG_WELCOME, true, false);
            pWelcomeFrag.setFragmentAlreadyAdded(true);
        }
    }

    public void createConnectionsView()
    {
        Log.d(FILENAME, "createConnectionsView");
        if (mConnectionsFrag.getIsFragmentAlreadyAdded() == true)
        {
            showFragment(TAG_CONNECTIONS);
            return;
        }
        android.app.FragmentManager managerF = getFragmentManager();
        if (managerF != null) {

            initFragment(R.id.connections, mConnectionsFrag, TAG_CONNECTIONS, true, false);
            mConnectionsFrag.setFragmentAlreadyAdded(true);
            hideFragment(TAG_WELCOME);
            showFragment(TAG_CONNECTIONS);
        }
    }


    //boolean isPagingViewCreated = false;
    public void createPagingView()
    {
        Log.d(FILENAME, "createPagingView");
        DataSource db = new DataSource(getApplicationContext());
        db.open();
        List<MyPhoneRecord> reqs = db.getMyPhoneRecords();
        db.close();

        for (MyPhoneRecord _req : reqs) {
            phone = _req.getPhone();
            mac = _req.getMac();
            phoneName = _req.getPhone_name();
            os = _req.getOs();
            sdk = (int) _req.getSdk();
            motoVersion = _req.getMotoVersion();
            serverUpdateRequired = (int) _req.getServerUpdateRequired();
            Log.d(FILENAME, "phone " + phone);
        }

        if (mac==null)
        {
            if (actionBar!=null)
            {
                actionBar.setSubtitle(mac);
            }
        }
        if (mTabPageAdapter.IsTabsPagerAdapterAdded()==false)
        {
            mTabPageAdapter.setTabsPagerAdapterAdded(true);
        }
        else
        {
            return;
        }
        hideFragment(TAG_WELCOME);
        //hideFragment(TAG_CONNECTIONS);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(mTabPageAdapter);

        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabsStrip.setViewPager(viewPager);
       // mControlsFrag.refresh();

        /*

        Spinner tankSelector = (Spinner) findViewById(R.id.spinSelectTank);
        tankSelector.setOnItemSelectedListener(this);

        spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.tankNames, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        tankSelector.setAdapter(spinnerAdapter);
        */
    }

    void removePagingView()
    {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(null);

        //PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        //tabsStrip.setViewPager(viewPager);

    }

    void hidePagingView()
    {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setVisibility(View.INVISIBLE);
        //PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        //tabsStrip.setViewPager(viewPager);
    }
    void showPagingView()
    {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setVisibility(View.VISIBLE);
        //PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        //tabsStrip.setViewPager(viewPager);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Log.d(FILENAME, "OnStart");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Log.d(FILENAME, "OnStop");
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        //restore Sleeptimers
        restoreSleepTimers();
        Log.d(FILENAME, "OnResume");
    }

    @Override
    protected void onPause() {

        // TODO Auto-generated method stub
        //Increase sleep timers:

        super.onPause();
        increaseSleepTimers();
        Log.d(FILENAME, "onPause");
    }




    public static List getPairedDevicesList()
    {
        return pairedDevicesList;
    }

    public boolean enableBluetooth()
    {
        Log.d(FILENAME, "enableBluetooth");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(FILENAME, "Bluetooth not supported");
            //textview1.append("device not supported");
            CharSequence text = "Bluetooth not supported";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(mContext, text, 15);
            toast.show();
            return false;
        }
        else
        {
            if (!mBluetoothAdapter.isEnabled())
            {
                if (mContext != null && mReceiver != null) {
                    if (isBtAdapterRegistered == true)
                    {
                        //mContext.unregisterReceiver(mReceiver);
                        isBtAdapterRegistered = false;
                    }
                }
                Log.d(FILENAME, "Bluetooth is not enabled, will attempt to enable it");
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                mBluetoothAdapter.enable();
            }
            else
            {
                Log.d(FILENAME,"Bluetooth is already enabled");
                initBluetoothConnection();
            }
            return  true;
        }
    }

    /* It is called when an activity completes.*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d(FILENAME, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            //CheckBluetoothState();
            Log.d(FILENAME, "REQUEST_ENABLE_BT received: resultCode :" + resultCode);
            Log.d(FILENAME, " data :" + data);
            if (resultCode==RESULT_OK)
            {
                Log.d(FILENAME, "bluetooth enabled successfully, now proceed with registration and discovery");
                initBluetoothConnection();
            }
            else
            {
                Log.d(FILENAME, "bluetooth enabled failed");
            }
        }
        else if (requestCode == REQUEST_DISCOVERABLE_BT)
        {
            Log.d(FILENAME, "REQUEST_DISCOVERABLE_BT : resultCode :" + resultCode);
            Log.d(FILENAME, "data :" + data);
        }
    }

    //private OnFragmentInteractionListener mListener;
    public void initBluetoothConnection()
    {
        Log.d(FILENAME, "initBluetoothConnection");
        if (arrayListPairedBluetoothDevices == null) {
            arrayListPairedBluetoothDevices = new ArrayList<BluetoothDevice>();
        }

        if ( isBtAdapterRegistered == false)
        {
            accessLocationPermission();

            registerReceiver();

            startDiscover();
        }
        else
        {
            //Let's do this only in case of forced restart
        }

        if (isReadThreadRunning == false)
        {
            readThread();
        }
        /*
        if (isMonitoringThreadRunning == false)
        {
            isMonitoringThreadRunning = true;
            monitoringConnectivityThread();
        }
        */
        //refreshPairedDevices();
    }


    private final int REQUEST_CODE_LOC = 11;

    private void accessLocationPermission() {
        Log.d(FILENAME, "accessLocationPermission");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            int accessCoarseLocation = mContext.checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION);
            int accessFineLocation = mContext.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);

            List<String> listRequestPermission = new ArrayList<String>();

            if (accessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (!listRequestPermission.isEmpty()) {
                String[] strRequestPermission = listRequestPermission.toArray(new String[listRequestPermission.size()]);
                requestPermissions(strRequestPermission, REQUEST_CODE_LOC);
            }
        }

    }

    //if discover fails- pairing doesn't returned in getPairedList -even if it was paired.
    //This function is a small heck - to check - if it returns in 10 iterations of discovery?
    static int MAX_DISCOVERY = 10;
    static int discoverCount = MAX_DISCOVERY;
    public void startDiscover()
    {
        Log.d(FILENAME,"startDiscover");
        //Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_);
        //startActivityForResult(enableBtIntent, REQUEST_);
        if (mBluetoothAdapter.isDiscovering())
        //Documentation says - we shall always cancel discovery before initiating the next discovery.
        {
            Log.d(FILENAME, "Discovery already in progress - wait for sometime");
            return;
        }

        Log.d(FILENAME, "Cancelling previous Bluetooth discovery");
        mBluetoothAdapter.cancelDiscovery();

        boolean result = mBluetoothAdapter.startDiscovery();
        if (result == true) {
            Log.d(FILENAME, "Bluetooth discovery returned success");
            //textview2.append("\nDiscovery Started successfully");

            return;
        }
        else
        {
            Log.e(FILENAME, "Bluetooth discovery returned failed");

            /*

            if (discoverCount > 0)
            {
                Log.d(FILENAME, "Re-attempting the bluetooth connection");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Device's Bluetooth have some issue. trying to disable and enable again", Toast.LENGTH_SHORT).show();
                    }
                });
                mBluetoothAdapter.disable();
                mBluetoothAdapter.enable();
                discoverCount--;
            }
            else
            {
                //show a dialog to reboot the phone as Bluetooth is havnig some issue.
                runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        Toast.makeText(mContext, "Device's Bluetooth have some issue. Please restart your phone", Toast.LENGTH_SHORT).show();
                    }
                });
                discoverCount = MAX_DISCOVERY;
            }
            */
        }
    }

    public boolean isBtAdapterRegistered = false;
    public void registerReceiver()
    {
        // Register for broadcasts when a device is discovered.
        Log.d(FILENAME, "registerReceiver");

        if ( isBtAdapterRegistered == false)
        {
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);

            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            filter.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
            filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
            filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
            mContext.registerReceiver(mReceiver, filter);
            isBtAdapterRegistered = true;
        }
    }


    public BlueToothDevicesReceiver mReceiver;

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_LOC:
                if (grantResults.length > 0) {
                    for (int gr : grantResults) {
                        // Check if request is granted or not
                        if (gr != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }
                    //TODO - Add your code here to start Discovery
                }
                break;
            default:
                return;
        }
    }

    //Called for pairing the device - one time only
    public boolean pair(BluetoothDevice bdDevice)
    {
        boolean result = connect(bdDevice);
        refreshPairedDevices(null, true);
        return result;
    }

    //Called for pairing the device - one time only
    public static Boolean connect(BluetoothDevice bdDevice) {
        Boolean bool = false;
        try {
            Log.i("Log", "service method is called ");
            Class cl = Class.forName("android.bluetooth.BluetoothDevice");
            Class[] par = {};
            Method method = cl.getMethod("createBond", par);
            Object[] args = {};
            bool = (Boolean) method.invoke(bdDevice);//, args);// this invoke creates the detected devices paired.
            //Log.i("Log", "This is: "+bool.booleanValue());
            //Log.i("Log", "devicesss: "+bdDevice.getName());
        } catch (Exception e) {
            Log.i("Log", "Inside catch of serviceFromDevice Method");
            e.printStackTrace();
        }

        return bool.booleanValue();
    }

    public boolean removeBond(BluetoothDevice btDevice)
            throws Exception {
        Class btClass = Class.forName("android.bluetooth.BluetoothDevice");
        Method removeBondMethod = btClass.getMethod("removeBond");
        Boolean returnValue = (Boolean) removeBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }

    public boolean createBond(BluetoothDevice btDevice)
            throws Exception {
        Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
        Method createBondMethod = class1.getMethod("createBond");
        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }
    @Override
    protected void onDestroy()
    {
        Log.d(FILENAME, "onDestroy");
        // Don't forget to unregister the ACTION_FOUND receiver.
        if (mContext != null && mReceiver != null) {
            if (isBtAdapterRegistered == true)
            {
                mContext.unregisterReceiver(mReceiver);
                isBtAdapterRegistered = false;
            }
        }
        super.onDestroy();
    }

    protected void deregisterBT()
    {
        Log.d(FILENAME, "deregisterBT");
        // Don't forget to unregister the ACTION_FOUND receiver.
        if (mContext != null && mReceiver != null) {
            if (isBtAdapterRegistered == true)
            {
                mContext.unregisterReceiver(mReceiver);
                isBtAdapterRegistered = false;
            }
        }
        //super.onDestroy();
    }


    boolean addedNew = false;
    public void refreshPairedDevices(BluetoothDevice newlyPairedDevice, boolean isPaired)
    {
        Log.d(FILENAME, "refreshPairedDevices");
        if (mBluetoothAdapter==null)
        {
            return;
        }
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        Log.d(FILENAME, "getBondedDevices returned size " + pairedDevices.size());
        arrayListPairedBluetoothDevices.clear();

        if (pairedDevices.size()==0 && newlyPairedDevice==null)
        {
            return;
        }

        else if (pairedDevices.size() > 0)
        {

            for (BluetoothDevice device : pairedDevices) {
                //arrayListpaired.add(device.getName()+"\n"+device.getAddress());
                Log.d(FILENAME, device.getName() + "\n" + device.getAddress());
                arrayListPairedBluetoothDevices.add(device);
                addedNew = addBtPairedDevice(device, true);
            }
            Log.e(FILENAME, "pairedDevicesList size" + pairedDevicesList.size());
        }
        else if (newlyPairedDevice!=null)
        {
            arrayListPairedBluetoothDevices.add(newlyPairedDevice);
            addedNew = addBtPairedDevice(newlyPairedDevice, isPaired);
            Log.e(FILENAME, "pairedDevicesList size" + pairedDevicesList.size());
        }

        /*
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionsFrag.update();
                if (addedNew == true) {
                    addedNew = false;
                    startNextFragment();
                }
            }
        });
        */
    }

    private boolean addBtPairedDevice(BluetoothDevice device, boolean isPaired)
    {
        boolean addedNew = false;
        Log.d(FILENAME, "addBtPairedDevice");
        try
        {
            //This is local mac
            String mac = device.getAddress();
            if (isItMotoStar(device.getName())==false)
            {
                Log.d(FILENAME, "This is not MotoStar - ignore it :: " + device.getName() + " :: " + device.getAddress());
                return false;
            }
            Log.d(FILENAME, "Adding MotoStar to PairedDevicesList :: " + device.getName() + " :: " + device.getAddress());
            //here we need to update it in db as well.
            DeviceObject deviceObject = new DeviceObject(getApplicationContext(), device.getName(), device.getAddress(), null, device, isPaired);
            addedNew = checkAndAddDeviceinPairedDevicesList (device.getAddress(), deviceObject);
            if(addedNew)
            {
                updateDeviceRecordinDb(deviceObject, device);
            }
            else
            {
                deviceObject=null;
            }
        }
        catch (Exception e)
        {
            Log.d(FILENAME, "Exception in getting socket to device" + e.getMessage());
        }
        return addedNew;
    }

    boolean deviceUpdateInProgress= false;
    private void updateDeviceRecordinDb(DeviceObject deviceObject, BluetoothDevice device)
    {
        //Also check in db - if this device is already present in db or not. If not - add the same in db.
        DataSource ds = new DataSource(mContext);
        ds.open();

        List<DevicesRecords> devicerecords = ds.getDeviceRecords();
        ds.close();
        if (devicerecords == null)
        {
            //do nothing
        }
        else //It means user has atleast entered his phone number.
        {
            boolean deviceRecordExists = false;
            for (DevicesRecords _req : devicerecords)
            {
                if ( _req.getMac().equals(device.getAddress()))
                {
                    deviceRecordExists = true;
                }
            }
            if (deviceRecordExists==false && deviceObject.isNameRequestedFromUser==false)
            {
                //inser the record - but before inserting the record - let's get the Local DeviceName from the user
                deviceObject.isNameRequestedFromUser = true;
                getDeviceLocalNameFromUserAndStoreDeviceRecord(deviceObject, deviceObject.deviceAddress);
            }
        }
    }


    public void makeDeviceDiscoverable() {
        Log.d(FILENAME, "makeDeviceDiscoverable");
        if ( mBluetoothAdapter==null)
        {
            Log.e(FILENAME, "bluetooth adapter is null");
            return;
        }
        if (!mBluetoothAdapter.isDiscovering())
        {
            Log.d(FILENAME,"MAKING YOUR DEVICE DISCOVERABLE");
//          out.append("MAKING YOUR DEVICE DISCOVERABLE");
            //Context context = getApplicationContext();
            CharSequence text = "MAKING YOUR DEVICE DISCOVERABLE";
            int duration = Toast.LENGTH_SHORT;
            //Toast toast = Toast.makeText(mContext, text, duration);
            //toast.show();
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(discoverableIntent, REQUEST_DISCOVERABLE_BT);
        }
    }

    public boolean isReadThreadRunning = false;

    long sleepTime = 10000; //1 seconds for now
    long sleepTimeMin = 5000; //1 seconds for now
    long sleepTimedefault = 5000;
    //long periodicPollTime = 20000;
    //long lastpollTimeCounter =0;
    long periodicLastEventsUpdateTime = 500000; //500 seconds for now: 10 minutes
    long totalTimeSinceLastEventsUpdateTime = periodicLastEventsUpdateTime +1;
    long periodicRefreshTime = 20000;
    long totalTimeSinceLastRefresh = periodicRefreshTime + 1;

    public void increaseSleepTimers()
    {
        sleepTimeMin = sleepTimedefault*4;
        sleepTime = sleepTimeMin;
        DeviceObject.increaseSleepTimers();
    }

    public void restoreSleepTimers()
    {
        sleepTimeMin = sleepTimedefault;
        sleepTime = sleepTimeMin;
        DeviceObject.restoreSleepTimers();
    }

    private void readThread() {
        new Thread() {
            @Override
            public void run() {

                String readBuffer = new String();
                isReadThreadRunning = true;
                try {

                    Thread.sleep(sleepTime);
                } catch (Exception e) {
                    Log.e(FILENAME, e.getMessage());
                }

                while (true) //infinite monitoring thread:
                {
                    Log.d(FILENAME, "pairedDevicesList.size " + pairedDevicesList.size());
                    if( (mConnectionsFrag!=null && mConnectionsFrag.fragmentAlreadyAdded == true) ||
                            mTabPageAdapter.isTabsPagerAdapterAdded==true)
                    {
                        enableBluetooth();
                    }
                    if (pairedDevicesList.size()==0 )
                    {
                        sleepTime = sleepTimeMin;
                        // It means there is some device which was paired earlier -as db entry is present -
                        // but now device is unpaired manually from bluetooth settings
                        //So - here need to check if device exists in scanlist or not.
                        //If device exists in scanlist then attempt auto-pairing request.
                        PairRemovedDevices();
                        if (pairedDevicesList.size()!=0)
                        {
                            sleepTime = sleepTimeMin;
                        }
                    }
                    else //if ( pairedDevicesList.size()> 0)
                    {
                        sleepTime = sleepTimeMin;
                        totalTimeSinceLastRefresh += sleepTime;
                        if (totalTimeSinceLastRefresh > periodicRefreshTime)
                        {
                            //Performance Case: CAn be optimized. It shall be refreshed less frequently and on the basis of notifications only
                            if (mTabPageAdapter.isTabsPagerAdapterAdded == true) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mControlsFrag.refresh();
                                    }
                                });
                            }
                            totalTimeSinceLastRefresh = 0;
                        }

                        synchronized (pairedDevicesList)
                        {
                            //Log.e(FILENAME, "5 pairedDevicesListSize " + pairedDevicesList.size());
                            for (Object deviceObject : pairedDevicesList)
                            {
                                if (deviceObject != null)
                                {
                                    /*
                                    ((DeviceObject) (deviceObject)).setPaired(true);
                                    //if (((DeviceObject) (deviceObject)).socket != null && ((DeviceObject) (deviceObject)).socket.isConnected())
                                    if (((DeviceObject) (deviceObject)).socket != null)
                                    {
                                        ((DeviceObject) (deviceObject)).periodicConnectionCheck();
                                        readBuffer = ((DeviceObject) (deviceObject)).read();
                                        if (readBuffer != null) {
                                            //Process the readBuffer
                                            Log.d(FILENAME, readBuffer);

                                        }
                                    }
                                    Log.e(FILENAME, "deviceObject " + ((DeviceObject) (deviceObject)).deviceAddress);
                                    if (lastpollTimeCounter>= periodicPollTime)
                                    {

                                        ((DeviceObject) (deviceObject)).periodicPoll(); //Periodic poll - itself skips 3 out of 4 attempts. Means it will be polled only 1 time out of 5 attempts:
                                        lastpollTimeCounter =0;
                                    }
                                    */
                                    if (totalTimeSinceLastEventsUpdateTime>=periodicLastEventsUpdateTime)
                                    {
                                        //Conditional - send last events to server::
                                        ((DeviceObject) (deviceObject)).encodeAndPostLastEvents();

                                        //Performance optimization needed - for following code
                                        //if network is connected
                                        if (ConnectivityReceiver.isConnected(mContext)==true)
                                        {
                                            checkAndPostVersionInfo((DeviceObject)deviceObject);
                                            checkAndPostmotoPhoneMap((DeviceObject)deviceObject);
                                        }
                                    }
                                }
                            }
                            if (totalTimeSinceLastEventsUpdateTime >= periodicLastEventsUpdateTime)
                            {
                                totalTimeSinceLastEventsUpdateTime=0;
                            }
                            else
                            {
                                totalTimeSinceLastEventsUpdateTime += sleepTime;
                            }
                        }
                    }
                    try
                    {
                        //lastpollTimeCounter +=sleepTime;
                        Thread.sleep(sleepTime);
                    }
                    catch (Exception e)
                    {
                        Log.e(FILENAME, e.getMessage());
                    }
                }
            }
        }.start();
    }


    /*
    public boolean isMonitoringThreadRunning = false;
    private void monitoringConnectivityThread() {
        new Thread() {
            @Override
            public void run() {
                long sleepTime = 3000; //3 seconds for now:
                while (true) //infinite monitoring thread:
                {
                    //if network is connected
                    if (ConnectivityReceiver.isConnected(mContext)==true) {
                        //if it requires to be updated to server then send to server and update in db.
                        checkAndUpdateVersionInfo(); //- it is currently trying to update it multiple times::
                    }
                    synchronized (pairedDevicesList) {
                        Log.e(FILENAME, "monitoringConnectivityThread pairedDevicesList.size " + pairedDevicesList.size());
                        for (Object deviceObject : pairedDevicesList)
                        //for (DeviceObject deviceObject : pairedDevicesList)
                        {
                            Log.e(FILENAME, "5 pairedDevicesListSize " + pairedDevicesList.size());
                            Log.e(FILENAME, "deviceObject " + ((DeviceObject) (deviceObject)).deviceAddress);
                            ((DeviceObject) (deviceObject)).periodicPoll();
                        }
                    }
                    try {
                        Thread.sleep(sleepTime);
                    } catch (Exception e) {
                        Log.e(FILENAME, e.getMessage());
                    }
                }
            }
        }.start();
    }
    */

    public void PairRemovedDevices()
    {
        Log.d(FILENAME, "PairRemovedDevices");
        DataSource ds = new DataSource(mContext);
        ds.open();
        List<DevicesRecords> devicerecords = ds.getDeviceRecords();
        ds.close();
        if (devicerecords == null)
        {
            //do nothing
            return;
        }
        Set<BluetoothDevice>pairedDevices=mBluetoothAdapter.getBondedDevices();
        Log.d(FILENAME, "getBondedDevices size" + pairedDevices.size());
        for(DevicesRecords _req:devicerecords)
        {
            boolean isDevicePaired = false;
            if (pairedDevices.size() > 0)
            {
                for (BluetoothDevice pairedDev : pairedDevices) {
                    if (pairedDev.getAddress().equals(_req.getMac())) {
                        isDevicePaired = true;

                        DeviceObject deviceObject = new DeviceObject(getApplicationContext(), _req.getDeviceName(), _req.getMac(), null,pairedDev, true);
                        checkAndAddDeviceinPairedDevicesList (_req.getMac(), deviceObject);
                        break;
                    }
                }
                if (isDevicePaired == true) {
                    continue; //check for next device in db.
                }
            }
            if (isDevicePaired==false) //device is not paired - check - if it is present in scan list
            {
                //boolean isDeviceScanned = false;
                if (mReceiver.tempScanDevicesObjList.size() > 0)
                {
                    for (int i = 0; i < mReceiver.tempScanDevicesObjList.size(); i++)
                    {
                        DeviceObject scannedDevice = (DeviceObject) mReceiver.tempScanDevicesObjList.get(i);
                        if (_req.getMac().equals(scannedDevice.deviceAddress))
                        {
                            Log.d(FILENAME, "Attempted to Pair with device " + scannedDevice.btDevice.getName() + "::: " + scannedDevice.btDevice.getAddress());
                            //pair the device
                            //means it can be paired
                            if (connect(scannedDevice.btDevice) == true)
                            {
                                Log.d(FILENAME, "Connect Success");
                                checkAndAddDeviceinPairedDevicesList (_req.getMac(), scannedDevice);
                            }
                            else
                            {
                                Log.d(FILENAME, "Connect Failure");
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    boolean checkAndAddDeviceinPairedDevicesList(String mac, DeviceObject dvObj)
    {
        boolean result = false;
        boolean isDevicePresentinPairedDeviceList = false;
        //Also find if device is there in pairDevicesList or not. If not - then it shall be added here:
        for(Object listDevice : pairedDevicesList)
        {
            if (((DeviceObject)listDevice).deviceAddress.equals(mac))
            {
                isDevicePresentinPairedDeviceList = true;
                break;
            }
        }
        if (isDevicePresentinPairedDeviceList==false)
        {
            result = true;
            pairedDevicesList.add(dvObj);
            dvObj.initThread();
        }
        return result;
    }

    static void checkAndPostmotoPhoneMap(DeviceObject dvObj) {
        Log.d(FILENAME, "checkAndUpdatemotoPhoneMap");
        if (dvObj == null)
        {
            return;
        }
        //Also check in db - if this device is already present in db or not. If not - add the same in db.
        DataSource ds = new DataSource(mContext);
        ds.open();

        List<DevicesRecords> devicerecords = ds.getDeviceRecords();
        ds.close();
        if (devicerecords == null) {
            //do nothing
        }
        else //It means user has atleast entered his phone number.
        {
            boolean motoPhoneMapUpdateRequired = false;
            for (DevicesRecords _req : devicerecords)
            {
                if (_req.getMac().equals(dvObj.deviceAddress))
                {
                    if (_req.getServerUpdateRequired() == 1) {
                        motoPhoneMapUpdateRequired = true;
                        if (motoPhoneMapUpdateRequired == true) {
                            dvObj.encodeAndPostmotoPhoneMap();
                        }
                    }
                    break;
                }
            }
        }
    }

    static void checkAndPostVersionInfo(DeviceObject dvObj) {
        Log.d(FILENAME, "checkAndUpdateVersionInfo");

        if (serverUpdateRequired == 1) //update required
        {
            if (dvObj == null) {
                return;
            }
            String jsonString = dvObj.encodeDeviceInfoJson();
            postPhoneRegister(jsonString);
        }
    }


    public static void postLiveEvent(String jsonString)
    {
        Log.d(FILENAME, "postLiveEvent");
        String jsonPath = "http://" + ConnectivityReceiver.BASE_URI + ConnectivityReceiver.PATH_LIVEVENT;
        postToServer(jsonString, jsonPath, ConnectivityReceiver.PATH_LIVEVENT, false);

    }

    public static void postLastEvents(String jsonString)
    {
        Log.d(FILENAME, "postLastEvents");
        String jsonPath = "http://" + ConnectivityReceiver.BASE_URI + ConnectivityReceiver.PATH_LASTVENT;
        postToServer(jsonString, jsonPath, ConnectivityReceiver.PATH_LASTVENT, false);
    }

    public static void postProfile(String jsonString)
    {
        Log.d(FILENAME, "postProfile");
        String jsonPath = "http://" + ConnectivityReceiver.BASE_URI + ConnectivityReceiver.PATH_PROFILE;
        postToServer(jsonString, jsonPath, ConnectivityReceiver.PATH_PROFILE, false);
    }

    public static void postDeviceInfo(String jsonString)
    {
        Log.d(FILENAME, "postDeviceInfo");
        String jsonPath = "http://" + ConnectivityReceiver.BASE_URI + ConnectivityReceiver.PATH_MOTOPHONEMAP;
        String subPath =  ConnectivityReceiver.PATH_MOTOPHONEMAP;
        postToServer(jsonString, jsonPath, subPath, false);
    }

    public static void postPhoneRegister(String jsonString)
    {
        Log.d(FILENAME, "postPhoneRegister");
        String jsonPath = "http://" + ConnectivityReceiver.BASE_URI + ConnectivityReceiver.PATH_VERSION;
        String subPath =  ConnectivityReceiver.PATH_VERSION;
        postToServer(jsonString, jsonPath, subPath, false);
    }


    public static void postToServer(String jsonString, String jsonPath, final String subPath, final boolean reportedByCustomer)
    {
        Log.d(FILENAME, "postLastEvents");
        if (ConnectivityReceiver.isConnected(mContext) == true)
        {
            if (ConnectivityReceiver.isConnected(mContext))
            {
                AsyncHttpClient client = new AsyncHttpClient();
                client.removeHeader("Content-Type");
                client.addHeader("Content-Type", "application/json;charset=UTF-8");
                client.addHeader("Accept", "application/json");
                StringEntity se = null;
                try
                {
                    se = new StringEntity(jsonString);
                    client.post(mContext, jsonPath, se, "application/json", new
                            AsyncHttpResponseHandler()
                            {
                                @Override
                                public void onSuccess(int statusCode, org.apache.http.Header[] headers,
                                                      byte[] responseBody)
                                {
                                    //Log.d(FILENAME, "http onSuccess: " + responseBody);
                                    //try {
                                    // JSON Object
                                    HashMap<String, String> hmap = new HashMap<String, String>();
                                    for (org.apache.http.Header h : headers) {
                                        hmap.put(h.getName(), h.getValue());
                                        if (h.getName().equals("Content-Length")) {
                                            if (h.getValue().equals("0")) {
                                                Log.d(FILENAME, "successfully response");
                                            }
                                        }
                                    }


                                    //serverUpdateRequired = 2;//update done..
                                    // update db as well.

                                    if (subPath.equals(ConnectivityReceiver.PATH_MOTOPHONEMAP))
                                    {
                                        DataSource ds = new DataSource(mContext);
                                        ds.open();
                                        ds.updateDeviceRecord(mac, 2);
                                        ds.close();
                                    }
                                    if (subPath.equals(ConnectivityReceiver.PATH_VERSION))
                                    {
                                        serverUpdateRequired = 2;//update done..
                                        // update db as well.
                                        DataSource ds = new DataSource(mContext);
                                        ds.open();
                                        ds.updateMyPhoneRecord(phone, serverUpdateRequired);
                                        ds.close();
                                    }
                                    if (reportedByCustomer==true)
                                    {
                                        if (subPath.equals(ConnectivityReceiver.PATH_CUSTOMINCEDENT))
                                        {
                                            //Inform User - Problem submitted successfully.
                                            CreateMessageSubmittedDialog("Issue has been reported successfully. Someone will get back to you in maximum 24 hours", mainActivitythis);
                                        }
                                    }
                                }
                                // When the response returned by REST has Http response code other than '200'
                                @Override
                                public void onFailure(int statusCode, org.apache.http.Header[] headers,
                                                      byte[] responseBody, Throwable
                                                              error)
                                {
                                    // Hide Progress Dialog
                                    //prgDialog.hide();
                                    Log.d(FILENAME, "http onFailure");
                                    if (reportedByCustomer==true) {
                                        if (subPath.equals(ConnectivityReceiver.PATH_CUSTOMINCEDENT)) {
                                            //Inform User - Problem submitted successfully.
                                            CreateMessageSubmittedDialog("There is some issue while reporting the problem. Check your internet connection and try again", MainActivity.mContext);
                                        }
                                    }
                                    // When Http response code is '404'
                                    if (statusCode == 404) {
                                        Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                                    }
                                    // When Http response code is '404'
                                    else if (statusCode == 409) {
                                        Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                                    }
                                    // When Http response code is '500'
                                    else if (statusCode == 500) {
                                        Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                                    }
                                    // When Http response code other than 404, 500
                                    else {
                                        Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                                    }
                                }
                            });
                }
                catch (UnsupportedEncodingException e)
                {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    void customerIssueReport(String problemTitle, String problemDescription, boolean reportedByCustomer)
    {
        Log.d(FILENAME, "customerIssueReport");
        //if ( serverUpdateRequired==1) //update required
        {
            try {
                //long differeneLat = Math.abs(latitude - mMyOldLat);
                //long differnceLong = Math.abs(longitude - mMyOldLong);
                String epochtime;
                JSONObject json = new JSONObject();

                json.put("mac", mac);
                json.put("phone", MainActivity.phone);
                json.put("DeviceName", MainActivity.phoneName);
                json.put("reported_date", TimeClock.get_date_month_year());
                json.put("reported_time", TimeClock.get_hr_min_sec());
                json.put("reportDate", TimeClock.getDayOfMonth());
                json.put("reportMonth", TimeClock.getMonth());
                json.put("reportYear", TimeClock.getyear());
                json.put("reportHour", TimeClock.getHourofDay());
                json.put("reportMinute", TimeClock.getMinute());
                json.put("reportSecond", TimeClock.getMonth());

                json.put("Os", MainActivity.os);
                json.put("Sdk", MainActivity.sdk);
                json.put("appVersion", DatabaseHelper.APP_VERSION);
                json.put("motoVersion", MainActivity.motoVersion);
                json.put("ProblemTitle", problemTitle);
                json.put("ProblemDescription", problemDescription);
                json.put("assignedTo", "New");
                json.put("LastUpdate", "");
                json.put("Remarks", "");
                String jsonString = json.toString();

                String jsonPath = "http://" + ConnectivityReceiver.BASE_URI + ConnectivityReceiver.PATH_CUSTOMINCEDENT;
                if (ConnectivityReceiver.isConnected(mContext))
                {
                    postToServer(jsonString, jsonPath,ConnectivityReceiver.PATH_CUSTOMINCEDENT, reportedByCustomer );
                }
                else
                {
                    //AlertDialog
                    createConnectivityAlertDialog();
                }
            }
            catch(Exception e)
            {
                Log.e(FILENAME,e.getMessage());
            }
        }
    }

    private ActionBar actionBar;
    public void showActionBar() {
        actionBar.show();
    }
    public void hideActionBar() {
        actionBar.hide();
    }

    public void showMenu() {
        showActionBar();
        //setDefauleMenu(false);
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }*/
    }

    private Menu mMenu;
    //private MenuItem mProfileButton;
    //private MenuItem mControlsButton;

    private MenuItem mIsAnnouncement;
    private MenuItem mConnections;
    private MenuItem mActionsSupport;
    //private MenuItem mConnectionsButton;
    //private MenuItem mAnalyticsButton;
    //private MenuItem mlastEventsButton;

    private MenuItem mPhoneButton;
    private MenuItem mTimeSyncButton;
    private MenuItem mRenameButton;
    private MenuItem mReset;
    private MenuItem mTutorial;
    public static boolean mDefaultMenu = true;

    public static void setDefauleMenu(boolean flag) {
        mDefaultMenu = flag;
    }

    //@Override
    public void activateDefaultMenu() {
        //hideActionBar();
        setDefauleMenu(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }
    }

    //@Override
    public void activateSelectedMenu() {
        //hideActionBar();
        setDefauleMenu(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        mMenu = menu;

        //mProfileButton = mMenu.findItem(R.id.action_profile);

        //mControlsButton = mMenu.findItem(R.id.action_controls);

        mIsAnnouncement = mMenu.findItem(R.id.action_announcement);
        mConnections = mMenu.findItem(R.id.connections);

        mActionsSupport = mMenu.findItem((R.id.action_support));
        //mConnectionsButton = mMenu.findItem(R.id.action_connections);
        //mAnalyticsButton = mMenu.findItem(R.id.action_analytics);
        //mlastEventsButton = mMenu.findItem(R.id.action_lastevents);

        mTimeSyncButton = menu.findItem((R.id.action_timesync));
        mRenameButton = menu.findItem(R.id.renameMoto);

        mPhoneButton = menu.findItem(R.id.phonecall);
        mReset = menu.findItem(R.id.action_reset);
        mTutorial = menu.findItem(R.id.tutorial);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        //mProfileButton = mMenu.findItem(R.id.action_profile);
        //mConnectionsButton = mMenu.findItem(R.id.action_connections);
        //mControlsButton = mMenu.findItem(R.id.action_controls);

        //mAnalyticsButton = mMenu.findItem(R.id.action_analytics);
        //mlastEventsButton = mMenu.findItem(R.id.action_lastevents);


        /*
        if (mProfileButton != null) {
            mProfileButton.setVisible(true);
        }

        if (mControlsButton != null) {
            mControlsButton.setVisible(true);
        }
        */

        if (mIsAnnouncement != null) {
            if (isAnnouncementOn()) {
                mIsAnnouncement.setIcon(R.drawable.speakeron);
            } else {
                mIsAnnouncement.setIcon(R.drawable.speakeroff);
            }
        }
        if (mConnections!=null) {
            mConnections.setVisible(true);
        }

        if (mActionsSupport !=null)
        {
            mActionsSupport.setVisible(true);
        }

        /*
        if (mConnectionsButton != null) {
            mConnectionsButton.setVisible(true);
        }

        if (mAnalyticsButton != null) {
            mAnalyticsButton.setVisible(true);
        }

        if (mlastEventsButton != null) {
            mlastEventsButton.setVisible(true);
        }
        */


        if (mPhoneButton !=null)
        {
            mPhoneButton.setVisible(true);
        }

        if (mTimeSyncButton!=null)
        {
            mTimeSyncButton.setVisible(true);
        }

        if (mRenameButton!=null)
        {
            mRenameButton.setVisible(true);
        }


        if (mReset !=null)
        {
            mReset.setVisible(true);
        }
        if (mTutorial!=null)
        {
            mTutorial.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */

        //Just for a Test
        //selectedTank++;
        //if (selectedTank==3)
        //{
        //    selectedTank = 0;
        //}
        Intent intent;
        boolean result = false;
        switch (item.getItemId()) {

            /*
            case R.id.action_profile:
                Vibrate();
                enableBluetooth(); //Move to Profile.. need to fix
                //onNavigateToProfile();
                result = true;
                break;

            case R.id.action_controls:
                //onNavigateToControlView();
                enableBluetooth();
                Vibrate();
                result = true;
                break;
            */

            case R.id.action_announcement:
                Vibrate();
                //delibrate Crash
                //String xyz = null;
                //xyz.length();
                if (mIsAnnouncement != null) {
                    if (isAnnouncementOn()) {
                        mIsAnnouncement.setIcon(R.drawable.speakeroff);
                        setIsAnnouncementOn(false);
                    } else {
                        mIsAnnouncement.setIcon(R.drawable.speakeron);
                        setIsAnnouncementOn(true);
                    }
                }
                break;

            case R.id.action_support:
                //onNavigateToAnalytics();
                Vibrate();
                createCustomIssueReport();
                //createEmailDialog();
                result = true;
                break;


            case R.id.connections:
                Vibrate();
                //refreshPairedDevices(null, true);
                onNavigateToConnections();
                result = true;
                break;


            /*
            case R.id.action_analytics:
                //onNavigateToAnalytics();
                Vibrate();
                result = true;
                break;


            case R.id.action_lastevents:
                //onNavigateToAnalytics();
                Vibrate();
                result = true;
                Intent dbmanager = new Intent(this, AndroidDatabaseManager.class);
                startActivity(dbmanager);
                break;
                */

            case R.id.phonecall:
                Vibrate();

                // User chose the "Settings" item, show the app settings UI...

                createPhoneDialog(); //commented temporarily
                String chard = null;
                //chard.length();



                return true;

            case R.id.renameMoto:
                Vibrate();
                changeDeviceLocalNameAlert();
                result = true;
                break;

            case R.id.action_timesync:
                Vibrate();
                SyncDeviceTime();
                //createResetDialog();
                break;

            case R.id.action_reset:
                Vibrate();
                createResetDialog();
                break;
            case R.id.tutorial:
                Vibrate();
                CreateCommonDialog(tutorialContent);

            default:
                result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    static TextToSpeech t1;

    public void SyncDeviceTime()
    {
        if (mSelectedDevice!=null)
        {
            mSelectedDevice.writeSynch(TimeClock.encodeTime());
        }
    }

    public static void speechInit()
    {
        t1=new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    // t1.setLanguage(Locale.UK);
                }
            }
        });

        if (t1!=null)
        {
            t1.setLanguage(Locale.US);
        }
    }

    public static void announce(String body)
    {
        if (isAnnouncementOn)
        {
            String toSpeak = "MotoStar " + body;
            if (t1 != null && isAnnouncementOn) {
                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    void createResetDialog()
    {
        final CharSequence[] items = {" Yes "," No "};
        // arraylist to keep the selected items
        final ArrayList seletedItems=new ArrayList();
        int inputSelection = -1;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("After Reset You will loose all previous data in app");

        /*
        builder.setSingleChoiceItems(items,inputSelection,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        inputSelection = item;
                        dialog.dismiss();
                    }
                })
           */
                /*
        builder.setSingleChoiceItems(items, null,
                new DialogInterface.OnMultiChoiceClickListener() {
                    // indexSelected contains the index of item (of which checkbox checked)
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected,
                                        boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            // write your code when user checked the checkbox
                            seletedItems.add(indexSelected);
                        } else if (seletedItems.contains(indexSelected)) {
                            // Else, if the item is already in the array, remove it
                            // write your code when user Uchecked the checkbox
                            seletedItems.remove(Integer.valueOf(indexSelected));
                        }
                    }
                })
                */

                // Set the action buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                        //  Your code when user clicked on OK
                        //  You can write the code  to save the selected item here
                        //Just a temporarily code to test the specific scenario:

                        /*
                        DataSource ds = new DataSource(getApplicationContext());
                        ds.open();
                        List<MyRecord> reqs = ds.getMyRecords();
                        if (reqs.size() > 0) {
                            ds.deleteMyRecord(reqs.get(0));
                        }
                        ds.close();
                        */
                        MyPhoneRecord.clearFromDB(mContext);
                        DevicesRecords.clearFromDB(mContext);
                        EventRecord.clearFromDB(mContext);
                        LastEventRecord.clearFromDB(mContext);
                        DevicesLocalNameRecords.clearFromDB(mContext);
                        AlarmRecord.clearFromDB(mContext);
                        LastAlarmRecord.clearFromDB(mContext);
                        pairedDevicesList.clear();
                        //tempScanDevicesObjList.clear();
                        //devicesNames=null;
                        //devicesMacs=null;


                        hideFragment(TAG_CONNECTIONS);
                        //hideFragment(TAG_PROFILES);
                        removePagingView();
                        mTabPageAdapter.setTabsPagerAdapterAdded(false);
                        //mTabPageAdapter.setTabsPagerAdapterAdded(false);
                        //mConnectionsFrag.setFragmentAlreadyAdded(false);
                        //pWelcomeFrag.setFragmentAlreadyAdded(false);
                        //setFragmentAlreadyAdded(false);


                        //Move to welcome fragment - how?
                        startNextFragment();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel

                    }
                });

        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }


    void createConnectivityAlertDialog()
    {
        final CharSequence[] items = {" Yes "," No "};
        // arraylist to keep the selected items
        final ArrayList seletedItems=new ArrayList();
        int inputSelection = -1;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please connect to Internet to report problem or call on phone");
        // Set the action buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id)
            {


                //Move to welcome fragment - how?
                startNextFragment();
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel

                    }
                });

        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }

    public void CreateCommonDialog(String msg)
    {
        final CharSequence[] items = {" Yes "," No "};
        // arraylist to keep the selected items
        final ArrayList seletedItems=new ArrayList();
        int inputSelection = -1;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg);
        // Set the action buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }

    public static void CreateMessageSubmittedDialog(String msg, Context context)
    {
        final CharSequence[] items = {" Yes "," No "};
        // arraylist to keep the selected items
        final ArrayList seletedItems=new ArrayList();
        int inputSelection = -1;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg);
        // Set the action buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }

    void createCustomIssueReport()
    {
        ContextThemeWrapper ctw = new ContextThemeWrapper(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        AlertDialog.Builder alert = new AlertDialog.Builder(ctw);

        //AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);
        alert.setMessage("Describe your Problem here. Someone will get back to you earliest possible.");
        alert.setTitle("Incident Reporting");

        alert.setView(edittext);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //What ever you want to do with the value
                //Editable YouEditTextValue = edittext.getText();
                //OR
                String YouEditTextValue = edittext.getText().toString();
                customerIssueReport(YouEditTextValue, YouEditTextValue, true);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });
        alert.show();
    }

    final int MAXLENTH_DEVICELOCALNAME = 10;
    void getDeviceLocalNameFromUserAndStoreDeviceRecord(final DeviceObject deviceObj, final String mac)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);

        alert.setTitle("Device Name for #" + mac);
        alert.setMessage("Please Give a Name to your MotoStar. Name can be upto 10 characters");

        alert.setView(edittext);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //What ever you want to do with the value
                //Editable YouEditTextValue = edittext.getText();
                //OR
                String deviceLocalName = edittext.getText().toString();

                if (deviceLocalName != null) {
                    if (deviceLocalName.length() > MAXLENTH_DEVICELOCALNAME) {
                        deviceLocalName = deviceLocalName.substring(0, MAXLENTH_DEVICELOCALNAME);
                    }
                    DevicesLocalNameRecords.storeUniqueNameRecord(mContext, deviceLocalName, mac);
                    DevicesRecords.storeUniqueNameRecordNew(mContext, deviceObj.deviceName, mac, PAIRED, (long) (deviceObj.state), (long) serverUpdateRequired);
                    DataSource ds = new DataSource(mContext);
                    ds.open();
                    ds.updateDeviceRecord(mac, 1);
                    ds.close();
                    deviceObj.encodeAndPostmotoPhoneMap();

                    //Here only - initate the chat integration::
                    //inform WelcomeFragment to create group
                    if (pWelcomeFrag!=null)
                    {
                        pWelcomeFrag.initCreateGroup(mac, mContext);
                    }

                    startNextFragment();
                }
                //customerIssueReport(YouEditTextValue,YouEditTextValue, true);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        //String deviceLocalName = getDeviceLocalNameFromMac(mac);
        //if (deviceLocalName==null)
        {
            alert.show();
        }
    }

    void changeDeviceLocalNameAlert()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);

        alert.setTitle("Device ReName - #" + getDeviceLocalNameFromMac(mac));
        alert.setMessage("Please Give a new Name to your old MotoStar -." + getDeviceLocalNameFromMac(mac) + " - " + mac + " Name can be upto 10 characters");

        alert.setView(edittext);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //What ever you want to do with the value
                //Editable YouEditTextValue = edittext.getText();
                //OR
                String deviceLocalName = edittext.getText().toString();

                if (deviceLocalName!=null)
                {
                    if (deviceLocalName.length()>MAXLENTH_DEVICELOCALNAME)
                    {
                        deviceLocalName = deviceLocalName.substring(0,MAXLENTH_DEVICELOCALNAME);
                    }
                    //DevicesLocalNameRecords.updateDeviceRecord(deviceLocalName, mac);
                    //DevicesRecords.storeUniqueNameRecordNew(mContext, deviceObj.deviceName, mac, PAIRED, (long) (deviceObj.state), (long) serverUpdateRequired);
                    DataSource ds = new DataSource(mContext);
                    ds.open();
                    ds.updateDeviceLocalNameRecord(mac, deviceLocalName);
                    ds.close();
                    //update in local array:
                    setDeviceMacFromLocalName(mac, deviceLocalName);
                    //refresh the mainactivity view:
                    populateDeviceSelectSpinner();

                    //deviceObj.encodeAndPostmotoPhoneMap();
                }
                //customerIssueReport(YouEditTextValue,YouEditTextValue, true);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });
        alert.show();
    }


    void createEmailDialog()
    {
        final CharSequence[] items = {" Email "," Phone "};
        // arraylist to keep the selected items
        final ArrayList seletedItems=new ArrayList();
        int inputSelection = -1;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please select Ok & select gmail to contact MotoStar Support");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id)
            {
                //  Your code when user clicked on OK
                //  You can write the code  to save the selected item here
                //Just a temporarily code to test the specific scenario:
                sendEmail();
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel

                    }
                });

        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }
    void createPhoneDialog()
    {
        final CharSequence[] items = {" Email "," Phone "};
        // arraylist to keep the selected items
        final ArrayList seletedItems=new ArrayList();
        int inputSelection = -1;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please select Ok -to call MotoStar Support");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id)
            {
                //  Your code when user clicked on OK
                //  You can write the code  to save the selected item here
                //Just a temporarily code to test the specific scenario:
                userCallSelected(support_Phone);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel

                    }
                });

        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }


    public static boolean isAnnouncementOn = true;

    /*
    public static void setIsAnnouncementOn(boolean flag) {
        isAnnouncementOn = flag;
    }
    */


    public static void setIsAnnouncementOn(boolean flag)
    {

        isAnnouncementOn = flag;
        SharedPreference.getInstance().saveSharedPreferenceSpeaker(MainActivity.mContext, flag);
    }

    public static boolean getSharedPrefIsAnnouncementOn(Context context)
    {
        SharedPreference.getInstance().retreiveSharedPreference(context);
        isAnnouncementOn = SharedPreference.getInstance().getSpeakerOn();
        return isAnnouncementOn;
    }

    public boolean isAnnouncementOn() {
        return isAnnouncementOn;
    }


    public void hideFragment(String tag) {
        Log.d(FILENAME, "hideFragment " + tag);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if ((ft == null) || (fragment == null)) {
            return;
        }
        ft.hide(fragment);
        //ft.addToBackStack(null);
        try {
            ft.commit();
            //TBD; It can crash here due to known implementation in android. Proper solution not found - so handled exception instead.
        } catch (IllegalStateException exception) {
            Log.e(FILENAME, "IllegalStateException - try again" + exception.getMessage());
            exception.printStackTrace();
            return;
        }
    }

    public boolean showFragment(String tag) {
        Log.d(FILENAME, "showFragment " + tag);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if ((ft == null) || (fragment == null)) {
            return false;
        }
        ft.show(fragment);
        //ft.addToBackStack(null);
        try {
            ft.commit();
        } catch (IllegalStateException exception) {
            Log.e(FILENAME, "IllegalStateException - try again" + exception.getMessage());
            exception.printStackTrace();
            return false;
        }
        return true;
    }


    public void initFragment(int id, Fragment fragment, String tag, boolean toShow, boolean toAddBackStack)
    {
        Log.d(FILENAME, "initFragment");

        transaction = fragmentManager.beginTransaction();
        transaction.add(id, fragment, tag);
        if (toShow == false) {
            transaction.hide(fragment);
        } else {
            transaction.show(fragment);
        }
        /*
        if(toAddBackStack)
        {
            transaction.addToBackStack(null);
        }
        */
        transaction.commit();
    }

    public static FragmentManager fragmentManager;
    public static FragmentTransaction transaction = null;
    public static FragmentConnections mConnectionsFrag;

    WelcomeFragment pWelcomeFrag;

    public static Activity mainActivity;

    public static FragmentControl mControlsFrag;
    //public static FragmentControl mControlsFrag2;
    //public static FragmentControl mControlsFrag3;

    public static fragment_analytics mAnalyticsFrag;
    //public static fragment_analytics mAnalyticsFrag2;
    //public static fragment_analytics mAnalyticsFrag3;

    public static fragment_lastEventsList mLastEventsFrag;
    //public static fragment_lastEventsList mLastEventsFrag2;
    //public static fragment_lastEventsList mLastEventsFrag3;

    public static fragment_profile mProfileFrag;


    public static fragment_alarm mAlarmsFrag;
    public static fragment_lastAlarmsList mLastAlarmsFrag;

    //public static fragment_profile mProfileFrag2;
    //public static fragment_profile mProfileFrag3;

    String TAG_CONNECTIONS = "TAG_CONNECTIONS_FRAGMENT";
    String TAG_CONTROLS = "TAG_CONTROLS_FRAGMENT";
    String TAG_PROFILES = "TAG_PROFILES_FRAGMENT";
    String TAG_ANALYTICS = "TAG_ANALYTICS_FRAGMENT";
    String TAG_LASTEVENTS = "TAG_LASTEVENTS_FRAGMENT";
    String TAG_WELCOME = "TAG_WELCOME_FRAGMENT";

    public void onNavigateToConnections() {
        Vibrate();
        showMenu();
        if (false == showFragment(TAG_CONNECTIONS))
        {
            createConnectionsView();
        }
        hidePagingView();
        if (mConnectionsFrag!=null)
        {
            mConnectionsFrag.update();
        }
        /*
        hideFragment(TAG_PROFILES);
        hideFragment(TAG_ANALYTICS);
        hideFragment(TAG_CONTROLS);
        hideFragment(TAG_LASTEVENTS);
        */
    }

    public void onNavigateToPagingView() {
        Vibrate();
        showMenu();
        hideFragment(TAG_CONNECTIONS);
        showPagingView();
        /*
        showFragment(TAG_PROFILES);
        showFragment(TAG_ANALYTICS);
        showFragment(TAG_CONTROLS);
        showFragment(TAG_LASTEVENTS);
        */
    }

    /*

    public void onNavigateToControlView() {
        Vibrate();
        showMenu();
        hideFragment(TAG_CONNECTIONS);
        hideFragment(TAG_PROFILES);
        hideFragment(TAG_ANALYTICS);
        showFragment(TAG_CONTROLS);
    }

    public void onNavigateToConnections() {
        Vibrate();
        showMenu();
        showFragment(TAG_CONNECTIONS);
        hideFragment(TAG_PROFILES);
        hideFragment(TAG_ANALYTICS);
        hideFragment(TAG_CONTROLS);
    }

    public void onNavigateToProfile() {
        Vibrate();
        showMenu();
        hideFragment(TAG_CONNECTIONS);
        hideFragment(TAG_ANALYTICS);
        hideFragment(TAG_CONTROLS);
        showFragment(TAG_PROFILES);
    }


    public void onNavigateToAnalytics() {
        Vibrate();
        showMenu();
        hideFragment(TAG_CONNECTIONS);
        hideFragment(TAG_PROFILES);
        hideFragment(TAG_CONTROLS);
        showFragment(TAG_ANALYTICS);
    }
    */

    public void Vibrate() {
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(100);
    }

    //Spinner interface
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        //if ( parent.getItemAtPosition(pos)==0)
        selectedTank = pos;
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }


    protected void sendEmail() {

        Log.i("Send email", "");
        //Toast.makeText(this, "Select Email", Toast.LENGTH_SHORT).show();
        String[] TO = {"contactus@bitskeytechnologies.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact Support " + support_Phone);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hi Support, \n Please contact us back -");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.d(FILENAME, "Finished sending email...");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }


    protected void sendEmail(String userName) {

        Log.i("Send email", "");
        Toast.makeText(this, "Select Email", Toast.LENGTH_SHORT).show();
        String[] TO = {"contactus@bitskeytechnologies.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact Support" + support_Phone);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hi Support, \n Please contact us back -");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.d(FILENAME, "Finished sending email...");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    //@Override
    public void userCallSelected(String selectedUserName)
    {
        ChatXMPPService.sendToAllinGroup("Test Message");
        Toast.makeText(getApplicationContext(), "Calling Support ", Toast.LENGTH_LONG).show();
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(selectedUserName.trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
        Vibrate();
        return;
    }

    public static boolean isItMotoStar(String deviceName)
    {
        Log.d(FILENAME, "isItMotoStar");
        if (deviceName==null)
        {
            Log.d(FILENAME, "isItMotoStar:: FALSE :: This name is null returning");

            return false;
        }
        //return true;

        // *
        if (deviceName.equals("HC-05")==true)
        {
            Log.d(FILENAME, "isItMotoStar:: TRUE :: deviceName" + deviceName);
            return true;

        }
        else {
            Log.d(FILENAME, "isItMotoStar:: FALSE :: deviceName" + deviceName);
            return false;
        }
        // */


    }

    public void updateConnectionView(int state)
    {
        if (state== DeviceObject.STATE_CONNECTED)
        {
            setConnectionStatus("Connected", Color.BLACK, Color.GREEN);
        }
        else if (state== DeviceObject.STATE_CONNECTING)
        {
            setConnectionStatus("Connecting...", Color.BLACK, Color.YELLOW);
        }
        /*
        if (state== DeviceObject.STATE_CONNECTED)
        {
            setConnectionStatus("Connected", Color.BLACK, Color.GREEN);
        }
        else if (state== DeviceObject.STATE_CONNECTING)
        {
            setConnectionStatus("Connecting...", Color.BLACK, Color.YELLOW);
        }*/


        else
        {
            setConnectionStatus("Paired", Color.BLACK, Color.WHITE);
        }
    }

    public void updateConnectionView(DeviceObject dvObj)
    {
        int instanceId = 0;
        if (ivConnection==null) return;
        if (dvObj!=null)
        {
            if(dvObj.isPaired)
            {
                if (dvObj.motorStatus==null)
                {
                    setConnectionStatus("Initializing...", Color.BLACK, Color.WHITE);
                }
                if (dvObj.rcStateObj.rc_state == RcState.RC_STATE.RC_STATE_MASTER)
                {
                    setConnectionStatus("Connected (B)", Color.BLACK, Color.GREEN);
                }
                else if (dvObj.rcStateObj.rc_state == RcState.RC_STATE.RC_STATE_REMOTE_CONNECTED)
                //else if (dvObj.state == dvObj.STATE_CONNECTING)
                {
                    Log.d(FILENAME, "Trying to Connect, need to return");
                    setConnectionStatus("Connected (I)", Color.BLACK, Color.GREEN);
                }
                else if (dvObj.rcStateObj.rc_state == RcState.RC_STATE.RC_STATE_ENQUIRING)
                //else if (dvObj.state == dvObj.STATE_CONNECTING)
                {
                    Log.d(FILENAME, "Trying to Connect, need to return");
                    setConnectionStatus("Connecting...", Color.BLACK, Color.YELLOW);
                }

                /*
                else if (dvObj.rcStateObj.rc_state == RcState.RC_STATE.RC_STATE_OFFLINE)
                //else if (dvObj.state == dvObj.STATE_CONNECTING)
                {
                    Log.d(FILENAME, "Trying to Connect, need to return");
                    setConnectionStatus("Connecting...", Color.BLACK, Color.YELLOW);
                }
                */

                else //only Paired
                {
                    Log.d(FILENAME, "Not Connected, need to return");
                    setConnectionStatus("Paired", Color.BLACK, Color.WHITE);
                }
            }
            else {
                Log.d(FILENAME, "Not Paired");
                setConnectionStatus("Pairing", Color.WHITE, Color.RED);
                enableBluetooth();
            }
        }
        else
        {
            setConnectionStatus("Pairing", Color.WHITE, Color.BLUE);
        }
        return;
        //this.notifyDataSetChanged();
    }

    public void setConnectionStatus(String text, int textColor, int backgroundColor)
    {
        ivConnection.setText(text);
        ivConnection.setTextColor(textColor);
        ivConnection.setBackgroundColor(backgroundColor);
        //announce(text);
        //ivConnection.setImageResource(R.drawable.ic_bluetooth_connected);
    }

    public static int getScanDevicesSize()
    {
        return BlueToothDevicesReceiver.tempScanDevicesObjList.size();
    }

    public static String tutorialContent =
            "  Welcome to MotoStar Tutorial\n" +

            "\nMotoStar Device\n"+
            " Input - 220V, 50 Hz\n" +
            " Warning AC High Voltage\n" +
            " Work safely - Keep away from Children.\n" +

            "\nAutoStart\n" +
                    "  a)Water In detected\n" +
                    "  b)At PreConfig Time\n" +

            "\nAutoStop\n" +
                    "  a)  Water not there -2 minutes\n" +
                    "  b) OR Tank full\n" +

            "\nForceStop\n" +
                    "  a) Press small red button\n" +
                    "  b) SwitchOff the Main Switch\n" +

            "\nForceStart\n" +
                    "  Press Small Red Button\n" +

            "\nAutoStart Time:\n" +
                    "  a)Configure from the mobile app\n" +
                    "  b)Default PreConfigured Times\n" +
                    "    5:45,6:00,6:15,6:30,6:45,7:00 AM\n" +
                    "    7:00,7:15,7:30,7:45,8:00,8:15 PM\n" +

            "\nAuto Start - Disabled Time\n" +
                    "  a) 1 PM to 5 PM \n" +
                    "  b) 11 PM to 5 AM\n" +

            "\nConfigure Android App:\n" +
                " a) Lanunch App-keep near MotoStar\n" +
                " b) Ensure MotoStar is powered on.\n" +
                " c) In the mobile App:\n" +
                "    - Enter your mobile number & \n"  +
                "      click Next\n" +
                "    - In App-Click Bluetooth icon\n" +
                "    - Click Pair Button\n" +
                "      Wait a minute for pairing\n" +
                "    - Enter default password- 1234\n" +
                "    - In Paired List-lick on next\n" +
                "    - Enter name for your MotoStar\n" +
                "    - Default screen will appear & \n" +
                "      will connect to MotoStar\n" +
                "      On connecting successfully\n" +
                "      it displays status Connected\n" +

            "\nChange AutoStart times:\n" +
                " Swipe right in monitoring screen\n" +
                " It will take to profile screen\n" +
                " Enter desired profile\n" +
                " ie-Morning start time, #retry etc\n" +
                " ie-Morning Start at 7:00,#rety as 8\n" +
                "   and retry_interval as 10 minutes\n" +
                "   will trigger start at\n" +
                "   7:00, 7:10, 7:20, 7:30, ... 8:10\n" +
                " Click Push button to save changes\n" +
                " and to push them to MotoStar\n" +

            "\nLive Events from MotoStar:\n" +
            " Live triggers reported in bluetooth range\n" +

            "\n Last Events from MataStar:\n" +
            " See trigger's last condition hit\n" +
            " indicating - if sensors are working fine.\n" +

            "\n What to do In case of any Issue\n" +
            "   a) Contact MotoStar helpdesk from app\n" +
            "   b) Query/issue will be addressed ASAP.\n" +

            "\nTroubleShooting Tips:\n" +
                "\na) MotoStar connects to one App at a time\n" +
                "\nb) If motostar not displayed after scan\n" +
                "   Eensure following:\n" +
                "     MotoStar is turned on\n" +
                "     It is near to the Phone App\n" +
                "     If it doesn't resolve\n" +
                "       - Turn-Off/On mobile bluetooth.\n" +
                "     If it doesn't resolve\n" +
                "       - Restart your phone once\n" +
                "     If it doesn't find motostar\n" +
                "       - Contact helpdesk from \n" +
                "         menuitem in your phone.\n" +

                "\nc) If your MotoStar is not running\n"+
                "       at the desired time\n" +
                "     Connect App with Motostar\n" +
                "        it will auto correct time\n"+
                "     If the time is still not correct\n" +
                "        Cell behind Motostar need replacement\n" +
                "        Means(ServiceTime- Call HelpDesk)\n" +

                "\nd> If level indications are not correct:\n" +
                "     Means time for periodic service\n" +
                "     Please call MotoStar HelpDesk\n" +
                "     from menu item in your app\n" +

                "\ne> Any other issue with MotoStar or App\n" +
                "     Please call MotoStar HelpDesk\n" +
                "     from menu item in your app\n" +
                "\n\n  Thanks";


    //Remove all the records of events except current month and previous month.
    public void cleanupHistoricRecords()
    {
        int currentMonth = TimeClock.getMonth();

        //First delete from 0 to current-2 months records
        if (currentMonth > 2)
        {
            for (int i = 0; i < currentMonth -1; i++)
            {
                EventRecord.clearFromDB(this, i);
            }
        }

        //Now delete current month +1 to 12 records. However if it Jan/Feb - do not delte December records:
        int clearLastMonth = currentMonth<=1?11:12;
        for (int i = currentMonth+1; i<=clearLastMonth;i++)
        {
            EventRecord.clearFromDB(this, i);
        }
    }
}
