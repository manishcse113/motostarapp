package com.bitskeytechnologies.www.motostar.Device;

/**
 * Created by Manish on 8/3/2017.
 */

public class EquipmentState
{

    enum EQUIP_STATE {
        EQUIP_STATE_NC, //Not connected
        EQUIP_STATE_SYNCING,
        EQUIP_STATE_ON,
        EQUIP_STATE_ON_REQUESTED,
        EQUIP_STATE_OFF,
        EQUIP_STATE_OFF_REQUESTED,
    };

    enum EQUIP_STATE_TRIGGER
    {
        EQUIP_TRIGGER_CMD_ON,
        EQUIP_TRIGGER_CMD_OFF,
        EQUIP_TRIGGER_ON_SUCC,
        EQUIP_TRIGGER_OFF_SUCC,
        EQUIP_TRIGGER_SYNC_DONE,
        EQUIP_TRIGGER_CONNECTION_BREAK,
        EQUIP_TRIGGER_CONNECTION_DONE

    }

    EQUIP_STATE equip_state;

    public EQUIP_STATE getState() {
        return equip_state;
    }

    public  void setState(EQUIP_STATE state) {
        equip_state = state;
        return;
    }

    public void updateEquipmentState(EQUIP_STATE_TRIGGER trigger)
    {
        switch(equip_state) {
            case EQUIP_STATE_NC:
            {
                switch (trigger) {
                    case EQUIP_TRIGGER_CONNECTION_DONE:
                        equip_state = EQUIP_STATE.EQUIP_STATE_SYNCING;

                        //Send refresh command - remote or local
                        break;
                }
            }
            break;
            case EQUIP_STATE_SYNCING:
            {
                switch (trigger) {
                    case EQUIP_TRIGGER_CONNECTION_BREAK:
                        equip_state = EQUIP_STATE.EQUIP_STATE_NC;
                        break;
                    case EQUIP_TRIGGER_ON_SUCC:
                        equip_state = EQUIP_STATE.EQUIP_STATE_ON;
                        break;
                    case EQUIP_TRIGGER_OFF_SUCC:
                        equip_state = EQUIP_STATE.EQUIP_STATE_OFF;
                        break;
                }
            }
            break;
            case EQUIP_STATE_ON:
            {
                switch (trigger) {
                    case EQUIP_TRIGGER_CONNECTION_BREAK:
                        equip_state = EQUIP_STATE.EQUIP_STATE_NC;
                        break;

                    case EQUIP_TRIGGER_CMD_OFF:
                        equip_state = EQUIP_STATE.EQUIP_STATE_OFF_REQUESTED;
                        break;
                }
            }
            break;


            case EQUIP_STATE_OFF_REQUESTED:
            {
                switch (trigger) {
                    case EQUIP_TRIGGER_CONNECTION_BREAK:
                        equip_state = EQUIP_STATE.EQUIP_STATE_NC;
                        break;
                    case EQUIP_TRIGGER_OFF_SUCC:
                        equip_state = EQUIP_STATE.EQUIP_STATE_OFF;
                        break;
                }
            }
            break;

            case EQUIP_STATE_ON_REQUESTED:
            {
                switch (trigger) {
                    case EQUIP_TRIGGER_CONNECTION_BREAK:
                        equip_state = EQUIP_STATE.EQUIP_STATE_NC;
                        break;
                    case EQUIP_TRIGGER_ON_SUCC:
                        equip_state = EQUIP_STATE.EQUIP_STATE_ON;
                        break;
                }
            }
            break;

            case EQUIP_STATE_OFF:
            {
                switch (trigger) {
                    case EQUIP_TRIGGER_CONNECTION_BREAK:
                        equip_state = EQUIP_STATE.EQUIP_STATE_NC;
                        break;

                    case EQUIP_TRIGGER_CMD_ON:
                        equip_state = EQUIP_STATE.EQUIP_STATE_ON_REQUESTED;
                        break;
                }
            }
            break;
        }
    }

}