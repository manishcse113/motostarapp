package com.bitskeytechnologies.www.motostar.Chat.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.bitskeytechnologies.www.motostar.Chat.gen.ChatXMPPService;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.fragments.WelcomeFragment;

/**
 * Created by Manish on 22-09-2015.
 */

public class LoginBackground { //extends AsyncTask<Void,Void,Void> {
    public static ProgressDialog progress;
    private final String FILENAME = "GBC LoginBackground#";
    private String userName;
    private String password;
    private Context context;
    //static boolean autologin = false;

    public LoginBackground(Context context)
    {
        Log.d(FILENAME, "LoginBackground");
        progress = new ProgressDialog(context);
        userName = WelcomeFragment.regInfo.getUserName();//activity.mUsername;
        password = WelcomeFragment.regInfo.getPassword();//activity.mPassword;
        //context = activity.getApplicationContext();

        //Following is added - to login in backgrounds
        //if (LoginActivity.autologin == true) {
            ChatXMPPService.startActionLogin(context, userName, password, "anything");
        //}
        ChatConnection connection = ChatConnection.getInstance();
        connection.connect(context, userName, password);
        boolean done = false;
        while(!done)
        {
            if(!(connection.getStatus() == ChatConnection.Status.RUNNING))
            {
                done=true;
            }
            else
            {
                Log.v(FILENAME, "Network not Available!");
            }
        }
        Log.d(FILENAME, "doInBackground exit");
    }
}