package com.bitskeytechnologies.www.motostar.fragments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitskeytechnologies.www.motostar.EventsAlarms.AlarmItem;
import com.bitskeytechnologies.www.motostar.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 1/14/2017.
 */

public class LastAlarmListAdapter extends ArrayAdapter {
    int groupid;

    private static List lastAlarmsList = new ArrayList();
    //private DeviceObject deviceObject;
    Context context;
    private final String FILENAME = "MS LastAlarmAdapter#";
    //private UserListCallback callback;
    //private UserListActivityCallback callbackActivity;

    Map<String, String> map;
    Map<String, String> treeMap;

    public LastAlarmListAdapter(Context context, int textViewResourceId)
    {
        super(context, textViewResourceId);
    }

    public LastAlarmListAdapter(Context context, int vg, List alarmsList)
    {

        super(context, vg, lastAlarmsList);
        lastAlarmsList = alarmsList;
        addUsersList(lastAlarmsList);
        //alarmsList = deviceObject.alarmList;
        this.context = context;
        //this.deviceObject = device;
        groupid = vg;
        //this.alarmsList.clear();
        //this.lastAlarmsList.addAll(deviceObject.alarmList);

        //readThread();
        //monitoringConnectivityThread();
    }
    public void updateLastAlarmList(List alarmsList)
    {
        this.lastAlarmsList = alarmsList;
        addUsersList(lastAlarmsList);

    }


    //@Override
    public void add(AlarmItem object) {
        //alarmsList.add(object);
        super.add(object);
        super.notify();
        // here - inform all - to update themselves with the new information.
    }

    public void update(AlarmItem object) {
        //search message based on unique id: receiptId and replace the contents.
        boolean updated = false;
        for (Object alarm : lastAlarmsList) {
            if (((AlarmItem) (alarm)).alarmText.equals(((AlarmItem) (object)).alarmText)) {
                ((AlarmItem) (alarm)).alarmValue = ((AlarmItem) (object)).alarmValue;
                updated = true;
                break;
            }
        }
        if (!updated) {
            add(object);
        }
        super.notifyDataSetInvalidated();
        //super.notify();
    }

    public void addUsersList(List objectList) {
        updateDevicesList(objectList);
    }

    public void updateDevicesList(List objectList)
    {
        //search message based on unique id: receiptId and replace the contents.
        for (Object userObject : objectList) {
            update((AlarmItem) (userObject));
        }

        //super.notify();
    }

    public void clear() {
        //alarmsList.clear();
        super.clear();
    }

    public void delete(AlarmItem object) {
        for (Object stat : lastAlarmsList) {
            if (((AlarmItem) (stat)).alarmText.equals(((AlarmItem) (object)).alarmText)) {
                //alarmsList.remove(object);
                //userObjList.
                super.remove(object);
                break;
            }
        }
    }

    //@Override
    public void delete(int position) {
        int index = position;
        //alarmsList.remove(index);
        super.remove(index);
    }

    public int getCount() {
        return this.lastAlarmsList.size();
    }

    public AlarmItem getItem(int index) {
        AlarmItem object = (AlarmItem) lastAlarmsList.get(index);
        return object;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View itemView = inflater.inflate(groupid, parent, false);
        View fragment_list_view = inflater.inflate(R.layout.fragment_lastalarms, null);
        //Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(R.layout.alarm_item, (ViewGroup) fragment_list_view, false);
            populateUserListLayout(inflater, row, position);
        } else {
            //You get here in case of scroll
            populateUserListLayout(inflater, row, position);
        }
        //((ViewGroup) fragment_list_view).addView(row);
        return row;
    }

    public View populateUserListLayout(LayoutInflater inflater, final View user_view, final int position) {
        //Log.d(FILENAME, "inflateUserListLayout");
        try {
            TextView alarmNameView = (TextView) user_view.findViewById(R.id.alarmName);
            String alarmName = alarmNameView.getText().toString();

            AlarmItem uObj = (AlarmItem) (lastAlarmsList.get(position));
            alarmNameView.setText(uObj.alarmText);

            TextView alarmValueView = (TextView) user_view.findViewById(R.id.alarmValue);
            alarmValueView.setText(uObj.getAlarmValue());

            alarmNameView.setSelected(true);

            ImageView alarmReasonView = (ImageView) user_view.findViewById(R.id.alarmReason);
            if (alarmReasonView != null)
            {
                if (uObj.reason == AlarmItem.Raise)
                {
                    alarmReasonView.setImageResource(R.drawable.ic_alarm_raise);
                }
                else if (uObj.reason == AlarmItem.Clear)
                {
                    alarmReasonView.setImageResource(R.drawable.ic_alarm_clear);
                }
                else
                {
                    alarmReasonView.setImageResource(R.drawable.ic_alarm_none);
                }
            }
        } catch (Exception e) {
            Log.d(FILENAME, "Exception" + e.getMessage());
        }
        return user_view;
    }

    /*
    private fragment_lastAlarmsList callback;
    //private ScanDevicesListInterface callbackActivity;

    public void setCallback(fragment_lastAlarmsList callback) {
        Log.d(FILENAME, "setCallback");
        this.callback = callback;
    }
    */

    /*
    public void setScanDevlicesListCallback(FragmentConnections callback) {
        Log.d(FILENAME, "setScanDevlicesListCallback");
        this.callbackActivity = callback;
    }
    */

    public interface StatsListInterface
    {
        //public void unPair(DeviceObject device);
    }

}