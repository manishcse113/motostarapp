package com.bitskeytechnologies.www.motostar.Chat.gen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Preeti on 06-09-2015.
 */


public class SmsReciever extends BroadcastReceiver {

    private static final String TAG = "Message recieved";
    private static boolean isValidMsgRecvd = false;

    public static void setFlagValidMsgRecvd()
    {
        isValidMsgRecvd = true;
        return;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(isValidMsgRecvd)
            return;;
        Bundle pudsBundle = intent.getExtras();
        Object[] pdus = (Object[]) pudsBundle.get("pdus");
        SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
        Log.i(TAG, messages.getMessageBody());
        Toast.makeText(context, "GrocChat1:SMS Received : " + messages.getMessageBody(),
                Toast.LENGTH_LONG).show();

        Intent smsIntent = new Intent("SMSR");
        smsIntent.putExtra("SmsBody", messages.getMessageBody());
        context.sendBroadcast(smsIntent);
        //context.sendBroadcast(smsIntent,"");

//            if(messages.getMessageBody().contains(AuthenticateBySMS.getOTPPrefix()))
//            {
//                Intent smsIntent = new Intent("SmsReciever");
//                smsIntent.putExtra("SmsBody",messages.getMessageBody());
//                context.sendBroadcast(smsIntent);
//            }


//            FragmentManager managerF = getFragmentManager();
//            RegisterFragment regFrag = (RegisterFragment)managerF.findFragmentByTag("RegisterFragment");
//            regFrag.smsRecieved(messages.getMessageBody());
    }

}
