package com.bitskeytechnologies.www.motostar.Chat.login;

import android.content.Context;
import android.util.Log;

//import com.bitskeytechnologies.www.motostar.Chat.chat.ChatBubbleActivity;
import com.bitskeytechnologies.www.motostar.db.DataSource;
import com.bitskeytechnologies.www.motostar.Chat.gen.ChatXMPPService;
import com.bitskeytechnologies.www.motostar.Chat.gen.FileSharing;
//import com.bitskeys.track.profile.ProfileInfo;
//import com.bitskeys.track.profile.VCardWrappers;
//import com.bitskeys.track.users.UserMaps;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.sasl.SASLMechanism;
import org.jivesoftware.smack.sasl.provided.SASLDigestMD5Mechanism;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.TLSUtils;
import org.jivesoftware.smackx.address.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.CloseIQProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.OpenIQProvider;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.chatstates.packet.ChatStateExtension;
import org.jivesoftware.smackx.commands.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.delay.provider.LegacyDelayInformationProvider;
import org.jivesoftware.smackx.disco.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.disco.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.filetransfer.FileTransferNegotiator;
import org.jivesoftware.smackx.iqlast.packet.LastActivity;
import org.jivesoftware.smackx.iqprivate.PrivateDataManager;
import org.jivesoftware.smackx.muc.packet.GroupChatInvitation;
import org.jivesoftware.smackx.muc.provider.MUCAdminProvider;
import org.jivesoftware.smackx.muc.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.muc.provider.MUCUserProvider;
import org.jivesoftware.smackx.offline.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.offline.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.privacy.provider.PrivacyProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.search.UserSearch;
import org.jivesoftware.smackx.sharedgroups.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.si.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.jivesoftware.smackx.vcardtemp.provider.VCardProvider;
import org.jivesoftware.smackx.xdata.provider.DataFormProvider;
import org.jivesoftware.smackx.xhtmlim.provider.XHTMLExtensionProvider;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

//import com.bitskeys.track.gen.FileTransferWrapper;

//import org.jivesoftware.smackx.IBBProvider.*;

/**
 * Created by Manish on 22-09-2015.
 */
public class ChatConnection {

    private final String FILENAME = "GBC ChatConnectio#";
    static XMPPTCPConnection mConnection;
    Roster mRoster;
    //UserMaps mUsersListMap;
    String mUserName;
    String mPassword;
    public int MAXCONNECTRETRIES = 1;
    static Context context;

    public static enum Status {
        FINISHED, PENDING, RUNNING;
    }

    static Status status;
    private static ChatConnection ourInstance = new ChatConnection();

    public static ChatConnection getInstance() {
        return ourInstance;
    }

    private ChatConnection() {
        status = Status.PENDING;
    }

    public static Status getStatus()
    {
        return status;
    }


    /*
    public ProfileInfo LoadProfileInfo()
    {
        ProfileInfo pInfo = null;
        VCardManager vCardManager = VCardManager.getInstanceFor(mConnection);
        try
        {
            VCard vcard = vCardManager.loadVCard();
            if(vcard != null)
            {
                String firstName = vcard.getFirstName();
                String lastName = vcard.getLastName();
                String emailIdWork = vcard.getEmailWork();
                String emailIdHome = vcard.getEmailHome();
                String nickName = vcard.getNickName();
                byte[] avatarByte = vcard.getAvatar();
                pInfo = new ProfileInfo(firstName,lastName,nickName,emailIdWork);
                pInfo.setAvatarImage(avatarByte);
            }

        }
        catch (Exception ex)
        {
            //TODO
        }

        return pInfo;
    }
    public boolean CreateandSaveVCard(ProfileInfo pInfo)
    {
        boolean result = false;

        VCard vcardTemp = VCardWrappers.loadMyVCard(mConnection);
        if(vcardTemp != null)
        {
            vcardTemp.setEmailWork(pInfo.getEmailIdHome());
            vcardTemp.setEmailWork(pInfo.getEmaildIdOff());
            vcardTemp.setNickName(pInfo.getNickName());
            vcardTemp.setOrganization(pInfo.getOrganizationName());
            vcardTemp.setFirstName(pInfo.getFirstName());
            vcardTemp.setLastName(pInfo.getLastName());
            vcardTemp.setAvatar(pInfo.getImageBytes());

            vcardTemp.setField(VCardWrappers.VCardEmailKey, pInfo.getEmaildIdOff());
            vcardTemp.setField(VCardWrappers.VCardDisplayName,pInfo.getNickName());
            vcardTemp.setField(VCardWrappers.organizatioNameKey,pInfo.getOrganizationName());
            vcardTemp.setField(VCardWrappers.VCardFirstNameKey,pInfo.getFirstName());
            vcardTemp.setField(VCardWrappers.VCardLastNameKey,pInfo.getLastName());
        }
        else
        {
            vcardTemp = pInfo.getVCard();
        }

        result = VCardWrappers.saveVCard(mConnection, vcardTemp);
        return result;

    }
    */

    boolean isServerConnected = false;
    boolean isLoginSuccessful = false;
    public boolean connect(final Context context, String userName, String password)
    {

        mConnection = ChatConnection.getInstance().getConnection();
        if (mConnection != null) {
            isServerConnected = mConnection.isConnected();
            isLoginSuccessful = ChatConnection.getInstance().isLoginSuccessful();
        }

        if ((isServerConnected==true) && (isLoginSuccessful==true))
        {
            return true;
        }

        if (status == Status.RUNNING)
        {
           return false;
        }


        mConnection = null;
        status = Status.RUNNING;
        mUserName = userName;
        mPassword = password;
        this.context = context;
        Log.d(FILENAME, "connect");
        SmackConfiguration.DEBUG = false;
        XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        Log.d(FILENAME, "configBuilder takes time");
        //Enabled to see the debugging message of xml in logcat
        //configBuilder.setDebuggerEnabled(true);
        configBuilder.setUsernameAndPassword(mUserName, mPassword);

        //TBD: Following to be moved to config file --later
        //Manish:: TBD: Trying to login with resourceId
        configBuilder.setServiceName(ChatXMPPService.DOMAINJID);
        //configBuilder.setServiceName("/"+ChatXMPPService.DOMAIN);
        //configBuilder.setServiceName("/bitskeys.com");
        configBuilder.setServiceName(ChatXMPPService.SERVICE_NAME);
        configBuilder.setCompressionEnabled(true);
        //configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible);
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        configBuilder.setHost(ChatXMPPService.HOSTIP);
        configBuilder.setPort(5222);
        Log.d(FILENAME, "Trying Login: User: " + mUserName + " Password: " + mPassword);
        try {
            TLSUtils.acceptAllCertificates(configBuilder);
        } catch (Exception ex) {
            String message = ex.getMessage();
            Log.e(FILENAME, message);
        }
        configBuilder.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        configure(new ProviderManager());

        mConnection = new XMPPTCPConnection(configBuilder.build());
        DeliveryReceiptManager dm = DeliveryReceiptManager.getInstanceFor(mConnection);
        dm.autoAddDeliveryReceiptRequests();
        //dm.setDefaultAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);
        //dm.setAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);
        //dm.enableAutoreceipts();
        dm.addReceiptReceivedListener(new ReceiptReceivedListener() {
            public void onReceiptReceived(String fromJid, String toJid, String receiptId, Stanza receipt) {
                // If the receiving entity does not support delivery receipts,
                // then the receipt received listener may not get invoked.
                //changeMessageDeliveryStatus(receiptId, ChatConstants.DS_ACKED);
                //get from userName
                if (fromJid==null)
                {
                    return;
                }
                String name = fromJid.substring(0, fromJid.indexOf("@"));
                //get to RemoteuserName
                if (toJid==null)
                {
                    return;
                }
                String remoteUsername = toJid.substring(0, toJid.indexOf("@"));
                Log.d(FILENAME, "Receipt Received from " + name + "to " + remoteUsername);

                //FileTransferWrapper.updateChatRecordRequest(context, ChatXMPPService.mUserName, remoteUsername, receiptId, ChatBubbleActivity.MESSAGE_ACKED);
                //call loadAndUpdate:
                DataSource db = new DataSource(context);
                db.open();
                //Here name and remoteuserName are swapped - take care:: Manish
                db.updateChatRecord(remoteUsername, name, receiptId, ChatXMPPService.MESSAGE_ACKED);
                db.close();

                //record updated - how to intimate GUI?
                FileSharing.sendUpdateChatRecord(name, receiptId, ChatXMPPService.MESSAGE_ACKED);
                //ChatRecord.LoadAndUpdateChatRecord(remoteUsername, name, false, receiptId, ChatBubbleActivity.MESSAGE_ACKED);
            }
        });

        /*
        ReadReceiptReceivedListener readListener = new ReadReceiptReceivedListener()
        {
            @Override
            public void onReceiptReceived(String fromJid, String toJid, String packetId)
            {
                Log.i("Read", "Message Read Successfully");
            }
        };
        ReadReceiptManager.getInstanceFor(mConnection).addReadReceivedListener(readListener);
        */

        SASLMechanism mechanism = new SASLDigestMD5Mechanism();
        SASLAuthentication.registerSASLMechanism(mechanism);
        SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");
        SASLAuthentication.unBlacklistSASLMechanism("DIGEST-MD5");

        //Make max 4 retries in case of failure
        boolean connected = false;
        int retrycount = 0;
        while(connected==false && retrycount++ < MAXCONNECTRETRIES) {
            // Connect to the server
            try
            {
                try
                {
                    mConnection.connect();
                }
                catch (Exception e)
                {
                    String msg = e.getMessage();
                    Log.e(FILENAME + "Error", " Connection try#" + retrycount + "failed::" + msg);
                    if (msg.equals("Client is already connected"))
                    {
                        mConnection.login();
                        connected = true;
                        break;
                    }
                }
                mConnection.login();
                connected = true;
            } catch (Exception ex) {
                String msg = ex.getMessage();
                Log.e(FILENAME + "Error", " Connection try#" + retrycount + "failed::" + msg);
                if (msg.equals("Client is already logged in"))
                {
                    connected = true;
                    break;
                }
            }
        }
        if (connected==true) {
            Log.d(FILENAME, " Login successful");
            status = Status.FINISHED;

            //dm.setDefaultAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);
            //dm.setAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);

            //dm.enableAutoreceipts();
        }
        else{
            Log.d(FILENAME, " Login failed for times #" + MAXCONNECTRETRIES + "Check Server or connection and Retry login again");
            status = Status.PENDING;
        }
        return connected;
    }

    public XMPPTCPConnection getConnection()
    {
        return mConnection;
    }
    public static boolean isLoginSuccessful()
    {
        if (status == Status.FINISHED)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public void configure(ProviderManager pm) {
        // Private Data Storage
        pm.addIQProvider("query", "jabber:iq:private",
                new PrivateDataManager.PrivateDataIQProvider());
        // Time
        try {
            pm.addIQProvider("query", "jabber:iq:time",
                    Class.forName("org.jivesoftware.smackx.packet.Time"));
        } catch (ClassNotFoundException e) {
            Log.w("TestClient",
                    "Can't load class for org.jivesoftware.smackx.packet.Time");
        }
        /*
        // Roster Exchange
        pm.addExtensionProvider("x", "jabber:x:roster",
                new RosterExchangeProvider());
        // Message Events
        pm.addExtensionProvider("x", "jabber:x:event",
                new MessageEventProvider());
        */
        // Chat State
        pm.addExtensionProvider("active",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        pm.addExtensionProvider("composing",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        pm.addExtensionProvider("paused",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        pm.addExtensionProvider("inactive",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        pm.addExtensionProvider("gone",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());


        // XHTML
        pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im",
                new XHTMLExtensionProvider());
        // Group Chat Invitations
        pm.addExtensionProvider("x", "jabber:x:conference",
                new GroupChatInvitation.Provider());
        // Service Discovery # Items
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
                new DiscoverItemsProvider());
        // Service Discovery # Info
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
                new DiscoverInfoProvider());
        // Data Forms
        pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());
        // MUC User
        pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
                new MUCUserProvider());
        // MUC Admin
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
                new MUCAdminProvider());
        // MUC Owner
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
                new MUCOwnerProvider());
        /* Following was causing random error in parsing date and thus connection was getting closed sometimes: Manish. Next line is alternate solution to this
        // Delayed Delivery
        pm.addExtensionProvider("x", "jabber:x:delay",
                new DelayInformationProvider());
                */
        pm.addExtensionProvider("x", "jabber:x:delay",
                new LegacyDelayInformationProvider());

        // Version
        try {
            pm.addIQProvider("query", "jabber:iq:version",
                    Class.forName("org.jivesoftware.smackx.packet.Version"));

        } catch (ClassNotFoundException e) {
            // Not sure what's happening here.
        }
        // VCard
        pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());
        // Offline Message Requests
        pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
                new OfflineMessageRequest.Provider());
        // Offline Message Indicator
        pm.addExtensionProvider("offline",
                "http://jabber.org/protocol/offline",
                new OfflineMessageInfo.Provider());
        // Last Activity
        pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());
        // User Search
        pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());
        // SharedGroupsInfo
        pm.addIQProvider("sharedgroup",
                "http://www.jivesoftware.org/protocol/sharedgroup",
                new SharedGroupsInfo.Provider());
        // JEP-33: Extended Stanza Addressing
        pm.addExtensionProvider("addresses",
                "http://jabber.org/protocol/address",
                new MultipleAddressesProvider());
        // FileTransfer
        pm.addIQProvider("si", "http://jabber.org/protocol/si",
                new StreamInitiationProvider());

        pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
                new BytestreamsProvider());

        FileTransferNegotiator.IBB_ONLY = true;

        /* If enabled, it crashes login-seems a known issue with smack
        pm.addIQProvider("open", "http://jabber.org/protocol/ibb", new OpenIQProvider());
        pm.addIQProvider("close","http://jabber.org/protocol/ibb", new CloseIQProvider());
        pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new DataPacketProvider());
        */



        pm.addIQProvider("open","http://jabber.org/protocol/ibb", new OpenIQProvider());
        //
         pm.addIQProvider("close","http://jabber.org/protocol/ibb", new CloseIQProvider());
         pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new DataFormProvider());

        //pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new DataPacketProvider());

        //
        // Privacy
        pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());
        pm.addIQProvider("command", "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider());
        pm.addExtensionProvider("malformed-action",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.MalformedActionError());
        pm.addExtensionProvider("bad-locale",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.BadLocaleError());
        pm.addExtensionProvider("bad-payload",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.BadPayloadError());
        pm.addExtensionProvider("bad-sessionid",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.BadSessionIDError());
        pm.addExtensionProvider("session-expired",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.SessionExpiredError());

        pm.addExtensionProvider(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
        pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT, new DeliveryReceiptRequest().getNamespace(), new DeliveryReceiptRequest.Provider());

        //pm.getInstance().addExtensionProvider(ReadReceipt.ELEMENT, ReadReceipt.NAMESPACE, new ReadReceipt.Provider());
        /*
        DeliveryReceiptManager dm = DeliveryReceiptManager.getInstanceFor(mConnection);
        dm.autoAddDeliveryReceiptRequests();
        //dm.setDefaultAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);
        //dm.setAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);
        //dm.enableAutoreceipts();
        dm.addReceiptReceivedListener(new ReceiptReceivedListener() {
            public void onReceiptReceived(String fromJid, String toJid, String receiptId, Stanza receipt) {
                // If the receiving entity does not support delivery receipts,
                // then the receipt received listener may not get invoked.
                //changeMessageDeliveryStatus(receiptId, ChatConstants.DS_ACKED);
                //get from userName
                String name = fromJid.substring(0, fromJid.indexOf("@"));
                //get to RemoteuserName
                String remoteUsername = toJid.substring(0, toJid.indexOf("@"));
                Log.d(FILENAME, "Receipt Received from " + name + "to " + remoteUsername);

                //FileTransferWrapper.updateChatRecordRequest(context, ChatXMPPService.mUserName, remoteUsername, receiptId, ChatBubbleActivity.MESSAGE_ACKED);
                //call loadAndUpdate:
                //ChatRecrd.LoadAndUpdateChatRecord(context, name, remoteUsername, false, receiptId, ChatBubbleActivity.MESSAGE_ACKED);
            }
        });
        */

        /*
        DeliveryReceiptManager dm=DeliveryReceiptManager.getInstanceFor(mConnection);
        dm.enableAutoReceipts();
        dm.registerReceiptReceivedListener(new ReceiptReceivedListener() {
                                               public void onReceiptReceived(String fromJid, String toJid, String receiptId) {
                                                   Log.d(SmackImpl.class, "got delivery receipt for " + receiptId);
                                                   changeMessageDeliveryStatus(receiptId, ChatConstants.DS_ACKED);
                                               }
                                           }
        );
        */

        //SmackConfiguration.setKeepAliveInterval(-1);


    }
}

