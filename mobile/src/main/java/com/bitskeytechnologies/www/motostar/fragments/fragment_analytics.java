package com.bitskeytechnologies.www.motostar.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.EventsAlarms.EventItem;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link fragment_analytics.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link fragment_analytics#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_analytics extends Fragment
        implements DeviceObject.EventUpdateListner
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String FILENAME = "MS fragment_analytics";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    DeviceObject dvObj;
    private static int instanceCount = 0;
    private int instanceId = 0;

    public fragment_analytics() {
        // Required empty public constructor
        // Required empty public constructor
        instanceId = instanceCount;
        instanceCount++;
        if (instanceCount == 1) //assumption is that - we will have max 3 tanks for now.
        {
            instanceCount = 0;
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragment_analytics.
     */
    // TODO: Rename and change types and number of parameters
    public static fragment_analytics newInstance(String param1, String param2) {
        fragment_analytics fragment = new fragment_analytics();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    ImageButton iBRefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mContext = getActivity();
        View view = inflater.inflate(R.layout.fragment_analytics, container, false);
        //getStats();
        //dvObj.setCallbackEvents(this, getActivity());

        iBRefresh = (ImageButton) view.findViewById(R.id.left_button);
        eventsLv = (ListView) view.findViewById(R.id.eventsList);

        iBRefresh.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View arg0) {
                                             if (mListener != null) {
                                                 mListener.Vibrate();
                                             }
                                             if (MainActivity.getPairedDevicesList() != null) {
                                                 if (MainActivity.getPairedDevicesList().size() > 0) {
                                                     //dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                                                     dvObj = MainActivity.getSelectedDevice();
                                                     if (dvObj == null) {
                                                         return;
                                                     }
                                                     //dvObj.setCallback(this , getActivity());

                                                     //connected status
                                                     if (dvObj.isPaired) {
                                                         dvObj.writeSynch('0');
                                                     }
                                                 }
                                             }
                                             refresh();
                                         }
                                     }
        );
        refresh();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public boolean isResumed = false;
    @Override
    public void onResume() {
        super.onResume();
        Log.d(FILENAME, "onResume");
        isResumed = true;
        refresh();
        //mListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(FILENAME, "onPause");
        isResumed = false;
        //mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
        void updateConnectionView(DeviceObject dvObj);
        void Vibrate();
    }

    EventsListAdapter eventsListAdapter = null;
    private ListView eventsLv = null;
    DeviceObject deviceObject;
    Context mContext;
    int vg = 1;

    public void reInitEventsList()
    {
        eventsListAdapter=null;
        //refresh();
        //eventsLv= null;

    }

    private void getStats() {
        if (MainActivity.getPairedDevicesList() != null) {
            if (MainActivity.getPairedDevicesList().size() > 0) {
                //dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                dvObj = MainActivity.getSelectedDevice();
                if (dvObj==null)
                {
                    return;
                }
            } else {
                Log.e(FILENAME, "No device associated");
                return;
            }
        }

        Log.d(FILENAME, "getStats");
        int vg = 1;
        if (eventsListAdapter == null) {
            if (eventsLv != null) {
                Log.d(FILENAME, "Creating new devicesList");
                eventsListAdapter = new EventsListAdapter(mContext, vg, dvObj);
                eventsLv.setAdapter(eventsListAdapter);
                eventsListAdapter.setCallback(this);
                eventsListAdapter.notifyDataSetChanged();
                scrollMyListViewToBottom();
            } else {
                Log.d(FILENAME, "Invalid case");
            }
        }
        if (eventsListAdapter != null) {
            Log.d(FILENAME, "updating the existing list");
            eventsListAdapter.clear();
            eventsListAdapter.setCallback(this);
            eventsListAdapter.updateDevicesList(MainActivity.getPairedDevicesList());
            eventsListAdapter.notifyDataSetChanged();
            scrollMyListViewToBottom();
        }
    }

    public void addEvent(EventItem eventItem)
    {
        //Need to send request to fetch from device
        if (MainActivity.getPairedDevicesList() != null)
        {
            if (MainActivity.getPairedDevicesList().size() > 0)
            {
                dvObj = MainActivity.getSelectedDevice();
                if (dvObj==null)
                {
                    return;
                }
                //dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
            }
            else
            {
                Log.e(FILENAME, "No device associated");
                return;
            }
        }

        Log.d(FILENAME, "addEvent");
        int vg = 1;
        if (eventsListAdapter == null) {
            if (eventsLv != null) {
                Log.d(FILENAME, "Creating new devicesList");
                eventsListAdapter = new EventsListAdapter(mContext, vg, dvObj);
                eventsLv.setAdapter(eventsListAdapter);
                eventsListAdapter.setCallback(this);
                //statsListAdapter.add(eventItem);
                eventsListAdapter.notifyDataSetChanged();
                scrollMyListViewToBottom();
            }
            else
            {
                Log.d(FILENAME, "Invalid case");
            }
        }
        if (eventsListAdapter != null) {
            Log.d(FILENAME, "updating the existing list");
            //statsListAdapter.clear();
            //statsListAdapter.setCallback(this);
            //eventsListAdapter.add(eventItem); //adapter is already working on the list directly from deviceobject.
            eventsListAdapter.notifyDataSetChanged();
            scrollMyListViewToBottom();
        }
        //In any case - send this to server as well:
        //MainActivity.postLiveEvent(dvObj,eventItem);

    }

    public void refresh()
    {
        Log.d(FILENAME, "refresh");
        if (isResumed==false)
        {
            return;
        }
        if (mContext==null)
        {
            return;
        }
        //Need to send request to fetch from device
        if (MainActivity.getPairedDevicesList() != null)
        {
            if (MainActivity.getPairedDevicesList().size() > 0)
            {
                dvObj = MainActivity.getSelectedDevice();
                if (dvObj==null)
                {
                    return;
                }
                //dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                if (mListener!=null) {
                    mListener.updateConnectionView(dvObj);
                }

                dvObj.setCallbackEvents(this, getActivity());


                int vg = 1;
                //if (eventsListAdapter == null)
                {
                    if (eventsLv != null) {
                        Log.d(FILENAME, "Creating new devicesList");
                        eventsListAdapter = new EventsListAdapter(mContext, vg, dvObj);

                        eventsLv.setAdapter(eventsListAdapter);
                        eventsListAdapter.setCallback(this);
                        //statsListAdapter.add(eventItem);
                        eventsListAdapter.notifyDataSetChanged();
                        scrollMyListViewToBottom();
                    } else {
                        //eventsLv = (ListView) view.findViewById(R.id.eventsList);
                        eventsListAdapter = new EventsListAdapter(mContext, vg, dvObj);
                        eventsLv.setAdapter(eventsListAdapter);
                        eventsListAdapter.setCallback(this);
                        //statsListAdapter.add(eventItem);
                        eventsListAdapter.notifyDataSetChanged();
                        scrollMyListViewToBottom();
                        Log.d(FILENAME, "Invalid case");
                    }
                }
            }
        }
        else
        {
            if (mListener!=null) {
                mListener.updateConnectionView(null);
            }
        }
    }

    private void scrollMyListViewToBottom() {
        eventsLv.post(new Runnable() {
            @Override
            public void run() {
                if (eventsListAdapter!=null && eventsLv!=null) {
                    // Select the last row so it will scroll into view...
                    eventsLv.setSelection(eventsListAdapter.getCount() - 1);
                }
                // Select the last row so it will scroll into view...
            }
        });
    }
}
