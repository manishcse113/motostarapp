package com.bitskeytechnologies.www.motostar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

/**
 * Created by Manish on 20-02-2016.
 */
public class EventRecord extends DBObject {
    final static String FILENAME = "EventRecord";

    private final static String DB_TABLE = "EventRecord";

    private final static String COLUMN_DEVICE_NAME = "device_name";
    private final static String COLUMN_EVENT_NAME = "event_name";
    private final static String COLUMN_EVENT_ID = "eventId";
    private final static String COLUMN_DATE = "date";
    private final static String COLUMN_MONTH = "month";
    private final static String COLUMN_YEAR = "year";
    private final static String COLUMN_HR = "hour";
    private final static String COLUMN_MINUTE = "minute";
    private final static String COLUMN_SECOND = "second";
    private final static String COLUMN_REASON = "reason";
    private final static String COLUMN_EVENT_VALUE = "event_value";

    private final static String[] ALL_COLUMNS = new String[]{
            COLUMN_ID,
            COLUMN_DEVICE_NAME,
            COLUMN_EVENT_NAME,
            COLUMN_EVENT_ID,
            COLUMN_DATE,
            COLUMN_MONTH,
            COLUMN_YEAR,
            COLUMN_HR,
            COLUMN_MINUTE,
            COLUMN_SECOND,
            COLUMN_REASON,
            COLUMN_EVENT_VALUE
    };

    private final static String CREATE_TABLE =
            "CREATE TABLE " + DB_TABLE + " (" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_DEVICE_NAME + " text, " +
                    COLUMN_EVENT_NAME + " text, " +
                    COLUMN_EVENT_ID + " integer not null, " +
                    COLUMN_DATE + " integer not null, " +
                    COLUMN_MONTH + " integer not null, " +
                    COLUMN_YEAR + " integer not null, " +
                    COLUMN_HR + " integer not null, " +
                    COLUMN_MINUTE + " integer not null, " +
                    COLUMN_SECOND + " integer not null, " +
                    COLUMN_REASON + " integer not null, " +
                    COLUMN_EVENT_VALUE + " text " +
                    ");";
    String deviceName = null;
    String eventName = null;
    long eventId = 0;
    long date = 0;
    long month = 0;
    long year = 0;
    long hr = 0;
    long minute = 0;
    long second = 0;
    long reason = 100;
    String event_value = null;


    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion,
                                 int newVersion) {
        if (oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
            onCreate(db);
        }
    }

    public static List<EventRecord> loadFromDatabase(SQLiteDatabase db) {
        Vector<EventRecord> v = new Vector<EventRecord>();

        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, null,
                null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    /*
    public static EventRecord loadFromDatabase(SQLiteDatabase db, long id) {
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_ID + " = " + id,
                null, null, null, null);
        cursor.moveToFirst();
        EventRecord req = null;
        if(!cursor.isAfterLast())
            req = fromCursor(cursor);
        cursor.close();
        return req;
    }
    */


    public static List<EventRecord> loadFromDatabase(SQLiteDatabase db, String deviceName) {
        Vector<EventRecord> v = new Vector<EventRecord>();
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_DEVICE_NAME +  " =?", new String[]{deviceName},
                null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public static List<EventRecord> loadFromDatabase(SQLiteDatabase db, long month)
    {
        Vector<EventRecord> v = new Vector<EventRecord>();
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_MONTH +  " = " + month,
                null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public String getDeviceName()
    {
        return deviceName;
    }
    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    private static EventRecord fromCursor(Cursor cursor) {
        EventRecord req = new EventRecord();
        int i = 0;
        req.setId(cursor.getLong(i++));
        req.setDeviceName(cursor.getString(i++));
        req.setEventName(cursor.getString(i++));

        req.setEventId(cursor.getLong(i++));
        req.setDate(cursor.getLong(i++));
        req.setMonth(cursor.getLong(i++));
        req.setYear(cursor.getLong(i++));
        req.setHr(cursor.getLong(i++));
        req.setMin(cursor.getLong(i++));
        req.setSec(cursor.getLong(i++));
        req.setReason(cursor.getLong(i++));
        req.setEventValue(cursor.getString(i++));
        return req;
    }

    public final static String EVENT_RECORD_ID = "eventRecord";

    @Override
    protected String getTableName() {
        return DB_TABLE;
    }

    @Override
    protected void putValues(ContentValues values) {
        values.put(COLUMN_DEVICE_NAME, getDeviceName());
        values.put(COLUMN_EVENT_NAME, getEventName());
        values.put(COLUMN_EVENT_ID, getEventId());
        values.put(COLUMN_DATE, getDate());
        values.put(COLUMN_MONTH, getMonth());
        values.put(COLUMN_YEAR, getYear());
        values.put(COLUMN_HR, getHr());
        values.put(COLUMN_MINUTE, getMin());
        values.put(COLUMN_SECOND, getSec());
        values.put(COLUMN_REASON, getReason());
        values.put(COLUMN_EVENT_VALUE, getEventValue());
    }

    public String getEventName()
    {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public long getEventId()
    {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public long getDate()
    {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getMonth()
    {
        return month;
    }

    public void setMonth(long month) {
        this.month = month;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public long getHr()
    {
        return hr;
    }

    public void setHr(long hr) {
        this.hr = hr;
    }

    public long getMin()
    {
        return minute;
    }

    public void setMin(long minute) {
        this.minute = minute;
    }

    public long getSec() {
        return second;
    }

    public void setSec(long second) {
        this.second = second;
    }


    public long getReason() {
        return reason;
    }

    public void setReason(long reason) {
        this.reason = reason;
    }

    public String getEventValue() {
        return event_value;
    }

    public void setEventValue(String event_value) {
        this.event_value = event_value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof EventRecord))
            return false;
        EventRecord other = (EventRecord) obj;

        if (deviceName == null) {
            if (other.deviceName!=null)
                return false;
        } else if (!deviceName.equals(other.deviceName)) {
            return false;
        }

        if (eventName == null) {
            if (other.eventName!=null)
                return false;
        } else if (!eventName.equals(other.eventName)) {
            return false;
        }


        if (eventId != other.eventId) {
            return false;
        }


        if (date != other.date) {
            return false;
        }

        if (month != other.month) {
            return false;
        }

        if (year != other.year) {
            return false;
        }

        if (hr != other.hr) {
            return false;
        }

        if (minute != other.minute) {
            return false;
        }

        if (second != other.second) {
            return false;
        }

        if (reason != other.reason) {
            return false;
        }



        if (event_value == null) {
            if (other.event_value!=null)
                return false;
        } else if (!event_value.equals(other.event_value)) {
            return false;
        }

        return true;
    }

    public static void storeEventRecord(Context context, String deviceName, String eventName,  long eventId, long date, long month, long year, long hr, long min, long second, long reason, String event_value)
    {
        EventRecord req = new EventRecord();
        req.setDeviceName(deviceName);
        req.setEventName(eventName);
        req.setEventId(eventId);
        req.setDate(date);
        req.setMonth(month);
        req.setYear(year);
        req.setHr(hr);
        req.setMin(min);
        req.setSec(second);


        req.setReason(reason);
        req.setEventValue(event_value);


        DataSource db = new DataSource(context);

        try {
            db.open();

        /*
        boolean recordExists = false;
        List<LocationRecord> reqs = db.getLocationRecords(userName);
        for(LocationRecord _req : reqs) {
            if (_req.getUserName().equalsIgnoreCase(userName) == true)
            {
                recordExists = true;
                break;
            }
        }
        if (recordExists == false) {
        */

            db.persistEventRecord(req);
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "exeption in db.open");
            e.printStackTrace();
            db.close();
        }
        //}


    }


    public static void clearFromDB(Context context, String deviceName)
    {
        DataSource db = new DataSource(context);
        try {
            db.open();
            List<EventRecord> reqs = db.getEventRecords(deviceName);
            for (EventRecord _req : reqs) {
                db.deleteEventRecord(_req);
            }
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "Error in location update");
            e.printStackTrace();
            db.close();
        }
    }

    public static void clearFromDB(Context context, long month)
    {
        DataSource db = new DataSource(context);
        try {
            db.open();
            List<EventRecord> reqs = db.getEventRecords(month);
            for (EventRecord _req : reqs) {
                db.deleteEventRecord(_req);
            }
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "Error in location update");
            e.printStackTrace();
            db.close();
        }
    }

    public static void clearFromDB(Context context)
    {
        DataSource db = new DataSource(context);
        db.open();
        List<EventRecord> reqs = db.getEventRecords();
        for(EventRecord _req : reqs) {
            db.deleteEventRecord(_req);
        }
        db.close();
    }

}

