package com.bitskeytechnologies.www.motostar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

/**
 * Created by Manish on 20-02-2016.
 */
public class LastAlarmRecord extends DBObject {
    final static String FILENAME = "LastAlarmRecord";

    private final static String DB_TABLE = "LastAlarmRecord";

    private final static String COLUMN_DEVICE_NAME = "device_name";
    private final static String COLUMN_ALARM_NAME = "alarm_name";
    private final static String COLUMN_ALARM_ID = "alarmId";
    private final static String COLUMN_DATE = "date";
    private final static String COLUMN_MONTH = "month";
    private final static String COLUMN_YEAR = "year";
    private final static String COLUMN_HR = "hour";
    private final static String COLUMN_MINUTE = "minute";
    private final static String COLUMN_SECOND = "second";
    private final static String COLUMN_REASON = "reason";
    private final static String COLUMN_ALARM_VALUE = "alarm_value";

    private final static String[] ALL_COLUMNS = new String[]{
            COLUMN_ID,
            COLUMN_DEVICE_NAME,
            COLUMN_ALARM_NAME,
            COLUMN_ALARM_ID,
            COLUMN_DATE,
            COLUMN_MONTH,
            COLUMN_YEAR,
            COLUMN_HR,
            COLUMN_MINUTE,
            COLUMN_SECOND,
            COLUMN_REASON,
            COLUMN_ALARM_VALUE
    };

    private final static String CREATE_TABLE =
            "CREATE TABLE " + DB_TABLE + " (" +
                    COLUMN_ID + " integer, " +
                    COLUMN_DEVICE_NAME + " text, " +
                    COLUMN_ALARM_NAME + " text, " +
                    COLUMN_ALARM_ID + " integer not null, " +
                    COLUMN_DATE + " integer not null, " +
                    COLUMN_MONTH + " integer not null, " +
                    COLUMN_YEAR + " integer not null, " +
                    COLUMN_HR + " integer, " +
                    COLUMN_MINUTE + " integer, " +
                    COLUMN_SECOND + " integer, " +
                    COLUMN_REASON + " integer, " +
                    COLUMN_ALARM_VALUE + " text " +
                    ");";
    String deviceName = null;
    String alarmName = null;
    long alarmId = 0;
    long date = 0;
    long month = 0;
    long year = 0;
    long hr = 0;
    long minute = 0;
    long second = 0;
    long reason = 0;
    String alarm_value = null;


    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion,
                                 int newVersion) {
        if (oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
            onCreate(db);
        }
    }

    public static List<LastAlarmRecord> loadFromDatabase(SQLiteDatabase db) {
        Vector<LastAlarmRecord> v = new Vector<LastAlarmRecord>();

        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, null,
                null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public static LastAlarmRecord loadFromDatabase(SQLiteDatabase db, long id) {
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_ID + " = " + id,
                null, null, null, null);
        cursor.moveToFirst();
        LastAlarmRecord req = null;
        if(!cursor.isAfterLast())
            req = fromCursor(cursor);
        cursor.close();
        return req;
    }


    public static List<LastAlarmRecord> loadFromDatabase(SQLiteDatabase db, String deviceName) {
        Vector<LastAlarmRecord> v = new Vector<LastAlarmRecord>();
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_DEVICE_NAME +  " =?", new String[]{deviceName},
                null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public String getDeviceName()
    {
        return deviceName;
    }
    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    private static LastAlarmRecord fromCursor(Cursor cursor) {
        LastAlarmRecord req = new LastAlarmRecord();
        int i = 0;
        req.setId(cursor.getLong(i++));
        req.setDeviceName(cursor.getString(i++));
        req.setAlarmName(cursor.getString(i++));

        req.setAlarmId(cursor.getLong(i++));
        req.setDate(cursor.getLong(i++));
        req.setMonth(cursor.getLong(i++));
        req.setYear(cursor.getLong(i++));
        req.setHr(cursor.getLong(i++));
        req.setMin(cursor.getLong(i++));
        req.setSec(cursor.getLong(i++));
        req.setReason(cursor.getLong(i++));
        req.setAlarmValue(cursor.getString(i++));
        return req;
    }

    public final static String ALARM_RECORD_ID = "lastAlarmRecord";

    @Override
    protected String getTableName() {
        return DB_TABLE;
    }

    @Override
    protected void putValues(ContentValues values) {
        values.put(COLUMN_DEVICE_NAME, getDeviceName());
        values.put(COLUMN_ALARM_NAME, getAlarmName());
        values.put(COLUMN_ALARM_ID, getAlarmId());
        values.put(COLUMN_DATE, getDate());
        values.put(COLUMN_MONTH, getMonth());
        values.put(COLUMN_YEAR, getYear());
        values.put(COLUMN_HR, getHr());
        values.put(COLUMN_MINUTE, getMin());
        values.put(COLUMN_SECOND, getSec());
        values.put(COLUMN_REASON, getReason());
        values.put(COLUMN_ALARM_VALUE, getAlarmValue());
    }

    public String getAlarmName()
    {
        return alarmName;
    }

    public void setAlarmName(String alarmName) {
        this.alarmName = alarmName;
    }

    public long getAlarmId()
    {
        return alarmId;
    }

    public void setAlarmId(long alarmId) {
        this.alarmId = alarmId;
    }

    public long getDate()
    {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getMonth()
    {
        return month;
    }

    public void setMonth(long month) {
        this.month = month;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public long getHr()
    {
        return hr;
    }

    public void setHr(long hr) {
        this.hr = hr;
    }

    public long getMin()
    {
        return minute;
    }

    public void setMin(long minute) {
        this.minute = minute;
    }

    public long getSec() {
        return second;
    }

    public void setSec(long second) {
        this.second = second;
    }


    public long getReason() {
        return reason;
    }

    public void setReason(long reason) {
        this.reason = reason;
    }

    public String getAlarmValue() {
        return alarm_value;
    }

    public void setAlarmValue(String alarm_value) {
        this.alarm_value = alarm_value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof LastAlarmRecord))
            return false;
        LastAlarmRecord other = (LastAlarmRecord) obj;

        if (deviceName == null) {
            if (other.deviceName!=null)
                return false;
        } else if (!deviceName.equals(other.deviceName)) {
            return false;
        }

        if (alarmName == null) {
            if (other.alarmName!=null)
                return false;
        } else if (!alarmName.equals(other.alarmName)) {
            return false;
        }

        if (alarmId != other.alarmId) {
            return false;
        }

        if (date != other.date) {
            return false;
        }

        if (month != other.month) {
            return false;
        }

        if (year != other.year) {
            return false;
        }

        if (hr != other.hr) {
            return false;
        }

        if (minute != other.minute) {
            return false;
        }

        if (second != other.second) {
            return false;
        }

        if (reason != other.reason) {
            return false;
        }

        if (alarm_value == null) {
            if (other.alarm_value!=null)
                return false;
        } else if (!alarm_value.equals(other.alarm_value)) {
            return false;
        }

        return true;
    }

    public static void storeAlarmRecord(Context context, String deviceName, String alarmName,  long alarmId, long date, long month, long year, long hr, long min, long second, long reason, String alarm_value)
    {
        LastAlarmRecord req = new LastAlarmRecord();

        req.setDeviceName(deviceName);
        req.setAlarmName(alarmName);
        req.setAlarmId(alarmId);
        req.setDate(date);
        req.setMonth(month);
        req.setYear(year);
        req.setHr(hr);
        req.setMin(min);
        req.setSec(second);
        req.setReason(reason);
        req.setAlarmValue(alarm_value);
        DataSource db = new DataSource(context);

        try {
            db.open();
            db.persistLastAlarmRecord(req);
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "exeption in db.open");
            e.printStackTrace();
            db.close();
        }
    }
    public static void updateLastAlarmRecord( Context context, String deviceName, String alarmName,  long alarmId, long date, long month, long year, long hr, long min, long second, long reason, String alarm_value)
    {
        DataSource ds = new DataSource(context);
        ds.open();
        SQLiteDatabase db = ds.getDb();
        ContentValues values = new ContentValues();
        values.put(COLUMN_DATE, date);
        values.put(COLUMN_MONTH, month);
        values.put(COLUMN_YEAR, year);
        values.put(COLUMN_HR, hr);
        values.put(COLUMN_MINUTE, min);
        values.put(COLUMN_SECOND, second);
        values.put(COLUMN_REASON, reason);
        values.put(COLUMN_ALARM_VALUE, alarm_value);
        db.update(DB_TABLE, values, COLUMN_ALARM_NAME + " = ? " + "AND " + COLUMN_DEVICE_NAME + " = ? ", new String[]{alarmName, deviceName});
        ds.close();
    }

    public static void clearFromDB(Context context, String deviceName)
    {
        DataSource db = new DataSource(context);
        try {
            db.open();
            List<LastAlarmRecord> reqs = db.getLastAlarmRecords(deviceName);
            for (LastAlarmRecord _req : reqs) {
                db.deleteLastAlarmRecord(_req);
            }
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "Error in clearAlarm");
            e.printStackTrace();
            db.close();
        }
    }

    public static void clearFromDB(Context context)
    {
        DataSource db = new DataSource(context);
        db.open();
        List<LastAlarmRecord> reqs = db.getLastAlarmRecords();
        for(LastAlarmRecord _req : reqs) {
            db.deleteLastAlarmRecord(_req);
        }
        db.close();
    }
}

