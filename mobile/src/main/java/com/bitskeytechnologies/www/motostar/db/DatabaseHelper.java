package com.bitskeytechnologies.www.motostar.db;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bitskeytechnologies.www.motostar.Chat.db.ChatRecord;
import com.bitskeytechnologies.www.motostar.Chat.db.MyRecord;
import com.bitskeytechnologies.www.motostar.Chat.db.UserRecord;

import java.util.ArrayList;
import java.util.logging.Logger;

//Manish
public class DatabaseHelper extends SQLiteOpenHelper {
	
	private Logger logger = Logger.getLogger(getClass().getName());
	
	private final static String DB_NAME = "MotoStar2";
	public final static int APP_VERSION = 1;
	public final static int DB_VERSION = 5;
	
	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{

		//ChatRecord.onCreate(db);
		//MyRecord.onCreate(db);
		DevicesRecords.onCreate(db);
		EventRecord.onCreate(db);
		LastEventRecord.onCreate(db);
		MyPhoneRecord.onCreate(db);
		DevicesLocalNameRecords.onCreate(db);
		AlarmRecord.onCreate(db);
		LastAlarmRecord.onCreate(db);

		ChatRecord.onCreate(db);
		MyRecord.onCreate(db);
		UserRecord.onCreate(db);

		//LocationRecord.onCreate(db);
		//ProfileImageRecord.onCreate(db);
		//MyLocationRecord.onCreate(db);
		//RoomRecord.onCreate(db);
		//RoomRecordNew.onCreate(db);
		/*
		SIPIdentity.onCreate(db);
		PTTChannel.onCreate(db);
		ENUMSuffix.onCreate(db);
		
		//Manish: added for Ctalk, commented SIP5060
		CTalkRegisterRequest.onCreate(db);
		SIP5060ProvisioningRequest.onCreate(db);
		*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		logger.info("Upgrading DB from version " + oldVersion + " to version " + newVersion);

		switch(oldVersion)
		{
			case 1:
				//MyLocationRecord.onUpgrade(db, oldVersion, newVersion);
			case 2:
				DevicesLocalNameRecords.onCreate(db);
			case 3:
				AlarmRecord.onCreate(db);
				LastAlarmRecord.onCreate(db);
			case 4:
				ChatRecord.onCreate(db);
				MyRecord.onCreate(db);
				UserRecord.onCreate(db);

			case 8:
				//Just change whatever has been changed in db from 7 to 8 here.
				//Do not change everything from hereon.
		}
	}


	public ArrayList<Cursor> getData(String Query){
		//get writable database
		SQLiteDatabase sqlDB = this.getWritableDatabase();
		String[] columns = new String[] { "mesage" };
		//an array list of cursor to save two cursors one has results from the query
		//other cursor stores error message if any errors are triggered
		ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
		MatrixCursor Cursor2= new MatrixCursor(columns);
		alc.add(null);
		alc.add(null);


		try{
			String maxQuery = Query ;
			//execute the query results will be save in Cursor c
			Cursor c = sqlDB.rawQuery(maxQuery, null);


			//add value to cursor2
			Cursor2.addRow(new Object[] { "Success" });

			alc.set(1,Cursor2);
			if (null != c && c.getCount() > 0) {


				alc.set(0,c);
				c.moveToFirst();

				return alc ;
			}
			return alc;
		} catch(SQLException sqlEx){
			Log.d("printing exception", sqlEx.getMessage());
			//if any exceptions are triggered save the error message to cursor an return the arraylist
			Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
			alc.set(1,Cursor2);
			return alc;
		} catch(Exception ex){

			Log.d("printing exception", ex.getMessage());

			//if any exceptions are triggered save the error message to cursor an return the arraylist
			Cursor2.addRow(new Object[] { ""+ex.getMessage() });
			alc.set(1,Cursor2);
			return alc;
		}
	}
}
