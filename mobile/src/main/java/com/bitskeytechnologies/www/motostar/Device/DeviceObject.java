package com.bitskeytechnologies.www.motostar.Device;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;

import com.bitskeytechnologies.www.motostar.ConnectivityReceiver;
import com.bitskeytechnologies.www.motostar.EventsAlarms.AlarmItem;
import com.bitskeytechnologies.www.motostar.EventsAlarms.EventItem;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.RC.RcMessages;
import com.bitskeytechnologies.www.motostar.RC.RcState;
import com.bitskeytechnologies.www.motostar.TimeClock;
import com.bitskeytechnologies.www.motostar.db.AlarmRecord;
import com.bitskeytechnologies.www.motostar.db.DataSource;
import com.bitskeytechnologies.www.motostar.db.DatabaseHelper;
import com.bitskeytechnologies.www.motostar.db.DevicesRecords;
import com.bitskeytechnologies.www.motostar.db.EventRecord;
import com.bitskeytechnologies.www.motostar.db.LastAlarmRecord;
import com.bitskeytechnologies.www.motostar.db.LastEventRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by Manish on 1/14/2017.
 */


/*
Events List::
post /addMoto/
get /moto/all
get /moto/mac/macId
get /moto/phone/phone
get /moto/phone2/phone2
get /moto/email/email

post /addMotoVersion/
get motoVersion/all
get /motoVersion/mac/macId
get /motoVersion/phone/phone

post /addIncident
get motoIncident/all
get /motoIncident/mac/macId
get /motoIncident/phone/phone
get /motoIncident/Id/id

post /addAppIncident
get motoAppIncident/all
get /motoAppIncident/mac/macId
get /motoAppIncident/phone/phone
get /motoAppIncident/Id/id

post /addLiveEvent
get motoLiveEvent/all
get /motoLiveEvent/mac/macId
get /motoLiveEvent/phone/phone
get /motoLiveEvent/date/date
get /motoLiveEvent/Id/id

post /addLastEvent
get motoLastEvent/all
get /motoLastEvent/mac/macId
get /motoLastEvent/phone/phone
get /motoLastEvent/Id/id
*/

public class DeviceObject extends Object {
    public String deviceName;
    public String deviceLName;
    public boolean phoneMapUpdateRequired = false;

    public String deviceAddress;
    public boolean isNameRequestedFromUser = false;
    public boolean isSelected = false;
    public boolean isPaired;
    ParcelUuid[] uuids;
    BluetoothSocket socket;
    public OutputStream outStream;
    public InputStream inStream;
    public BluetoothDevice btDevice;

    private static String FILENAME = "MS DeviceObject";
    public int state;
    public boolean busy;

    static Context context;
    // Convert normal decimal numbers to binary coded decimal

    public decodeMotorStatus motorStatus;

    public  List uniqueLastEventList = new ArrayList();
    public  List eventList = new ArrayList();

    public  List uniqueLastAlarmList = new ArrayList();
    public  List alarmList = new ArrayList();



    public void encodeAndPostLastEvents()
    {
        String jsonLastEventsString = EventItem.encodeLastEventsJson(context, uniqueLastEventList, deviceAddress);
        if (jsonLastEventsString!=null)
        {
            MainActivity.postLastEvents(jsonLastEventsString);
        }
    }

    public void encodeAndPostLiveEvent(DeviceObject dvObj, EventItem eventItem)
    {
        String jsonLiveEventsString = EventItem.encodeLiveEventsJson(dvObj, eventItem);
        if (jsonLiveEventsString!=null)
        {
            MainActivity.postLiveEvent(jsonLiveEventsString);
        }
    }

    public void encodeAndPostConnectEvent(DeviceObject dvObj, int state )
    {
        String jsonLiveEventsString = encodeConnectJson(dvObj, state);
        if (jsonLiveEventsString!=null)
        {
            MainActivity.postLiveEvent(jsonLiveEventsString);
        }
    }

    public void encodeAndPostmotoPhoneMap()
    {
        String jsonDeviceInfoString = encodeDeviceInfoJson();
        if (jsonDeviceInfoString!=null)
        {
            MainActivity.postDeviceInfo(jsonDeviceInfoString);
        }
    }

    public void encodeAndPostProfile()
    {
        String jsonProfileString = motorStatus.encodeProfileJson();
        if (jsonProfileString!=null)
        {
            MainActivity.postProfile(jsonProfileString);
        }
    }

    public String encodeConnectJson(DeviceObject dvObj, int state)
    {
        Log.d(FILENAME, "encodeConnectJson");
        String jsonString = null;
        if ( dvObj == null)
        {
            return jsonString;
        }

        try {
            //long differeneLat = Math.abs(latitude - mMyOldLat);
            //long differnceLong = Math.abs(longitude - mMyOldLong);
            String epochtime;
            JSONObject json = new JSONObject();
            json.put("mac", dvObj.deviceAddress);
            json.put("phone", MainActivity.phone);
            json.put("DeviceName", MainActivity.phoneName);
            json.put("reported_date", TimeClock.get_date_month_year());
            json.put("reported_time", TimeClock.get_hr_min_sec());
            json.put("reportDate", TimeClock.getDayOfMonth());
            json.put("reportMonth", TimeClock.getMonth());
            json.put("reportYear", TimeClock.getyear());
            json.put("reportHour", TimeClock.getHourofDay());
            json.put("reportMinute", TimeClock.getMinute());
            json.put("reportSecond", TimeClock.getMonth());

            if (state==STATE_CONNECTED)
            {
                json.put("event_name", "CONNECTED");
                json.put("eventId", 100);
            }
            else
            {
                json.put("event_name", "DISCONNECTED");
                json.put("eventId", 200);
            }
            jsonString = json.toString();

        }
        catch (JSONException e)
        {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return jsonString;
        }
        return jsonString;
    }




    //#############:
    //Following is to send the information in motoPhoneMap for any new device registering with this phone:
    public String encodeDeviceInfoJson()
    {
        String jsonString = null;
        try {
            JSONObject json = new JSONObject();
            json.put("mac", deviceAddress);
            json.put("phone", MainActivity.phone);
            json.put("DeviceName", MainActivity.phoneName);
            json.put("reported_date", TimeClock.get_date_month_year());
            json.put("reported_time", TimeClock.get_hr_min_sec());
            json.put("reportDate", TimeClock.getDayOfMonth());
            json.put("reportMonth", TimeClock.getMonth());
            json.put("reportYear", TimeClock.getyear());
            json.put("reportHour", TimeClock.getHourofDay());
            json.put("reportMinute", TimeClock.getMinute());
            json.put("reportSecond", TimeClock.getMonth());


            json.put("Os", MainActivity.os);
            json.put("Sdk", MainActivity.sdk);
            json.put("appVersion", DatabaseHelper.APP_VERSION);
            json.put("motoVersion", MainActivity.motoVersion);
            jsonString = json.toString();
        }
        catch (JSONException e)
        {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
        }
        return jsonString;
    }


    public void raiseAlarm(int alarmId)
    {
        if (((AlarmItem)uniqueLastAlarmList.get(alarmId)).reason==AlarmItem.Clear)
        {
            final AlarmItem alarm = new AlarmItem(deviceAddress, alarmId, AlarmItem.Raise);
            processAlarmOnMainThread(alarm);

            //uniqueLastAlarmList.set(alarm.alarmId, alarm);
            //alarmList.add(alarm);
            //invokeOnMainThread();

        }
    }

    public void clearAlarm(int alarmId)
    {
        if (((AlarmItem)uniqueLastAlarmList.get(alarmId)).reason==AlarmItem.Raise)
        {
            final AlarmItem alarm = new AlarmItem(deviceAddress, alarmId, AlarmItem.Clear);
            processAlarmOnMainThread(alarm);
            //uniqueLastAlarmList.set(alarm.alarmId, alarm);
            //alarmList.add(alarm);
            //invokeOnMainThread();

        }
    }

    public void processAlarmOnMainThread(final AlarmItem alarm)
    {
        if (MainActivity.mContext == null)
        {
            Log.e(FILENAME, "MainActivity.mContext is null");
            return;
        }
        Handler mainHandler = new Handler(MainActivity.mContext.getMainLooper());
        Runnable myRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                uniqueLastAlarmList.set(alarm.alarmId, alarm);
                alarmList.add(alarm);
                MainActivity.refreshConnectionStatus();
                MainActivity.refreshCurrentFragment();
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    //####################


    // Time between sending the idle filler to confirm communication, must be smaller than the timeout constant.
    private final int minCommInterval = 900;
    // Time after which the communication is deemed dead
    private final int timeout = 120000;
    private long lastComm;
    private boolean initializedOnce = false;
    // Constants that indicate the current connection state
    //public final static int STATE_NONE = 0;
    //public final static int STATE_CONNECTING = 1;
    //public final static int STATE_CONNECTED = 2;

    public static int staticId =0;

    public int id =0;
    public DeviceObject(Context ctxt, String deviceName, String deviceAddress, BluetoothSocket socket, BluetoothDevice btDevice, boolean isPaired) {
        super();
        id = staticId++ ; //unique id for each device in ram:
        this.deviceName = deviceName;

        this.deviceAddress = deviceAddress;
        this.isPaired = isPaired;
        this.socket = socket;
        this.btDevice = btDevice;
        setState(STATE_NONE);
        //this.state = STATE_NONE;
        context = ctxt;
        motorStatus = new decodeMotorStatus(ctxt, this, deviceAddress);
        motorStatus.initMotorStatus();
        busy = false;

        rcStateObj = new RcState(deviceAddress);
        EventItem.initDbLastEventList(context, deviceAddress);
        EventItem.initUniqueEventList(context, uniqueLastEventList, deviceAddress);
        EventItem.iniEventList(context, eventList, deviceAddress);

        AlarmItem.initDbLastAlarmList(context, deviceAddress);
        AlarmItem.initUniqueAlarmList(context, uniqueLastAlarmList, deviceAddress);
        AlarmItem.iniAlarmList(context, alarmList, deviceAddress);

        if (motorStatus!=null)
        {
            motorStatus.initMotorLastStatus();
        }
        //Following is to be initiated only for the devices present in pairedDevicesList from mainActivity
        //initThread();
    }


    static long sleepTime = 2000; //1 seconds for now
    //long sleepTimeMin = 1000; //1 seconds for now
    static long sleepTimeDefault = 2000;
    static long periodicPollTime = 20000;
    static long lastpollTimeCounter =0;

    public static void increaseSleepTimers()
    {
        sleepTime = sleepTimeDefault*1;
        periodicPollTime = periodicPollTime*3;
    }

    public static void restoreSleepTimers()
    {
        sleepTime = sleepTimeDefault;
        periodicPollTime = periodicPollTime/3;
    }


    public void initThread()
    {
        new Thread()
        {
            @Override
            public void run()
            {
                init();
                initializedOnce = true;

                String readBuffer = new String();

                while (true)
                {
                    setPaired(true);
                    periodicConnectionCheck();
                    if (socket != null)
                    {
                        do {
                            readBuffer = read();
                            if (readBuffer   != null) {
                                Log.d(FILENAME, readBuffer);
                            }
                        } while(readBuffer != null);
                    }
                    Log.d(FILENAME, "deviceObject " + deviceAddress);
                    if (lastpollTimeCounter>= periodicPollTime)
                    {
                        periodicPoll(); //Periodic poll - itself skips 3 out of 4 attempts. Means it will be polled only 1 time out of 5 attempts:
                        lastpollTimeCounter =0;
                    }
                    try
                    {
                        lastpollTimeCounter +=sleepTime;
                        Thread.sleep(sleepTime);
                    }
                    catch (Exception e)
                    {
                        Log.e(FILENAME, e.getMessage());
                    }
                }
            }
        }.start();
    }

    private static final UUID MY_UUID = UUID.fromString("0000110E-0000-1000-8000-00805F9B34FB");

    public boolean init() {
        Log.d(FILENAME, "init ");
        setState(STATE_CONNECTING);
        if (socket == null)
        {
            if (isPaired==false)
            {
                setState(STATE_NONE);
                Log.d(FILENAME, "init " + deviceAddress + " Device is not paired yet, wait for it to be paired ");
                return false;
            }
            //proceed only, if it has been paired. Let's put a sleep of 500 ms - as  suggested in some forums
            try {
                //setState(STATE_CONNECTING);
                Thread.sleep(500);
                Log.d(FILENAME, "Device is paired , get the socket ");
                if(btDevice.getBondState()==btDevice.BOND_BONDED)
                {
                    Log.d(FILENAME, btDevice.getName());
                    //BluetoothSocket mSocket=null;
                    try {
                        socket = btDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID);
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        Log.e(FILENAME, "socket not created");
                        e1.printStackTrace();
                    }
                }
                else
                {
                    setState(STATE_NONE);
                    return false;
                }
                socket.connect();
                setState(STATE_CONNECTED);
                //MainActivity.announce("Connected");
                initStreams();
                updateLastComm();
                return true;
            }
            catch (Exception ie)
            {
                Log.e(FILENAME, "First Exception in connecting to device" + ie.getMessage());
                try {
                    ParcelUuid[] uuids = btDevice.getUuids();
                    socket.close();
                    socket = btDevice.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                    socket.connect();
                    setState(STATE_CONNECTED);
                    //MainActivity.announce("Connected");
                    initStreams();
                    updateLastComm();
                    return true;
                }
                catch(Exception excep)
                {
                    setState(STATE_NONE);
                    Log.e(FILENAME, "Exception in getting socket for device" + excep.getMessage());
                }
            }
        }
        else
        {
            try {
                if (socket != null && this.socket.isConnected())
                {
                }
                socket.connect();
                setState(STATE_CONNECTED);
                //MainActivity.announce("Connected");
                initStreams();
                updateLastComm();
                return true;
            } catch (IOException e) {
                Log.e(FILENAME, "Exception in connecting to device " + e.getMessage());
                try {
                    //socket.close();
                    socket = (BluetoothSocket) (btDevice.getClass().getMethod("createRfcommSocket", new Class[]{int.class}).invoke(btDevice, 1));
                    socket.connect();
                    setState(STATE_CONNECTED);
                    initStreams();
                    updateLastComm();
                    return true;
                } catch (Exception ie) {
                    Log.e(FILENAME, "Exception in connecting to device" + ie.getMessage());
                    try {
                        ParcelUuid[] uuids = btDevice.getUuids();

                        socket = btDevice.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                        socket.connect();
                        setState(STATE_CONNECTED);
                        //MainActivity.announce("Connected");
                        initStreams();
                        updateLastComm();
                        return true;
                    } catch (Exception excep)
                    {
                        Log.e(FILENAME, "Exception in getting socket for device" + excep.getMessage());
                        try {
                            socket.close();
                            socket = null;
                            Log.d(FILENAME, "socket for device closed " + excep.getMessage());
                        }
                        catch (Exception ex)
                        {
                            Log.e(FILENAME, "Scoket couldnot be closed " + btDevice.getAddress());
                        }
                        setState(STATE_NONE);
                        //Log.d(FILENAME, "changed the socket state to NONE");
                    }
                }
            }
        }
       return false;
    }

    public void initStreams()
    {
        Log.d(FILENAME, "initStreams " + deviceAddress);
        if (socket != null && socket.isConnected()) {
            try {
                outStream = socket.getOutputStream();
                inStream = socket.getInputStream();
            } catch (IOException e) {
                Log.e(FILENAME, "exception in opensing stream" + e.getMessage());
            }
        }
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public void setPaired(boolean flag) {
        this.isPaired = flag;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public void invokeOnMainThread()
    {
        if (MainActivity.mContext == null)
        {
            Log.e(FILENAME, "MainActivity.mContext is null");
            return;
        }
        Handler mainHandler = new Handler(MainActivity.mContext.getMainLooper());
        Runnable myRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                MainActivity.refreshConnectionStatus();
                MainActivity.refreshCurrentFragment();
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    public void updateConnectionStatus()
    {
        if (MainActivity.mContext == null)
        {
            Log.e(FILENAME, "MainActivity.mContext is null");
            return;
        }
        Handler mainHandler = new Handler(MainActivity.mContext.getMainLooper());
        Runnable myRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                MainActivity.refreshConnectionStatus();
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }


    public RcState rcStateObj;
    public String rcMaster;

    public synchronized void setState(int newState)
    {
        //if (D)
        Log.i(FILENAME, "Connection status: " + state + " -> " + newState);
        //MainActivity.updateConnectionView(newState);
        if ((state == STATE_NONE || state == STATE_CONNECTING)&& newState == STATE_CONNECTED) {
            //Ma rinActivity.announce(MainActivity.getDeviceLocalNameFromMac(deviceAddress) + " Connected");
            // Get a handler that can be used to post to the main thread
            //callback.refresh();
            state = newState;

            rcStateObj.updateRCState(RcState.RC_STATE_TRIGGER.TRIGGER_DEVICE_CONNECTED);
            invokeOnMainThread();
            encodeAndPostConnectEvent(this, newState);

        }
        else if (state == STATE_CONNECTED && newState != STATE_CONNECTED )
        {
            //MainActivity.announce(MainActivity.getDeviceLocalNameFromMac(deviceAddress) + " DisConnected");
            state = newState;
            rcStateObj.updateRCState(RcState.RC_STATE_TRIGGER.TRIGGER_DEVICE_DISCONNECTED);
            invokeOnMainThread();
            //callback.refresh();
            encodeAndPostConnectEvent(this, newState);
            //callback.refresh();
            //encodeAndPostConnectEvent(this, newState);
        }
        else if (state!=newState)
        {
            state = newState;
            invokeOnMainThread();
        }
        state = newState;
        updateConnectionStatus();
    }

    final int writeExceptionMaxCount = 1;
    int writeExeptionCurrentCount =0;
    private boolean write(char out) {
        Log.d(FILENAME, "write");
        if (outStream == null) {
            Log.d(FILENAME, "outStream = null");
            return false;
        }
        //if (D)
        Log.d(FILENAME, "Write: " + out);
        try {
            outStream.write(out);
            // End packet with a new line
            outStream.write('\n');
            writeExeptionCurrentCount = 0;
            return true;
        }
        catch (IOException e)
        {

            writeExeptionCurrentCount++;
            if (writeExeptionCurrentCount >= writeExceptionMaxCount)
            {
                writeExeptionCurrentCount =0 ;
                setState(STATE_NONE);
                e.printStackTrace();
            }
            else {
                Log.e(FILENAME, "write unccessful - but will ignore for now #" + writeExeptionCurrentCount + "  " + e.getMessage() + " \n");
                e.printStackTrace();
            }
        }
        return false;
    }

    private boolean write(String out) {
        Log.d(FILENAME, "write");
        if (outStream == null) {
            Log.d(FILENAME, "outStream = null");
            return false;
        }
        //if (D)
        Log.d(FILENAME, "Write: " + out);
        try {
            outStream.write(out.getBytes());
            // End packet with a new line
            outStream.write('\n');
            writeExeptionCurrentCount = 0;
            return true;
        }
        catch (IOException e)
        {
            writeExeptionCurrentCount++;
            if (writeExeptionCurrentCount >= writeExceptionMaxCount) {
                writeExeptionCurrentCount = 0;
                setState(STATE_NONE);
                e.printStackTrace();

            }
        }
        return false;
    }

    /**
     * Stop this device connection
     */
    public synchronized void disconnect()
    {
        Log.d(FILENAME, "Trying Stop");
        if (socket.isConnected() == false)
        {
            setState(STATE_NONE);
            cancel();
        }
        else
        {
            setState(STATE_NONE);
            Log.d(FILENAME, "Socket is connected, some temporary issue, trying reinit");

        }
        //sendMessage(MSG_CANCEL, "Connection ended");
    }

    public void cancel() {
        Log.d(FILENAME, "cancel");
    }

    /**
     * Updates the communication time counter, use with the timeout thread to
     * check for broken connection.
     */
    public synchronized void updateLastComm() {
        lastComm = System.currentTimeMillis();
    }

    /**
     * This method sends data to the Bluetooth device in an unsynchronized
     * manner, actually it calls the write() method inside the connected thread,
     * but it also makes sure the device is not busy. If "r" is sent (reset
     * flag) it will pass all flags and will be sent even if the device is busy.
     *
     * @param out String to send to the Bluetooth device
     * @return Success of failure to write
     */
    boolean writtingInProgress = false;
    public boolean writeSynch(final char out)
    {
        Log.d(FILENAME, "writeSynch");
        // The device hasn't finished processing last command, reset commands ("r") it always get sent
        if (busy ) {
            //if (D)
            Log.d(FILENAME, "Busy");
            return false;
        }
        busy = true;
        boolean ret = false;
        synchronized (this)
        {
            // Make sure the connection is live
            if (state != STATE_CONNECTED)
            {
                return false;
            }
            //write(out.getBytes());
            //write("\n".getBytes());
            //in this thread it was trying to re-init in case write fails.
            // //This is where -seemingly it used to hang the main thread. as writesync is being called from main thread as well.
            new Thread() {
                @Override
                public void run()
                {
                    // implemented lock::
                    while (writtingInProgress == true)
                    {
                        try
                        {
                            Thread.sleep(300);
                        }
                        catch (Exception e)
                        {
                            Log.e(FILENAME, "exception in sleep");
                        }
                    }
                    writtingInProgress = true;
                    //outStream.write(out);
                    write(out);
                    writtingInProgress = false;
                }
            }.start();
        }
        busy = false;
        return true;
    }

    public boolean writeSynch(final String out)
    {
        Log.d(FILENAME, "writeSynch");
        // The device hasn't finished processing last command, reset commands ("r") it always get sent
        if (busy ) {
            //if (D)
            Log.d(FILENAME, "Busy");
            return false;
        }
        busy = true;
        boolean ret = false;
        synchronized (this)
        {
            // Make sure the connection is live
            if (state != STATE_CONNECTED)
            {
                return false;
            }
            //write(out.getBytes());
            //write("\n".getBytes());
            //in this thread it was trying to re-init in case write fails.
            // //This is where -seemingly it used to hang the main thread. as writesync is being called from main thread as well.
            new Thread() {
                @Override
                public void run()
                {
                    // implemented lock::
                    while (writtingInProgress == true)
                    {
                        try
                        {
                            Thread.sleep(300);
                        }
                        catch (Exception e)
                        {
                            Log.e(FILENAME, "exception in sleep");
                        }
                    }
                    writtingInProgress = true;
                    write(out);
                    writtingInProgress = false;
                }
            }.start();
        }
        busy = false;
        return true;
    }


    public int currentSkipPollCount = 0;
    public int MaxSkipPollCount = 1;
    public boolean periodicConnectionCheck()
    {
        //Log.d(FILENAME, "periodicConnectionCheck");
        if (ConnectivityReceiver.isConnected(context))
        {
            rcStateObj.updateRCState(RcState.RC_STATE_TRIGGER.TRIGGER_INERNET_CONNECTED);
        }
        else
        {
            rcStateObj.updateRCState(RcState.RC_STATE_TRIGGER.TRIGGER_INERNET_DISCONNECTED);
        }
        if (socket==null)
        {
            Log.e(FILENAME, deviceAddress + " socket is null");
            if ( initializedOnce == true)
            {
                //It can be initialized from periodic poll - only if default init from constructor is complete
                init();
            }
            return false; //It is being initialized it seems::
        }
        if (socket.isConnected())
        {
            if (state == STATE_NONE)
            {
                Log.e(FILENAME, "Not Connected");
                if ( initializedOnce == true)
                {
                    init();
                }
            }
            else
            {
                setState(STATE_CONNECTED);
                return true;
            }
        }
        else
        {
            setState(STATE_NONE);
            Log.e(FILENAME, "Not Connected");
            if ( initializedOnce == true)
            {
                init();
            }
        }
        return false;
    }

    public boolean periodicPoll()
    {
        Log.d(FILENAME, "periodicPoll");
        currentSkipPollCount++;
        if (currentSkipPollCount < MaxSkipPollCount)
        {
            return false;
        }
        else
        {
            currentSkipPollCount = 0; //Poll only in this case; //This is done as read is given high priority then poll.
        }
        if (socket==null)
        {
            return false; //It is being initialized it seems::
        }

        if (socket.isConnected()) {
            if (state == STATE_CONNECTING || state == STATE_CONNECTED) {
                synchronized (this) {
                    // Filler hash to confirm communication with device when idle
                    if (System.currentTimeMillis() - lastComm > minCommInterval && !busy && state == STATE_CONNECTED) {
                        Log.d(FILENAME, deviceAddress + " writeSync");
                        writeSynch('0');
                        return true;
                    }
                    // Communication timed out
                    if (System.currentTimeMillis() - lastComm > timeout) {
                        //if (D)
                        Log.e(FILENAME, "Timeout");
                        disconnect();
                        return false;
                        //break;
                    }
                }
            }
        }
        motorStatus.checkAndRaiseAlarms();
        return false;
    }

    byte[] buffer = new byte[1024];
    byte ch;
    int bytes;
    String input;
    public String read()
    {
        if (socket.isConnected())
        {
            Log.d(FILENAME, deviceAddress + " connected ");
            setState(STATE_CONNECTED);
            //if (state == STATE_CONNECTING || state == STATE_CONNECTED)
            // {
                try
                {
                    // Make a packet, use \n (new line or NL) as packet end
                    // println() used in Arduino code adds \r\n to the end of the stream
                    bytes = 0;
                    {
                        int count = 0;
                        if (inStream!=null && inStream.available()>0)
                        {
                            while ((ch = (byte) inStream.read()) != '\n') {
                                count++;
                                buffer[bytes++] = ch;
                                if (count > 200) {
                                    break;
                                }
                            }
                        }
                    }
                    //bytes = deviceObject.inStream.read(buffer, bytes, BUFFER_SIZE - bytes);
                    // Prevent read errors (if you mess enough with it)
                    if (bytes > 0)
                    {
                        processReadBuffer(buffer, bytes);
                    }
                    busy = false;
                    // Update last communication time to prevent timeout
                    //updateLastComm();
                } catch (IOException e) {
                    // read() will inevitably throw an error, even when just disconnecting
                    Log.e(FILENAME, "Failed to read");
                    e.printStackTrace();
                    try
                    {
                        Log.e(FILENAME, deviceAddress + " Closing Socket ????? ");
                        socket.close();
                    }
                    catch(Exception error)
                    {
                        Log.e(FILENAME, error.getMessage());
                    }
                    writeSynch('0');
                }
            //}
        }
        if (bytes > 0) {
            return input;
        } else {
            return null;
        }
    }

    public void startDevice()
    {
        motorPowerToggle();
    }
    public void stopDevice()
    {
        motorPowerToggle();
    }

    public void motorPowerToggle()
    {
        if (motorStatus!=null && motorStatus.motorSwitchCondition==true)
        {
            if (writeSynch('3')==true); //to turn off
            {
                motorStatus.motorSwitchCondition = false;
            }
        }
        else if (motorStatus!=null && motorStatus.motorSwitchCondition==false)
        {
            if (writeSynch('2')==true); //to turn on
            {
                motorStatus.motorSwitchCondition = true;
            }
        }
        return;
    }

    DeviceUpdateListner callback; //implemented by control
    DeviceUpdateListner callbackProfile; //implemented by profile

    EventUpdateListner callbackEvents;
    LastEventUpdateListner callbackLastEvents;

    public void setCallback(DeviceUpdateListner callback, Context context)
    {
        if (this.callback == null)
        {
            this.callback = callback;
            this.context = context;
        }
    }

    public void setCallbackProfile(DeviceUpdateListner callbackProfile, Context context)
    {
        if (this.callbackProfile == null)
        {
            this.callbackProfile = callbackProfile;
            this.context = MainActivity.mContext;
            //this.context = context;
        }
    }

    public void setCallbackEvents(EventUpdateListner callback, Context context)
    {
        if (this.callbackEvents == null)
        {
            this.callbackEvents = callback;
            this.context = context;
        }
    }

    public void setCallbackLastEvents(LastEventUpdateListner callbacklastEvents, Context context)
    {
        if (this.callbackLastEvents == null)
        {
            this.callbackLastEvents = callbacklastEvents;
            this.context = context;
        }
    }

    public interface DeviceUpdateListner {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
        public void refresh();
    }

    public interface EventUpdateListner {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
        public void addEvent(EventItem evet);
    }

    public interface LastEventUpdateListner {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
        public void getLastEvents();
    }

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device


    void processReadBuffer(byte[] buffer, int bytes)
    {
        // The carriage return (\r) character has to be removed
        try
        {
            input = new String(buffer, "UTF-8").substring(0, bytes - 1);
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "unsupported conversion of string");
            return;
        }
        //if (D)
        Log.d(FILENAME, "Read: " + deviceAddress + " :: " + input);

        //Hack: Bug Fix: Sometimes there are some initial 0's that are coming as part of read and then the abc etc. is coming.
        //To fux this skip through all initial '0' till 'a' is encountered or end of lenth is reached.
        int i = 0;
        while(buffer[i]=='0')
        {
            i++;
            if(i>=bytes - 1) break;
        }
        // slice from index 5 to index 9

        byte [] newBuffer = Arrays.copyOfRange(buffer, i,bytes);
        bytes= bytes-i;

        if (newBuffer[0] == 'a' && newBuffer[1] == 'b' && newBuffer[2] == 'c') //it means our command
        {
            processCommandResponse(newBuffer);
            RcMessages.broadcastStatus(deviceAddress, bytes, newBuffer);
            //byte[] tempBuffer = RcMessages.broadcastStatus(deviceAddress,bytes,buffer);
            //processCommandResponse(tempBuffer);
        }
        else if (newBuffer[0] == 'a' && newBuffer[1] == 'b' && newBuffer[2] == 'd') //it means events are received:
        {
            processLastEvents(newBuffer,bytes);
            RcMessages.broadcastLastEvents(deviceAddress, bytes, newBuffer);
        }
        else if (newBuffer[0] == 'a' && newBuffer[1] == 'b' && newBuffer[2] == 'e') //it means event is received:
        {
            processEvent(newBuffer, bytes);
            RcMessages.broadcastEvent(deviceAddress,bytes,newBuffer);
        }
        updateLastComm();
    }

    public void processCommandResponse(byte[] buffer)
    {
        motorStatus.decode(buffer, bytes);
        motorStatus.printMotorState();
        Log.d(FILENAME, deviceAddress + " command received ");
        //if (callback!=null)
        {
            // Get a handler that can be used to post to the main thread
            Handler mainHandler = new Handler(MainActivity.mContext.getMainLooper());
            Runnable myRunnable = new Runnable() {
                @Override
                public void run()
                {
                    /*
                    //MainActivity.refreshCurrentFragment();
                    if (callback != null) {
                        callback.refresh();
                    }
                    if (callbackProfile != null) {
                        //callbackProfile.refresh();
                    } else {
                        Log.e(FILENAME, "callback is null?");
                    }
                    */
                    MainActivity.refreshCurrentFragment();
                } // This is your code
            };
            mainHandler.post(myRunnable);
        }
    }

    public void processLastEvents(byte[] buffer, int bytes) {
        EventItem.decodeLastEvents(deviceAddress, buffer, bytes, uniqueLastEventList);
        Log.d(FILENAME, deviceAddress + " Last event received ");
        //motorStatus.printMotorState();
        //if (callback!=null)
        {
            // Get a handler that can be used to post to the main thread
            Handler mainHandler = new Handler(context.getMainLooper());
            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    //MainActivity.refreshCurrentFragment();
                    if (callbackLastEvents != null) {
                        callbackLastEvents.getLastEvents();
                    } else {
                        Log.e(FILENAME, "callback is null?");
                    }
                    MainActivity.refreshCurrentFragment();
                } // This is your code
            };
            mainHandler.post(myRunnable);
        }
    }

    public void processEvent(byte[] buffer, int bytes)
    {
        final EventItem event = new EventItem(deviceAddress, buffer, bytes);
        motorStatus.updateStatusFromEvent(event);
        eventList.add(event);
        uniqueLastEventList.set(event.eventId, event);

        Log.d(FILENAME, deviceAddress + " Event received ");
        //motorStatus.decode(buffer, bytes);
        //motorStatus.printMotorState();
        //if (callback!=null)
        {
            MainActivity.announce(event.eventText);

            // Get a handler that can be used to post to the main thread
            Handler mainHandler = new Handler(context.getMainLooper());
            Runnable myRunnable = new Runnable() {
                @Override
                public void run()
                {

                    if (callbackEvents != null) {
                        callbackEvents.addEvent(event);
                    }
                    else
                    {
                        Log.e(FILENAME, "callback is null?");
                    }
                    MainActivity.refreshCurrentFragment();
                } // This is your code
            };
            mainHandler.post(myRunnable);
            encodeAndPostLiveEvent(this, event);
        }
    }
}