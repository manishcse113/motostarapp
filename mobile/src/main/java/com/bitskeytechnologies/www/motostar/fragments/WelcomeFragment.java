package com.bitskeytechnologies.www.motostar.fragments;

//import android.app.Fragment;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


//import com.bitskeytechnologies.www.motostar.Chat.Profile.UserRegistration;
import com.bitskeytechnologies.www.motostar.Chat.RestAPI.OpenfireRestAPI;
import com.bitskeytechnologies.www.motostar.Chat.db.MyRecord;
import com.bitskeytechnologies.www.motostar.Chat.gen.SmsReciever;
import com.bitskeytechnologies.www.motostar.Chat.login.LoginBackgroundNoGUI;
import com.bitskeytechnologies.www.motostar.Init.AppGeneralSettings;
import com.bitskeytechnologies.www.motostar.Init.AuthenticateBySMS;
import com.bitskeytechnologies.www.motostar.Init.CountryInfo;
import com.bitskeytechnologies.www.motostar.Init.RegistrationInfo;
import com.bitskeytechnologies.www.motostar.Init.Utility;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.R;
import com.bitskeytechnologies.www.motostar.db.DataSource;
import com.bitskeytechnologies.www.motostar.db.MyPhoneRecord;

import com.bitskeytechnologies.www.motostar.Chat.login.LoginBackground;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 3-10-2017.
 */
public class WelcomeFragment extends Fragment implements OpenfireRestAPI.CreateGroupCallback,OpenfireRestAPI.RegisterUserCallback,OpenfireRestAPI.UserAddToGroupCallback, LoginBackgroundNoGUI.LoginProcessCallback {


    private View rootView;
//    ArrayList<ChatInfo> chatInfoArray = new ArrayList<ChatInfo>();

    String[] countryName;
    String[] countryCode;
    String[] isoCodes;

    public static String FILENAME = "MS WelcomeFragment";
    ArrayList<String> ccInfoArray = new ArrayList<String>();
    Map<String,CountryInfo> countryInfoMap = new HashMap<String,CountryInfo>();
    Map<String,CountryInfo> countryCodesMap = new HashMap<String,CountryInfo>();
    Map<String,CountryInfo> isoCodeTwoDigitsMap = new HashMap<String,CountryInfo>();
    Map<String,CountryInfo> isoCodeThreeDigitsMap = new HashMap<String,CountryInfo>();
    String ServerOTP = new String("");
    public static RegistrationInfo regInfo = null;
    //UserRegistration uRegObj = null;
    //String UserName;
    //String Password;
    //String EmailId;

    WelcomeFragment myReference = null;
    TextView errorMsg=null;
    //public static RegistrationInfo regInfo = null;
    //UserRegistration uRegObj = null;

    OpenfireRestAPI.RegisterUserCallback regCallBack;
    OpenfireRestAPI.UserAddToGroupCallback uagCallBack;
    LoginBackgroundNoGUI.LoginProcessCallback loginProcessCallback;
    OpenfireRestAPI.CreateGroupCallback cgCallBack;

    String UserName;
    String Password;
    String EmailId;
    String UserDisplayName;
    String UserGroupName;

    public void LoginSuccess()
    {
        disableProgressBar();
        launchPostRegFragment();
    }
    public void LoginFailure(int statusCode, String errMessage)
    {
        //Neeraj : TODO, see if we will use statusCode in future, right now its being passed as dummy
        //
        disableProgressBar();
        ShowErrorOnFragment(errMessage);
    }

    public void UAGonSuccess()
    {
        //Here - do we need to refresh the roster?

        /*
        OpenfireRestAPI.instantiateLoginBgNoGUI(getActivity().getApplicationContext(),
                OpenfireRestAPI.getUserName(), OpenfireRestAPI.getPassWord(), loginProcessCallback);
        launchPostRegFragment();
        */
    }
    public void UAGonFailure(int statusCode)
    {
        //TODO show error message on text box.
        switch (statusCode)
        {
            case 0:
                /* Probable case of no internet connectivity or unreachable hostname */
                ShowErrorOnFragment("Adding to Group Failed - Please check internet connection!!");
                break;
            default:
                break;
        }
    }

    public void createGroupOnSuccess(String groupName)
    {
        //Add user to group
        OpenfireRestAPI.addUserToGroup(getActivity(), groupName, uagCallBack);

        /*
        OpenfireRestAPI.instantiateLoginBgNoGUI(getActivity().getApplicationContext(),
                OpenfireRestAPI.getUserName(), OpenfireRestAPI.getPassWord(), loginProcessCallback);
        launchPostRegFragment();
        */
    }

    public void createGroupOnFailure(int statusCode, String groupName)
    {
        //TODO show error message on text box.
        switch (statusCode)
        {
            case 0:
                /* Probable case of no internet connectivity or unreachable hostname */
                //ShowErrorOnFragment("Please check internet connection!!");
                break;
            default:
                OpenfireRestAPI.addUserToGroup(getActivity(), groupName, uagCallBack);
                break;
        }
    }



    public void onSuccess() //On success of register
    {
        OpenfireRestAPI.instantiateLoginBgNoGUI(getActivity().getApplicationContext(),
                OpenfireRestAPI.getUserName(), OpenfireRestAPI.getPassWord(), loginProcessCallback);
        launchPostRegFragment();

        //UAGonSuccess();
        //Manish:: Following shall be done later:
        //OpenfireRestAPI.addUserToGroup(getActivity(), uagCallBack);
    }
    public void onFailure(int statusCode) //on Failure of register - do the same thing.. just login
    {
        switch(statusCode)
        {
            /* This means username already exists */
            case 409:
                //OpenfireRestAPI.addUserToGroup(getActivity(), uagCallBack);
                //onSuccess();
                break;
            case 0:
                ShowErrorOnFragment("Register failure - Please check internet connection!!");
                disableProgressBar();
                onSuccess();
                /* This looks like internet not available or host not reachable */
                //TextView tv =
            default:
                /*Neeraj: This condition should never be reached. In case it is reached,
                 let user do not realize the real cause :) */
                ShowErrorOnFragment("Please check internet connection!!");
                disableProgressBar();
                break;

        }
    }
    public String getUserName()
    {
        return UserName;
    }
    public void setUserName(String userName)
    {
        UserName = userName;
    }

    public String getPassword()
    {
        return Password;
    }

    public void setPassword(String password)
    {
        Password = password;
    }

    public String getEmailId()
    {
        return EmailId;
    }

    public void setEmailId(String emailId)
    {
        EmailId = emailId;
    }

    public String getUserDisplayName()
    {
        return UserDisplayName;
    }

    public void setUserDisplayName(String userDisplayName)
    {
        UserDisplayName = userDisplayName;
    }

    public String getGroupName()
    {
        return UserGroupName;
    }

    public void setGroupName(String userGroupName)
    {
        UserGroupName = userGroupName;
    }




    public interface OnWelcomFragmentDone {
        // TODO: Update argument type and name
        //void createPagingView();
        //void createConnectionsView();
        //void initBluetoothConnection();
        void startNextFragment();
    }

    private OnWelcomFragmentDone mListener;


    boolean fragmentAlreadyAdded = false;
    public boolean getIsFragmentAlreadyAdded()
    {
        return fragmentAlreadyAdded;
    }
    public void setFragmentAlreadyAdded(boolean flag)
    {
        fragmentAlreadyAdded = flag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //rootView = inflater.inflate(R.layout.welcome_fragment_layout,container,false);
        rootView = inflater.inflate(R.layout.welcome_frag_layout,container,false);
        AppGeneralSettings.setFirstTimeRun();
        countryName = rootView.getResources().getStringArray(R.array.country_names);
        countryCode = rootView.getResources().getStringArray(R.array.country_codes);
        isoCodes = rootView.getResources().getStringArray(R.array.country_iso_code);
        for( int i =0;i<countryCode.length;i++)
        {
            String isoCodeCombineString = isoCodes[i];
            String isoCodeTwoDigits= new String("") ;
            String isoCodeThreeDigits = new String("");
            String delim = "/";
            String[] tokens = isoCodeCombineString.split(delim);
            if(tokens.length == 2) {
                isoCodeTwoDigits = tokens[0];
                isoCodeThreeDigits = tokens[1];
            }

            CountryInfo ccInfo = new CountryInfo(countryName[i],countryCode[i],isoCodeTwoDigits,isoCodeThreeDigits);
            String combinedStr = countryName[i] + "( +" + countryCode[i] + " )";
            countryInfoMap.put(combinedStr, ccInfo);
            countryCodesMap.put("+" + countryCode[i],ccInfo);
            if(isoCodeTwoDigits.length()>0)
                isoCodeTwoDigitsMap.put(isoCodeTwoDigits.toLowerCase(), ccInfo);
            if(isoCodeThreeDigits.length()>0)
                isoCodeThreeDigitsMap.put(isoCodeThreeDigits.toLowerCase(),ccInfo);

            ccInfoArray.add(combinedStr);
        }

        try {
            regCallBack = (OpenfireRestAPI.RegisterUserCallback) this;
        } catch (ClassCastException e) {
            throw new ClassCastException(this.toString()
                    + " must implement RegisterUserCallback");
        }

        try {
            uagCallBack = (OpenfireRestAPI.UserAddToGroupCallback) this;
        } catch (ClassCastException e) {
            throw new ClassCastException(this.toString()
                    + " must implement UserAddToGroupCallback");
        }

        try {
            loginProcessCallback = (LoginBackgroundNoGUI.LoginProcessCallback) this;
        } catch (ClassCastException e) {
            throw new ClassCastException(this.toString()
                    + " must implement LoginProcessCallback");
        }

        try {
            cgCallBack = (OpenfireRestAPI.CreateGroupCallback) this;
        } catch (ClassCastException e) {
            throw new ClassCastException(this.toString()
                    + " must implement LoginProcessCallback");
        }

        return rootView;
    }

    boolean isCountryCodeValid(String CountryCodeWithPlus)
    {
        if( countryCodesMap.get(CountryCodeWithPlus) != null)
        {
            return true;
        }
        return false;
    }

    boolean isPhoneNumberValid(String phoneNumber)
    {
        if((phoneNumber == null) || (phoneNumber.length()!=10) )
            return false;
        else if(phoneNumber.contains("+") || phoneNumber.contains("#") || phoneNumber.contains(",") || phoneNumber.contains("N")||
                phoneNumber.contains("*") || phoneNumber.contains("/")|| phoneNumber.contains(";")|| phoneNumber.contains("(")
                )
            return false;

        return true;
    }

    private boolean verifyBySMS(String smsBody)
    {
        //if(!smsBody.equalsIgnoreCase(ServerOTP))
        //    return false;
        return true;
    }

    public void disableProgressBar()
    {
        ProgressBar verifyPB = (ProgressBar)rootView.findViewById(R.id.verify_progressBar);
        verifyPB.setVisibility(View.GONE);
    }


    public void enableProgressBar()
    {
        ProgressBar verifyPB = (ProgressBar)rootView.findViewById(R.id.verify_progressBar);
        if(verifyPB != null)
        {
            verifyPB.setVisibility(View.VISIBLE);
            verifyPB.setProgress(50);
        }
    }


    //Manish : This is to be seen
    public void launchPostRegFragment()
    {
        startAutoLogin(); //This will start ChatXmppService

        if (mListener!=null)
        {
            mListener.startNextFragment();
        }

        /*
        FragmentManager managerF = getFragmentManager();
        try {
            managerF.popBackStack(null, managerF.POP_BACK_STACK_INCLUSIVE);
        }catch(Exception ex)
        {
            Log.d("ERROR","Ignoring exception as suggested on stackoverflow :)");
        }


        FragmentTransaction transaction = managerF.beginTransaction();
        ProfileCreateFragment profCreateFrag = new ProfileCreateFragment();
        transaction.replace(R.id.base_fragment_layout, profCreateFrag);
        transaction.addToBackStack(null);
        //transaction.add(R.id.base_fragment_layout, profCreateFrag, "ProfileCreateFragment");
        transaction.commit();
        */
        disableProgressBar();
    }


    public void smsRecieved(String smsBody)
    {
//        Log.d("Recieved SMS",smsBody);
        if(verifyBySMS(smsBody))
        {
            SmsReciever.setFlagValidMsgRecvd();
            //uRegObj.RegisterUser();
        }
    }

    public void ShowErrorOnFragment(String errorStr)
    {
        TextView errmsg_tv = (TextView)rootView.findViewById(R.id.reg_errmsg_tv);
        errmsg_tv.setText(errorStr);
        errmsg_tv.setTextColor(Color.RED);
        /*
        if(uRegObj != null)
            uRegObj.CancelTimer();
        */
    }

    public void ShowInfoOnFragment(String errorStr)
    {
        TextView errmsg_tv = (TextView)rootView.findViewById(R.id.reg_errmsg_tv);
        errmsg_tv.setText(errorStr);
        errmsg_tv.setTextColor(Color.GREEN);
        /*
        if(uRegObj != null)
            uRegObj.CancelTimer();
            */
    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button agreeButton = (Button)rootView.findViewById(R.id.agree_button);

        ArrayAdapter<String> c_names = new ArrayAdapter<String>(rootView.getContext(),android.R.layout.simple_dropdown_item_1line,
                ccInfoArray);
        //AutoCompleteTextView country_names_tv = (AutoCompleteTextView)rootView.findViewById(R.id.cname_auto_tv);
        //country_names_tv.setAdapter(c_names);

        final EditText cc_ev = (EditText)rootView.findViewById(R.id.cc_ev);
       /* Try to get current country location from SIM */
        TelephonyManager tm = (TelephonyManager)rootView.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String locale = tm.getSimCountryIso();
        CountryInfo cinfo = null;
        if(locale.length() == 2)
            cinfo = isoCodeTwoDigitsMap.get(locale.toLowerCase());
        else if(locale.length() == 3)
            cinfo = isoCodeThreeDigitsMap.get(locale.toLowerCase());

        if(cinfo != null)
        {
            //country_names_tv.setText(cinfo.getCountryName(), TextView.BufferType.EDITABLE);
            cc_ev.setText("+" + cinfo.getCountryCode(), TextView.BufferType.EDITABLE);
        }

        /*

        country_names_tv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position;
                String str = String.valueOf(position);
                String item = parent.getItemAtPosition(position).toString();
                CountryInfo info = countryInfoMap.get(item);
                String code = "+" + info.getCountryCode();
                EditText cc_ev = (EditText) rootView.findViewById(R.id.cc_ev);
                cc_ev.setText(code, TextView.BufferType.EDITABLE);
                //  String locale = rootView.getContext().getResources().getConfiguration().locale.getCountry();
                // String locale = rootView.getContext().getResources().getConfiguration().locale.getDisplayCountry();


            }
        });
        */


        //Button verifyButton = (Button)rootView.findViewById(R.id.verify_button);
        agreeButton.setOnClickListener(new Button.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               //TODO
                                               boolean isValidInput = true;
                                               TextView errmsg_tv = (TextView) rootView.findViewById(R.id.reg_errmsg_tv);

                                               TextView phone_ev = (TextView) rootView.findViewById(R.id.phone_tv);
                                               String phoneNumber = null;
                                               if (phone_ev != null) {
                                                   phoneNumber = phone_ev.getText().toString();
                                               }
                                               TextView cc_ev = (TextView) rootView.findViewById(R.id.cc_ev);
                                               String cc = null;
                                               if (cc_ev != null) {
                                                   cc = cc_ev.getText().toString();
                                               }

                                               if ((cc.length() >= 0)) {
                                                   String temp = new String("") + cc_ev.getText().charAt(0);
                                                   if (!temp.equalsIgnoreCase("+")) {
                                                       cc = "+" + cc;
                                                   }
                                               }
                                               if (!isCountryCodeValid(cc)) {
                                                   errmsg_tv.setText("Error:Please enter valid country code!!");
                                                   errmsg_tv.setTextColor(Color.RED);
                                                   isValidInput = false;
                                               } else if (!isPhoneNumberValid(phoneNumber)) {
                                                   errmsg_tv.setText("Error:Please enter valid phone number!!");
                                                   errmsg_tv.setTextColor(Color.RED);
                                                   isValidInput = false;
                                               }

                                               if (isValidInput) {
                                                   errmsg_tv.setText("");
                                                   SmsManager smsManager = SmsManager.getDefault();
                                                   ServerOTP = AuthenticateBySMS.getRegisterationOTPFromServer();
                                                   //smsManager.sendTextMessage(cc + phoneNumber, null, ServerOTP, null, null);

//                                                  //TODO: Passing empty string for Corporate Id
                                                   //Populate the registration info which will be used if authenticated
                                                   //successfully by SMS
                                                   String luserName = cc + phoneNumber;
                                                   luserName.replace("+", "");
                                                   regInfo = new RegistrationInfo(luserName, "");


                                                   String phone = cc+phoneNumber;

                                                   String phone_name = Utility.getDeviceName();
                                                   //String os = "TBD";
                                                   String os = android.os.Build.VERSION.RELEASE; // e.g. myVersion := "1.6"
                                                   int sdk = android.os.Build.VERSION.SDK_INT; // e.g. sdkVersion := 8;

                                                   String mac = "NA";
                                                   String moto_version = "TBD";
                                                   long update_required = 1;

                                                   MyPhoneRecord.storeRecord((Context) mListener, phone, mac, phone_name, os, sdk, moto_version, update_required);
                                                   MyRecord.storeRecord((Context)mListener,phone,phone,"NA",phone,"dd","aa");


                                                   setUserName(regInfo.getUserName());
                                                   setPassword(regInfo.getPassword());
                                                   //setGroupName(regInfo.getPassword());

                                                   //uRegObj.Initialize();
                                                   OpenfireRestAPI.setUserName(regInfo.getUserName());
                                                   OpenfireRestAPI.setPassWord(regInfo.getPassword());

                                                   OpenfireRestAPI.registerUser(getActivity(), regCallBack);

                                                   // hide keyboard as well
                                                   InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                                   imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);


                                                   enableProgressBar();
                                               }
                                           }
                                       }
        );


        /*
        EditText phone_ev = (EditText) rootView.findViewById(R.id.phone_tv);
        //phone_ev.setSelected(true);
        phone_ev.setSelection(0);
        phone_ev.setFocusableInTouchMode(true);
        phone_ev.requestFocus();
        */

        /*
        agreeButton.setOnClickListener(new Button.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v) {
                                               //FragmentManager managerF = getFragmentManager();
                                               //managerF.popBackStack(null, managerF.POP_BACK_STACK_INCLUSIVE);
                                               boolean isValidInput = true;
                                               TextView errmsg_tv = (TextView)rootView.findViewById(R.id.reg_errmsg_tv);

                                               EditText phone_ev = (EditText) rootView.findViewById(R.id.phone_tv);
                                               String phoneNumber  = null;
                                               if( phone_ev != null)
                                               {
                                                   phoneNumber = phone_ev.getText().toString();
                                               }
                                               EditText cc_ev = (EditText)rootView.findViewById(R.id.cc_ev);
                                               String cc = null;
                                               if(cc_ev != null)
                                               {
                                                   cc = cc_ev.getText().toString();
                                               }

                                               if((cc.length() >= 0) )
                                               {
                                                   String temp = new String("") +  cc_ev.getText().charAt(0);
                                                   if(!temp.equalsIgnoreCase("+"))
                                                   {
                                                       cc = "+" + cc;
                                                   }
                                               }
                                               if(!isCountryCodeValid(cc) )
                                               {
                                                   errmsg_tv.setText("Error:Please enter valid country code!!");
                                                   errmsg_tv.setTextColor(Color.RED);
                                                   isValidInput = false;
                                               }
                                               else if(!isPhoneNumberValid(phoneNumber))
                                               {
                                                   errmsg_tv.setText("Error:Please enter valid phone number!!");
                                                   errmsg_tv.setTextColor(Color.RED);
                                                   isValidInput = false;
                                               }

                                               if(isValidInput)
                                               {
                                                   errmsg_tv.setText("");
                                                   SmsManager smsManager = SmsManager.getDefault();
                                                   ServerOTP = AuthenticateBySMS.getRegisterationOTPFromServer();
                                                   //smsManager.sendTextMessage(cc + phoneNumber, null, ServerOTP, null, null);

//                                                  //TODO: Passing empty string for Corporate Id
                                                   //Populate the registration info which will be used if authenticated
                                                   //successfully by SMS
                                                   String luserName = cc+phoneNumber;
                                                   luserName.replace("+","");
                                                   regInfo = new RegistrationInfo(luserName,"");

                                                   // hide keyboard as well
                                                   InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                                   imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                                                   enableProgressBar();

                                                   //Manish: following needs to be integrated somewhere:
                                                   if (mListener!=null)
                                                   {
                                                       //Create the entry in db:: //phone number has to be primary key in db
                                                       //Also send this info to server - if connected
                                                       //if not connected - store this info - to send it later to the server after reading from db.
                                                       //connectivity thread can check - on connectivity gain and send it later then.

                                                       String phone = cc+phoneNumber;

                                                       String phone_name = Utility.getDeviceName();
                                                       //String os = "TBD";
                                                       String os = android.os.Build.VERSION.RELEASE; // e.g. myVersion := "1.6"
                                                       int sdk = android.os.Build.VERSION.SDK_INT; // e.g. sdkVersion := 8;

                                                       String mac = "NA";
                                                       String moto_version = "TBD";
                                                       long update_required = 1;
                                                       MyPhoneRecord.storeRecord((Context) mListener, phone, mac, phone_name, os, sdk, moto_version, update_required);
                                                       mListener.startNextFragment();
                                                   }
                                               }
                                   }
                                                                   }
        );
        */


    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWelcomFragmentDone) {
            mListener = (OnWelcomFragmentDone) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnWelcomeFragmentDone");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    boolean autologin = false;
    public void startAutoLogin()
    {
        Log.d(FILENAME, "startAutoLogin");

        autologin = true;
        // When Email Edit View and Password Edit View have values other than Null
        if (Utility.isNotNull(regInfo.getUserName()) && Utility.isNotNull(regInfo.getPassword())) {
            new LoginBackground(MainActivity.mContext);//.execute();
            //finish();
        } else {
            //Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }
    }

    public void initCreateGroup(String groupName, Context ctxt)
    {
        OpenfireRestAPI.createGroup(groupName,ctxt,cgCallBack );
    }

}
