package com.bitskeytechnologies.www.motostar.EventsAlarms;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.TimeClock;
import com.bitskeytechnologies.www.motostar.db.AlarmRecord;
import com.bitskeytechnologies.www.motostar.db.DataSource;
import com.bitskeytechnologies.www.motostar.db.EventRecord;
import com.bitskeytechnologies.www.motostar.db.LastAlarmRecord;
import com.bitskeytechnologies.www.motostar.db.LastEventRecord;

import java.sql.Time;
import java.util.List;

/**
 * Created by Manish on 7/21/2017.
 */



public class AlarmItem {
    public int alarmId;
    public String alarmText;
    public String alarmValue;

    public byte date;
    public byte month;
    public byte year;
    public byte hour;
    public byte minute;
    public byte second = 0;
    public byte reason = 100;
    public int readBytes =255;
    public long referencTime = 0;

    public String deviceAddress = null;

    public static String FILENAME = "MS AlarmItem";
    public Context mContext = null;
        /*
        public AlarmItem(String alarmText, String alarmValue) {
            this.alarmText = alarmText;
            this.alarmValue = alarmValue;
        }
        */

    public void setAlarmValue(String alarmValue) {
        this.alarmValue = alarmValue;
    }
    public String getAlarmValue() {
        return alarmValue;
    }
    public String getAlarmText() {
        return alarmText;
    }
    public void setAlarmValue(byte date, byte month, byte year, byte hr, byte minute) {
        alarmValue = new String(
                String.valueOf(date) + "/" +
                        String.valueOf(month) + "/" +
                        String.valueOf(year) + "--" +
                        String.valueOf(hr) + ":" +
                        String.valueOf(minute));
    }

    public static String getAlarmName(int id) {
        return alarmNames[id];
    }
    public static int getAlarmId(String alarmName) {
        int id = 0;
        for (String eName : alarmNames) {
            if (eName.equals(alarmName)) {
                break;
            }
            id++;
        }
        return id;
    }

    public AlarmItem(String deviceAddress, Context context, int id, String name, String value)
    {
        alarmId = id;
        alarmText = name;
        alarmValue = value;
        this.deviceAddress = deviceAddress;
        mContext = context;
        LastAlarmRecord.updateLastAlarmRecord(MainActivity.mContext, deviceAddress, alarmText, alarmId, date, month, year, hour, minute, second, reason, alarmValue);
    }

    public AlarmItem(String deviceAddress, Context context, int id, String name, String value, byte reason)
    {
        alarmId = id;
        alarmText = name;
        alarmValue = value;
        this.deviceAddress = deviceAddress;
        mContext = context;
        this.reason = reason;
        LastAlarmRecord.updateLastAlarmRecord(MainActivity.mContext, deviceAddress, alarmText, alarmId, date, month, year, hour, minute, second, reason, alarmValue);

    }

    /*
    public AlarmItem(String deviceAddress, Context context, byte[] buffer, int maxbyte)
    {
        mContext = context;
        this.deviceAddress = deviceAddress;
        int i = 0;
        i++;//for a
        i++; //for b
        i++; //for e
        if (maxbyte < i) return;
        alarmId = (byte) (buffer[i++] - (char) ('0'));
        alarmText = getAlarmName(alarmId);
        if (maxbyte < i) return;
        date = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        month = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        year = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        hour = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        minute = (byte) (buffer[i++] - (char) ('0'));

        setAlarmValue(date, month, year, hour, minute);
        if (maxbyte < i) return;
        if (alarmId==0 ||alarmId==1) //alarm Id 0 & 1 has reason field as well.
        {
            reason = (byte) (buffer[i++] - (char) ('0'));
        }
        //reason is to be added yet.
        readBytes = i;

        if (mContext == null && MainActivity.mContext!=null)
        {
            mContext =MainActivity.mContext;
        }

        AlarmRecord.storeAlarmRecord(mContext, deviceAddress, alarmText, alarmId, date, month, year, hour, minute, second, reason, alarmValue);
        LastAlarmRecord.updateLastAlarmRecord(mContext, deviceAddress, alarmText, alarmId, date, month, year, hour, minute, second, reason, alarmValue);
        calculateReferenceTime();
    }

    public AlarmItem(String deviceAddress, Context context, byte[] buffer, int maxbyte, int offset)
    {
        this.deviceAddress = deviceAddress;
        int i = offset;
        if (maxbyte < i) return;
        alarmId = (byte) (buffer[i++] - (char) ('0'));
        alarmText = getAlarmName(alarmId);
        if (maxbyte < i) return;
        date = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        month = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        year = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        hour = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        minute = (byte) (buffer[i++] - (char) ('0'));

        setAlarmValue(date, month, year, hour, minute);
        if (maxbyte < i) return;

        if (alarmId==0 ||alarmId==1) //alarm Id 0 & 1 has reason field as well.
        {

            reason = (byte) (buffer[i++] - (char) ('0'));
        }
        //reason is to be added yet.
        readBytes = i;
        calculateReferenceTime();

        LastAlarmRecord.updateLastAlarmRecord(MainActivity.mContext, deviceAddress, alarmText, alarmId, date, month, year, hour, minute, second, reason, alarmValue);
    }

    public AlarmItem(int id, byte year, byte month, byte date, byte hour, byte second)
    {
        alarmId = id;
        alarmText = getAlarmName(alarmId);
        this.date = date;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.minute = minute;

        setAlarmValue(date, month, year, hour, minute);
        calculateReferenceTime();
        LastAlarmRecord.updateLastAlarmRecord(MainActivity.mContext, deviceAddress, alarmText, alarmId, date, month, year, hour, minute, second, reason, alarmValue);
    }
    */

    public AlarmItem(String deviceAddress, String alarmName, long alarmId, long date, long month, long year, long hr, long min, long second, long reason)
    {
        this.deviceAddress = deviceAddress;
        this.alarmText = alarmName;
        this.alarmId = (int)alarmId;
        this.date = (byte) date;
        this.month = (byte)month;
        this.year = (byte)year;
        this.hour = (byte)hr;
        this.minute = (byte)min;
        this.second = (byte)second;
        this.reason = (byte)reason;

        setAlarmValue(this.date, this.month, this.year, this.hour, this.minute);
        //this.alarmValue = alarmValue;
        calculateReferenceTime();
        LastAlarmRecord.updateLastAlarmRecord(MainActivity.mContext, deviceAddress, alarmText, alarmId, date, month, year, hour, minute, second, reason, alarmValue);
    }

    public AlarmItem(String deviceAddress, int alarmId, long reason)
    {
        this.deviceAddress = deviceAddress;
        this.alarmText = alarmNames[alarmId];
        this.alarmId = (int)alarmId;
        this.date = (byte) TimeClock.getDayOfMonth();
        this.month = (byte)TimeClock.getMonth();
        this.year = (byte)TimeClock.getyear();
        this.hour = (byte)TimeClock.getHourofDay();
        this.minute = (byte)TimeClock.getMinute();
        this.second = (byte)TimeClock.getSecond();
        this.reason = (byte)reason;

        setAlarmValue(this.date, this.month, this.year, this.hour, this.minute);
        //this.alarmValue = alarmValue;
        calculateReferenceTime();

        LastAlarmRecord.updateLastAlarmRecord(MainActivity.mContext, deviceAddress, alarmText, alarmId, date, month, year, hour, minute, second, reason, alarmValue);

    }


    public long calculateReferenceTime()
    {
        referencTime = (((((this.year - 2017)*12 + (this.month-1))*30 + this.date-1)*24 + this.hour)*60 + this.minute)*60 + this.second;
        return referencTime;
    }


    public static int lastAlarmIndexLevel1 =0;
    public static int lastAlarmIndexLevel2 =1;
    public static int lastAlarmIndexLevel3 =2;
    public static int lastAlarmIndexLevel4 =3;
    public static int lastAlarmIndexWater =4;
    public static int lastAlarmIndexTimer =5;

    public static String alarmNames [] =
            {
                    "Level-1 Sensor",
                    "Level-2 Sensor",
                    "Level-3 Sensor",
                    "Level-4 Sensor",
                    "Water Sensor",
                    "Timer",
            };

    public static String alarmValues [] =
            {
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0"
            };

    public static int Raise = 1;
    public static int Clear = 0;


    public static void initUniqueAlarmList(Context context, List uniqueLastAlarmList, String macAddress)
    {
        int i =0;
        DataSource db = new DataSource(context);
        db.open();

        List<LastAlarmRecord> reqs = db.getLastAlarmRecords(macAddress);
        for (LastAlarmRecord _req : reqs)
        {
            AlarmItem alarm = new AlarmItem(
                    macAddress,
                    _req.getAlarmName(),
                    _req.getAlarmId(),
                    _req.getDate(),
                    _req.getMonth(),
                    _req.getYear(),
                    _req.getHr(),
                    _req.getMin(),
                    _req.getSec(),
                    _req.getReason()
            );

            //if (_req.getReason()==AlarmItem.Raise)
            //{
            uniqueLastAlarmList.add(alarm);
            //}
            i++;
        }
        db.close();
    }

    public static void iniAlarmList(Context context, List alarmList, String macAddress)
    {
        int i =0;
        DataSource db = new DataSource(context);
        db.open();
        //use mac-address instead of deviceName here:
        //eventtable - will store macaddress in deviceName field:
        List<AlarmRecord> reqs = db.getAlarmRecords(macAddress);
        for (AlarmRecord _req : reqs)
        {
            final AlarmItem alarm = new AlarmItem(macAddress,
                    _req.getAlarmName(),
                    _req.getAlarmId(),
                    _req.getDate(),
                    _req.getMonth(),
                    _req.getYear(),
                    _req.getHr(),
                    _req.getMin(),
                    _req.getSec(),
                    _req.getReason()
            );
            alarmList.add(alarm);
            i++;
        }
        db.close();
    }


    public static void initDbLastAlarmList(Context context, String macAddress)
    {
        DataSource db = new DataSource(context);
        db.open();
        List<LastAlarmRecord> reqs = db.getLastAlarmRecords(macAddress);
        if (reqs!=null && reqs.size() < AlarmItem.alarmNames.length) {
            //means - it hasn't been initialized before so initialize it
            db.close();
            LastAlarmRecord.clearFromDB(context, macAddress);

            storeLastAlarmRecords(context, macAddress);
        }
        else //Just close the db
        {
            db.close();
        }
    }
    public static void storeLastAlarmRecords(Context context, String macAddress) {
        int i =0;
        byte reason =0;
        for (String alarmName : AlarmItem.alarmNames)
        {
            LastAlarmRecord.storeAlarmRecord(context, macAddress, alarmName, i, 1, 1, 1, 0, 0, 0, reason, AlarmItem.alarmValues[i]);
            i++;
        }
    }


    /*
    public void raiseAlarm(List uniqueLastAlarmList, int alarmId, String macAddress)
    {
        if (((AlarmItem)uniqueLastAlarmList.get(alarmId)).reason==AlarmItem.Clear)
        {
            final AlarmItem alarm = new AlarmItem(macAddress, alarmId, AlarmItem.Raise);
            processAlarmOnMainThread(alarm);

            //uniqueLastAlarmList.set(alarm.alarmId, alarm);
            //alarmList.add(alarm);
            //invokeOnMainThread();

        }
    }

    public void clearAlarm(List uniqueLastAlarmList,int alarmId, String macAddress)
    {
        if (((AlarmItem)uniqueLastAlarmList.get(alarmId)).reason==AlarmItem.Raise)
        {
            final AlarmItem alarm = new AlarmItem(macAddress, alarmId, AlarmItem.Clear);
            processAlarmOnMainThread(alarm);
            //uniqueLastAlarmList.set(alarm.alarmId, alarm);
            //alarmList.add(alarm);
            //invokeOnMainThread();

        }
    }

    public static void processAlarmOnMainThread(final AlarmItem alarm, List uniqueLastAlarmList, List alarmList)
    {
        if (MainActivity.mContext == null)
        {
            Log.e(FILENAME, "MainActivity.mContext is null");
            return;
        }
        Handler mainHandler = new Handler(MainActivity.mContext.getMainLooper());
        Runnable myRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                uniqueLastAlarmList.set(alarm.alarmId, alarm);
                alarmList.add(alarm);
                MainActivity.refreshConnectionStatus();
                MainActivity.refreshCurrentFragment();
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }
    */



}
