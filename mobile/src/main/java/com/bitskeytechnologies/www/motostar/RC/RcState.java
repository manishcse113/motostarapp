package com.bitskeytechnologies.www.motostar.RC;

import android.util.Log;

import com.bitskeytechnologies.www.motostar.Chat.gen.ChatXMPPService;
import com.bitskeytechnologies.www.motostar.ConnectivityReceiver;
import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.TimeClock;

/**
 * Created by Manish on 8/2/2017.
 */
public class RcState

{
    public static String FILNAME = "MS RcState";

    public String deviceAddress;
    public RcPeriodic rcPeriodic;// = new RcPeriodic();


    public enum RC_STATE {
        RC_STATE_OFFLINE,
        RC_STATE_REMOTE_CONNECTED,
        RC_STATE_MASTER,
        RC_STATE_ENQUIRING
    };

    public enum RC_STATE_TRIGGER
    {
        TRIGGER_DEVICE_CONNECTED, //trigger on directly connecting with device on bluetooth
        TRIGGER_DEVICE_DISCONNECTED, //on bluetooth connection break
        TRIGGER_ENQUIRY_TIMEOUT, // No response received from remoteMaster
        TRIGGER_RESPONSE_RECEIVED, //master responded to my connect request
        TRIGGER_INERNET_CONNECTED, //On connected with internet - If I am not master - I want to enquire - if anyone is master..
        TRIGGER_INERNET_DISCONNECTED //On disconnecteing with internet..
    }

    public RC_STATE rc_state = RC_STATE.RC_STATE_OFFLINE;

    public RC_STATE getState() {
        return rc_state;
    }

    public  void setState(RC_STATE state) {
        rc_state = state;
        return;
    }

    long lastPeriodicConnectTime;

    public RcState(String deviceAddress)
    {
        if (deviceAddress==null)
        {
            Log.e(FILENAME, "deviceaddress is null - RcState not initialized");
            return;
        }
        this.deviceAddress = deviceAddress;
        rcPeriodic = new RcPeriodic();
        rcPeriodic.startPeriodic();
        lastPeriodicConnectTime = TimeClock.getEpocTime();

    }

    //returns if state was changed or not
    public boolean updateRCState(RC_STATE_TRIGGER trigger)
    {
        boolean stateChange = false;
        switch(rc_state) {
            case RC_STATE_OFFLINE:
            {
                switch (trigger) {
                    case TRIGGER_DEVICE_CONNECTED:
                        rc_state = RC_STATE.RC_STATE_MASTER;
                        //start broadcasting periodically - as I am master: it is happening from periodic thread anways: so no need to start again
                        stateChange = true;
                        break;
                    case TRIGGER_INERNET_CONNECTED:
                        //send message to all connected users:: --
                        //Start a timer to ensure timeout do not happen. On timeout again initiate the enquiry.
                        rc_state = RC_STATE.RC_STATE_ENQUIRING;
                        stateChange = true;
                        break;
                }
            }
            break;
            case RC_STATE_ENQUIRING:
            {
                switch (trigger) {
                    case TRIGGER_DEVICE_CONNECTED:
                        rc_state = RC_STATE.RC_STATE_MASTER;
                        stateChange = true;
                        break;
                    //case TRIGGER_ENQUIRY_TIMEOUT:
                        //send message to all connected users:: --
                        //Start a timer to ensure timeout do not happen. On timeout again initiate the enquiry.
                    //    rc_state = RC_STATE.RC_STATE_ENQUIRING;
                    //    break;
                    case TRIGGER_RESPONSE_RECEIVED:
                        rc_state = RC_STATE.RC_STATE_REMOTE_CONNECTED;
                        stateChange = true;
                        break;
                    case TRIGGER_INERNET_DISCONNECTED:
                        rc_state = RC_STATE.RC_STATE_OFFLINE;
                        stateChange = true;
                        break;
                }
            }
            break;
            case RC_STATE_MASTER:
            {
                switch (trigger) {
                    case TRIGGER_DEVICE_DISCONNECTED:
                        //if internet connected, send eqnuiry and go to enquiry
                        //send message to all connected users:: --
                        //Start a timer to ensure timeout do not happen. On timeout again initiate the enquiry.

                        if (ConnectivityReceiver.isConnected(MainActivity.mContext))
                        {
                            rc_state = RC_STATE.RC_STATE_ENQUIRING;
                            stateChange = true;
                            RcMessages.broadcastMasterDisConnected(deviceAddress);
                        }
                        else
                        {
                            rc_state = RC_STATE.RC_STATE_OFFLINE;
                            stateChange = true;
                        }
                        break;
                }
            }
            break;
            case RC_STATE_REMOTE_CONNECTED:
            {
                switch (trigger)
                {
                    case TRIGGER_DEVICE_CONNECTED:
                        rc_state = RC_STATE.RC_STATE_MASTER;
                        stateChange = true;
                        break;
                    case TRIGGER_ENQUIRY_TIMEOUT:
                        //send message to all connected users:: --
                        //Start a timer to ensure timeout do not happen. On timeout again initiate the enquiry.
                        rc_state = RC_STATE.RC_STATE_ENQUIRING;
                        stateChange = true;
                        //update - here RemoteDisconnected::

                        break;
                    case TRIGGER_DEVICE_DISCONNECTED:
                        rc_state = RC_STATE.RC_STATE_OFFLINE;
                        stateChange = true;
                        break;
                    case TRIGGER_RESPONSE_RECEIVED:
                        lastPeriodicConnectTime = TimeClock.getEpocTime();
                        stateChange = true;
                        break;
                        //record last time - for declaring timeout from periodic thread:

                }
            }
            break;
        }
        return stateChange;
    }


    public static String FILENAME = "MS RcPeriodic";
    //If master - periodically broadcast
    public static int periodicBroadcastTime = 3000; //3 seconds
    //If remote - periodically check for timeout of master request:
    public static int periodicMaxTimeout = 20000; //30 seconds
    public static int timeWithoutBroadcast = 0; //reset every time master is received. otherwise incremented max till periodicMaxtimeout
    public class RcPeriodic {

        public RcPeriodic()
        {

        }
        //
        public void startPeriodic()
        {
            new Thread() {
                @Override
                public void run()
                {
                    while (true)
                    {
                        if (rc_state==RC_STATE.RC_STATE_MASTER)
                        {
                            //Send periodic masterbroadcast
                                RcMessages.broadcastMasterConnected(deviceAddress);
                        }
                        else if ( rc_state==RC_STATE.RC_STATE_ENQUIRING)
                        {
                                //send to self to keep chat alive::
                                //RcMessages.sendMasterEnquiry(deviceAddress, ChatXMPPService.USER_NAME, 0);
                                //RcMessages.broadcastMasterEnquiry(deviceAddress);
                        }
                        else if ( rc_state==RC_STATE.RC_STATE_REMOTE_CONNECTED)
                        {
                            long currentTime = TimeClock.getEpocTime();
                            if ((currentTime - lastPeriodicConnectTime) > periodicMaxTimeout)
                            {
                                //declare timeout - to trigger state change
                                updateRCState(RC_STATE_TRIGGER.TRIGGER_ENQUIRY_TIMEOUT);
                            }
                            RcMessages.sendMasterEnquiry(deviceAddress, ChatXMPPService.USER_NAME,0);
                            //Last received MasterConnected>max time - declare timeout - update state:
                        }
                        try
                        {
                            Thread.sleep(periodicBroadcastTime);
                        } catch (Exception e) {
                            Log.e(FILENAME, e.getMessage());
                        }
                    }
                }
            }.start();
        }
    }
}
