package com.bitskeytechnologies.www.motostar.RC;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.bitskeytechnologies.www.motostar.Chat.gen.ChatXMPPService;
import com.bitskeytechnologies.www.motostar.Chat.users.UserObject;
import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.TimeClock;
import com.google.android.gms.nearby.messages.internal.MessageType;

import org.jivesoftware.smack.chat.Chat;

/**
 * Created by Manish on 8/2/2017.
 */
public class RcMessages {


    public static String FILENAME = "MS RcMessages";
    final static int COMMAND_START = 0; //command to turnon equipment
    final static int COMMAND_STOP = 1;  //command to switchoff equipment
    final static int COMMAND_REFRESH = 2; //command to refresh data from equipment

    final static int ACK_START = 3; //ack of message delivered to master
    final static int ACK_STOP = 4; //ack of message delivered to master
    final static int ACK_REFRESH = 5; //ack of message delivered to master

    final static int RESPONSE_START = 6; //response of start - success/failure
    final static int RESPONSE_STOP = 7; //response of stop - success/failure
    final static int RESPONSE_REFRESH = 8; //response of refresh - perhaps shall share the detailed status here



    final static int BROADCAST_MASTER_CONNECTED = 9; //sent to everyone that master phone is connected

    final static int BROADCAST_MASTER_ENQUIRY = 10; //sent to everyone that master phone is connected

    final static int BROADCAST_MASTER_DISCONNECTED = 11; //sent to everyone that master phone is disconnected - no longer a master device

    final static int BROADCAST_EVENT = 12;  //events are broadcast as received
    final static int BROADCAST_ALARM = 13; //alarms are broadcast as received
    final static int BROADCAST_PROFILE = 14; //profile is broadcast after becoming master or on refresh
    final static int BROADCAST_LAST_EVENTS = 15; //broadcast all the last events after becoming master or on refresh
    final static int BROADCAST_LAST_ALARMS = 16; //broadcast all the last alarms after becoming master or on refresh
    final static int BROADCAST_STATUS = 17; //broadcast the status after becoming master or on refresh

    //Broadcast - send messages:

    public static void broadcastMasterConnected(String groupName)
    {
        Log.d(FILENAME, "broadcastMasterConnected");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_MASTER_CONNECTED, 0, null);
        rcMessage.encodeMessage();
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    //No eqnuiry to be sent.. Master will broadcast itself..
    public static void broadcastMasterEnquiry(String groupName)
    {
        Log.d(FILENAME, "broadcastMasterEnquiry");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_MASTER_ENQUIRY, 0, null);
        rcMessage.encodeMessage();
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    public static void broadcastMasterDisConnected(String groupName)
    {
        Log.d(FILENAME, "broadcastMasterDisConnected");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_MASTER_DISCONNECTED, 0, null);
        rcMessage.encodeMessage();
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    public static void broadcastEvent(String groupName, int length, byte[] buffer)
    {
        Log.d(FILENAME, "broadcastEvent");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_EVENT, length, buffer);
        rcMessage.encodeMessage();
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    public static void broadcastLastEvents(String groupName, int length, byte[] buffer)
    {
        Log.d(FILENAME, "broadcastLastEvents");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_LAST_EVENTS, length, buffer);
        rcMessage.encodeMessage();
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }


    public static void broadcastAlarm(String groupName, int length, byte[] buffer)
    {
        Log.d(FILENAME, "broadcastAlarm");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_ALARM, length, buffer);
        rcMessage.encodeMessage();
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    public static void broadcastLastAlarms(String groupName, int length, byte[] buffer)
    {
        Log.d(FILENAME, "broadcastLastAlarms");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_LAST_ALARMS, length, buffer);
        rcMessage.encodeMessage();
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    public static void broadcastProfile(String groupName, int length, byte[] buffer)
    {
        Log.d(FILENAME, "broadcastProfile");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_PROFILE, length, buffer);
        rcMessage.encodeMessage();
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    public static void broadcastStatus(String groupName, int length, byte[] buffer)
    {
        Log.d(FILENAME, "broadcastStatus");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_STATUS, length, buffer);
        rcMessage.encodeMessage();
        //rcMessage.decodeMessage(rcMessage.encodedMessage);
        //return rcMessage.message;
        ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    //############################ simple one one messages ###################

    public static void sendCommandStart( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendCommandStart");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.COMMAND, COMMAND_START, 0, null);
    }

    public static void sendCommandStop( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendCommandStop");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.COMMAND, COMMAND_STOP, 0, null);

    }

    public static void sendCommandRefresh( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendCommandRefresh");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.COMMAND, COMMAND_REFRESH, 0, null);
    }

    public static void sendAckStart( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendAckStart");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.ACK, ACK_START, 0, null);
    }

    public static void sendAckStop( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendAckStop");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.ACK, ACK_STOP, 0, null);
    }

    public static void sendAckRefresh( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendAckRefresh");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.ACK, ACK_REFRESH, 0, null);
    }

    public static void sendRespStart( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendRespStart");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.RESPONSE, RESPONSE_START, 0, null);
    }

    public static void sendRespStop( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendRespStop");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.ACK, RESPONSE_STOP, 0, null);
    }

    public static void sendRespRefresh( Context context, String groupName, String receiverUserName,int equipmmentId )
    {
        Log.d(FILENAME, "sendRespRefresh");
        sendMessage(context, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.ACK, RESPONSE_REFRESH, 0, null);
    }


    //No eqnuiry to be sent.. Master will broadcast itself..
    public static void sendMasterEnquiry(String groupName,String receiverUserName,int equipmmentId)
    {
        Log.d(FILENAME, "broadcastMasterEnquiry");
        RcMessage rcMessage = new RcMessage(groupName, null, null, 0,RcMessage.RcMessageType.BROADCAST , BROADCAST_MASTER_ENQUIRY, 0, null);
        rcMessage.encodeMessage();
        sendMessage(MainActivity.mContext, groupName, null, receiverUserName, equipmmentId, RcMessage.RcMessageType.BROADCAST, BROADCAST_MASTER_ENQUIRY, 0, null);
        //ChatXMPPService.sendToAllinGroup(rcMessage.encodedMessage);
    }

    public static void sendMessage( Context context, String groupName, String senderUserName, String receiverUserName, int equipmmentId, RcMessage.RcMessageType messageType, int messageId, int length, byte[] message)
    {
        RcMessage rcMessage = new RcMessage(groupName, senderUserName, receiverUserName, equipmmentId, messageType, messageId, length, message);
        rcMessage.encodeMessage();
        //send from here::
        final long timeStamp = TimeClock.getEpocTime();
        String receiptId = ChatXMPPService.SendMessage(
                context,
                rcMessage.encodedMessage,
                receiverUserName,
                ChatXMPPService.CHAT_MESSAGE_TYPE_TEXT,
                timeStamp,
                false,
                false
        ); //false means first time being sent
    }

    public static void handleReceivedMessage(String encodedMessage, String sender)
    {
        Log.d(FILENAME, "handleReceivedMessage");
        RcMessage rcMessage = new RcMessage(encodedMessage);
        rcMessage.senderUserName = sender;
        rcMessage.decodeMessage(encodedMessage);
        //process received Message here:

        //First check - if this groupId - registered or not?
        if (MainActivity.getIndexOfSelectedDevice(rcMessage.grouName)== -1)
        {
            //This device is never registered for this mobile. Let's ignore the received message
            return;
        }

        //invokeOnMainThread(rcMessage.messageId, rcMessage);


        switch(rcMessage.messageId)
        {
            case COMMAND_START: processCommandStart(rcMessage); break;
            case COMMAND_STOP: processCommandStop(rcMessage); break;
            case COMMAND_REFRESH: processCommandRefresh(rcMessage); break;

            case ACK_START: processAckStart(rcMessage); break;
            case ACK_STOP: processAckStop(rcMessage); break;
            case ACK_REFRESH: processAckRefresh(rcMessage); break;

            case RESPONSE_START: processResponseStart(rcMessage); break;
            case RESPONSE_STOP: processResponseStop(rcMessage); break;
            case RESPONSE_REFRESH: processResponseRefresh(rcMessage); break;

            case BROADCAST_ALARM : processAlarm(rcMessage); break;
            case BROADCAST_EVENT : processEvent(rcMessage); break;

            case BROADCAST_LAST_ALARMS : processLastAlarms(rcMessage); break;
            case BROADCAST_LAST_EVENTS : processLastEvents(rcMessage); break;

            case BROADCAST_MASTER_CONNECTED : processBroadcastMasterConnected(rcMessage); break;
            case BROADCAST_MASTER_DISCONNECTED : processBroadcastMasterDisConnected(rcMessage); break;

            case BROADCAST_MASTER_ENQUIRY : processBroadcastMasterEnquiry(rcMessage); break;
            case BROADCAST_STATUS : processStatus(rcMessage); break;

        }

    }

    /*
    public static void invokeOnMainThread(int messageId, final RcMessage rcMessage)
    {
        if (MainActivity.mContext == null)
        {
            Log.e(FILENAME, "MainActivity.mContext is null");
            return;
        }
        Handler mainHandler = new Handler(MainActivity.mContext.getMainLooper());
        Runnable myRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                switch(rcMessage.messageId)
                {
                    case COMMAND_START: processCommandStart(rcMessage); break;
                    case COMMAND_STOP: processCommandStop(rcMessage); break;
                    case COMMAND_REFRESH: processCommandRefresh(rcMessage); break;

                    case ACK_START: processAckStart(rcMessage); break;
                    case ACK_STOP: processAckStop(rcMessage); break;
                    case ACK_REFRESH: processAckRefresh(rcMessage); break;

                    case RESPONSE_START: processResponseStart(rcMessage); break;
                    case RESPONSE_STOP: processResponseStop(rcMessage); break;
                    case RESPONSE_REFRESH: processResponseRefresh(rcMessage); break;

                    case BROADCAST_ALARM : processAlarm(rcMessage); break;
                    case BROADCAST_EVENT : processEvent(rcMessage); break;

                    case BROADCAST_LAST_ALARMS : processLastAlarms(rcMessage); break;
                    case BROADCAST_LAST_EVENTS : processLastEvents(rcMessage); break;

                    case BROADCAST_MASTER_CONNECTED : processBroadcastMasterConnected(rcMessage); break;
                    case BROADCAST_MASTER_DISCONNECTED : processBroadcastMasterDisConnected(rcMessage); break;

                    case BROADCAST_MASTER_ENQUIRY : processBroadcastMasterEnquiry(rcMessage); break;
                    case BROADCAST_STATUS : processStatus(rcMessage); break;

                }

            } // This is your code
        };
        mainHandler.post(myRunnable);
    }
    */

    public static void processCommandStart(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processCommandStart");
        // if connected - send start command to device
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        //if (dvObj.)
        if (dvObj==null) {return;}
        dvObj.startDevice();
    }

    public static void processCommandStop(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processCommandStop");
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        //if (dvObj.)
        if (dvObj==null) {return;}
        dvObj.stopDevice();
    }

    public static void processCommandRefresh(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processCommandRefresh");
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        if (dvObj==null) {return;}
        if (dvObj.isPaired) {
            dvObj.writeSynch('0');
        }
    }

    public static void processAckStart(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processAckStart");
    }

    public static void processAckStop(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processAckStop");
    }

    public static void processAckRefresh(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processAckRefresh");
    }

    public static void processResponseStart(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processResponseStart");
    }

    public static void processResponseStop(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processResponseStop");
    }

    public static void processResponseRefresh(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processResponseRefresh");
    }


    public static void processBroadcastMasterConnected(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processBroadcastMasterConnected");

        //Main Thread--execute::
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        if (dvObj==null) {return;}
        //first update rcState:
        boolean statChange = dvObj.rcStateObj.updateRCState(RcState.RC_STATE_TRIGGER.TRIGGER_RESPONSE_RECEIVED);
        if (statChange)
        {
            dvObj.rcMaster=rcMessage.senderUserName;
            dvObj.invokeOnMainThread(); //updates connection status:
        }


    }

    public static void processBroadcastMasterDisConnected(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processBroadcastMasterDisConnected");
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        //first update rcState:
        if (dvObj==null) {return;}

        boolean statChange = dvObj.rcStateObj.updateRCState(RcState.RC_STATE_TRIGGER.TRIGGER_ENQUIRY_TIMEOUT);
        if (statChange)
        {
            dvObj.rcMaster=null;
            dvObj.invokeOnMainThread(); //updates connection status:
        }
    }

    public static void processBroadcastMasterEnquiry(RcMessage rcMessage)
    {

        /* Do nothing here for now.. this is only dummy message being sent to self currently - so that chat keeps alive:
        Log.d(FILENAME, "processBroadcastMasterEnquiry");
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        //first update rcState:
        if (dvObj==null) {return;}
        if (dvObj.rcStateObj.rc_state == RcState.RC_STATE.RC_STATE_MASTER)
        {
            broadcastMasterConnected(rcMessage.grouName);
        }
        //dvObj.rcStateObj.updateRCState(RcState.RC_STATE_TRIGGER.TRIGGER_ENQUIRY_TIMEOUT);
        */
    }




    public static void processEvent(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processEvent");
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        if (dvObj==null) {return;}
        dvObj.processEvent(rcMessage.message, rcMessage.messageLenth);
    }

    public static void processStatus(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processStatus");
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        if (dvObj==null) {return;}
        dvObj.processCommandResponse(rcMessage.message);
    }

    public static void processLastEvents(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processLastEvents");
        DeviceObject dvObj = MainActivity.getDeviceReference(rcMessage.grouName);
        if (dvObj==null) {return;}
        dvObj.processLastEvents(rcMessage.message, rcMessage.messageLenth);
    }

    public static void processLastAlarms(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processLastAlarms");
    }


    public static void processAlarm(RcMessage rcMessage)
    {
        Log.d(FILENAME, "processAlarm");

    }

}
