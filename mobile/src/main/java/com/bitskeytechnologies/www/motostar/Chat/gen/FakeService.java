package com.bitskeytechnologies.www.motostar.Chat.gen;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bitskeytechnologies.www.motostar.R;

//import com.bitskeys.track.R;

public class FakeService extends Service {

    private static final String FILENAME = "GBC FakeService";
    public FakeService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(FILENAME, "onCreate FakeService  ");
        runAsForeground();
    }


    private void runAsForeground(){
        Log.d(FILENAME, "runAsForeground +");
        Intent notificationIntent = new Intent(this, FakeService.class);
        PendingIntent pendingIntent= PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification=new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_icon)
                .setContentText(getString(R.string.app_title))
                .setContentIntent(pendingIntent).build();
        //notification.flags |= Notification.FLAG_NO_CLEAR;
        startForeground(42, notification);
    }

    @Override
    public void onDestroy() {
        Log.d(FILENAME, "onDestroy service FakeSerivce +");
        stopForeground(true);
        // Cancel the persistent notification.
        //mNM.cancel(R.string.remote_service_started);
        // Tell the user we stopped.
        //Toast.makeText(this, R.string.remote_service_stopped, Toast.LENGTH_SHORT).show();
    }
}
