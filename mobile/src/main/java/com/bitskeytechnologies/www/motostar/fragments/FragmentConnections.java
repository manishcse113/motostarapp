package com.bitskeytechnologies.www.motostar.fragments;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.bitskeytechnologies.www.motostar.BlueToothDevicesReceiver;
import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.R;

import java.util.ArrayList;

/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * {@link FragmentConnections.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentConnections#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentConnections extends Fragment
        implements ScanDevicesList.ScanDevicesListInterface,
        PairedDevicesList.PairedDevicesListInterface

{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    //private static final String ARG_PARAM1 = "param1";
    //private static final String ARG_PARAM2 = "param2";
    // The request code must be 0 or greater.
    //private static final int PLUS_ONE_REQUEST_CODE = 0;
    // The URL to +1.  Must be a valid URL.
    //private final String PLUS_ONE_URL = "http://developer.android.com";
    // TODO: Rename and change types of parameters
    //private String mParam1;
    //private String mParam2;
    //private PlusOneButton mPlusOneButton;

    Context mContext;
    private static String FILENAME = "MS FragConnections#";

    //final TextView out=(TextView)findViewById(R.id.out);
    private static View view;

    ImageButton buttonScan;
    //Button buttonOff;
    //Button buttonDesc;
    TextView textScan;
    TextView textScanHint;
    TextView textScanList;
    ListView listScan;
    TextView textPairList;
    ListView listPair;
    ImageButton buttonMonitor;
    ImageButton buttonProfile;

    PairedDevicesList pairDevicesAdapter = null;
    private ListView pairedLv = null;

    ScanDevicesList scanDevicesAdapter = null;
    private ListView scanlv = null;

    ArrayList<BluetoothDevice> arrayListPairedBluetoothDevices = null;
    //ArrayList<BluetoothDevice> arrayListBluetoothDevices = null;

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_DISCOVERABLE_BT = 0;
    //BluetoothAdapter btAdapter;

    // Create a BroadcastReceiver for ACTION_FOUND.
    private BroadcastReceiver mReceiver = null;

    static HandleSeacrh handleSeacrh;
    BluetoothDevice bdDevice;
    BluetoothClass bdClass;
    BluetoothAdapter mBluetoothAdapter;

    public boolean fragmentAlreadyAdded = false;

    public boolean getIsFragmentAlreadyAdded()
    {
        return fragmentAlreadyAdded;
    }
    public void setFragmentAlreadyAdded(boolean flag)
    {
        fragmentAlreadyAdded = flag;
    }
    //private OnFragmentInteractionListener mListener;

    public interface OnConnectionsFragmentDone {
        // TODO: Update argument type and name
        //void createPagingView();
        //void createConnectionsView();
        void startNextFragment();
        //void initBluetoothConnection();
        void makeDeviceDiscoverable();
        //void getPairedDevices();
        boolean pair(BluetoothDevice btDevice);
        boolean enableBluetooth();
        void selectDevice(String mac);
        void Vibrate();
    }
    private OnConnectionsFragmentDone mListener;

    //final static String FILENAME = "MS FragConnections";
    public FragmentConnections() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentConnections.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentConnections newInstance(String param1, String param2)
    {
        Log.d(FILENAME, "newInstance");
        FragmentConnections fragment = new FragmentConnections();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.d(FILENAME, "onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }

        handleSeacrh = new HandleSeacrh();
        arrayListPairedBluetoothDevices = new ArrayList<BluetoothDevice>();
        //arrayListBluetoothDevices = new ArrayList<BluetoothDevice>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        Log.d(FILENAME, "onCreateView");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_connections, container, false);

        if (container == null) {
            return null;
        }
        scanlv = (ListView) view.findViewById(R.id.listViewDetected);
        pairedLv = (ListView) view.findViewById(R.id.listViewPaired);

        buttonScan = (ImageButton) view.findViewById(R.id.buttonScan);
        textScan = (TextView) view.findViewById(R.id.textScan);
        textScanHint = (TextView) view.findViewById(R.id.textScanHint);
        //buttonOff = (Button) view.findViewById(R.id.buttonOff);
        //buttonDesc = (Button) view.findViewById(R.id.buttonDesc);

        //buttonSearch = (Button) view.findViewById(R.id.buttonSearch);

        textScanList = (TextView) view.findViewById(R.id.textScanList);
        //listScan = (ListView) view.findViewById(R.id.listViewDetected);

        textPairList = (TextView) view.findViewById(R.id.textPairList);
        //listPair = (ListView) view.findViewById(R.id.listViewPaired);

        //buttonMonitor = (ImageButton) view.findViewById(R.id.left_button);
        //buttonProfile = (ImageButton) view.findViewById(R.id.right_button);

        //Bluetooth On
        buttonScan.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                //initiateScan();


                textScanHint.setText("Scanning in Progress..\nIt may take few seconds...");
                Log.d(FILENAME, "Scanning in Progress..It may take few seconds...");
                if (mListener!=null)
                {
                    mListener.Vibrate();
                    mListener.enableBluetooth();
                }

                if (MainActivity.mTabPageAdapter.IsTabsPagerAdapterAdded()==false)
                {

                    if (MainActivity.getPairedDevicesList().size() > 0) {
                        Log.d(FILENAME, "Scanning done. device found");
                        // if one of them contains our device, then go to next fragment
                        mListener.startNextFragment();
                    }
                }
                else
                {
                    Log.d(FILENAME, "Scanning done. No device found");
                }
                textScanHint.setText("Scanning done. Ensure MotoStar is in Bluetooth Range and powered on");
            }
        });
        //Find the +1 button
        //mPlusOneButton = (PlusOneButton) view.findViewById(R.id.plus_one_button);
        updateScannedDevicesListView();
        updatePairedDevicesView();
        return view;
    }


    /*
    public void initiateScan()
    {
        textScan.setText("Scanning in Progress..\nIt may take few seconds...");
        Log.d(FILENAME, "Scanning in Progress..It may take few seconds...");
        if (mListener!=null)
        {
            mListener.enableBluetooth();
        }

        if (MainActivity.getPairedDevicesList().size()>0)
        {
            Log.d(FILENAME, "Scanning done. device found");
            // if one of them contains our device, then go to next fragment
            mListener.startNextFragment();
        }
        else
        {
            Log.d(FILENAME, "Scanning done. No device found");
        }
        textScan.setText("Scanning done\nYou need to rescan,if your device is not listed below");
    }
    */


    public boolean isResumed = false;
    @Override
    public void onResume() {
        super.onResume();
        Log.d(FILENAME, "onResume");
        isResumed = true;
        //refresh();
        //mListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(FILENAME, "onPause");
        isResumed = false;
        //mListener = null;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context)
    {
        Log.d(FILENAME, "onAttach");
        super.onAttach(context);
        mContext = context;

        if (arrayListPairedBluetoothDevices == null) {
            arrayListPairedBluetoothDevices = new ArrayList<BluetoothDevice>();
        }

        if (context instanceof OnConnectionsFragmentDone) {
            mListener = (OnConnectionsFragmentDone) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnWelcomeFragmentDone");
        }
    }

    @Override
    public void onDetach()
    {
        Log.d(FILENAME, "onDetach");

        // Make sure we're not doing discovery anymore
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.cancelDiscovery();
        }


        if ( scanDevicesAdapter!= null)
        {
            scanDevicesAdapter.clear();
            scanDevicesAdapter = null;
        }

        if ( pairDevicesAdapter!= null)
        {
            pairDevicesAdapter.clear();
            pairDevicesAdapter = null;
        }


        if (mReceiver!=null)
        {
            mContext.unregisterReceiver(mReceiver);
        }

        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
    }

    private void updatePairedDevicesView()
    {
        Log.d(FILENAME, "updatePairedDevicesView");
        int vg = 1;
        if (pairDevicesAdapter == null)
        {
            if (pairedLv != null)
            {
                Log.d(FILENAME, "Creating new devicesList");
                pairDevicesAdapter = new PairedDevicesList(mContext, vg, MainActivity.getPairedDevicesList());
                pairedLv.setAdapter(pairDevicesAdapter);
                pairDevicesAdapter.setCallback(this);
                pairDevicesAdapter.notifyDataSetChanged();
            }
            else
            {
                Log.d(FILENAME, "Invalid case");
            }
        }
        if (pairDevicesAdapter != null)
        {
            /*
            Log.d(FILENAME, "updating the existing list");
            pairDevicesAdapter.setCallback(this);

            pairDevicesAdapter.updateDevicesList(MainActivity.getPairedDevicesList());
            pairDevicesAdapter.notifyDataSetChanged();
            */
            Log.d(FILENAME, "Creating new devicesList");
            pairDevicesAdapter = new PairedDevicesList(mContext, vg, MainActivity.getPairedDevicesList());
            pairedLv.setAdapter(pairDevicesAdapter);
            pairDevicesAdapter.setCallback(this);
            pairDevicesAdapter.notifyDataSetChanged();
         }
    }
public void updateScannedDevicesListView() {
        Log.d(FILENAME, "getScannedDevicesList");
        int vg = 2;
        if (scanDevicesAdapter == null)
        {
            if (scanlv != null) {
                Log.d(FILENAME, "Creating new devicesList");
                if (MainActivity.getScanDevicesSize()>0)
                {
                    if (textScanList==null)
                    {
                        textScanList = (TextView) view.findViewById(R.id.textScanList);
                    }
                    textScanList.setText("Pair with MotoStar...");
                }

                scanDevicesAdapter = new ScanDevicesList(mContext, vg, BlueToothDevicesReceiver.tempScanDevicesObjList);
                scanlv.setAdapter(scanDevicesAdapter);
                scanDevicesAdapter.setCallback(this);
                scanDevicesAdapter.notifyDataSetChanged();
            }
            else
            {
                Log.d(FILENAME, "Invalid case");
            }
        }
        if (scanDevicesAdapter != null) {
            Log.d(FILENAME, "updating existing scan devicesList");
            if (MainActivity.getScanDevicesSize() > 0) {
                textScanList.setText("Pair with MotoStar...");
            }
            scanDevicesAdapter = new ScanDevicesList(mContext, vg, BlueToothDevicesReceiver.tempScanDevicesObjList);
            scanlv.setAdapter(scanDevicesAdapter);
            scanDevicesAdapter.setCallback(this);
            scanDevicesAdapter.notifyDataSetChanged();

            //scanDevicesAdapter.updateDevicesList(BlueToothDevicesReceiver.tempScanDevicesObjList);
            //scanDevicesAdapter.notifyDataSetChanged();
        }
    }
    class HandleSeacrh extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 111:

                    break;

                default:
                    break;
            }
        }
    }

    public void pair(DeviceObject device)
    {
        Log.d(FILENAME, "pair");

        if (mListener!=null) {
            //This device is being paired
            mListener.pair(device.btDevice);
            mListener.Vibrate();
            //refresh scan device list:
            updateScannedDevicesListView();
            //refresh paired device list:
            updatePairedDevicesView();
            mListener.selectDevice(device.deviceAddress);
            mListener.startNextFragment();
        }
    }


    public void nextView(DeviceObject device)
    {
        Log.d(FILENAME, "nextView");

        if (mListener!=null)
        {
            mListener.Vibrate();
            mListener.startNextFragment();
            mListener.selectDevice(device.deviceAddress);

        }
    }

    public void unPair(DeviceObject device) {
        Log.d(FILENAME, "unPair");
        /*
        try
        {
            Boolean removeBonding = removeBond(device.btDevice);
            if (removeBonding)
            {
                //refresh scan device list:
                getScannedDevicesList();
                //refresh paired device list:
                getPairedDevices();
            }
            Log.i("Log", "Removed" + removeBonding);
        }
        catch (Exception e)
        {
            Log.e(FILENAME, e.getMessage());
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        */
    }

    public void update()
    {
        updateScannedDevicesListView();
        updatePairedDevicesView();
    }
}