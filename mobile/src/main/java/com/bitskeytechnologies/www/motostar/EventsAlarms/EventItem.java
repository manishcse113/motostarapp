package com.bitskeytechnologies.www.motostar.EventsAlarms;

import android.content.Context;
import android.util.Log;

import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.TimeClock;
import com.bitskeytechnologies.www.motostar.db.DataSource;
import com.bitskeytechnologies.www.motostar.db.EventRecord;
import com.bitskeytechnologies.www.motostar.db.LastEventRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Manish on 7/21/2017.
 */

public class EventItem {
    public int eventId;
    public String eventText;
    public String eventValue;

    public byte date;
    public byte month;
    public byte year;
    public byte hour;
    public byte minute;
    public byte second = 0;
    public byte reason = 100;
    public int readBytes =255;

    public long referencTime = 0;

    public static String FILENAME = "MS eventItem";
    public String deviceAddress = null;
        /*
        public EventItem(String eventText, String eventValue) {
            this.eventText = eventText;
            this.eventValue = eventValue;
        }
        */

    public void setEventValue(String eventValue) {
        this.eventValue = eventValue;
    }
    public String getEventValue() {
        return eventValue;
    }
    public String getEventText() {
        return eventText;
    }
    public void setEventValue(byte date, byte month, byte year, byte hr, byte minute) {
        eventValue = new String(
                String.valueOf(date) + "/" +
                        String.valueOf(month) + "/" +
                        String.valueOf(year) + "--" +
                        String.valueOf(hr) + ":" +
                        String.valueOf(minute));
    }
    String getEventName(int id) {
        return eventNames[id];
    }
    int getEventId(String eventName) {
        int id = 0;
        for (String eName : eventNames) {
            if (eName.equals(eventName)) {
                break;
            }
            id++;
        }
        return id;
    }

    public EventItem(String deviceAddress, int id, String name, String value)
    {
        this.deviceAddress = deviceAddress;
        eventId = id;
        eventText = name;
        eventValue = value;
    }

    public EventItem(String deviceAddress, byte[] buffer, int maxbyte)
    {
        this.deviceAddress = deviceAddress;
        int i = 0;
        i++;//for a
        i++; //for b
        i++; //for e
        if (maxbyte < i) return;
        eventId = (byte) (buffer[i++] - (char) ('0'));
        eventText = getEventName(eventId);
        if (maxbyte < i) return;
        date = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        month = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        year = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        hour = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        minute = (byte) (buffer[i++] - (char) ('0'));

        setEventValue(date, month, year, hour, minute);
        if (maxbyte < i) return;
        if (eventId==0 ||eventId==1) //event Id 0 & 1 has reason field as well.
        {
            reason = (byte) (buffer[i++] - (char) ('0'));
        }
        //reason is to be added yet.
        readBytes = i;

        /*
        if (context == null && MainActivity.mContext!=null)
        {
            context =MainActivity.mContext;
        }
        */

        EventRecord.storeEventRecord(MainActivity.mContext, deviceAddress, eventText, eventId, date, month, year, hour, minute, second, reason, eventValue);
        LastEventRecord.updateLastEventRecord(MainActivity.mContext, deviceAddress, eventText, eventId, date, month, year, hour, minute, second, reason, eventValue);
        calculateReferenceTime();
    }

    public EventItem(String deviceAddress, byte[] buffer, int maxbyte, int offset)
    {
        this.deviceAddress = deviceAddress;
        int i = offset;
        if (maxbyte < i) return;
        eventId = (byte) (buffer[i++] - (char) ('0'));
        eventText = getEventName(eventId);
        if (maxbyte < i) return;
        date = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        month = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        year = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        hour = (byte) (buffer[i++] - (char) ('0'));
        if (maxbyte < i) return;
        minute = (byte) (buffer[i++] - (char) ('0'));

        setEventValue(date, month, year, hour, minute);
        if (maxbyte < i) return;

        if (eventId==0 ||eventId==1) //event Id 0 & 1 has reason field as well.
        {

            reason = (byte) (buffer[i++] - (char) ('0'));
        }
        //reason is to be added yet.
        readBytes = i;
        calculateReferenceTime();

        LastEventRecord.updateLastEventRecord(MainActivity.mContext, deviceAddress, eventText, eventId, date, month, year, hour, minute, second, reason, eventValue);
    }

    public EventItem(String deviceAddress, int id, byte year, byte month, byte date, byte hour, byte second)
    {
        this.deviceAddress = deviceAddress;
        eventId = id;
        eventText = getEventName(eventId);
        this.date = date;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.minute = minute;

        setEventValue(date, month, year, hour, minute);
        calculateReferenceTime();
        LastEventRecord.updateLastEventRecord(MainActivity.mContext, deviceAddress, eventText, eventId, date, month, year, hour, minute, second, reason, eventValue);
    }

    public EventItem(String deviceAddress, String eventName, long eventId, long date, long month, long year, long hr, long min, long second, long reason, String eventValue)
    {
        this.deviceAddress = deviceAddress;
        this.eventText = eventName;
        this.eventId = (int)eventId;
        this.date = (byte) date;
        this.month = (byte)month;
        this.year = (byte)year;
        this.hour = (byte)hr;
        this.minute = (byte)min;
        this.second = (byte)second;
        this.reason = (byte)reason;
        this.eventValue = eventValue;
        calculateReferenceTime();
    }

    public long getEpocSeconds()
    {
        return referencTime;
    }

    public long calculateReferenceTime()
    {

        referencTime = (((((this.year - 2017)*12 + (this.month-1))*30 + this.date-1)*24 + this.hour)*60 + this.minute)*60 + this.second;
        return referencTime;
    }



    public static int lastEventIndexStart = 0;
    public static int lastEventIndexStop = 1;
    public static int lastEventIndexAutoStart = 2;
    public static int lastEventIndexAutoStop = 3;
    public static int lastEventIndexwaterDetect = 4;
    public static int lastEventIndexwaterLost = 5;
    public static int lastEventIndexFullDetect = 6;
    public static int lastEventIndexFullLost = 7;
    public static int lastEventIndexHighDetect = 8;
    public static int lastEventIndexHighLost = 9;
    public static int lastEventIndexMidDetect = 10;
    public static int lastEventIndexMidLost = 11;
    public static int lastEventIndexLowDetect = 12;
    public static int lastEventIndexLowLost = 13;
    public static int lastEventIndexAutoOff = 14;
    public static int lastEventIndexAutoOn = 15;
    public static int lastEventIndexLastSyncup = 16;

    public static String eventNames [] =
            {
                    "Motor Started",
                    "Motor Stopped",
                    "Auto Started",
                    "Auto Stopped",
                    "Water Detected",
                    "Water Lost ",
                    "Full Level Detected",
                    "Full Level Lost",
                    "High Level Detected",
                    "High Level Lost",
                    "Mid Level Detected",
                    "Mid Level Lost",
                    "Low Level Detected",
                    "Low Level Lost",

                    "Auto Mode Off",
                    "Auto Mode On",
                    "Last Syncup"
            };

    public static String eventValues [] =
            {
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0",
                    "1/Jan/2000--0:0"
            };


    public static void iniEventList(Context context, List eventList, String macAddress)
    {
        int i =0;
        DataSource db = new DataSource(context);
        db.open();
        //use mac-address instead of deviceName here:
        //eventtable - will store macaddress in deviceName field:
        List<EventRecord> reqs = db.getEventRecords(macAddress);
        for (EventRecord _req : reqs)
        {
            final EventItem event = new EventItem(macAddress,
                    _req.getEventName(),
                    _req.getEventId(),
                    _req.getDate(),
                    _req.getMonth(),
                    _req.getYear(),
                    _req.getHr(),
                    _req.getMin(),
                    _req.getSec(),
                    _req.getReason(),
                    _req.getEventValue()
            );
            eventList.add(event);
            i++;
        }
        db.close();
    }

    public static void initUniqueEventList(Context context, List uniqueLastEventList, String macAddress)
    {
        int i =0;
        DataSource db = new DataSource(context);
        db.open();

        List<LastEventRecord> reqs = db.getLastEventRecords(macAddress);
        for (LastEventRecord _req : reqs)
        {
            EventItem event = new EventItem(
                    macAddress,
                    _req.getEventName(),
                    _req.getEventId(),
                    _req.getDate(),
                    _req.getMonth(),
                    _req.getYear(),
                    _req.getHr(),
                    _req.getMin(),
                    _req.getSec(),
                    _req.getReason(),
                    _req.getEventValue()
            );
            uniqueLastEventList.add(event);
            i++;
        }
        db.close();

    }


    public static void initDbLastEventList(Context context, String macAddress)
    {
        DataSource db = new DataSource(context);
        db.open();
        List<LastEventRecord> reqs = db.getLastEventRecords(macAddress);
        if (reqs!=null && reqs.size() < EventItem.eventNames.length) {
            //means - it hasn't been initialized before so initialize it
            db.close();
            LastEventRecord.clearFromDB(context, macAddress);

            storeLastEventRecords(context, macAddress);
        }
        else //Just close the db
        {
            db.close();
        }
    }
    public static void storeLastEventRecords(Context context, String macAddress) {
        //DataSource db = new DataSource(context);
        //db.open();
        int i =0;
        for (String eventName : EventItem.eventNames)
        {
            LastEventRecord.storeEventRecord(context, macAddress, eventName, i, 1, 1, 1, 0, 0, 0, 100, EventItem.eventValues[i]);
            i++;
        }
        //db.close();
    }




    public static void decodeLastEvents(String deviceAdddress, byte[] buffer, int maxbyte, List uniqueLastEventList) {
        int i = 0;
        i++;//for a
        i++; //for b
        i++; //for d

        int readBytes = i;
        while (readBytes < maxbyte-1)
        {
            final EventItem event = new EventItem(deviceAdddress, buffer, maxbyte, readBytes);
            readBytes = event.readBytes;
            uniqueLastEventList.set(event.eventId, event);
        }
    }


    public static String encodeLastEventsJson(Context context, List uniqueLastEventList, String macAddress)
    {
        String jsonString = null;
        try {
            JSONObject json = new JSONObject();
            json.put("mac", macAddress);
            json.put("phone", MainActivity.phone);
            json.put("DeviceName", MainActivity.phoneName);
            json.put("reported_date", TimeClock.get_date_month_year());
            json.put("reported_time", TimeClock.get_hr_min_sec());
            json.put("reportDate", TimeClock.getDayOfMonth());
            json.put("reportMonth", TimeClock.getMonth());
            json.put("reportYear", TimeClock.getyear());
            json.put("reportHour", TimeClock.getHourofDay());
            json.put("reportMinute", TimeClock.getMinute());
            json.put("reportSecond", TimeClock.getMonth());


            int i = 0;
            for (String eventName : EventItem.eventNames) {
                json.put( ((EventItem)(uniqueLastEventList.get(i))).eventText, ((EventItem)(uniqueLastEventList.get(i))).eventValue);
                i++;
            }
            jsonString = json.toString();
        }
        catch (JSONException e)
        {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
        }
        return jsonString;
    }

    public static String encodeLiveEventsJson(DeviceObject dvObj, EventItem eventItem)
    {
        Log.d(FILENAME, "encodeLiveEventsJson");
        String jsonString = null;
        if ( dvObj == null)
        {
            return jsonString;
        }
        else if ( eventItem == null)
        {
            return jsonString;
        }

        try {
            //long differeneLat = Math.abs(latitude - mMyOldLat);
            //long differnceLong = Math.abs(longitude - mMyOldLong);
            String epochtime;
            JSONObject json = new JSONObject();
            json.put("mac", dvObj.deviceAddress);
            json.put("phone", MainActivity.phone);
            json.put("DeviceName", MainActivity.phoneName);
            json.put("reported_date", TimeClock.get_date_month_year());
            json.put("reported_time", TimeClock.get_hr_min_sec());
            json.put("reportDate", TimeClock.getDayOfMonth());
            json.put("reportMonth", TimeClock.getMonth());
            json.put("reportYear", TimeClock.getyear());
            json.put("reportHour", TimeClock.getHourofDay());
            json.put("reportMinute", TimeClock.getMinute());
            json.put("reportSecond", TimeClock.getMonth());

            json.put("event_name", eventItem.eventText);
            json.put("eventId", eventItem.eventId);
            json.put("date", eventItem.date);
            json.put("month", eventItem.month);
            json.put("year", eventItem.year);
            json.put("hour", eventItem.hour);
            json.put("minute", eventItem.minute);
            json.put("second", eventItem.second);
            json.put("reason", eventItem.reason);
            json.put("eventValue", eventItem.eventValue);
            jsonString = json.toString();

        }
        catch (JSONException e)
        {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return jsonString;
        }
        return jsonString;
    }


}

