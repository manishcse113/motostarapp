package com.bitskeytechnologies.www.motostar.fragments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.EventsAlarms.AlarmItem;
import com.bitskeytechnologies.www.motostar.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 1/14/2017.
 */

public class AlarmsListAdapter extends ArrayAdapter {
    int groupid;

    private static List alarmsList = new ArrayList();
    private DeviceObject deviceObject;
    Context context;
    private final String FILENAME = "MS AlarmsListAdapter#";
    //private UserListCallback callback;
    //private UserListActivityCallback callbackActivity;

    Map<String, String> map;
    Map<String, String> treeMap;

    public AlarmsListAdapter(Context context, int textViewResourceId)
    {
        super(context, textViewResourceId);
    }

    public AlarmsListAdapter(Context context, int vg, DeviceObject device) {
        super(context, vg, alarmsList);
        deviceObject = device;
        alarmsList = deviceObject.alarmList;
        this.context = context;
        this.deviceObject = device;
        groupid = vg;
        //this.alarmsList.clear();
        //this.alarmsList.addAll(deviceObject.alarmList);

        //readThread();
        //monitoringConnectivityThread();
    }


    //@Override
    public void add(AlarmItem object) {
        alarmsList.add(object);
        //super.add(object);

        // here - inform all - to update themselves with the new information.
    }

    public void update(AlarmItem object) {
        //search message based on unique id: receiptId and replace the contents.
        boolean updated = false;
        for (Object stat : alarmsList) {
            if (((AlarmItem) (stat)).alarmText.equals(((AlarmItem) (object)).alarmText)) {
                ((AlarmItem) (stat)).alarmValue = ((AlarmItem) (object)).alarmValue;
                updated = true;
                break;
            }
        }
        if (!updated) {
            add(object);
        }

        //super.notify();
    }

    public void addUsersList(List objectList) {
        updateDevicesList(objectList);
    }

    public void updateDevicesList(List objectList)
    {
        //search message based on unique id: receiptId and replace the contents.
        for (Object userObject : objectList) {
            update((AlarmItem) (userObject));
        }

        //super.notify();
    }

    public void clear() {
        //alarmsList.clear();
        super.clear();
    }

    public void delete(AlarmItem object) {
        for (Object stat : alarmsList) {
            if (((AlarmItem) (stat)).alarmText.equals(((AlarmItem) (object)).alarmText)) {
                //alarmsList.remove(object);
                //userObjList.
                super.remove(object);
                break;
            }
        }
    }

    //@Override
    public void delete(int position) {
        int index = position;
        //alarmsList.remove(index);
        super.remove(index);
    }

    public int getCount() {
        return this.alarmsList.size();
    }

    public AlarmItem getItem(int index) {
        AlarmItem object = (AlarmItem) alarmsList.get(index);
        return object;
    }

    View fragment_list_view = null;
    View row = null;
    LayoutInflater inflater = null;
    public View getView(int position, View convertView, ViewGroup parent)
    {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View itemView = inflater.inflate(groupid, parent, false);
        //Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        row = convertView;
        if (row == null) {
            fragment_list_view = inflater.inflate(R.layout.fragment_analytics, null);
            row = inflater.inflate(R.layout.alarm_item, (ViewGroup) fragment_list_view, false);
            populateUserListLayout(inflater, row, position);
        } else {
            //You get here in case of scroll
            populateUserListLayout(inflater, row, position);
        }
        //((ViewGroup) fragment_list_view).addView(row);
        return row;
    }

    TextView alarmNameView = null;
    AlarmItem uObj = null;
    TextView alarmValueView = null;
    public View populateUserListLayout(LayoutInflater inflater, final View user_view, final int position) {
        //Log.d(FILENAME, "inflateUserListLayout");
        try {
            alarmNameView = (TextView) user_view.findViewById(R.id.alarmName);
            //String alarmName = alarmNameView.getText().toString();
            uObj = (AlarmItem) (alarmsList.get(position));
            alarmNameView.setText(uObj.alarmText);
            alarmValueView = (TextView) user_view.findViewById(R.id.alarmValue);
            alarmValueView.setText(uObj.getAlarmValue());

            ImageView alarmReasonView = (ImageView) user_view.findViewById(R.id.alarmReason);
            if (alarmReasonView != null)
            {
                if (uObj.reason == AlarmItem.Raise)
                {
                    alarmReasonView.setImageResource(R.drawable.ic_alarm_raise);
                }
                else if (uObj.reason == AlarmItem.Clear)
                {
                    alarmReasonView.setImageResource(R.drawable.ic_alarm_clear);
                }
                else
                {
                    alarmReasonView.setImageResource(R.drawable.ic_alarm_none);
                }
            }

            //alarmNameView.setSelected(true);

        } catch (Exception e) {
            Log.d(FILENAME, " Exception " + e.getMessage());
        }
        return user_view;
    }

    private fragment_analytics callback;
    //private ScanDevicesListInterface callbackActivity;

    public void setCallback(fragment_analytics callback) {
        Log.d(FILENAME, "setCallback");
        this.callback = callback;
    }

    /*
    public void setScanDevlicesListCallback(FragmentConnections callback) {
        Log.d(FILENAME, "setScanDevlicesListCallback");
        this.callbackActivity = callback;
    }
    */

    public interface StatsListInterface
    {
        //public void unPair(DeviceObject device);
    }

}