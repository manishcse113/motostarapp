package com.bitskeytechnologies.www.motostar.db;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Manish on 5/2/2016.
 */
public class SharedPreference {

    private static String FILENAME = "MS SharedPreference";
    private void SharedPreference()
    {
        speakerOn = true;
     //   trackingOn = true;
     //   locationDialog = true;
        //startTrackingDay = 2;
        //lastTrackingDay = 6;
        //startTrackingTime = 9;
        //stopTrackingTime = 18;

        //trackingInterval = 30; //in seconds
        instanceCount++;
    }

    static String speakerOnPreferenceName = "SpeakerOn";
//    static String trackingPreferenceName = "trackingOn";
 //   static String locationDialogPreferenceName = "locationDialogOn";

   // static String startTrackingDayPreferenceName = "startTrackingDay";
   // static String lastTrackingDayPreferenceName = "lastTrackingDay";
   // static String startTrackingTimePreferenceName = "startTrackingTime";
   // static String lastTrackingTimePreferenceName = "lastTrackingTime";
   // static String trackingIntervalPreferenceName = "trackingInterval";

    static boolean speakerOn;
   // static boolean trackingOn;
   // static boolean locationDialog;



//    static int startTrackingDay = 2;
//    static int lastTrackingDay = 6;
 //   static int startTrackingTime = 9;
 //   static int stopTrackingTime = 18;
 //   static int trackingInterval = 30; //in seconds

    static int instanceCount = 0;
    private static SharedPreference sharedPreference;
    private static String MY_PREFS_NAME = "MotoStarPreference";

    public static SharedPreference getInstance()
    {
        if (instanceCount == 0 )
        {
            sharedPreference = new SharedPreference();
            instanceCount++;
        }
        return sharedPreference;

    }

    public static void saveSharedPreferenceSpeaker(Context context, boolean speakerOn)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(speakerOnPreferenceName, speakerOn);
        //editor.putBoolean(trackingOnPreferenceName, trackingOn);
        editor.commit();
    }

    /*
    public static void saveSharedPreferenceTracking(Context context, boolean trackingOn)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(trackingPreferenceName, trackingOn);
        //editor.putBoolean(trackingOnPreferenceName, trackingOn);
        editor.commit();
    }

    public static void saveSharedPreferenceLocationDialog(Context context, boolean locationDialog)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(locationDialogPreferenceName, locationDialog);
        //editor.putBoolean(trackingOnPreferenceName, trackingOn);
        editor.commit();
    }

    public static void saveSharedPreferenceTrackingStartDay(Context context, int trackingStartDay)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(startTrackingDayPreferenceName, trackingStartDay);
        //editor.putBoolean(trackingOnPreferenceName, trackingOn);
        editor.commit();
    }

    public static void saveSharedPreferenceTrackingLastDay(Context context, int trackingLastDay)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(lastTrackingDayPreferenceName, trackingLastDay);
        //editor.putBoolean(trackingOnPreferenceName, trackingOn);
        editor.commit();
    }

    public static void saveSharedPreferenceTrackingStartTime(Context context, int trackingStartTime)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(startTrackingTimePreferenceName, trackingStartTime);
        //editor.putBoolean(trackingOnPreferenceName, trackingOn);
        editor.commit();
    }

    public static void saveSharedPreferenceTrackingStopTime(Context context, int trackingStopTime)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(lastTrackingTimePreferenceName, trackingStopTime);
        //editor.putBoolean(trackingOnPreferenceName, trackingOn);
        editor.commit();
    }

    public static void saveSharedPreferenceTrackingInterval(Context context, int trackingInterval)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(trackingIntervalPreferenceName, trackingInterval);
        //editor.putBoolean(trackingOnPreferenceName, trackingOn);
        editor.commit();
    }
    */

    public static void retreiveSharedPreference( Context context)
    {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        //String restoredText = prefs.getString("text", null);
        //if (restoredText != null) {
        speakerOn = prefs.getBoolean(speakerOnPreferenceName, true);//"No name defined" is the default value.
        /*
        trackingOn = prefs.getBoolean(trackingPreferenceName, true); //0 is the default value.
        locationDialog = prefs.getBoolean(locationDialogPreferenceName, true); //0 is the default value.

        startTrackingDay = prefs.getInt(startTrackingDayPreferenceName, startTrackingDay);
        lastTrackingDay = prefs.getInt(lastTrackingDayPreferenceName, lastTrackingDay);

        startTrackingTime = prefs.getInt(startTrackingTimePreferenceName, startTrackingTime);
        stopTrackingTime = prefs.getInt(lastTrackingTimePreferenceName, stopTrackingTime);

        trackingInterval = prefs.getInt(trackingIntervalPreferenceName, trackingInterval); //180 seconds by default
*/
        //}
    }

    public boolean getSpeakerOn()
    {
        return speakerOn;
    }

/*
    public boolean getTrackingOn()
    {
        return trackingOn;
    }

    public boolean getLocationDialogPref()
    {
        return locationDialog;
    }

    public int getStartTrackingDay()
    {
        return startTrackingDay;
    }

    public int getLastTrackingDay()
    {
        return lastTrackingDay;
    }

    public int getStartTrackingTime()
    {
        return startTrackingTime;
    }

    public int getLastTrackingTime()
    {
        return stopTrackingTime;
    }

    public int getTrackingInterval()
    {
        return trackingInterval;
    }
    */
}
