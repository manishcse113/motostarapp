package com.bitskeytechnologies.www.motostar.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitskeytechnologies.www.motostar.R;

public class SlidingFragment extends Fragment
{
    public static final String m_Page = "PAGE";
    public static final String FILENAME = "MS SlidingFragment";
    private int mPage;
    public static SlidingFragment newInstance(int page)
    {
        Bundle args = new Bundle();
        args.putInt(m_Page, page);
        SlidingFragment fragment = new SlidingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(m_Page);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view;
        Log.d(FILENAME, "OnCreateView " + mPage);
        switch (mPage)
        {

            case 1:
                view = inflater.inflate(R.layout.fragment_connections, container, false);
                break;
            case 2:
                view = inflater.inflate(R.layout.fragment_control, container, false);
                break;
            case 3:
                view = inflater.inflate(R.layout.fragment_control, container, false);
                break;
            case 4:
                view = inflater.inflate(R.layout.fragment_control, container, false);
                break;
            case 5:
                view = inflater.inflate(R.layout.fragment_control, container, false);
                break;
            default:
                view = inflater.inflate(R.layout.fragment_connections, container, false);
                break;

            //TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            //tvTitle.setText("Fragment #" + mPage);

        }
            return view;
    }
}
