package com.bitskeytechnologies.www.motostar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

public class DevicesRecords extends DBObject
{
	private static String FILENAME = "DevicesRecords";
	private final static String DB_TABLE = "DevicesRecords";

	private final static String COLUMN_DEVICE_NAME = "device_name";
	private final static String COLUMN_MAC = "mac_address";
	private final static String COLUMN_PAIRED_MODE = "paried";
	private final static String COLUMN_STATE = "state";
	private final static String COLUMN_SERVER_UPDATE_REQUIRED = "server_update";
	private final static String COLUMN_MOR_START_HR = "mor_start_hr";
	private final static String COLUMN_MOR_START_MIN = "mor_start_min";
	private final static String COLUMN_MOR_RETRIES = "mor_retries";
	private final static String COLUMN_EVE_START_HR = "eve_start_hr";
	private final static String COLUMN_EVE_START_MIN = "eve_start_min";
	private final static String COLUMN_EVE_RETRIES = "eve_retries";
	private final static String COLUMN_RETRY_INTERVAL = "retry_interval";
	private final static String COLUMN_NO_WATER_STOP_TIME = "no_water_stop_time";


	private final static String[] ALL_COLUMNS = new String[] {
		COLUMN_ID,
			COLUMN_DEVICE_NAME,
			COLUMN_MAC,
			COLUMN_PAIRED_MODE,
			COLUMN_STATE,
			COLUMN_SERVER_UPDATE_REQUIRED,
			COLUMN_MOR_START_HR,
			COLUMN_MOR_START_MIN,
			COLUMN_MOR_RETRIES,
			COLUMN_EVE_START_HR,
			COLUMN_EVE_START_MIN,
			COLUMN_EVE_RETRIES,
			COLUMN_RETRY_INTERVAL,
			COLUMN_NO_WATER_STOP_TIME
		/*
		COLUMN_PHONE_NUMBER,
		COLUMN_VALIDATION_CODE_1,
		COLUMN_VALIDATION_CODE_2,
		COLUMN_CREATION_TIMESTAMP,
		COLUMN_AUTH_PASSWORD
		*/
	};

	private final static String CREATE_TABLE =
			"CREATE TABLE " + DB_TABLE + " (" +
			COLUMN_ID + " integer, " +
					COLUMN_DEVICE_NAME  + " text, " +
					COLUMN_MAC + " text primary key, " +
					COLUMN_PAIRED_MODE + " text, " +
					COLUMN_STATE + " integer, " +
					COLUMN_SERVER_UPDATE_REQUIRED + " integer, " +
					COLUMN_MOR_START_HR + " integer, " +
			COLUMN_MOR_START_MIN + " integer, " +
			COLUMN_MOR_RETRIES + " integer, " +
			COLUMN_EVE_START_HR + " integer, " +
			COLUMN_EVE_START_MIN + " integer, " +
			COLUMN_EVE_RETRIES + " integer, " +
			COLUMN_RETRY_INTERVAL + " integer, " +
			COLUMN_NO_WATER_STOP_TIME + " integer " +

					/*
			COLUMN_PHONE_NUMBER + " text, " +
			COLUMN_VALIDATION_CODE_1 + " text, " +
			COLUMN_VALIDATION_CODE_2 + " text, " +
			COLUMN_CREATION_TIMESTAMP + " integer not null, " +
			COLUMN_AUTH_PASSWORD + " text not null	*/
			");";

	String deviceName = null;
	String mac = null;
	String paired_mode = null;
	long state = 0;
	long serverUpdateRequired = 1;
	long mor_start_hr = 6;
	long mor_start_min =0;
	long mor_retries = 2;
	long eve_start_hr = 19;
	long eve_start_min =0;
	long eve_retries = 2;
	long retry_interval = 10;
	long no_water_stop_time = 2;


	/*
	String phoneNumber = null;
	String validationCode1 = null;
	String validationCode2 = null;
	long creationTimestamp = 0;
	String authPassword = null;
	*/

	public static void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	public static void onUpgrade(SQLiteDatabase db, int oldVersion,
			int newVersion) {
		if(oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
			onCreate(db);
		}
	}

	public DevicesRecords() {
		//setCreationTimestamp(System.currentTimeMillis() / 1000);
	}
	
	public static List<DevicesRecords> loadFromDatabase(SQLiteDatabase db) {
		Vector<DevicesRecords> v = new Vector<DevicesRecords>();
		
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, null,
				null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}
	
	public static DevicesRecords loadFromDatabase(SQLiteDatabase db, long id) {
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_ID + " = " + id,
				null, null, null, null);
		cursor.moveToFirst();
		DevicesRecords req = null;
		if(!cursor.isAfterLast())
			 req = fromCursor(cursor);
		cursor.close();
		return req;
	}


	public static List<DevicesRecords> loadFromDatabase(SQLiteDatabase db, String deviceName) {
		Vector<DevicesRecords> v = new Vector<DevicesRecords>();
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_DEVICE_NAME +  " =?", new String[]{deviceName},
				null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	private static DevicesRecords fromCursor(Cursor cursor) {
		DevicesRecords req = new DevicesRecords();
		int i = 0;
		req.setId(cursor.getLong(i++));
		req.setDeviceName(cursor.getString(i++));
		req.setMac(cursor.getString(i++));
		req.setPairedMode(cursor.getString(i++));
		req.setState(cursor.getLong(i++));
		req.setServerUpdateRequired(cursor.getLong(i++));
		req.setMorStartHr(cursor.getLong(i++));
		req.setMorStartMin(cursor.getLong(i++));
		req.setMorRetries(cursor.getLong(i++));
		req.setEveStartHr(cursor.getLong(i++));
		req.setEveStartMin(cursor.getLong(i++));
		req.setEveRetries(cursor.getLong(i++));
		req.setRetryInterval(cursor.getLong(i++));
		req.setNoWaterStopTime(cursor.getLong(i++));



		//req.

		/*
		req.setId(cursor.getLong(i++));
		req.setPhoneNumber(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setCreationTimestamp(cursor.getLong(i++));
		req.setAuthPassword(cursor.getString(i++));
		*/
		
		return req;
	}

	public final static String DEVICE_RECORD_ID = "deviceRecord";
	
	@Override
	protected String getTableName() {
		return DB_TABLE;
	}
	
	@Override
	protected void putValues(ContentValues values) {
		values.put(COLUMN_DEVICE_NAME, getDeviceName());
		values.put(COLUMN_MAC, getMac());
		values.put(COLUMN_PAIRED_MODE, getPairingMode());
		values.put(COLUMN_STATE, getState());
		values.put(COLUMN_SERVER_UPDATE_REQUIRED, getServerUpdateRequired());
		values.put(COLUMN_MOR_START_HR, getMorStartHr());
		values.put(COLUMN_MOR_START_MIN, getMorStartMin());
		values.put(COLUMN_MOR_RETRIES, getMorRetries());
		values.put(COLUMN_EVE_START_HR, getEveStartHr());
		values.put(COLUMN_EVE_START_MIN, getEveStartMin());
		values.put(COLUMN_EVE_RETRIES, getEveRetries());
		values.put(COLUMN_RETRY_INTERVAL, getRetryInterval());
		values.put(COLUMN_NO_WATER_STOP_TIME, getNoWaterStopTime());

		/*
		values.put(COLUMN_PHONE_NUMBER, getPhoneNumber());
		values.put(COLUMN_VALIDATION_CODE_1, getValidationCode1());
		values.put(COLUMN_VALIDATION_CODE_2, getValidationCode2());
		values.put(COLUMN_CREATION_TIMESTAMP, getCreationTimestamp());
		values.put(COLUMN_AUTH_PASSWORD, getAuthPassword());
		*/
	}



	public String getMac()
	{
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getPairingMode()
	{
		return paired_mode;
	}

	public void setPairedMode(String paired_mode) {
		this.paired_mode = paired_mode;
	}

	public Long getState()
	{
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Long getServerUpdateRequired()
	{
		return serverUpdateRequired;
	}

	public void setServerUpdateRequired(Long serverUpdateRequired)
	{
		this.serverUpdateRequired = serverUpdateRequired;
	}
	public Long getMorStartHr() { return mor_start_hr;}
	public void setMorStartHr (Long hr) {mor_start_hr=hr;}

	public Long getMorStartMin() { return mor_start_min;}
	public void setMorStartMin (Long min) {mor_start_min=min;}

	public Long getMorRetries() { return mor_retries;}
	public void setMorRetries (Long retries) {mor_retries=retries;}

	public Long getEveStartHr() { return eve_start_hr;}
	public void setEveStartHr (Long hr) {eve_start_hr=hr;}

	public Long getEveStartMin() { return eve_start_min;}
	public void setEveStartMin (Long min) {eve_start_min=min;}

	public Long getEveRetries() { return eve_retries;}
	public void setEveRetries (Long retries) {eve_retries=retries;}

	public Long getRetryInterval() { return retry_interval;}
	public void setRetryInterval (Long interval) {retry_interval=interval;}

	public Long getNoWaterStopTime() { return no_water_stop_time;}
	public void setNoWaterStopTime (Long stopTime) {no_water_stop_time=stopTime;}


	/*
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getValidationCode1() {
		return validationCode1;
	}

	public void setValidationCode1(String validationCode1) {
		this.validationCode1 = validationCode1;
	}

	public String getValidationCode2() {
		return validationCode2;
	}

	public void setValidationCode2(String validationCode2) {
		this.validationCode2 = validationCode2;
	}

	public long getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(long creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getAuthPassword() {
		return authPassword;
	}

	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}
	*/

	/*
	@Override
	public String getTitleForMenu() {
		return getId() + "";
	}

	@Override
	public String getKeyForIntent() {
		return DEVICE_RECORD_ID;
	}
	*/

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		/*
		result = prime * result
				+ ((authPassword == null) ? 0 : authPassword.hashCode());
		result = prime * result
				+ (int) (creationTimestamp ^ (creationTimestamp >>> 32));
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result
				+ ((validationCode1 == null) ? 0 : validationCode1.hashCode());
		result = prime * result
				+ ((validationCode2 == null) ? 0 : validationCode2.hashCode());
		*/
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof DevicesRecords))
			return false;
		DevicesRecords other = (DevicesRecords) obj;

		if (deviceName == null) {
			if (other.deviceName!=null)
				return false;
		} else if (!deviceName.equals(other.deviceName)) {
			return false;
		}

		if (mac == null) {
			if (other.mac!=null)
				return false;
		} else if (!mac.equals(other.mac)) {
			return false;
		}

		if (paired_mode == null) {
			if (other.paired_mode!=null)
				return false;
		} else if (!paired_mode.equals(other.paired_mode)) {
			return false;
		}

		if (state!=other.state) {
			return false;
		}



		/*
		if (authPassword == null) {
			if (other.authPassword != null)
				return false;
		} else if (!authPassword.equals(other.authPassword))
			return false;
		if (creationTimestamp != other.creationTimestamp)
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (validationCode1 == null) {
			if (other.validationCode1 != null)
				return false;
		} else if (!validationCode1.equals(other.validationCode1))
			return false;
		if (validationCode2 == null) {
			if (other.validationCode2 != null)
				return false;
		} else if (!validationCode2.equals(other.validationCode2))
			return false;
		*/
		return true;
	}


	public static void storeUniqueNameRecordNew(Context context, String deviceName, String mac, String paired_mode, Long state, Long serverUpdate,
			Long mor_start_hr, Long mor_start_min, Long mor_retries, Long eve_start_hr, Long eve_start_min, Long eve_retries, Long retry_interval, Long stopTime)
	{

		DataSource db = new DataSource(context);
		try {
			db.open();
			ContentValues data=new ContentValues();
			data.put(COLUMN_ID,0); //putting ID as 0 for now:
			data.put(COLUMN_DEVICE_NAME, deviceName);
			data.put(COLUMN_MAC,mac);
			data.put(COLUMN_PAIRED_MODE, paired_mode); //"Paired", "unPaired"
			data.put(COLUMN_STATE,state);
			data.put(COLUMN_SERVER_UPDATE_REQUIRED, serverUpdate);

			data.put(COLUMN_MOR_START_HR,mor_start_hr);
			data.put(COLUMN_MOR_START_MIN,mor_start_min);
			data.put(COLUMN_MOR_RETRIES,mor_retries);

			data.put(COLUMN_EVE_START_HR,eve_start_hr);
			data.put(COLUMN_EVE_START_MIN,eve_start_min);
			data.put(COLUMN_EVE_RETRIES,eve_retries);

			data.put(COLUMN_RETRY_INTERVAL,retry_interval);
			data.put(COLUMN_NO_WATER_STOP_TIME,stopTime);


			//db.db.update(DB_TABLE, data, COLUMN_USER_NAME + " like " + "'" + userName + "'", null);
			String sqlQuery =
			"insert or replace into "+DB_TABLE +
							" (" +
								COLUMN_ID + "," +
								COLUMN_DEVICE_NAME + "," +
								COLUMN_MAC + "," +
								COLUMN_PAIRED_MODE + "," +
								COLUMN_STATE + "," +
								COLUMN_SERVER_UPDATE_REQUIRED  + "," +
								COLUMN_MOR_START_HR + "," +
								COLUMN_MOR_START_MIN + "," +
								COLUMN_MOR_RETRIES + "," +
								COLUMN_EVE_START_HR + "," +
								COLUMN_EVE_START_MIN + "," +
								COLUMN_EVE_RETRIES + "," +
								COLUMN_RETRY_INTERVAL + "," +
								COLUMN_NO_WATER_STOP_TIME +

							") values " +
							"(" +
								"( select " + COLUMN_ID + " from " + DB_TABLE + " where " + COLUMN_MAC + " = " + "'" + mac + "'" + ")," +
								"'" + deviceName +  "'" + "," +
								"'" + mac +  "'" + "," +
								"'" + paired_mode + "'" + "," +
								"'" + state + "'" + "," +
								"'" + serverUpdate + "'" + "," +
								"'" + mor_start_hr + "'" + "," +
								"'" + mor_start_min + "'" + "," +
								"'" + mor_retries + "'" + "," +
								"'" + eve_start_hr + "'" + "," +
								"'" + eve_start_min + "'" + "," +
								"'" + eve_retries + "'" + "," +
								"'" + retry_interval + "'" + "," +
								"'" + stopTime + "'" +
							");";
			db.db.execSQL(sqlQuery);
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
			db.close();
		}
	}

	public static void storeUniqueNameRecordNew(Context context, String deviceName, String mac, String paired_mode, Long state, Long serverUpdate)
	{

		DataSource db = new DataSource(context);
		try {
			db.open();
			ContentValues data=new ContentValues();
			data.put(COLUMN_ID,0); //putting ID as 0 for now:
			data.put(COLUMN_DEVICE_NAME, deviceName);
			data.put(COLUMN_MAC,mac);
			data.put(COLUMN_PAIRED_MODE, paired_mode); //"Paired", "unPaired"
			data.put(COLUMN_STATE,state);
			data.put(COLUMN_SERVER_UPDATE_REQUIRED, serverUpdate);


			//db.db.update(DB_TABLE, data, COLUMN_USER_NAME + " like " + "'" + userName + "'", null);
			String sqlQuery =
					"insert or replace into "+DB_TABLE +
							" (" +
							COLUMN_ID + "," +
							COLUMN_DEVICE_NAME + "," +
							COLUMN_MAC + "," +
							COLUMN_PAIRED_MODE + "," +
							COLUMN_STATE + "," +
					COLUMN_SERVER_UPDATE_REQUIRED +
							") values " +
							"(" +
							"( select " + COLUMN_ID + " from " + DB_TABLE + " where " + COLUMN_MAC + " = " + "'" + mac + "'" + ")," +
							"'" + deviceName +  "'" + "," +
							"'" + mac +  "'" + "," +
							"'" + paired_mode + "'" + "," +
							"'" + state + "'" + "," +
							"'" + serverUpdate + "'" +
							");";
			db.db.execSQL(sqlQuery);
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
			db.close();
		}
	}

	public static void LoadAndUpdateDeviceRecord(SQLiteDatabase db,
						String mac, long mor_start_hr, long mor_start_min, long mor_retries, long eve_start_hr, long eve_start_min, long eve_retries,
						long retry_interval, long stopTime)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_MAC, mac);
		//values.put(COLUMN_SERVER_UPDATE_REQUIRED, server_update_required);

		values.put(COLUMN_MOR_START_HR,mor_start_hr);
		values.put(COLUMN_MOR_START_MIN,mor_start_min);
		values.put(COLUMN_MOR_RETRIES,mor_retries);

		values.put(COLUMN_EVE_START_HR,eve_start_hr);
		values.put(COLUMN_EVE_START_MIN,eve_start_min);
		values.put(COLUMN_EVE_RETRIES,eve_retries);

		values.put(COLUMN_RETRY_INTERVAL,retry_interval);
		values.put(COLUMN_NO_WATER_STOP_TIME,stopTime);
		db.update(DB_TABLE, values, COLUMN_MAC + " = ? ", new String[]{mac});
	}

	public static void LoadAndUpdateDeviceRecord(SQLiteDatabase db,
												 String mac, long server_update_required)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_MAC, mac);
		values.put(COLUMN_SERVER_UPDATE_REQUIRED, server_update_required);
		db.update(DB_TABLE, values, COLUMN_MAC + " = ? ", new String[]{mac});
	}



	public static void storeNameRecord(Context context, String deviceName,String mac, String paired_mode, Long state) {
		DevicesRecords req = new DevicesRecords();
		req.setDeviceName(deviceName);
		req.setMac(mac);
		req.setPairedMode(paired_mode);
		req.setState(state);

		DataSource db = new DataSource(context);

		try {
			db.open();
			db.persistDeviceRecord(req);
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
		}
	}

	public static void storeUniqueNameRecord(Context context, String deviceName,String mac, String paired_mode, Long state) {
		DevicesRecords req = new DevicesRecords();
		req.setDeviceName(deviceName);
		req.setMac(mac);
		req.setPairedMode(paired_mode);
		req.setState(state);

		DataSource db = new DataSource(context);

		try {
			db.open();

			boolean recordExists = false;
			List<DevicesRecords> reqs = db.getDeviceRecords(deviceName);
			for (DevicesRecords _req : reqs) {
				if (_req.getDeviceName().equalsIgnoreCase(deviceName) == true) {
					recordExists = true;
					break;
				}
			}
			if (recordExists == false) {
				db.persistDeviceRecord(req);
			}
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
			db.close();
		}
	}

	public static void clearFromDB(Context context, String deviceName)
	{
		DataSource db = new DataSource(context);
		db.open();
		List<DevicesRecords> reqs = db.getDeviceRecords(deviceName);
		for(DevicesRecords _req : reqs) {
			db.deleteDeviceRecord(_req);
		}
		db.close();
	}

	public static void clearFromDB(Context context)
	{
		DataSource db = new DataSource(context);
		db.open();
		List<DevicesRecords> reqs = db.getDeviceRecords();
		for(DevicesRecords _req : reqs) {
			db.deleteDeviceRecord(_req);
			//deleteDeviceRecord(db.db);
		}
		db.close();
	}

	public static void deleteDeviceRecord(SQLiteDatabase db) {
		//if(getId() != -1) {
			db.execSQL("DELETE FROM " + DB_TABLE +
					";");
		//}
		//setId(-1);
	}
}
