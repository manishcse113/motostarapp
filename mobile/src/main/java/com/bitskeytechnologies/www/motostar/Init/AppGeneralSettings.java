package com.bitskeytechnologies.www.motostar.Init;

/**
 * Created by swarnkarn on 12/26/2015.
 */
public class AppGeneralSettings {
    static boolean debugFlag= true;
    static boolean isFirstTimeRunFlag = false;

    public static boolean isDebugEnabled()
    {
        return debugFlag;
    }
    public static void setFirstTimeRun()
    {
        isFirstTimeRunFlag = true;
    }
    public static boolean isIsFirstTimeRun()
    {
        return isFirstTimeRunFlag;
    }
}
