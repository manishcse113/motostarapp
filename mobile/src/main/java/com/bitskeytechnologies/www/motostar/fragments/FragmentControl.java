package com.bitskeytechnologies.www.motostar.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitskeytechnologies.www.motostar.Device.DeviceObject;
import com.bitskeytechnologies.www.motostar.MainActivity;
import com.bitskeytechnologies.www.motostar.R;
import com.bitskeytechnologies.www.motostar.RC.RcMessages;
import com.bitskeytechnologies.www.motostar.RC.RcState;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentControl.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentControl#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentControl extends Fragment implements DeviceObject.DeviceUpdateListner {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static int instanceCount = 0;
    private int instanceId = 0;

    private String FILENAME = "msFragmentControl";

    private OnFragmentInteractionListener mListener;
    
    public static int myBlueColor = 0x0084FF;

    public FragmentControl() {
        // Required empty public constructor
        instanceId = instanceCount;
        instanceCount++;
        if (instanceCount==1) //assumption is that - we will have max 1 tanks for now.
        {
            instanceCount = 0;
        }
    }

    @Override
    public void finalize()
    {

        //instanceCount--;
    }

        //@Override
    public void setArugument(int instanceId) {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentControl.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentControl newInstance(String param1, String param2) {
        FragmentControl fragment = new FragmentControl();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private View view;

    TextView tvMonitoring;
    //TextView tvConnection;
    //TextView ivConnection;


    TextView tvAutoMode;
    TextView tvWaterFlow;
    TextView tvMotor;


    //TextView tvEmergency;
    //TextView tvInfo;

    TextView tvAutoStartText;
    TextView tvAutoStartTime;

    TextView tvLastRefreshedText;
    TextView tvAutoRefreshedTime;

    TextView tvManualStartStop;


    TextView ivAutoMode;
    TextView ivWater;
    TextView ivMotor;
    TextView levelInfo;
    TextView warningInfo;

    ImageView ivEmergency;

    ImageView ivLevel;
    ImageView ivManual;

    ImageButton iBRefresh;
    ImageButton iBManual;
    //ImageButton iBSave;

    //based on Instance ID
    DeviceObject dvObj = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_control, container, false);

        if (container == null) {
            return null;
        }

        tvMonitoring = (TextView) view.findViewById(R.id.monitoring);
        //tvConnection = (TextView) view.findViewById(R.id.connection);
        //ivConnection = (TextView) view.findViewById(R.id.SyncButton);

        tvAutoMode =   (TextView) view.findViewById(R.id.mode);

        tvWaterFlow =  (TextView) view.findViewById(R.id.flow);
        tvMotor  = (TextView) view.findViewById(R.id.motor);

        //tvEmergency  = (TextView) view.findViewById(R.id.emergency);
        //tvInfo  = (TextView) view.findViewById(R.id.info);

        tvAutoStartText = (TextView) view.findViewById(R.id.NextAutoStartText);
        tvAutoStartTime = (TextView) view.findViewById(R.id.NextAutoStartTime);

        tvLastRefreshedText = (TextView) view.findViewById(R.id.LastRefereshedText);
        tvAutoRefreshedTime = (TextView) view.findViewById(R.id.LastRefreshTime);

        tvManualStartStop =  (TextView) view.findViewById(R.id.manualText);


        ivAutoMode =   (TextView) view.findViewById(R.id.ModeButton);
        ivWater = (TextView) view.findViewById(R.id.WaterFlowButton);
        ivMotor = (TextView) view.findViewById(R.id.MotorOnStatusButton);

        levelInfo = (TextView) view.findViewById(R.id.LevelTextInfoButton);
        warningInfo = (TextView) view.findViewById(R.id.WarningButton);

        //ivEmergency = (ImageView) view.findViewById(R.id.EmergencyStatusButton);
        //ivInfo = (ImageView) view.findViewById(R.id.info);

        ivLevel = (ImageView) view.findViewById(R.id.LevelFullButton);
        ivManual = (ImageView) view.findViewById(R.id.ManualButton);

        iBRefresh = (ImageButton) view.findViewById(R.id.left_button);
        //iBSave = (ImageButton) view.findViewById(R.id.right_button);
        //iBManual = (ImageButton) view.findViewById(R.id.ManualButton);

        ivManual.setOnClickListener( new View.OnClickListener()
             {
                  @Override
                  public void onClick(View arg0)
                  {
                      if (mListener!=null) {
                          mListener.Vibrate();
                      }
                      if (MainActivity.getPairedDevicesList()!=null)
                      {
                          if (MainActivity.getPairedDevicesList().size() > 0) {
                              //dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                              dvObj = MainActivity.getSelectedDevice();
                              if (dvObj==null)
                              {
                                  return;
                              }
                          }
                      }
                      //This function doesn't need to have any significance here..
                      if (dvObj!=null)
                      {
                          if (dvObj.rcStateObj.rc_state== RcState.RC_STATE.RC_STATE_MASTER)
                          {
                              dvObj.motorPowerToggle();

                              if (dvObj.motorStatus != null && dvObj.motorStatus.motorSwitchCondition) {
                                  String text = "On";
                                  ivMotor.setText(text);
                                  ivMotor.setTextColor(Color.BLACK);
                                  ivMotor.setBackgroundColor(Color.GREEN);
                                  //ivManual.
                                  ivManual.setImageResource(R.drawable.ic_stop);

                              } else {
                                  String text = "Off";
                                  ivMotor.setText(text);
                                  ivMotor.setTextColor(Color.BLACK);
                                  ivMotor.setBackgroundColor(Color.YELLOW);
                                  ivManual.setImageResource(R.drawable.ic_start);
                                  //ivMotor.setImageResource(R.drawable.ic_power_on2);
                              }
                          }
                          else if (dvObj.rcStateObj.rc_state== RcState.RC_STATE.RC_STATE_REMOTE_CONNECTED)
                          {
                              if (dvObj.motorStatus != null && dvObj.motorStatus.motorSwitchCondition) {
                                  RcMessages.sendCommandStop(MainActivity.mContext, dvObj.deviceAddress, dvObj.rcMaster, 0);

                                  String text = "Turning Off";
                                  ivMotor.setText(text);
                                  ivMotor.setTextColor(Color.BLACK);
                                  ivMotor.setBackgroundColor(Color.GREEN);
                                  //ivManual.
                                  ivManual.setImageResource(R.drawable.ic_start);

                              } else {
                                  RcMessages.sendCommandStart(MainActivity.mContext,dvObj.deviceAddress,dvObj.rcMaster,0);
                                  String text = "Turning On";
                                  ivMotor.setText(text);
                                  ivMotor.setTextColor(Color.BLACK);
                                  ivMotor.setBackgroundColor(Color.YELLOW);
                                  ivManual.setImageResource(R.drawable.ic_stop);
                                  //ivMotor.setImageResource(R.drawable.ic_power_on2);
                              }
                          }

                      }
                      else
                      {
                          Log.e(FILENAME, "No device associated with this fragment");
                      }

                  }
             }
        );

        iBRefresh.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View arg0) {
                     if (mListener!=null) {
                         mListener.Vibrate();
                     }
                     if (MainActivity.getPairedDevicesList() != null) {
                         if (MainActivity.getPairedDevicesList().size() > 0) {
                             //dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                             dvObj = MainActivity.getSelectedDevice();
                             if (dvObj==null)
                             {
                                 return;
                             }
                             //dvObj.setCallback(this , getActivity());

                             //connected status
                             if (dvObj.isPaired) {
                                 dvObj.writeSynch('0');
                             }
                         }
                     }
                     refresh();
                 }
             }
        );
        /*
        iBSave.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0)
             {
                 mListener.Vibrate();
                 //This function doesn't need to have any significance here..
                 if (MainActivity.getPairedDevicesList()!=null) {
                     if (MainActivity.getPairedDevicesList().size() > instanceId) {
                         dvObj = (DeviceObject) MainActivity.getPairedDevicesList().get(instanceId);
                     }
                 }
             }
         }
        );
        */
        refresh();

        return view;
    }


    public boolean isResumed = false;
    @Override
    public void onResume() {
        super.onResume();
        Log.d(FILENAME, "onResume");
        isResumed = true;
        refresh();
        //mListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(FILENAME, "onPause");
        isResumed = false;
        //mListener = null;
    }

    public static int timeSyncRetries = 0;
    public static int MAX_TIME_SYNC_RETRIES  = 10;

    //Update Default View
    public void refresh()
    {
        //Need to send request to fetch from device

        if (isResumed==false)
        {
            return;
        }

        if (ivMotor==null) //just a check that it is initialized.
        {
            if (mListener!=null)
            {
                mListener.updateConnectionView(null);
            }
            return;
        }
        if (MainActivity.getPairedDevicesList()!=null)
        {
            if ( MainActivity.getPairedDevicesList().size() > 0)
            {
                //dvObj = (DeviceObject)MainActivity.getPairedDevicesList().get(instanceId);
                dvObj = MainActivity.getSelectedDevice();
                if (dvObj==null)
                {
                    return;
                }
                dvObj.setCallback(this, getActivity());

                //res = getResources();
                if (mListener!=null)
                {
                    mListener.updateConnectionView(dvObj);
                }
                //connected status
                if(dvObj.isPaired)
                {
                    //mListener.Vibrate();
                    //dvObj.writeSynch("0");
                    if (dvObj.state == dvObj.STATE_CONNECTED)
                    {
                        String text = "Connected";
                    }
                    else if (dvObj.state == dvObj.STATE_CONNECTING)
                    {
                        Log.d(FILENAME, "Trying to Connect, need to return");
                        String text = "Unkown";
                        //setUnknownInfo(text);
                        //return;
                    }
                    else //only Paired
                    {
                        Log.d(FILENAME, "Not Connected, need to return");
                        String text = "Unkown";
                        //setWaterLevel();

                        //setUnknownInfo(text);
                        //return;
                    }

                    //dvObj.deviceName;
                    if (dvObj.motorStatus==null)
                    {
                        //tvInfo.setTextColor(Color.RED);
                        String text = "Connecting";
                        //setUnknownInfo(text);
                        return;
                    }

                    //If here, means connected:
                    setWaterLevel();
                    //water flow
                    if(dvObj.motorStatus.isWaterInFlow)
                    {
                        String text = "On";
                        setWaterInfo(text, Color.BLACK, Color.GREEN);
                        //ivWater.setImageResource(R.drawable.ic_water_flow3);
                    }
                    else
                    {
                        String text = "Off";
                        setWaterInfo(text, Color.BLACK, Color.YELLOW);
                        //ivWater.setImageResource(R.drawable.ic_water_no_flow);
                    }

                    if(dvObj.motorStatus.manualModeState==true)
                    {
                        String text = "Off";
                        setAutoMode(text, Color.BLACK, Color.YELLOW);
                        //ivWater.setImageResource(R.drawable.ic_water_flow3);
                    }
                    else
                    {
                        String text = "On";
                        setAutoMode(text, Color.BLACK, Color.GREEN);
                        //ivWater.setImageResource(R.drawable.ic_water_no_flow);
                    }


                    //motor on/off
                    if(dvObj.motorStatus.motorSwitchCondition)
                    {
                        String text = "On";
                        setMotorInfo(text, Color.BLACK, Color.GREEN);
                        tvManualStartStop.setText("Stop");
                        ivManual.setImageResource(R.drawable.ic_stop);

                        //setMotorInfo(text, Color.BLACK, Color.GREEN);
                    }
                    else
                    {
                        String text = "Off";
                        setMotorInfo(text, Color.BLACK, Color.YELLOW);
                        tvManualStartStop.setText("Start");
                        ivManual.setImageResource(R.drawable.ic_start);
                    }

                    //last refreshed at time
                    {
                        String str = String.valueOf(dvObj.motorStatus.refreshDate) + "/"
                                + String.valueOf(dvObj.motorStatus.refreshMonth) + "::"
                                + String.valueOf(dvObj.motorStatus.refreshhour) + ":"
                                + String.valueOf(dvObj.motorStatus.refreshminute);

                        tvAutoRefreshedTime.setText(str);
                    }
                    //is Time in sync?
                    if (dvObj.motorStatus.isTimeinSync)
                    {
                        String text = "Time is in sync";
                        setWarningInfo(text, Color.BLACK, Color.GREEN);
                        timeSyncRetries = 0;
                    }
                    else
                    {
                        String text = "Time is out of Sync";
                        setWarningInfo(text, Color.WHITE, Color.RED);
                        timeSyncRetries++;
                        if (timeSyncRetries==MAX_TIME_SYNC_RETRIES)
                        {
                            createTimeSyncDialog();
                        }
                    }
                    //next start schedule time
                    //dvObj.motorStatus.remainingMinutesAutoStart;
                    String str2 = String.valueOf(dvObj.motorStatus.remainingMinutesAutoStart);
                    tvAutoStartTime.setText("In " + str2 + " minutes");
                }
                else
                {
                    Log.d(FILENAME, "Not Paired");
                    //Here - it shall attempt to Pair:
                    if (mListener!=null) {
                        mListener.enableBluetooth();
                    }
                    String text = "Unkown";
                    setUnknownInfo(text);
                    //ivConnection.setImageResource(R.drawable.ic_emergency);
                    return;
                    //not paired
                }
                //Update levels
            }
            else
            {
                String text = "Unkown";
                setUnknownInfo(text);
            }
        }
        else
        {
            if (mListener!=null) {
                mListener.updateConnectionView(null);
            }
            //Resources res = getResources();
            String text = "Unkown";
            setUnknownInfo(text);
        }
        //this.notifyDataSetChanged();
    }

    //int temp = 0;
    void setWaterLevel()
    {
        /*
        temp++;
        temp = temp%4;
        if (temp==0)
        {
            dvObj.motorStatus.isTankFilled = true;
        }
        else if (temp==1)
        {
            dvObj.motorStatus.isTankHigh = true;
            dvObj.motorStatus.isTankFilled = false;
        }
        else if (temp==2)
        {
            dvObj.motorStatus.isTankMid = true;
            dvObj.motorStatus.isTankHigh = false;
            dvObj.motorStatus.isTankFilled = false;
        }
        else
        {
            dvObj.motorStatus.isTankMid = false;
            dvObj.motorStatus.isTankHigh = false;
            dvObj.motorStatus.isTankFilled = false;
        }
        */

        if(dvObj.motorStatus.isTankFilled)
        {
            setLevelImage(R.drawable.ic_level4);
            String text = "Level4";
            setLevelInfo(text, Color.BLACK, Color.GREEN);
        }
        else if(dvObj.motorStatus.isTankHigh)
        {
            setLevelImage(R.drawable.ic_level3);
            String text = "Level3";
            setLevelInfo(text, Color.BLACK, Color.GREEN);
        }
        else if(dvObj.motorStatus.isTankMid)
        {
            setLevelImage(R.drawable.ic_level2);
            String text = "Level2";
            setLevelInfo(text, Color.BLACK, Color.GREEN);
        }
        else if(dvObj.motorStatus.isTankLow)
        {
            setLevelImage(R.drawable.ic_level1);
            String text = "Level1";
            setLevelInfo(text, Color.BLACK, Color.YELLOW);
        }

        else if(dvObj.motorStatus.isTankVeryLow)
        {
            setLevelImage(R.drawable.ic_level0);
            String text = "Leve0";
            setLevelInfo(text, Color.WHITE, Color.RED);
        }
    }

    void setLevelImage(int imageId)
    {

        ivLevel.setImageBitmap(null);
        ivLevel.destroyDrawingCache();
        ivLevel.setImageDrawable(null);
        ivLevel.setImageResource(android.R.color.transparent);
        ivLevel.setImageResource(0);
        //ivLevel.setImageResource(R.drawable.ic_level_none);
        ivLevel.setImageResource(imageId);
    }


    void setUnknownInfo(String text)
    {
        setWarningInfo(text, Color.WHITE, myBlueColor);
        setLevelInfo(text, Color.WHITE, myBlueColor);
        setWaterInfo(text, Color.WHITE, myBlueColor);
        setMotorInfo(text, Color.WHITE, myBlueColor);
        setAutoMode(text, Color.WHITE, myBlueColor);
        tvAutoStartTime.setText("In " + text + " minutes");
        tvAutoRefreshedTime.setText(text);
    }

    void setWarningInfo(String text, int textColor, int backgroundColor)
    {
        warningInfo.setText(text);
        warningInfo.setTextColor(textColor);
        warningInfo.setBackgroundColor(backgroundColor);
    }

    void setLevelInfo (String text, int textColor, int backgroundColor)
    {
        levelInfo.setText(text);
        levelInfo.setTextColor(textColor);
        levelInfo.setBackgroundColor(backgroundColor);
    }
    void setWaterInfo(String text, int textColor, int backgroundColor)
    {
        ivWater.setText(text);
        ivWater.setTextColor(textColor);
        ivWater.setBackgroundColor(backgroundColor);
    }

    void setMotorInfo (String text, int textColor, int backgroundColor)
    {
        ivMotor.setText(text);
        ivMotor.setTextColor(textColor);
        ivMotor.setBackgroundColor(backgroundColor);
    }

    void setAutoMode (String text, int textColor, int backgroundColor)
    {
        ivAutoMode.setText(text);
        ivAutoMode.setTextColor(textColor);
        ivAutoMode.setBackgroundColor(backgroundColor);
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
        boolean enableBluetooth();
        void updateConnectionView(DeviceObject dvObj);
        void Vibrate();
    }


    void createTimeSyncDialog()
    {
        final CharSequence[] items = {" Email "," Phone "};
        // arraylist to keep the selected items
        final ArrayList seletedItems=new ArrayList();
        int inputSelection = -1;

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.mainActivitythis);
        builder.setMessage("Warning : Seems Battery in MotoStar at the backside has drained. Please replace battery and consider connecting with mobile again for accurate results. If problem still doesn't resolve - consider calling support from top menu");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id)
            {
                //  Your code when user clicked on OK
                //  You can write the code  to save the selected item here
                //Just a temporarily code to test the specific scenario:
                //sendEmail();
                timeSyncRetries = 0;
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel
                        timeSyncRetries = 0;
                    }
                });

        Dialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }
}
