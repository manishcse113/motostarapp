package com.bitskeytechnologies.www.motostar.fragments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bitskeytechnologies.www.motostar.EventsAlarms.EventItem;
import com.bitskeytechnologies.www.motostar.R;
import com.bitskeytechnologies.www.motostar.fragments.fragment_lastEventsList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 1/14/2017.
 */

public class LastEventsListAdapter extends ArrayAdapter {
    int groupid;

    private static List lastEventsList = new ArrayList();
    //private DeviceObject deviceObject;
    Context context;
    private final String FILENAME = "MS LastEventsListAdapter#";
    //private UserListCallback callback;
    //private UserListActivityCallback callbackActivity;

    Map<String, String> map;
    Map<String, String> treeMap;

    public LastEventsListAdapter(Context context, int textViewResourceId)
    {
        super(context, textViewResourceId);
    }

    public LastEventsListAdapter(Context context, int vg, List eventsList)
    {

        super(context, vg, lastEventsList);
        lastEventsList = eventsList;
        addUsersList(lastEventsList);
        //eventsList = deviceObject.eventList;
        this.context = context;
        //this.deviceObject = device;
        groupid = vg;
        //this.eventsList.clear();
        //this.lastEventsList.addAll(deviceObject.eventList);

        //readThread();
        //monitoringConnectivityThread();
    }
    public void updateLastEventList(List eventsList)
    {
        this.lastEventsList = eventsList;
        addUsersList(lastEventsList);

    }


    //@Override
    public void add(EventItem object) {
        //eventsList.add(object);
        super.add(object);
        super.notify();
        // here - inform all - to update themselves with the new information.
    }

    public void update(EventItem object) {
        //search message based on unique id: receiptId and replace the contents.
        boolean updated = false;
        for (Object event : lastEventsList) {
            if (((EventItem) (event)).eventText.equals(((EventItem) (object)).eventText)) {
                ((EventItem) (event)).eventValue = ((EventItem) (object)).eventValue;
                updated = true;
                break;
            }
        }
        if (!updated) {
            add(object);
        }
        super.notifyDataSetInvalidated();
        //super.notify();
    }

    public void addUsersList(List objectList) {
        updateDevicesList(objectList);
    }

    public void updateDevicesList(List objectList)
    {
        //search message based on unique id: receiptId and replace the contents.
        for (Object userObject : objectList) {
            update((EventItem) (userObject));
        }

        //super.notify();
    }

    public void clear() {
        //eventsList.clear();
        super.clear();
    }

    public void delete(EventItem object) {
        for (Object stat : lastEventsList) {
            if (((EventItem) (stat)).eventText.equals(((EventItem) (object)).eventText)) {
                //eventsList.remove(object);
                //userObjList.
                super.remove(object);
                break;
            }
        }
    }

    //@Override
    public void delete(int position) {
        int index = position;
        //eventsList.remove(index);
        super.remove(index);
    }

    public int getCount() {
        return this.lastEventsList.size();
    }

    public EventItem getItem(int index) {
        EventItem object = (EventItem) lastEventsList.get(index);
        return object;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View itemView = inflater.inflate(groupid, parent, false);
        View fragment_list_view = inflater.inflate(R.layout.fragment_lastevents, null);
        //Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(R.layout.event_item, (ViewGroup) fragment_list_view, false);
            populateUserListLayout(inflater, row, position);
        } else {
            //You get here in case of scroll
            populateUserListLayout(inflater, row, position);
        }
        //((ViewGroup) fragment_list_view).addView(row);
        return row;
    }

    public View populateUserListLayout(LayoutInflater inflater, final View user_view, final int position) {
        //Log.d(FILENAME, "inflateUserListLayout");
        try {
            TextView eventNameView = (TextView) user_view.findViewById(R.id.eventName);
            String eventName = eventNameView.getText().toString();

            EventItem uObj = (EventItem) (lastEventsList.get(position));
            eventNameView.setText(uObj.eventText);

            TextView eventValueView = (TextView) user_view.findViewById(R.id.eventValue);
            eventValueView.setText(uObj.getEventValue());

            eventNameView.setSelected(true);


        } catch (Exception e) {
            Log.d(FILENAME, " Exception " + e.getMessage());
        }
        return user_view;
    }

    private fragment_lastEventsList callback;
    //private ScanDevicesListInterface callbackActivity;

    public void setCallback(fragment_lastEventsList callback) {
        Log.d(FILENAME, "setCallback");
        this.callback = callback;
    }

    /*
    public void setScanDevlicesListCallback(FragmentConnections callback) {
        Log.d(FILENAME, "setScanDevlicesListCallback");
        this.callbackActivity = callback;
    }
    */

    public interface StatsListInterface
    {
        //public void unPair(DeviceObject device);
    }

}