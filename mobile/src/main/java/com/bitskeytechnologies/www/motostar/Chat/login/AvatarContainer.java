package com.bitskeytechnologies.www.motostar.Chat.login;

import java.util.LinkedHashMap;

/**
 * Created by Manish on 1/2/2016.
 */
public class AvatarContainer {

    static private int MAX_AVATAR_INFO_IN_MEMORY=25;
    static private LinkedHashMap map = new LinkedHashMap();
    static private int count = 0;

    static public boolean add(String rawJid, AvatarInfo aInfo)
    {
        if(count >= MAX_AVATAR_INFO_IN_MEMORY || aInfo == null)
            return  false;
        map.put(rawJid, aInfo);
        byte[] imageByte = aInfo.getImageBytes();
        if(imageByte != null && imageByte.length > 0)
          count++;
        return true;
    }

    static public boolean remove(String rawJid)
    {
        AvatarInfo aInfo = get(rawJid);
        if(aInfo != null )
        {
            map.remove(rawJid);
            if(aInfo.getImageBytes().length >0)
                count--;
        }

        return true;
    }

    static public AvatarInfo get(String rawJid)
    {
        AvatarInfo aInfo = (AvatarInfo)map.get(rawJid);
        return aInfo;

    }
}
