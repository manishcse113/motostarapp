package com.bitskeytechnologies.www.motostar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

/**
 * Created by Manish on 20-02-2016.
 */
public class LastEventRecord extends DBObject {
    final static String FILENAME = "LastEventRecord";

    private final static String DB_TABLE = "LastEventRecord";

    private final static String COLUMN_DEVICE_NAME = "device_name";
    private final static String COLUMN_EVENT_NAME = "event_name";
    private final static String COLUMN_EVENT_ID = "eventId";
    private final static String COLUMN_DATE = "date";
    private final static String COLUMN_MONTH = "month";
    private final static String COLUMN_YEAR = "year";
    private final static String COLUMN_HR = "hour";
    private final static String COLUMN_MINUTE = "minute";
    private final static String COLUMN_SECOND = "second";
    private final static String COLUMN_REASON = "reason";
    private final static String COLUMN_EVENT_VALUE = "event_value";

    private final static String[] ALL_COLUMNS = new String[]{
            COLUMN_ID,
            COLUMN_DEVICE_NAME,
            COLUMN_EVENT_NAME,
            COLUMN_EVENT_ID,
            COLUMN_DATE,
            COLUMN_MONTH,
            COLUMN_YEAR,
            COLUMN_HR,
            COLUMN_MINUTE,
            COLUMN_SECOND,
            COLUMN_REASON,
            COLUMN_EVENT_VALUE
    };

    private final static String CREATE_TABLE =
            "CREATE TABLE " + DB_TABLE + " (" +
                    COLUMN_ID + " integer, " +
                    COLUMN_DEVICE_NAME + " text, " +
                    COLUMN_EVENT_NAME + " text, " +
                    COLUMN_EVENT_ID + " integer not null, " +
                    COLUMN_DATE + " integer not null, " +
                    COLUMN_MONTH + " integer not null, " +
                    COLUMN_YEAR + " integer not null, " +
                    COLUMN_HR + " integer, " +
                    COLUMN_MINUTE + " integer, " +
                    COLUMN_SECOND + " integer, " +
                    COLUMN_REASON + " integer, " +
                    COLUMN_EVENT_VALUE + " text " +
                    ");";
    String deviceName = null;
    String eventName = null;
    long eventId = 0;
    long date = 0;
    long month = 0;
    long year = 0;
    long hr = 0;
    long minute = 0;
    long second = 0;
    long reason = 0;
    String event_value = null;


    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion,
                                 int newVersion) {
        if (oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
            onCreate(db);
        }
    }

    public static List<LastEventRecord> loadFromDatabase(SQLiteDatabase db) {
        Vector<LastEventRecord> v = new Vector<LastEventRecord>();

        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, null,
                null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public static LastEventRecord loadFromDatabase(SQLiteDatabase db, long id) {
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_ID + " = " + id,
                null, null, null, null);
        cursor.moveToFirst();
        LastEventRecord req = null;
        if(!cursor.isAfterLast())
            req = fromCursor(cursor);
        cursor.close();
        return req;
    }


    public static List<LastEventRecord> loadFromDatabase(SQLiteDatabase db, String deviceName) {
        Vector<LastEventRecord> v = new Vector<LastEventRecord>();
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_DEVICE_NAME +  " =?", new String[]{deviceName},
                null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public String getDeviceName()
    {
        return deviceName;
    }
    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    private static LastEventRecord fromCursor(Cursor cursor) {
        LastEventRecord req = new LastEventRecord();
        int i = 0;
        req.setId(cursor.getLong(i++));
        req.setDeviceName(cursor.getString(i++));
        req.setEventName(cursor.getString(i++));

        req.setEventId(cursor.getLong(i++));
        req.setDate(cursor.getLong(i++));
        req.setMonth(cursor.getLong(i++));
        req.setYear(cursor.getLong(i++));
        req.setHr(cursor.getLong(i++));
        req.setMin(cursor.getLong(i++));
        req.setSec(cursor.getLong(i++));
        req.setReason(cursor.getLong(i++));
        req.setEventValue(cursor.getString(i++));
        return req;
    }

    public final static String EVENT_RECORD_ID = "lastEventRecord";

    @Override
    protected String getTableName() {
        return DB_TABLE;
    }

    @Override
    protected void putValues(ContentValues values) {
        values.put(COLUMN_DEVICE_NAME, getDeviceName());
        values.put(COLUMN_EVENT_NAME, getEventName());
        values.put(COLUMN_EVENT_ID, getEventId());
        values.put(COLUMN_DATE, getDate());
        values.put(COLUMN_MONTH, getMonth());
        values.put(COLUMN_YEAR, getYear());
        values.put(COLUMN_HR, getHr());
        values.put(COLUMN_MINUTE, getMin());
        values.put(COLUMN_SECOND, getSec());
        values.put(COLUMN_REASON, getReason());
        values.put(COLUMN_EVENT_VALUE, getEventValue());
    }

    public String getEventName()
    {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public long getEventId()
    {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public long getDate()
    {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getMonth()
    {
        return month;
    }

    public void setMonth(long month) {
        this.month = month;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public long getHr()
    {
        return hr;
    }

    public void setHr(long hr) {
        this.hr = hr;
    }

    public long getMin()
    {
        return minute;
    }

    public void setMin(long minute) {
        this.minute = minute;
    }

    public long getSec() {
        return second;
    }

    public void setSec(long second) {
        this.second = second;
    }


    public long getReason() {
        return reason;
    }

    public void setReason(long reason) {
        this.reason = reason;
    }

    public String getEventValue() {
        return event_value;
    }

    public void setEventValue(String event_value) {
        this.event_value = event_value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof LastEventRecord))
            return false;
        LastEventRecord other = (LastEventRecord) obj;

        if (deviceName == null) {
            if (other.deviceName!=null)
                return false;
        } else if (!deviceName.equals(other.deviceName)) {
            return false;
        }

        if (eventName == null) {
            if (other.eventName!=null)
                return false;
        } else if (!eventName.equals(other.eventName)) {
            return false;
        }

        if (eventId != other.eventId) {
            return false;
        }

        if (date != other.date) {
            return false;
        }

        if (month != other.month) {
            return false;
        }

        if (year != other.year) {
            return false;
        }

        if (hr != other.hr) {
            return false;
        }

        if (minute != other.minute) {
            return false;
        }

        if (second != other.second) {
            return false;
        }

        if (reason != other.reason) {
            return false;
        }

        if (event_value == null) {
            if (other.event_value!=null)
                return false;
        } else if (!event_value.equals(other.event_value)) {
            return false;
        }

        return true;
    }

    public static void storeEventRecord(Context context, String deviceName, String eventName,  long eventId, long date, long month, long year, long hr, long min, long second, long reason, String event_value)
    {
        LastEventRecord req = new LastEventRecord();

        req.setDeviceName(deviceName);
        req.setEventName(eventName);
        req.setEventId(eventId);
        req.setDate(date);
        req.setMonth(month);
        req.setYear(year);
        req.setHr(hr);
        req.setMin(min);
        req.setSec(second);
        req.setReason(reason);
        req.setEventValue(event_value);
        DataSource db = new DataSource(context);

        try {
            db.open();
            db.persistLastEventRecord(req);
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "exeption in db.open");
            e.printStackTrace();
            db.close();
        }
    }
    public static void updateLastEventRecord( Context context, String deviceName, String eventName,  long eventId, long date, long month, long year, long hr, long min, long second, long reason, String event_value)
    {
        DataSource ds = new DataSource(context);
        ds.open();
        SQLiteDatabase db = ds.getDb();
        ContentValues values = new ContentValues();
        values.put(COLUMN_DATE, date);
        values.put(COLUMN_MONTH, month);
        values.put(COLUMN_YEAR, year);
        values.put(COLUMN_HR, hr);
        values.put(COLUMN_MINUTE, min);
        values.put(COLUMN_SECOND, second);
        values.put(COLUMN_REASON, reason);
        values.put(COLUMN_EVENT_VALUE, event_value);
        db.update(DB_TABLE, values, COLUMN_EVENT_NAME + " = ? " + "AND " + COLUMN_DEVICE_NAME + " = ? ", new String[]{eventName, deviceName});
        ds.close();
    }

    public static void clearFromDB(Context context, String deviceName)
    {
        DataSource db = new DataSource(context);
        try {
            db.open();
            List<LastEventRecord> reqs = db.getLastEventRecords(deviceName);
            for (LastEventRecord _req : reqs) {
                db.deleteLastEventRecord(_req);
            }
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "Error in clearEvent");
            e.printStackTrace();
            db.close();
        }
    }

    public static void clearFromDB(Context context)
    {
        DataSource db = new DataSource(context);
        db.open();
        List<LastEventRecord> reqs = db.getLastEventRecords();
        for(LastEventRecord _req : reqs) {
            db.deleteLastEventRecord(_req);
        }
        db.close();
    }
}

